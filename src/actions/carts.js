export function addProduct(arr) {
  return { type: 'ADD_PRODUCT', arr };
}
export function setProduct(cart, amount) {
  return { type: 'SET_PRODUCT', cart, amount };
}
export function minusProduct(index) {
  return { type: 'MINUS_PRODUCT', index };
}

export function deleteProduct(index) {
  return { type: 'DELETE_PRODUCT', index };
}
