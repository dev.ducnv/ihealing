export function getLichDangCho(querry, skip) {
  return {
    type: 'GET_LICH_DANG_CHO',
    payload: querry,
    skip
  }
}

export function numberCalendar(number, type) {
  if (type === 'CLIENT') {
    return {
      type: 'PUT_NUMBER_CALENDAR_CL',
      payload: number,
    }
  } else {
    return {
      type: 'PUT_NUMBER_CALENDAR_EM',
      payload: number,
    }
  }
}
