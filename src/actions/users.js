import { WATCH_SAVE_USER_DATA, WATCH_UPDATE_USER_DATA } from './actionTypes'

export function saveUserToRedux(data, token) {
  return {
    type: WATCH_SAVE_USER_DATA,
    data,
    token
  };
}

export function updateUserToRedux(data) {
  return {
    type: WATCH_UPDATE_USER_DATA,
    data
  };
}
