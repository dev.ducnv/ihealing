/* eslint-disable handle-callback-err */
import url from '../url'
import { PostData, GetData, PostFormData, PutData } from '../helpers'

export const login = (body) => PostData(url.LOGIN, body, false).then(res => res).catch(err => null);
export const changePass = (body) => PostData(url.CHANGEPASS, body, false).then(res => res).catch(err => null);
// post của api này đẩy theo param
export const pushDeviceToken = (body) => PostData(url.PUSH_DEVICE_TOKEN + body, {}, true).then(res => res).catch(err => null);


export const getUserInfo = (body) => PostData(url.GET_USER_INFO, body, false).then(res => res).catch(err => null);
export const postUpdateInfo = (body) => PostData(url.POST_UPDATE_INFO, body, false).then(res => res).catch(err => null);
export const postUpdateAvatar = (body) => PostData(url.POST_UPDATE_AVATAR, body, false).then(res => res).catch(err => null);
export const register = (body) => PostData(url.REGISTER, body, false).then(res => res).catch(err => null);
export const searchArticle = (keySearch) => GetData(`${url.SEARCH_ARTICLE}${keySearch}`, {}, false).then(res => res).catch(err => null);
export const getNguoiBaoTro = (nhom, id) => GetData(`${url.GET_NGUOI_BAO_TRO}?ID_QuanLy=${id}&nhom=${nhom}`, {}, false).then(res => res).catch(err => null);
// metaData
export const getTinh = (customUrl) => GetData(customUrl || url.GET_TINH, {}, true).then(res => res).catch(err => null);
export const getHuyen = (param, customUrl) => GetData((customUrl || url.GET_HUYEN) + param, {}, true).then(res => res).catch(err => null);
export const getXa = (param, customUrl) => GetData((customUrl || url.GET_XA) + param, {}, true).then(res => res).catch(err => null);


// product & Services
export const getMetaDataService = () => GetData(url.GET_METADATA_SERVICE, {}, false).then(res => res).catch(err => null);
export const getMetaDataProduct = () => GetData(url.GET_METADATA_PRODUCT, {}, false).then(res => res).catch(err => null);
export const getListProduct = (querry) => GetData(url.GET_LIST_PRODUCT + querry, {}, false).then(res => res).catch(err => null);
export const getDetailProductById = (querry) => GetData(url.GET_DETAILED_PRODUCT + querry, {}, false).then(res => res).catch(err => null);
export const getOtherProduct = (querry) => GetData(url.GET_OTHER_PRODUCT + querry, {}, false).then(res => res).catch(err => null);
export const postCreateInvoice = (body) => PostData(url.POST_CREATE_INVOICE, body, true).then(res => res).catch(err => null);
export const getListDonHangDaDat = (body) => PostData(url.GET_DON_HANG_DA_DAT, body, true).then(res => res).catch(err => null);

export const getLichSuMuaDuocLieu = (body) => PostData(url.GET_LICH_SU_DUOC_LIEU, body, true).then(res => res).catch(err => null);


export const getListRating = () => GetData(url.GET_LIST_RATING, {}, true).then(res => res).catch(err => null);
export const ratingById = (querry) => GetData(url.RATING_BY_ID + querry, {}, true).then(res => res).catch(err => null);
export const getDetailInvoice = (querry) => GetData(url.GET_DETAIL_INVOICE + querry, {}, true).then(res => res).catch(err => null);
export const getXoa = (querry) => GetData(url.CANCEL_INVOICE + querry, {}, true).then(res => res).catch(err => null);


// Managetment
export const getListAccept = () => GetData(url.GET_LIST_WAITING_ACCEPT, {}, true).then(res => res).catch(err => null);
export const postAccept = (body) => PostData(url.POST_ACCEPT_USER, body, true).then(res => res).catch(err => null);
export const postDeny = (body) => PostData(url.POST_DENY_USER, body, true).then(res => res).catch(err => null);
export const getListEmployee = () => GetData(url.GET_LIST_NESTED_EMPLOYEE, {}, true).then(res => res).catch(err => null);
export const forgotPass = (body) => PostData(url.FORGOT_PASS, body, true).then(res => res).catch(err => null);
export const findPresenter = (querry) => GetData(url.SEARCH_PRESENTER + querry, {}, false).then(res => res).catch(err => null);
export const searchNguoiBaoTro = (querry) => GetData(url.SEARCH_NGUOI_BAO_TRO + querry, {}, false).then(res => res).catch(err => null);
export const searchNguoiGioiThieu = (querry) => GetData(url.SEARCH_NGUOI_GIOI_THIEU + querry, {}, false).then(res => res).catch(err => null);
export const getYeuCauTuVan = () => GetData(url.GET_YEU_CAU_TU_VAN, {}, true).then(res => res).catch(err => null);
export const updateYeuCauTuVan = (body) => PostData(url.POST_UPDATE_YEU_CAU_TU_VAN, body, true).then(res => res).catch(err => null);
// Calendar
export const getCalendarByDay = (querry) => GetData(url.GET_CALENDAR_BYDAY + querry, {}, true).then(res => res).catch(err => null);
export const getCalendarByDayEmployee = (querry) => GetData(url.GET_CALENDAR_BYDAY_EMPLOYEE + querry, {}, true).then(res => res).catch(err => null);
export const updateStateWorking = (querry) => PostData(url.CONFIRM_WORKING + querry, {}, true).then(res => res).catch(err => null);
export const getHighlightNV = (querry) => GetData(url.GET_HIGHLIGHT_NHANVIEN + querry, {}, true).then(res => res).catch(err => null);
export const getHighlightKH = (querry) => GetData(url.GET_HIGHLIGHT_KH + querry, {}, true).then(res => res).catch(err => null);


export const getCalendarWaiting = (querry) => GetData(url.GET_CALENDAR_WAITING + querry, {}, true).then(res => res).catch(err => null);
// API NÀY DÙNG PHƯƠNG THỨC GET ĐỂ ĐẨY DATA LÊN
export const postBookCalendar = (querry) => GetData(url.BOOK_CALENDAR + querry, {}, true).then(res => res).catch(err => null);
export const updateCalendar = (body) => PostData(url.UPDATE_CALENDAR, body, true).then(res => res).catch(err => null);


// User Info
export const putImage = (formData) => PostFormData(url.PUT_IMAGE, formData, true).then(res => res).catch(err => null);
export const getAllNVCapTren = () => GetData(url.GET_ALL_NV_CAPTREN, {}, true).then(res => res).catch(err => null);
export const getNotif = () => GetData(url.GET_NOTIF, {}, true).then(res => res).catch(err => null);
export const getFormNotif = () => GetData(url.GET_FORM_NOTIF, {}, true).then(res => res).catch(err => null);
export const postNotigication = (body) => PostData(url.POST_NOTIF, body, true).then(res => res).catch(err => null);

// KPI
export const getKPI = (querry) => GetData(url.GET_KPI_BY_MONTH + querry, {}, true).then(res => res).catch(err => null);
export const getDetailKPI = (body) => PostData(url.GET_DETAIL_KPI, body, true).then(res => res).catch(err => null);
export const updateUserLocation = (body) => PostData(url.UPDATE_LOCATION_USER + body, {}, true).then(res => res).catch(err => null);

export const getDichVuHome = () => GetData(url.GET_DICH_VU_HOME, {}, true).then(res => res).catch(err => null);
export const getTrangThaiDV = () => GetData(url.GET_TRANG_THAI_DV, {}, true).then(res => res).catch(err => null);
export const setTrangThaiDV = (trangThai) => GetData(url.SET_TRANG_THAI_DV, trangThai, true).then(res => res).catch(err => null);
export const getThongBaoChuaDoc = () => GetData(url.TONG_THONG_BAO_CHUA_DOC, {}, true).then(res => res).catch(err => null);
export const getLichSuDichVu = (body) => GetData(url.GET_LICH_SU_DICH_VU + body, {}, true).then(res => res).catch(err => null);
export const getChuThichLich = () => GetData(url.GET_CHU_THICH_LICH, {}, true).then(res => res).catch(err => null);


// export const setTrangThaiDV = (trangThai) => GetData(url.GET_TRANG_THAI_DV, {}, true).then(res => res).catch(err => null);
