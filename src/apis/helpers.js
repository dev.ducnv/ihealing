/* eslint-disable no-console */
/* eslint-disable consistent-return */
/* eslint-disable no-return-await */
/**
 * helper.js - for storing reusable logic.
 */
import axios from 'axios';
import _ from 'lodash';
import AsyncStorageUtils from 'helpers/AsyncStorageUtils';
import NavigationService from 'routers/NavigationService';
import { Login, IntroScreen } from 'routers/screenNames';
import { Alert } from 'react-native';
import { getLangCode } from '../config/index';
import { TOKEN_EXPIRED } from './Status'

const instance = axios.create({
  // baseURL: `${env.serverURL}/api`,
  timeout: 15 * 1000,
});


export async function GetData(url, data, isAuth = true) {
  let myRequest = {
    method: 'get',
    params: {
      ...data,
    },
    url,
  }
  return await axios(myRequest)
    .then(response => response)
    .then(response => {
      if (isAuth && !_.isNull(response.data) && response.status === TOKEN_EXPIRED) {
        AsyncStorageUtils.remove(AsyncStorageUtils.KEY.INIT_STORGE)
        Alert.alert('Thông báo', 'Phiên đăng nhập của bạn đã hết hạn, vui lòng đăng nhập lại', [{
          text: 'Đồng ý',
          onPress: () => {
            NavigationService.reset(IntroScreen)
          }
        }])
        return true
      } else {
        return response
      }
    })
    .catch(error => error);
}

export async function PostData(url, json, isAuth = true) {
  let myRequest = {
    method: 'post',
    url,
    headers: {
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(json)
  }
  return await axios(myRequest)
    .then(response => response)
    .then(response => {
      if (isAuth && !_.isNull(response.data) && response.status === TOKEN_EXPIRED) {
        AsyncStorageUtils.remove(AsyncStorageUtils.KEY.INIT_STORGE)
        Alert.alert('Thông báo', 'Phiên đăng nhập của bạn đã hết hạn, vui lòng đăng nhập lại', [{
          text: 'Đồng ý',
          onPress: () => {
            NavigationService.reset(IntroScreen)
          }
        }])
        return true
      } else {
        return response
      }
    })
    .catch(error => error);
}

export async function PutData(url, json, isAuth = true) {
  let myRequest = {
    method: 'put',
    url,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(json)
  }
  return await axios(myRequest)
    .then(response => response)
    .then(response => {
      if (isAuth && !_.isNull(response.data) && response.status === TOKEN_EXPIRED) {
        AsyncStorageUtils.remove(AsyncStorageUtils.KEY.INIT_STORGE)
        Alert.alert('Thông báo', 'Phiên đăng nhập của bạn đã hết hạn, vui lòng đăng nhập lại', [{
          text: 'Đồng ý',
          onPress: () => {
            NavigationService.reset(IntroScreen)
          }
        }])
        return true
      } else {
        return response
      }
    })
    .catch(error => error);
}

export async function DelData(url, json, isAuth = true) {
  let myRequest = {
    method: 'del',
    url,
    headers: {
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(json)
  }
  return await axios.delete(url, myRequest)
    .then(response => response)
    .then(response => {
      if (isAuth && !_.isNull(response.data) && response.status === TOKEN_EXPIRED) {
        AsyncStorageUtils.remove(AsyncStorageUtils.KEY.INIT_STORGE)
        Alert.alert('Thông báo', 'Phiên đăng nhập của bạn đã hết hạn, vui lòng đăng nhập lại', [{
          text: 'Đồng ý',
          onPress: () => {
            NavigationService.reset(IntroScreen)
          }
        }])
        return true
      } else {
        return response
      }
    })
    .catch(error => error);
}


export async function PostFormData(url, json, isAuth = true) {
  let myRequest = {
    method: 'post',
    url,
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    data: json
  }
  return await axios(myRequest)
    .then(response => response)
    .then(response => {
      if (isAuth && !_.isNull(response.data) && response.status === TOKEN_EXPIRED) {
        AsyncStorageUtils.remove(AsyncStorageUtils.KEY.INIT_STORGE)
        Alert.alert('Thông báo', 'Phiên đăng nhập của bạn đã hết hạn, vui lòng đăng nhập lại', [{
          text: 'Đồng ý',
          onPress: () => {
            NavigationService.reset(IntroScreen)
          }
        }])
        return true
      } else {
        return response.data
      }
    })
    .catch(error => error);
}

export async function PutFormData(url, json, isAuth = true) {
  let myRequest = {
    method: 'put',
    url,
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    data: json
  }
  return await axios(myRequest)
    .then(response => response)
    .then(response => {
      if (isAuth && !_.isNull(response.data) && response.status === TOKEN_EXPIRED) {
        AsyncStorageUtils.remove(AsyncStorageUtils.KEY.INIT_STORGE)
        Alert.alert('Thông báo', 'Phiên đăng nhập của bạn đã hết hạn, vui lòng đăng nhập lại', [{
          text: 'Đồng ý',
          onPress: () => {
            NavigationService.reset(IntroScreen)
          }
        }])
        return true
      } else {
        return response.data
      }
    })
    .catch(error => error);
}


export async function fetch(url, data, isAuth = false, token) {
  let headers = null;
  let langCode = getLangCode()
  if (isAuth) {
    // let token = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.TOKEN)
    headers = {
      Authorization: `Bearer ${token}`,
      langCode
    };
  } else {
    headers = { langCode }
  }

  return instance
    .get(url, {
      params: {
        ...data,
      },
      headers,
    })
    .then(response => {
      if (isAuth && !_.isNull(response.data) && response.status === TOKEN_EXPIRED) {
        // showAlert(TYPE.ERROR, languages[langCode].sorry, languages[langCode].login_session_expired);
        // AsyncStorageUtils.remove(AsyncStorageUtils.KEY.USER_DATA)
        Alert.alert('Thông báo', 'Phiên đăng nhập của bạn đã hết hạn, vui lòng đăng nhập lại', [{
          text: 'Đồng ý',
          onPress: () => {
            NavigationService.reset(Login)
          }
        }])
        return true
      } else {
        return response.data
      }
    })
    .catch(error => error);
}
