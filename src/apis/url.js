import { NetworkSetting } from '../config/Setting';

export default {
  TEST_TITTLE: 'This is test tittle',
  DELETE_TOKEN: `${NetworkSetting.ROOT}/sotay/deviceToken/user/`,
  LOGIN: `${NetworkSetting.ROOT}/api/userinfo/checklogin`,
  CHANGEPASS: `${NetworkSetting.ROOT}/api/userinfo/changepass`,

  PUSH_DEVICE_TOKEN: `${NetworkSetting.ROOT}/api/nhanvienapp/update_idpush`,
  GET_USER_INFO: `${NetworkSetting.ROOT}/api/userinfo/login`,
  REGISTER: `${NetworkSetting.ROOT}/api/nhanvienapp/khachdangky`,
  GET_DETAIL_PRODUCT: `${NetworkSetting.ROOT}/api/mathang/gettop10mathangcungdanhmuc`,
  SEARCH_ARTICLE: `${NetworkSetting.ROOT}/api/tintuc/getallbaiviet?KeySearch=`,
  GET_NGUOI_BAO_TRO: `${NetworkSetting.ROOT}/api/nhanvienapp/getquanlyforkhachhangdangky`,
  FORGOT_PASS: `${NetworkSetting.ROOT}/api/userinfo/forgotpass`,
  SEARCH_PRESENTER: `${NetworkSetting.ROOT}/api/nhanvienapp/timkiemchuyengiadichvu?`,
  SEARCH_NGUOI_BAO_TRO: `${NetworkSetting.ROOT}/api/nhanvienapp/getquanlyforkhachhangdangky?`,
  SEARCH_NGUOI_GIOI_THIEU: `${NetworkSetting.ROOT}/api/nhanvienapp/getdsnguoigioithieu?`,


  // meta data
  GET_TINH: `${NetworkSetting.ROOT}/api/tinh/getall`,
  GET_XA: `${NetworkSetting.ROOT}/api/xaphuong/getbyidquan`,
  GET_HUYEN: `${NetworkSetting.ROOT}/api/quanhuyen/getbyidtinh`,
  // metadata datlich
  GET_TINH_DAT_LICH: `${NetworkSetting.ROOT}/api/tinh/getallfordatdichvu`,
  GET_XA_DAT_LICH: `${NetworkSetting.ROOT}/api/xaphuong/getbyidquan_fordatlich`,
  GET_HUYEN_DAT_LICH: `${NetworkSetting.ROOT}/api/quanhuyen/getbyidtinh_fordatlich`,


  // Product & Service
  GET_METADATA_PRODUCT: `${NetworkSetting.ROOT}/api/mathang/boLocSanPham`,
  GET_METADATA_SERVICE: `${NetworkSetting.ROOT}/api/mathang/boLocDichVu`,
  GET_LIST_PRODUCT: `${NetworkSetting.ROOT}/api/mathang/getallmathangNewv2`,

  GET_DETAILED_PRODUCT: `${NetworkSetting.ROOT}/api/mathang/getbyid`,
  GET_OTHER_PRODUCT: `${NetworkSetting.ROOT}/api/mathang/getallmathanglienquan`,

  POST_CREATE_INVOICE: `${NetworkSetting.ROOT}/api/donhang/taoMoiDonHang`,
  GET_DETAIL_INVOICE: `${NetworkSetting.ROOT}/api/donhang/danhSachChiTietDonHang`,

  GET_DON_HANG_DA_DAT: `${NetworkSetting.ROOT}/api/donhang/getlistdonghang`,
  GET_LIST_RATING: `${NetworkSetting.ROOT}/api/kehoachnhanvien/getkehoach_dalam_bykhachhang`,
  RATING_BY_ID: `${NetworkSetting.ROOT}/api/kehoachnhanvien/danhgia`,
  CANCEL_INVOICE: `${NetworkSetting.ROOT}/api/donhang/huydonhang`,

  // Managetment
  GET_LIST_WAITING_ACCEPT: `${NetworkSetting.ROOT}/api/nhanvienapp/getallnhanvien_chopheduyet`,
  POST_DENY_USER: `${NetworkSetting.ROOT}/api/nhanvienapp/tuchoidangkyquanly`,
  POST_ACCEPT_USER: `${NetworkSetting.ROOT}/api/nhanvienapp/pheduyetdangkyquanly`,
  GET_LIST_NESTED_EMPLOYEE: `${NetworkSetting.ROOT}/api/nhanvienapp/getallnhanvien_capduoi`,


  // Calendar
  GET_CALENDAR_BYDAY: `${NetworkSetting.ROOT}/api/kehoachnhanvien/getkehoachbykhachhang`,
  GET_CALENDAR_BYDAY_EMPLOYEE: `${NetworkSetting.ROOT}/api/kehoachnhanvien/getkehoachbynhanvien`,
  GET_CALENDAR_WAITING: `${NetworkSetting.ROOT}/api/mathang/getdichvubykhachhang`,
  BOOK_CALENDAR: `${NetworkSetting.ROOT}/api/kehoachnhanvien/update`,
  UPDATE_CALENDAR: `${NetworkSetting.ROOT}/api/kehoachnhanvien/updateexited`,
  CONFIRM_WORKING: `${NetworkSetting.ROOT}/api/kehoachnhanvien/`,

  GET_HIGHLIGHT_NHANVIEN: `${NetworkSetting.ROOT}/api/kehoachnhanvien/getdatekehoachbynhanvien`,
  GET_HIGHLIGHT_KH: `${NetworkSetting.ROOT}/api/kehoachnhanvien/getdatekehoachbykhachhang`,

  // UserInfo
  POST_UPDATE_INFO: `${NetworkSetting.ROOT}/api/nhanvienapp/suanhanvien`,
  POST_UPDATE_AVATAR: `${NetworkSetting.ROOT}/api/uploadfile/savefilenv`,
  PUT_IMAGE: `${NetworkSetting.ROOT}/api/uploadfile/savefilenv`,
  GET_ALL_NV_CAPTREN: `${NetworkSetting.ROOT}/api/nhanvienapp/getallnhanvien_captren`,
  GET_NOTIF: `${NetworkSetting.ROOT}/api/noti/canhbaohoatdong`,
  GET_FORM_NOTIF: `${NetworkSetting.ROOT}/api/noti/listthongbaokehoach`,
  POST_NOTIF: `${NetworkSetting.ROOT}/api/noti/guithongbaokehoach`,


  // kpi
  GET_KPI_BY_MONTH: `${NetworkSetting.ROOT} /api/nhanvienapp / getKPI`,
  GET_DETAIL_KPI: `${NetworkSetting.ROOT} /api/donhang / getlistdonghang`,

  // get yeu cau tu van
  GET_YEU_CAU_TU_VAN: `${NetworkSetting.ROOT}/api/yeucautuvan/getbytaikhoan`,
  POST_UPDATE_YEU_CAU_TU_VAN: `${NetworkSetting.ROOT}/api/yeucautuvan/updatetrangthai`,
  UPDATE_LOCATION_USER: `${NetworkSetting.ROOT}/api/nhanvienapp/update_location`,
  // home
  GET_DICH_VU_HOME: `${NetworkSetting.ROOT}/api/mathang/getdichvu_homepage`,
  GET_TRANG_THAI_DV: `${NetworkSetting.ROOT}/api/userinfo/gettrangthainhandichvu`,
  SET_TRANG_THAI_DV: `${NetworkSetting.ROOT}/api/userinfo/updatetrangthainhandichvu`,
  TONG_THONG_BAO_CHUA_DOC: `${NetworkSetting.ROOT}/api/noti/countcanhbaohoatdong`,
  GET_LICH_SU_DICH_VU: `${NetworkSetting.ROOT}/api/kehoachnhanvien/getkehoachbykhachhang`,
  GET_LICH_SU_DUOC_LIEU: `${NetworkSetting.ROOT}/api/donhang/lichsudonhang_duoclieu`,
  GET_CHU_THICH_LICH: `${NetworkSetting.ROOT}/api/kehoachnhanvien/getghichu`
  // GET_YEU_CAU_TU_VAN: `${NetworkSetting.ROOT} /api/yeucautuvan / getbytaikhoan`,
  // POST_UPDATE_YEU_CAU_TU_VAN: `${NetworkSetting.ROOT} /api/yeucautuvan / updatetrangthai`,
  // UPDATE_LOCATION_USER: `${NetworkSetting.ROOT} /api/nhanvienapp / update_location`
};
