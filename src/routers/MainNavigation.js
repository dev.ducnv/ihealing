import { createStackNavigator, createAppContainer } from 'react-navigation';
/**
 *  Home
 */
// import Home from '../features/Home';

import Home from '../features/TabHome';
import Management from '../features/TabHome/Item/Management';
// import Home from '../features/Home';
import DetailTraining from '../features/Infomation/Training/DetailTraining'
/**
 *  Intro
 */
import IntroScreen from '../features/Intro';
/**
 * Login
 */
import Login from '../features/Login';
/**
 * Register
 */
import Register from '../features/Register';
import RegisterStep2 from '../features/Register/ChuyenVien/Step2';
import RegisterStep3 from '../features/Register/ChuyenVien/Step3';
/**
 * Change Password
 */
import ChangePassword from '../features/ChangePassword';
/**
 *  DetailPrduct
 */
import DetailProduct from '../features/DetailProduct';
/**
 *  PaymentOrders
 */
import PaymentOrders from '../features/PaymentOrders/index';
/**
 *  CartProduct
 */
import CartProduct from '../features/CartProduct/index';
import ChiTietLichSuDonHang from '../features/LichSuDonHang/ChiTietLichSuDonHang'
import DanhGiaDonHang from '../features/LichSuDonHang/DanhGiaDonHang'


import SearchProduct from '../features/SearchScreen/SearchProduct'
import SearchArticle from '../features/SearchScreen/SearchArticle'
/**
 * ChangeInfo
 */
import ChangeInfo from '../features/ChangeInfo'
import * as screenNames from './screenNames'
/**
 * LichSuDonHang
 */
import LichSuDonHang from '../features/LichSuDonHang';
/**
 * LichChamSoc
 */
import LichChamSoc from '../features/TabHome/Item/WorkingCalendar';
import DoiLichChamSoc from '../features/TabHome/Item/WorkingCalendar/DoiLichChamSoc';

/**
 * Infomation
 */
import Training from '../features/Infomation/Training'
import Product from '../features/Infomation/Product'
import Service from '../features/Infomation/Service'
import HumanResource from '../features/Infomation/HumanResource'

/**
 * Muc tieu
 */
import Target from '../features/TabHome/Item/Target'
import ThongBao from '../features/ThongBao/ThongBao'
import ChiTietTB from '../features/ThongBao/ChiTietTB'
import ChiTietDoanhSo from '../features/TabHome/Item/Target/ChiTietDoanhSo'
import GuiThongBao from '../features/ThongBao/GuiThongBao'


console.disableYellowBox = true;

const AppNavigator = createStackNavigator(
  {

    [screenNames.Home]: { screen: Home },
    [screenNames.IntroScreen]: { screen: IntroScreen },

    [screenNames.Login]: { screen: Login },
    [screenNames.Register]: { screen: Register },
    [screenNames.ChangePassword]: { screen: ChangePassword },
    [screenNames.RegisterStep2]: { screen: RegisterStep2 },
    [screenNames.RegisterStep3]: { screen: RegisterStep3 },
    [screenNames.DetailTraining]: { screen: DetailTraining },

    [screenNames.DetailProduct]: { screen: DetailProduct },
    [screenNames.PaymentOrders]: { screen: PaymentOrders },
    [screenNames.CartProduct]: { screen: CartProduct },
    [screenNames.SearchProduct]: { screen: SearchProduct },
    [screenNames.SearchArticle]: { screen: SearchArticle },
    [screenNames.ChangeInfo]: { screen: ChangeInfo },
    [screenNames.LichSuDonHang]: { screen: LichSuDonHang },
    [screenNames.LichChamSoc]: { screen: LichChamSoc },
    [screenNames.DoiLichChamSoc]: { screen: DoiLichChamSoc },

    [screenNames.Management]: { screen: Management },
    [screenNames.Target]: { screen: Target },
    // infomation
    [screenNames.Training]: { screen: Training },
    [screenNames.Product]: { screen: Product },
    [screenNames.Service]: { screen: Service },
    [screenNames.HumanResource]: { screen: HumanResource },
    [screenNames.ChiTietLichSuDonHang]: { screen: ChiTietLichSuDonHang },
    [screenNames.DanhGiaDonHang]: { screen: DanhGiaDonHang },
    [screenNames.ThongBao]: { screen: ThongBao },
    [screenNames.ChiTietTB]: { screen: ChiTietTB },
    [screenNames.ChiTietDoanhSo]: { screen: ChiTietDoanhSo },
    [screenNames.GuiThongBao]: { screen: GuiThongBao }
  },
  {
    initialRouteName: screenNames.IntroScreen,
    headerMode: 'none'
  }
);
export default createAppContainer(AppNavigator);
