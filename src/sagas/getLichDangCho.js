import { put, call, takeEvery } from 'redux-saga/effects';
import { getCalendarWaiting } from '../apis';

const fetchGetAPI = (json) => getCalendarWaiting(json);


function* fetchGet(action) {
  try {
    const response = yield call(fetchGetAPI, action.payload);
    if (response.data) {
      yield put({ type: 'GET_LICH_DANG_CHO_SUCCESS', payload: response.data });
    } else {
      yield put({ type: 'GET_LICH_DANG_CHO_SUCCESS', payload: [] });
    }
    action.skip && action.skip(response.data)
  } catch (err) {
    yield put({ type: 'GET_LICH_DANG_CHO_SUCCESS', payload: [] });
  }
}

export function* watchFetchGetLich() {
  yield takeEvery('GET_LICH_DANG_CHO', fetchGet);
}
