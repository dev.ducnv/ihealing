import { all, fork } from 'redux-saga/effects';
import { watchSaveUserData, watchUpdateUserData } from './users';
import { watchNearPlaces } from './location';
import { watchFetchGetLich } from './getLichDangCho';
import { watchFetchGetArticle } from './getAllArticle'

export default function* rootSaga() {
  yield all([
    fork(watchSaveUserData),
    fork(watchUpdateUserData),
    fork(watchNearPlaces),
    fork(watchFetchGetLich),
    fork(watchFetchGetArticle)
  ]);
}
