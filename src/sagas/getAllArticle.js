import { put, call, takeEvery } from 'redux-saga/effects';
import { setAccount } from 'actions';
import NavigationService from 'routers/NavigationService';
import { Login } from 'routers/screenNames';
import AsyncStorageUtils from 'helpers/AsyncStorageUtils';
import R from 'assets/R';
import { Alert } from 'react-native';
import { searchArticle } from '../apis';

const fetchGetAPI = () => searchArticle('');


function* fetchGet(action) {
  try {
    const response = yield call(fetchGetAPI, action.payload);
    if (response.data) {
      yield put({ type: 'GET_ALL_ARTICLE_SUCCESS', payload: response.data });
    } else {
      yield put({ type: 'GET_ALL_ARTICLE_SUCCESS', payload: [] });
    }
    action.skip && action.skip(response.data)
  } catch (err) {
    Alert.alert('Thông báo', 'Phiên đăng nhập đã hết hạn, vui lòng đăng nhập lại', [{
      text: 'Ok',
      onPress: async () => {
        setAccount({})
        await AsyncStorageUtils.multiSet([
          [R.strings.INIT_STORGE, JSON.stringify(null)],
        ]);
        NavigationService.reset(Login)
      }
    }], { cancelable: false })

    yield put({ type: 'GET_ALL_ARTICLE_SUCCESS', payload: [] });
  }
}

export function* watchFetchGetArticle() {
  yield takeEvery('GET_ALL_ARTICLE', fetchGet);
}
