import React from 'react';
import { View, Text, Platform, TouchableOpacity, Alert } from 'react-native';
// import DeviceInfo from 'react-native-device-info';
import { connect } from 'react-redux'
import NavigationService from 'routers/NavigationService';
// import { showAlert, TYPE } from 'common/DropdownAlert';

import firebase from 'react-native-firebase';
import AsyncStorageUtils from './AsyncStorageUtils';

let PushNotification = require('react-native-push-notification');


class NotificationFirebase extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      users: null,
      cars: null,
      userData: null,
      isLoading: false,
      isExitDialogVisible: false,

    };
    this.isFocused = false
    this.deviceId;
  }

  async componentDidMount() {
    const fcmToken = await firebase.messaging().getToken();
    console.log('====>>nhan notification', fcmToken);

    if (Platform.OS === 'android') {
      PushNotification.configure({
        onRegister: async (token) => {
          console.log('====>>nhan notification', token);
          await AsyncStorageUtils.save(AsyncStorageUtils.KEY.FCM_TOKEN, JSON.stringify(token.token));
        },
        onNotification: async (notification) => {
          console.log('NOTIFICATION:', notification);
          this._onPressNotification(notification)
        },
        senderID: '1095108261917', // 984945520826
        permissions: {
          alert: true,
          badge: true,
          sound: true
        },
        popInitialNotification: true,
        requestPermissions: true,
        largeIcon: 'ic_launcher',
        smallIcon: 'ic_notification',
        color: '#3E4F6F',
        vibrate: true,
        vibration: 3
        // smallIcon: 'ic_stat_android_notification_icon',
      });
    } else {
      if (fcmToken) {
        await AsyncStorageUtils.save(AsyncStorageUtils.KEY.FCM_TOKEN, JSON.stringify(fcmToken));
      } else {
        // console.log('====>>no fcmToken', fcmToken)
        // user doesn't have a device token yet
      }
      firebase.messaging().requestPermission()
        .then(() => {
          // console.log('====>>dong y co hasPermission')
        })
        .catch(error => {
          // console.log('====>>tu choi hasPermission')
        });
      firebase.notifications().onNotification((notification) => {
        console.log('====>>nhan notification', notification);
        this.isFocused = true
        // showAlert(TYPE.INFO, notification.title, notification.message)
        firebase.notifications().displayNotification(notification);
      });
      firebase.notifications().onNotificationOpened(async (notificationOpen) => {
        console.log('====>>nhan notificationwwwww', notificationOpen);

        await this._onRemoteNotification(notificationOpen.notification);
      });
      firebase.notifications()
        .getInitialNotification()
        .then((remoteMessage) => {
          if (remoteMessage) {
            console.log('====>>nhan notificationwwwww', remoteMessage);
            this._onRemoteNotification(remoteMessage.notification);
          }
        });
    }
  }

  _onPressNotification = () => {
    if (this.isFocused) {
      NavigationService.navigate('ListThongBao');
    } else {
      this.setState({ isLoading: true })
      setTimeout(() => {
        NavigationService.navigate('ListThongBao');
      }, 3500)
    }
  }

  getOrUpdateData = () => {

  }

  componentWillUnmount() {

  }


  _onRegistered = async (deviceToken) => {
    console.log('_onRegistered ios', deviceToken);
    // let AccessToken = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.TOKEN)
  }

  _onRegistrationError = (error) => {
    console.log('Failed To Register For Remote Push ios', error.message, error.code);
  }

  _onRemoteNotification = async (notification) => {
    //  console.log('_onRemoteNotification ios', notification);
    console.log('_onRemoteNotification ios', notification);
  }

  _onLocalNotification = (notification) => {
    console.log('_onLocalNotification ios', notification);
  }


  render() {
    return (
      <View />
    );
  }
}
function mapStateToProps(state) {
  return {

  }
}
export default connect(mapStateToProps, {})(NotificationFirebase);
