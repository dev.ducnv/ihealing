export const StringSetting = {
  SUB_APPNAME: 'AIS',
  MAIN_APPNAME: 'AIS E-Note',
  VERSION_ANDROID: '0.0.1',
  VERSION_IOS: '0.0.1',
};
export const ImageSetting = {
  logoMain: require('../assets/images/general/logo.png'),
};
export const ColorsSetting = {
  MAIN_COLOR: '#050021',
};
export const OneSignalKey = {
  development: '9f81dc21-7054-4f5f-ad53-87f025d2d154',
  production: '9f81dc21-7054-4f5f-ad53-87f025d2d154',
};
export const NetworkSetting = {
  ROOT: 'https://api.ihealing.vn', // root api ihealing
  SOCKET: 'http://203.162.10.108:3070',
  IMAGE: 'https://api.ihealing.vn',
};
