// @flow
import React from 'react';
import {
  View,
  StyleSheet,
  ActivityIndicator
} from 'react-native';
import R from 'assets/R';
import { WIDTH, HEIGHT } from '../../config/Function';

type Props = {
  isLoading: boolean
}

export const LoadingComponent = (props: Props) => {
  const { style } = props
  if (!props.isLoading) return null;
  return (
    <View style={[styles.loadingContainer, style && style]}>
      <ActivityIndicator color={R.colors.yellow254} animating size="large" />
    </View>
  );
};
const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    elevation: 3,
    // left: WIDTH(360) / 2 - WIDTH(10),
    zIndex: 10,
    // top: HEIGHT(640) / 2,
    // position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: R.colors.black3
  },
});
