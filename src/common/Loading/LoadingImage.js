import React from 'react';
import { StyleSheet, TouchableOpacity, ActivityIndicator, View, Image } from 'react-native';
import FastImage from 'react-native-fast-image';
import R from '../../assets/R';
import { HEIGHT, getWidth } from '../../config';

class LoadingImage extends React.PureComponent<Props> {
  constructor(props: any) {
    super(props);
    this.state = {
      loading: true
    };
  }

  render() {
    const { item } = this.props
    return (
      <TouchableOpacity disabled={true}>
        <Image
          style={styles.swiper}
          resizeMode={FastImage.resizeMode.stretch}
          source={item}
          onLoadEnd={() => { this.setState({ loading: false }) }}
        />
        {
          this.state.loading && (
            <View style={{ position: 'absolute', alignSelf: 'center', top: HEIGHT(40) }}>
              <ActivityIndicator color={R.colors.yellow254} animating size="small" />
            </View>
          )
        }
      </TouchableOpacity>
    )
  }
}


export default LoadingImage

const styles = StyleSheet.create({
  swiper: {
    height: HEIGHT(125),
    width: getWidth(),
  },
})
