import React from 'react';
import { StyleSheet, Text, TouchableOpacity, Platform, ViewStyle, StyleProp } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import LinearGradient from 'react-native-linear-gradient';
import { HEIGHT, getFont, getLineHeight, WIDTH } from '../../config';
import R from '../../assets/R';

type Props = {
  title: string,
  hasImage: boolean,
  onButton: Function,
  width: number,
  height: number,
  style: StyleProp<ViewStyle>,
  titleStyle: Object,
  disable: boolean,
};

const BaseButton = (props: Props) => {
  const { title, hasImage, onButton, width, height, style, titleStyle, disable } = props;
  return (
    <LinearGradient
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 1 }}
      colors={[R.colors.yellow1c8, R.colors.yellow254, R.colors.yellow1c8]}
      style={[
        styles.btnStyle,
        { width: width || WIDTH(327), height: height || HEIGHT(48) },
        style && style,
      ]}
    >
      <TouchableOpacity
        style={[
          styles.flexCenter,
          hasImage && {
            flexDirection: 'row',
            paddingVertical: HEIGHT(12),
            paddingHorizontal: WIDTH(26),
          },
        ]}
        disable={disable}
        onPress={(event) => {
          onButton && onButton(event.nativeEvent.pageY);
        }}
      >
        {hasImage && (
          <Icon
            style={{ marginRight: WIDTH(8) }}
            name="ios-checkmark-circle"
            size={WIDTH(24)}
            color={R.colors.blackChecked}
          />
        )}
        <Text
          style={[
            Platform.OS === 'ios' ? styles.titleIOS : styles.title,
            {
              width: width
                ? hasImage
                  ? width - WIDTH(85)
                  : width - WIDTH(30)
                : WIDTH(290),
            },
            titleStyle && titleStyle,
          ]}
        >
          {title && title}
        </Text>
      </TouchableOpacity>
    </LinearGradient>
  );
};

export default BaseButton;

const styles = StyleSheet.create({
  btnStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: R.colors.yellow254,
    borderRadius: WIDTH(100),
  },
  title: {
    fontSize: getFont(16),
    color: R.colors.black0,
    fontWeight: '700',
    lineHeight: getLineHeight(24),
    flex: 1,
    textAlignVertical: 'center',
    textAlign: 'center',
  },
  titleIOS: {
    fontSize: getFont(16),
    color: R.colors.black0,
    fontWeight: '700',
    lineHeight: getLineHeight(24),
    // flex: 1,
    // textAlignVertical: "center",
    textAlign: 'center',
  },
  imageStyle: {
    width: WIDTH(24),
    height: WIDTH(24),
  },
  flexCenter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
