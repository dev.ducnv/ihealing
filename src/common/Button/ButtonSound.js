// @flow
import React from 'react';
import {
  StyleSheet, TouchableOpacity
} from 'react-native';
import FastImage from 'react-native-fast-image'
import { WIDTH } from '../../config';
import R from '../../assets/R';

type Props = {
  navigation: any,
}
const ButtonChatbot = (props: any) => (
  <TouchableOpacity
    onPress={() => props.onButton()}
    style={{ position: 'absolute', bottom: 10, right: 10 }}
  >
    {/* <LottieView
          source={IMAGE.robot}
          autoPlay
          style={styles.logoStart}
          loop
        /> */}
    <FastImage source={R.images.sound} style={styles.logoStart} />
  </TouchableOpacity>
);
export default ButtonChatbot;
const styles = StyleSheet.create({
  logoStart: {
    width: WIDTH(50),
    height: WIDTH(50),
  },
});
