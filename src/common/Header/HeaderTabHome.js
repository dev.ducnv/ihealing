import React from 'react';
import { View, Text, StatusBar, StyleSheet, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import Entypo from 'react-native-vector-icons/Entypo'
import TextGradient from '../View/TextGradient';
import NavigationService from '../../routers/NavigationService';
import { HEIGHT, WIDTH, getFont, getWidth } from '../../config';
import R from '../../assets/R';
import BaseButton from '../Button/BaseButton';

type Props = {
  searchEnable: boolean,
  title: string,
  onButtonSearch: Function,
  onButtonCart: Function,
  enableBack: boolean,
  cartEnable: boolean
}

export default class HeaderTabHome extends React.PureComponent<Props> {
  render() {
    const { title, onButtonSearch, onButton, notiButtonVisible, styleTitle, enableBack, cartEnable } = this.props
    return (
      <View style={{ alignItems: 'center', height: HEIGHT(65), backgroundColor: R.colors.colorMain, width: getWidth() }}>
        <StatusBar backgroundColor={R.colors.colorMain} barStyle="light-content" />
        <View style={styles.content}>
          {
            (enableBack && enableBack === true) && (
              <TouchableOpacity
                onPress={() => NavigationService.pop()}
                hitSlop={{ top: 25, bottom: 25, left: 25, right: 25 }}
              >
                <Entypo
                  name="chevron-left"
                  size={HEIGHT(32)}
                  color={R.colors.yellow254}
                />
              </TouchableOpacity>
            )
          }
          {styleTitle ? <Text style={[styles.textTitle, styleTitle && styleTitle]}>{title && title}</Text>
            : <View style={{ flex: 1 }}><TextGradient text={title} lineHeight={HEIGHT(40)} fontSize={getFont(32)} /></View>
          }
          {/*
            {
              (cartEnable && cartEnable === true) && (
                <View>
                  <TouchableOpacity
                    onPress={onButtonCart && onButtonCart}
                    hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                  >
                    <FastImage style={{ width: WIDTH(20), height: WIDTH(20), marginLeft: WIDTH(20) }} source={R.images.iconCart} resizeMode={FastImage.resizeMode.stretch} />
                  </TouchableOpacity>

                  <View style={styles.numCart}>
                    <Text style={{ color: R.colors.white, fontWeight: '500' }}>3</Text>
                  </View>
                </View>
              )
            }
          </View> */}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  textTitle: {
    fontSize: getFont(32),
    fontWeight: 'bold',
    color: R.colors.yellow254,
    fontFamily: R.fonts.TimesNewRomanBold
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: WIDTH(16),
    paddingVertical: HEIGHT(12),
    width: getWidth()
  },
  numCart: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: R.colors.red646,
    right: -WIDTH(9),
    top: -WIDTH(9),
    width: WIDTH(18),
    height: WIDTH(18),
    borderRadius: WIDTH(10)
  }
})
