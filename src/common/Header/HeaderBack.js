import React from 'react';
import { View, Text, StatusBar, StyleSheet, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import Entypo from 'react-native-vector-icons/Entypo';
import { connect } from 'react-redux';
import { CartProduct } from 'routers/screenNames';
import NavigationService from '../../routers/NavigationService';
import { HEIGHT, WIDTH, getFont, getWidth } from '../../config';
import R from '../../assets/R';

type Props = {
  title: string,
  onButtonSearch: Function,
  onButtonCart: Function,
  iconSearch: boolean,
  iconCart: boolean
}

class HeaderBack extends React.PureComponent<Props> {
  render() {
    const { title, onButtonSearch, onButtonCart, iconSearch, iconCart } = this.props
    return (
      <View style={{ alignItems: 'center', height: HEIGHT(53), backgroundColor: R.colors.colorMain, width: getWidth() }}>
        <StatusBar backgroundColor={R.colors.colorMain} barStyle="light-content" />
        <View style={styles.content}>
          <TouchableOpacity
            onPress={() => NavigationService.pop()}
            style={{ flexDirection: 'row', alignItems: 'center' }}
          >
            <Entypo
              name="chevron-left"
              size={HEIGHT(32)}
              color={R.colors.yellow254}
            />
            <Text style={styles.textTitle}>{title && title}</Text>
          </TouchableOpacity>

          <View style={{ flexDirection: 'row' }}>
            {
              iconSearch !== false
              && (
                <TouchableOpacity
                  onPress={onButtonSearch && onButtonSearch}
                // hitSlop={{ top: 25, bottom: 25, left: 25, right: 25 }}
                >
                  <FastImage style={{ width: WIDTH(20), height: WIDTH(20) }} source={R.images.iconSearch} resizeMode={FastImage.resizeMode.stretch} />
                </TouchableOpacity>
              )
            }
            {
              (iconCart !== false)
              && (
                <TouchableOpacity
                  style={{ marginLeft: WIDTH(20) }}
                  onPress={onButtonCart ? () => onButtonCart() : () => { NavigationService.navigate(CartProduct) }}
                  hitSlop={{ top: 30, bottom: 30, left: 30, right: 30 }}
                >
                  <View>
                    <FastImage style={{ width: WIDTH(20), height: WIDTH(20) }} source={R.images.iconCart} resizeMode={FastImage.resizeMode.stretch} />
                  </View>
                  {this.props.amount > 0
                    && (
                      <View
                        style={styles.numCart}
                      >
                        <Text style={{ color: R.colors.white, fontWeight: '500' }}>{this.props.amount}</Text>
                      </View>
                    )
                  }
                </TouchableOpacity>
              )
            }
          </View>
        </View>
      </View>
    )
  }
}

function mapStateToProps(state) {
  return {
    amount: state.cartReducers.amount,
  };
}

export default connect(mapStateToProps, {})(HeaderBack);

const styles = StyleSheet.create({
  textTitle: {
    fontSize: getFont(16),
    fontWeight: '500',
    color: R.colors.white,
    marginLeft: WIDTH(12)
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: WIDTH(16),
    paddingVertical: HEIGHT(12),
    width: getWidth()
  },
  numCart: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: R.colors.red646,
    right: -WIDTH(9),
    top: -WIDTH(9),
    width: WIDTH(18),
    height: WIDTH(18),
    borderRadius: WIDTH(10)
  }
})
