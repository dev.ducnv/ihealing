// @flow
import React from 'react'
import {
  Text, View, StyleSheet, TouchableOpacity, StatusBar, Platform
} from 'react-native'
// import from Lib
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
// import
import R from 'assets/R';
import { WIDTH, getFont, getHeight, getWidth } from '../../config';

type Props = {
  title: string,
  onButton: Function,
  onButtonRight: Function,
  linkImage: any,
  colorImage: any,
  save: any,
}
const HeaderReal = (props: any) => {
  const { title, onButton, linkImage, onButtonRight, colorImage, save } = props;
  return (
    <View style={styles.header}>
      <StatusBar backgroundColor={R.colors.colorPink} />
      <View style={{ alignItems: 'center', flexDirection: 'row' }}>
        <TouchableOpacity
          onPress={() => onButton()}
          style={styles.menu}
        >
          {
            (onButton === undefined)
            && <View style={{ width: 55 * getHeight() / 640 }} />
          }
          {
            (onButton !== undefined)
            && <Ionicons name="md-arrow-back" size={26 * getHeight() / 640} color={R.colors.white} />
          }
        </TouchableOpacity>
        <View style={styles.cntTextInput}>
          <Text numberOfLines={2} style={[styles.text, { fontSize: getFont(15), fontWeight: '500' }]}>
            {title}
          </Text>
        </View>
      </View>

      {
        (linkImage !== undefined) && (
          <TouchableOpacity
            style={{
              flex: 0,
              width: 55 * getHeight() / 640,
              alignItems: 'flex-end',
              marginRight: WIDTH(3),
            }}
            disabled={colorImage === 'white'}
            onPress={() => onButtonRight()}
          >
            {(linkImage.indexOf('zoom') < 0) && <Ionicons
              name={linkImage}
              size={23 * getHeight() / 640}
              color={colorImage !== undefined ? colorImage : 'white'}
            />}
            {(linkImage.indexOf('zoom') >= 0)
              && <MaterialIcons
                name={linkImage}
                size={23 * getHeight() / 640}
                color={colorImage !== undefined ? colorImage : 'white'}
              />
            }
          </TouchableOpacity>
        )
      }
      {
        (linkImage === undefined && save === undefined)
        && <View style={{ width: 55 * getHeight() / 640 }} />
      }
      {
        (linkImage === undefined && save !== undefined)
        && (
          <TouchableOpacity
            onPress={() => onButtonRight()}
            style={{ width: 55 * getHeight() / 640 }}
          >
            <Text style={[styles.text, {
              fontSize: getFont(14),
              fontWeight: 'bold'
            }]}
            >
              {save}
            </Text>
          </TouchableOpacity>
        )
      }
    </View>
  )
}
export default HeaderReal;
const styles = StyleSheet.create({
  header: {
    height: (43 * getHeight() / 640),
    width: getWidth(),
    flexDirection: 'row',
    backgroundColor: R.colors.colorPink,
    // marginTop: Platform.OS === 'ios' ? getStatusBarHeight() : 0,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingRight: 5,
  },
  menu: {
    flex: 0,
    paddingLeft: WIDTH(23),
    width: 55 * getHeight() / 640,
  },
  logo: {
    width: 26 * getHeight() / 640,
    height: getHeight() * (35 / 640),
  },
  cntlogoStart: {
    width: WIDTH(35 / 360),
    height: getHeight() * (35 / 640),
  },
  logoStart: {
    width: WIDTH(35),
    height: getHeight() * (25 / 640),
  },
  text: {
    color: R.colors.colorWhite,
    fontSize: getFont(14),
    textAlign: 'center',
    textAlignVertical: 'center',
    fontFamily: R.fonts.RobotoMedium
  },
  textMini: {
    color: R.colors.colorWhite,
    fontSize: getFont(13),
    textAlign: 'center',
    textAlignVertical: 'center',
    fontFamily: R.fonts.RobotoRegular,
  },
  cntTextInput: {
    marginLeft: WIDTH(23),
    alignItems: 'center',
    // height: (30 * STYLES.heightScreen / 640),
  },
  textInput: {
    width: getWidth() * 0.53,
    height: (30 * getHeight() / 640),
    backgroundColor: R.colors.headerConfig,
    fontSize: getFont(14),
    fontFamily: R.fonts.RobotoRegular,
    marginLeft: 5,
    // textAlign:'center',
    paddingTop: 0,
    paddingBottom: 0,
    textAlignVertical: 'center',
    color: R.colors.white,
  }
})
