// @flow
import React from 'react'
import {
  View, StyleSheet, StatusBar
} from 'react-native'
// import from Lib
import { SearchBar } from 'react-native-elements';
import _ from 'lodash'
// import
import { WIDTH, HEIGHT, getFont, getWidth } from '../../config';
import R from '../../assets/R';

type Props = {
  placeholder: string,
  onClear: Function,
  updateSearch: Function,
  onClear: Function,
  value: string,
}
const HeaderSearchBar = (props: Props) => {
  const { updateSearch, value, placeholder, onClear } = props;
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={R.colors.colorPink} />
      {
        !_.isUndefined(onClear)
        && <SearchBar
          onClear={() => onClear()}
          placeholder={placeholder}
          placeholderTextColor={R.colors.whitergba}
          onChangeText={(search) => updateSearch(search)}
          value={value}
          containerStyle={styles.containerSearch}
          inputContainerStyle={styles.inputContainerStyle}
          inputStyle={styles.inputStyle}
          searchIcon={{ color: R.colors.white, size: WIDTH(25) }}
          clearIcon={{ color: R.colors.white }}
          selectionColor={R.colors.white}
        />
      }
      {
        _.isUndefined(onClear)
        && <SearchBar
          placeholder={placeholder}
          placeholderTextColor={R.colors.whitergba}
          onChangeText={(search) => updateSearch(search)}
          value={value}
          containerStyle={styles.containerSearch}
          inputContainerStyle={styles.inputContainerStyle2}
          inputStyle={styles.inputStyle}
          searchIcon={{ color: R.colors.white, size: WIDTH(25) }}
          clearIcon={{ color: R.colors.white }}
        />
      }
    </View>
  )
}
export default HeaderSearchBar;
const styles = StyleSheet.create({
  containerSearch: {
    backgroundColor: R.colors.colorPink,
    width: WIDTH(340),
    height: HEIGHT(43),
    // borderWidth: 1,
    borderTopWidth: 0,
    borderBottomWidth: 0,
    borderRadius: WIDTH(5),
    // borderColor: Colors.colorPink,
    justifyContent: 'center',
    paddingHorizontal: WIDTH(18)
  },
  container: {
    width: getWidth(),
    height: HEIGHT(43),
    backgroundColor: R.colors.colorPink,
    flexDirection: 'row',
    alignItems: 'center'
  },
  inputStyle: {
    fontSize: getFont(15),
    fontFamily: R.fonts.Roboto,
    color: R.colors.white,
    height: HEIGHT(43)
  },
  inputContainerStyle: {
    backgroundColor: R.colors.colorPink,
    borderRadius: 8,
    height: HEIGHT(43),
  },
  inputContainerStyle2: {
    backgroundColor: R.colors.colorPink,
    borderTopWidth: 0,
    borderBottomWidth: 0,
    borderRadius: 8,
    height: HEIGHT(43),
    borderWidth: 0
  },
})
