import React from 'react';
import { View, Text, StatusBar, StyleSheet, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import { connect } from 'react-redux';
import NavigationService from 'routers/NavigationService';
import { CartProduct, ThongBao } from 'routers/screenNames';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { getThongBaoChuaDoc } from 'apis/Functions/users';
import { HEIGHT, WIDTH, getFont, getWidth } from '../../config';
import R from '../../assets/R';

type Props = {
  title: string,
  onButton: Function,
  onButtonCart: Function,
  styleTitle: Object
}

class HeaderTabHome extends React.PureComponent<Props> {
  state={
    totalUnRead: 0
  }

  componentDidMount() {
    this.getTotalUnRead()
  }

  getTotalUnRead = async () => {
    let restotalUnRead = await getThongBaoChuaDoc();
    console.log('totalUnRead_totalUnRead', restotalUnRead)
    this.setState({
      totalUnRead: restotalUnRead?.data ?? 0
    })
  }

  render() {
    const { title, onButton, onButtonCart, styleTitle } = this.props
    return (
      <View>
        <StatusBar backgroundColor={R.colors.colorMain} barStyle="light-content" />
        <View style={styles.content}>
          <FastImage
            source={R.images.logoHadoo}
            style={styles.img}
            resizeMode={FastImage.resizeMode.stretch}
          />
          <Text style={[styles.textTitle, styleTitle && styleTitle]}>{title && title}</Text>
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity
              onPress={() => NavigationService.navigate(ThongBao, {
                updateDidRead: this.getTotalUnRead
              })}
              hitSlop={styles.hitSlop}
              activeOpacity={0.6}
            >
              <MaterialIcons name="notifications" size={HEIGHT(26)} color={R.colors.borderItemCart} style={{ marginRight: WIDTH(0) }} />
              {this.state.totalUnRead > 0
                && (
                  <View
                    style={styles.numCart}
                  >
                    <Text style={{ color: R.colors.white, fontWeight: '500' }}>{this.state.totalUnRead}</Text>
                  </View>
                )
              }
              {/* <FastImage style={{ width: WIDTH(20), height: WIDTH(20) }} source={R.images.iconSearch} resizeMode={FastImage.resizeMode.stretch} /> */}
            </TouchableOpacity>
            <TouchableOpacity
              style={{ marginLeft: WIDTH(20) }}
              onPress={() => {
                NavigationService.navigate(CartProduct)
              }}
              hitSlop={{ top: 30, bottom: 30, left: 10, right: 30 }}
            >
              <View>
                <FastImage style={{ width: WIDTH(20), height: WIDTH(20) }} source={R.images.iconCart} resizeMode={FastImage.resizeMode.stretch} />
              </View>
              {this.props.amount > 0
                && (
                  <View
                    style={styles.numCart}
                  >
                    <Text style={{ color: R.colors.white, fontWeight: '500' }}>{this.props.amount}</Text>
                  </View>
                )
              }
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}
function mapStateToProps(state) {
  return {
    amount: state.cartReducers.amount,
  };
}
export default connect(mapStateToProps, {})(HeaderTabHome);

const styles = StyleSheet.create({
  textTitle: {
    fontFamily: R.fonts.TimesNewRomanBold,
    fontSize: getFont(20),
    lineHeight: HEIGHT(40),
    color: R.colors.yellow254,
  },
  img: {
    width: WIDTH(56),
    height: HEIGHT(48)
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: WIDTH(10),
    paddingRight: WIDTH(16),
    paddingBottom: HEIGHT(8),
    paddingTop: HEIGHT(11),
    width: getWidth(),
    backgroundColor: R.colors.colorMain
  },
  numCart: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: R.colors.red646,
    right: -WIDTH(9),
    top: -WIDTH(9),
    width: WIDTH(18),
    height: WIDTH(18),
    borderRadius: WIDTH(10)
  },
  hitSlop: {
    top: 20,
    bottom: 20,
    left: 20,
    right: 20
  }
})
