import React from 'react';
import { View, Text, StatusBar, StyleSheet, TextInput } from 'react-native';
import FastImage from 'react-native-fast-image';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Entypo from 'react-native-vector-icons/Entypo';
import NavigationService from 'routers/NavigationService';
import { connect } from 'react-redux';
import { HEIGHT, WIDTH, getFont, getWidth } from '../../config';
import R from '../../assets/R';


class HeaderSearch extends React.PureComponent {
  render() {
    const { title, onButtonSearch, onButtonCart, onChangeText, onButtonBack } = this.props
    return (
      <View style={{ alignItems: 'center', height: HEIGHT(53), backgroundColor: R.colors.colorMain, width: getWidth() }}>
        <StatusBar backgroundColor={R.colors.colorMain} barStyle="light-content" />
        <View style={styles.content}>
          <View
            style={{ flexDirection: 'row', alignItems: 'center' }}
          >
            <TouchableOpacity
              onPress={() => (onButtonBack ? onButtonBack() : NavigationService.pop())}
              hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
            >
              <Entypo
                name="chevron-left"
                size={HEIGHT(32)}
                color={R.colors.yellow254}
              />
            </TouchableOpacity>
            <TextInput
              value={title && title}
              style={styles.textTitle}
              // autoFocus={true}
              placeholder="Tìm kiếm"
              placeholderTextColor={R.colors.white}
              onChangeText={onChangeText}
            />
          </View>

          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity
              onPress={onButtonSearch && onButtonSearch}
              hitSlop={{ top: 25, bottom: 25, left: 25, right: 10 }}
            >
              <FastImage style={{ width: WIDTH(20), height: WIDTH(20) }} source={R.images.iconSearch} resizeMode={FastImage.resizeMode.stretch} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={onButtonCart && onButtonCart}
              hitSlop={{ top: 25, bottom: 25, left: 10, right: 25 }}
            >
              <FastImage style={{ width: WIDTH(20), height: WIDTH(20), marginLeft: WIDTH(20) }} source={R.images.iconCart} resizeMode={FastImage.resizeMode.stretch} />
            </TouchableOpacity>
            {this.props.amount > 0
              && (
                <View style={styles.numCart}>
                  <Text style={{ color: R.colors.white, fontWeight: '500' }}>{this.props.amount}</Text>
                </View>
              )
            }

          </View>
        </View>
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    amount: state.cartReducers.amount,
  };
}

export default connect(mapStateToProps, {})(HeaderSearch);

const styles = StyleSheet.create({
  textTitle: {
    fontSize: getFont(14),
    fontWeight: '500',
    color: R.colors.white,
    marginLeft: WIDTH(12),
    padding: 0,
    height: HEIGHT(36),
    width: WIDTH(240),
    paddingHorizontal: WIDTH(12),
    backgroundColor: R.colors.black3
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: WIDTH(16),
    paddingVertical: HEIGHT(12),
    width: getWidth()
  },
  numCart: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: R.colors.red646,
    right: -WIDTH(9),
    top: -WIDTH(9),
    width: WIDTH(18),
    height: WIDTH(18),
    borderRadius: WIDTH(10)
  }
})
