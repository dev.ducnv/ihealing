import React, { Component } from 'react';
import { View, Text, Modal, StyleSheet, TouchableOpacity, TouchableWithoutFeedback, ActivityIndicator } from 'react-native';
import Autocomplete from 'react-native-autocomplete-input';
import _ from 'lodash';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { findPresenter, searchNguoiGioiThieu } from '../../apis';
import { getFont, HEIGHT, WIDTH } from '../../config/Function';
import R from '../../assets/R';

class ModalSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      query: '',
      typingTimeout: 0,
      search: '',
      reRender: false,
    };
    this.resultSearch = []
    this.isSpa = 0
  }

  dataSearch = [
    { tenDayDu: 'Ngô Bảo Linh', dienThoai: '074947421', image: R.images.person1, maTaiKhoan: 'HDS2006000124' },
    { tenDayDu: 'Billie Eilish', dienThoai: '092548499', image: R.images.person1, maTaiKhoan: 'HDS2006000125' },
    { tenDayDu: 'Mai Nam Hải', dienThoai: '0254959595', image: R.images.person1, maTaiKhoan: 'HDS2006000126' }
  ]

  setModalVisible = (visible, isSpa = 0) => {
    this.setState({
      modalVisible: visible
    })
    this.isSpa = isSpa
    this._findPresenter(this.state.query)
  }

  componentDidMount() {
    this._changeName('')
  }

  onSearch = (query) => {
    if (query === '') {
      return this.dataSearch.slice(0, 5);
    }
    const regex = new RegExp(`${query.trim()}`, 'i');
    return this.dataSearch.filter(item => _.has(item, 'tenDayDu') && item.tenDayDu.search(regex) >= 0);
  }

  _findPresenter = async (text) => {
    this.setState({
      loading: true
    }, async () => {
      let query = `KeySearch=${text}`
      let resPresenter
      if (this.props.type === 'nguoiGioiThieu') {
        query = `KeySearch=${text}`
        resPresenter = await searchNguoiGioiThieu(query)
      } else {
        query = `isSpa=${this.isSpa}&KeySearch=${text}`
        resPresenter = await findPresenter(query)
      }
      if (resPresenter && resPresenter.data) {
        this.resultSearch = resPresenter.data
      } else this.resultSearch = []
      this.setState({ loading: false })
    })
  }

  _changeName = (events) => {
    let event = this.escapeRegExp(events)
    // console.log('event_event', event)
    this.setState({ search: event })
    if (this.state.typingTimeout) {
      clearTimeout(this.state.typingTimeout);
    }

    this.setState({
      query: event,
      typing: false,
      typingTimeout: setTimeout(async () => {
        this._findPresenter(this.state.query)
      }, 2000)
    });
  }

  escapeRegExp = (str) => str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '')

  render() {
    const { title } = this.props
    const { query } = this.state;
    const dataSearch = this.onSearch(query);
    const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
    return (
      <Modal
        animationType="slide"
        transparent={true}
        onRequestClose={() => { this._changeName(''); this.setModalVisible(false) }}
        visible={this.state.modalVisible}
      >
        <TouchableWithoutFeedback
          onPress={() => { this.setModalVisible(false) }}
        >
          <View style={styles.opacity}>
            <TouchableWithoutFeedback>
              <View style={styles.modal}>
                <Text style={styles.title}>{title}</Text>
                <View style={styles.container}>
                  <Autocomplete
                    autoCapitalize="none"
                    autoCorrect={false}
                    inputContainerStyle={styles.inputBox}
                    placeholderTextColor={R.colors.black0}
                    containerStyle={styles.autocompleteContainer}
                    // data={dataSearch.length === 1 && comp(query, dataSearch[0].tenDayDu) ? [] : dataSearch}
                    data={this.resultSearch}
                    defaultValue={query}
                    listStyle={{
                      width: WIDTH(331),
                      marginLeft: 0,
                      // marginTop: 5,
                      borderColor: R.colors.black0,
                      borderBottomLeftRadius: WIDTH(8),
                      borderBottomRightRadius: WIDTH(8),
                      maxHeight: HEIGHT(230)
                    }}

                    onChangeText={text => { this._changeName(text) }}
                    placeholder="Nhập ID, mã số, tên chuyên gia hoặc CSLK"
                    renderItem={({ item }) => (
                      <TouchableOpacity
                        style={{ paddingVertical: HEIGHT(5), paddingHorizontal: WIDTH(10) }}
                        onPress={() => {
                          this.props.onSearch(`${_.get(item, 'maTaiKhoan', 'ID hoặc số điện thoại người giới thiệu (nếu có)')}`, item || {})
                          this.setState({ query: _.has(item, 'maTaiKhoan') && item.maTaiKhoan })
                          this.setModalVisible(false)
                        }}
                      >
                        <Text style={styles.itemText}>
                          {`${`${_.get(item, 'tenDayDu', '')} - ${_.get(item, 'maTaiKhoan', '')}`}`}
                        </Text>
                      </TouchableOpacity>
                    )}
                  />
                  {this.state.query.length !== 0
                    && (
                      <TouchableOpacity
                        hitSlop={styles.hit}
                        onPress={() => { this._changeName('') }}
                        style={styles.iconDelText}
                      >
                        <AntDesign name="close" size={WIDTH(18)} color={R.colors.black0} />
                      </TouchableOpacity>
                    )
                  }
                  {this.state.query.length !== 0 && !this.state.loading && this.resultSearch.length === 0
                    && (
                      <View
                        style={styles.iconLoading}
                      >
                        <Text>Trống</Text>
                      </View>
                    )
                  }
                  {
                    this.state.query.length !== 0 && this.state.loading && (
                      <View
                        style={styles.iconLoading}
                      >
                        <ActivityIndicator
                          color="#000"
                          size="small"
                        />
                      </View>
                    )
                  }
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}

export default ModalSearch;

const styles = StyleSheet.create({
  opacity: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#rgba(0,0,0,0.7)'
  },
  modal: {
    backgroundColor: R.colors.white100,
    width: WIDTH(355),
    borderRadius: WIDTH(10),
    height: HEIGHT(400),
    paddingTop: HEIGHT(16),
    paddingBottom: HEIGHT(14),
    alignItems: 'center',
    paddingHorizontal: WIDTH(12)
  },
  body: {
    width: WIDTH(331)
  },
  title: {
    color: R.colors.black0,
    fontSize: getFont(16),
    fontWeight: 'bold',
    marginBottom: HEIGHT(8),
    marginTop: HEIGHT(12),
    alignSelf: 'center',
  },
  container: {
    marginTop: HEIGHT(6),
    flex: 1,
  },
  autocompleteContainer: {
    zIndex: 1,
    width: WIDTH(331),
  },
  itemText: {
    fontSize: 15,
    margin: 2,
    color: R.colors.black0
  },
  inputBox: {
    width: WIDTH(331),
    paddingVertical: HEIGHT(2),
    borderRadius: HEIGHT(8),
    minHeight: HEIGHT(52),
    paddingHorizontal: WIDTH(14),
    paddingRight: WIDTH(35),
    borderWidth: 1,
    borderColor: R.colors.black0,
    justifyContent: 'center'
  },

  iconDelText: {
    position: 'absolute',
    top: HEIGHT(3),
    right: WIDTH(8),
    zIndex: 1,
    height: HEIGHT(48),
    width: WIDTH(30),
    justifyContent: 'center',
    alignItems: 'center'
  },
  iconLoading: {
    position: 'absolute',
    top: HEIGHT(60),
    right: WIDTH(140),
    zIndex: 1,
    height: HEIGHT(48),
    width: WIDTH(50),
    justifyContent: 'center',
    alignItems: 'center'
  },
  hit: {
    top: 20,
    bottom: 20,
    left: 20,
    right: 20
  }
});
