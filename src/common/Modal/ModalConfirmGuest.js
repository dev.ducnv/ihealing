import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import BottomModal, { ModalContent, SlideAnimation } from 'react-native-modals';
import Icon from 'react-native-vector-icons/Ionicons';
import NavigationService from 'routers/NavigationService';
import { TRANG_THAI_LICH } from '../Item/ItemChiTietLich';
import BaseButton from '../Button/BaseButton';
import { WIDTH, getLineHeight, getFont, HEIGHT } from '../../config';
import R from '../../assets/R'

const ModalConfirmGuest = (props) => {
  const { onShowModal, visible, onButton } = props;
  return (
    <BottomModal
      modalAnimation={new SlideAnimation({
        initialValue: 0,
        slideFrom: 'bottom',
        useNativeDriver: true,
      })}
      visible={visible}
      onTouchOutside={() => {
        onShowModal();
      }}
      onHardwareBackPress={() => {
        onShowModal();
      }}
    >
      <ModalContent>
        <View style={styles.container}>
          <Icon
            name="ios-checkmark-circle"
            size={WIDTH(92)}
            color={R.colors.blackChecked}
          />
          <Text style={styles.success}>Xác nhận hoàn thành</Text>
          <Text style={[styles.username, { color: R.colors.black0, marginTop: HEIGHT(16) }]}>Hãy xác nhận rằng nhân viên đã hoàn thành buổi làm việc. Bạn có thể đánh giá chất lượng buổi làm việc sau khi hệ thống kiểm tra lại các thông tin và cập nhật tại lịch sử đơn hàng!</Text>

          <View style={styles.flexRow}>
            <BaseButton
              title="Xác nhận"
              width={WIDTH(186)}
              height={HEIGHT(48)}
              onButton={() => {
                setTimeout(() => {
                  onButton && onButton(TRANG_THAI_LICH.HOAN_THANH_LICH)
                }, 200)
              }}
            />
          </View>
        </View>
      </ModalContent>
    </BottomModal>
  )
}

export default ModalConfirmGuest;

const styles = StyleSheet.create({
  container: {
    width: WIDTH(320),
    height: HEIGHT(450),
    paddingHorizontal: WIDTH(8),
    paddingVertical: HEIGHT(28),
    alignItems: 'center',
    justifyContent: 'center'
  },
  success: {
    fontWeight: '500',
    fontSize: getFont(22),
    lineHeight: getLineHeight(30),
    color: R.colors.black0,
    textAlign: 'justify'
  },
  username: {
    fontWeight: '500',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.blackChecked,
    textAlign: 'center'
  },
  flexRow: {
    flexGrow: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: HEIGHT(28)
  },
  btnExit: {
    width: WIDTH(108),
    height: HEIGHT(48),
    borderRadius: WIDTH(100),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: R.colors.darkBlue,
    marginRight: WIDTH(17)
  }
})
