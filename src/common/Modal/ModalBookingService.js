import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import BottomModal, { ModalContent, SlideAnimation } from 'react-native-modals';
import Icon from 'react-native-vector-icons/Ionicons';
import NavigationService from 'routers/NavigationService';
import moment from 'moment'
import BaseButton from '../Button/BaseButton';
import { WIDTH, getLineHeight, getFont, HEIGHT, formatVND } from '../../config';
import R from '../../assets/R'

const ModalBookingService = (props) => {
  const { onShowModal, visible, time, date, tenDichVu, diaChiLam, tenNhanVien } = props;
  const parseDate = date ? date?.split('/') : moment(new Date()).format('DD/MM/YYYY').split('/')
  const parseTime = time?.split('h');
  let BatDau = moment(new Date(parseDate?.[2], parseDate?.[1] - 1, parseDate?.[0], parseTime?.[0], parseTime?.[1], 0, 0)).format('YYYY-MM-DD HH:mm:ss')
  let KetThuc = moment(new Date(parseDate?.[2], parseDate?.[1] - 1, parseDate?.[0], parseInt(parseTime?.[0], 10) + 1, parseTime?.[1], 0, 0)).format('YYYY-MM-DD HH:mm:ss')
  //   let Ngay = moment(new Date(parseDate?.[2], parseDate?.[1] - 1, parseDate?.[0], parseTime?.[0], parseTime?.[1], 0, 0)).format('YYYY-MM-DD HH:mm:ss')
  return (
    <BottomModal
      modalAnimation={new SlideAnimation({
        initialValue: 0,
        slideFrom: 'bottom',
        useNativeDriver: true,
      })}
      visible={visible}
      onTouchOutside={() => {
        onShowModal();
      }}
      onHardwareBackPress={() => {
        onShowModal();
      }}
    >
      <ModalContent>
        <View style={styles.container}>
          <Icon
            name="ios-checkmark-circle"
            size={WIDTH(92)}
            color={R.colors.blackChecked}
          />
          <Text style={styles.success}>Đặt lịch thành công</Text>
          <Text style={[styles.username, { marginTop: HEIGHT(20) }]}>{`Tên dịch vụ: ${tenDichVu || 'Chưa xác định'}`}</Text>
          <Text style={[styles.username]}>{`Địa chỉ làm: ${diaChiLam || 'Chưa xác định'}`}</Text>
          <Text style={[styles.username]}>{`Tên chuyên gia: ${tenNhanVien || 'Chưa xác định'}`}</Text>
          <Text style={[styles.username]}>{`Thời gian: Từ ${moment(BatDau).format('HH:mm DD/MM/YYYY')} đến ${moment(KetThuc).format('HH:mm DD/MM/YYYY')}`}</Text>
          {/* <Text style={[styles.username, { marginBottom: HEIGHT(24), color: 'red', fontWeight: 'bold' }]}>{`Tổng tiền: ${formatVND(tongTien)}`}</Text> */}

          <View style={styles.flexRow}>
            <BaseButton
              title="Đồng ý"
              width={WIDTH(186)}
              height={HEIGHT(48)}
              onButton={() => {
                onShowModal();
              }}
            />
          </View>
        </View>
      </ModalContent>
    </BottomModal>
  )
}

export default ModalBookingService;

const styles = StyleSheet.create({
  container: {
    width: WIDTH(320),
    height: HEIGHT(450),
    paddingHorizontal: WIDTH(8),
    paddingVertical: HEIGHT(28),
    alignItems: 'center',
    justifyContent: 'center'
  },
  success: {
    fontWeight: '500',
    fontSize: getFont(22),
    lineHeight: getLineHeight(30),
    color: R.colors.black0,
    textAlign: 'justify'
  },
  username: {
    fontWeight: '500',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    paddingHorizontal: WIDTH(25),
    color: R.colors.blackChecked,
    textAlign: 'justify',
    alignSelf: 'flex-start'
  },
  flexRow: {
    flexGrow: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: HEIGHT(28)
  },
  btnExit: {
    width: WIDTH(108),
    height: HEIGHT(48),
    borderRadius: WIDTH(100),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: R.colors.darkBlue,
    marginRight: WIDTH(17)
  }
})
