import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import BottomModal, { ModalContent, SlideAnimation } from 'react-native-modals';
import Icon from 'react-native-vector-icons/Ionicons';
import { TRANG_THAI_LICH } from '../Item/ItemChiTietLich';
import BaseButton from '../Button/BaseButton';
import { WIDTH, getLineHeight, getFont, HEIGHT } from '../../config';
import R from '../../assets/R'


const ModalConfirmEmployee = (props) => {
  const { onShowModal, visible, onButton, type } = props;
  return (
    <BottomModal
      modalAnimation={new SlideAnimation({
        initialValue: 0,
        slideFrom: 'bottom',
        useNativeDriver: true,
      })}
      visible={visible}
      onTouchOutside={() => {
        onShowModal();
      }}
      onHardwareBackPress={() => {
        onShowModal();
      }}
    >
      <ModalContent>
        {type === TRANG_THAI_LICH.DANG_CHO
          ? (
            <View style={styles.container}>
              <Icon
                name="ios-checkmark-circle"
                size={WIDTH(92)}
                color={R.colors.blackChecked}
              />
              <Text style={styles.success}>Nhận lịch làm</Text>
              <Text style={[styles.username, { color: R.colors.black0, marginTop: HEIGHT(16) }]}>Hãy chắc chắn rằng bạn có thể tiếp nhận công việc với thời gian và địa điểm này!</Text>

              <View style={styles.flexRow}>
                <TouchableOpacity
                  style={styles.btnExit}
                  onPress={() => {
                    setTimeout(() => {
                      onButton(TRANG_THAI_LICH.HUY_LICH)
                    }, 200)
                  }}
                >
                  <Text style={[styles.username, { color: R.colors.yellow254 }]}>Từ chối lịch</Text>
                </TouchableOpacity>
                <BaseButton
                  title="Nhận lịch"
                  width={WIDTH(186)}
                  height={HEIGHT(48)}
                  onButton={() => {
                    setTimeout(() => {
                      onButton(TRANG_THAI_LICH.DA_NHAN_LICH)
                    }, 200)
                  }}
                />
              </View>
            </View>
          )
          : (
            <View style={styles.container}>
              <Icon
                name="ios-checkmark-circle"
                size={WIDTH(92)}
                color={R.colors.blackChecked}
              />
              <Text style={styles.success}>Xác nhận bắt đầu làm việc</Text>
              <Text style={[styles.username, { color: R.colors.black0, marginTop: HEIGHT(16) }]}>Hãy chắc chắn rằng bạn đã đến địa chỉ và bắt đầu làm việc!</Text>

              <View style={styles.flexRow}>
                <BaseButton
                  title="Xác nhận"
                  width={WIDTH(186)}
                  height={HEIGHT(48)}
                  onButton={() => { onButton(TRANG_THAI_LICH.DANG_LAM_LICH) }}
                />
              </View>
            </View>
          )
        }
      </ModalContent>
    </BottomModal>
  )
}

export default ModalConfirmEmployee;

const styles = StyleSheet.create({
  container: {
    width: WIDTH(320),
    height: HEIGHT(450),
    paddingHorizontal: WIDTH(8),
    paddingVertical: HEIGHT(28),
    alignItems: 'center',
    justifyContent: 'center'
  },
  pickerStyle: {
    color: R.colors.yellow24C,
    backgroundColor: 'black'
  },
  success: {
    fontWeight: 'bold',
    fontSize: getFont(24),
    lineHeight: getLineHeight(30),
    color: R.colors.black0,
    textAlign: 'justify',
    alignItems: 'center'
  },
  username: {
    fontWeight: '500',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.blackChecked,
    textAlign: 'center'
  },
  flexRow: {
    flexGrow: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: HEIGHT(10)
  },
  btnExit: {
    width: WIDTH(108),
    height: HEIGHT(48),
    borderRadius: WIDTH(100),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: R.colors.darkBlue,
    marginRight: WIDTH(17)
  },

  textItem: {
    color: R.colors.yellow254,
    fontSize: getFont(15),
    fontFamily: R.fonts.Roboto
  }
})
