import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import BottomModal, { ModalContent, SlideAnimation } from 'react-native-modals';
import Icon from 'react-native-vector-icons/Ionicons';
import NavigationService from 'routers/NavigationService';
import BaseButton from '../Button/BaseButton';
import { WIDTH, getLineHeight, getFont, HEIGHT, formatVND } from '../../config';
import R from '../../assets/R'

const dataThanhToan = ['Thanh toán khi nhận hàng (COD)', 'Chuyển khoản ATM nội địa', 'Thanh toán tiền mặt trực tiếp']

const ModalBoooking = (props) => {
  const { onShowModal, visible, maDon, tongTien, thanhToan, msg } = props;
  return (
    <BottomModal
      modalAnimation={new SlideAnimation({
        initialValue: 0,
        slideFrom: 'bottom',
        useNativeDriver: true,
      })}
      visible={visible}
      onTouchOutside={() => {
        onShowModal();
      }}
      onHardwareBackPress={() => {
        onShowModal();
      }}
    >
      <ModalContent>
        <View style={styles.container}>
          <Icon
            name="ios-checkmark-circle"
            size={WIDTH(92)}
            color={R.colors.blackChecked}
          />
          <Text style={styles.success}>Thông báo!</Text>
          {thanhToan ? (<Text style={[styles.username, { marginTop: HEIGHT(20) }]}>{`Mã đơn hàng: ${maDon}`}</Text>) : null}
          <Text style={[styles.username]}>{`Hình thức thanh toán: ${dataThanhToan[thanhToan]}`}</Text>
          <Text style={[styles.username, { marginBottom: HEIGHT(24), color: 'red', fontWeight: 'bold' }]}>{`Tổng tiền: ${formatVND(tongTien)}`}</Text>
          <Text style={[styles.username, { color: R.colors.black0 }]}>{msg || 'Bạn có thể theo dõi đơn hàng của mình tại lịch sử đơn hàng!'}</Text>

          <View style={styles.flexRow}>
            <BaseButton
              title="Về trang chủ"
              width={WIDTH(186)}
              height={HEIGHT(48)}
              onButton={() => {
                NavigationService.navigate('Home');
                onShowModal();
              }}
            />
          </View>
        </View>
      </ModalContent>
    </BottomModal>
  )
}

export default ModalBoooking;

const styles = StyleSheet.create({
  container: {
    width: WIDTH(320),
    height: HEIGHT(450),
    paddingHorizontal: WIDTH(8),
    paddingVertical: HEIGHT(28),
    alignItems: 'center',
    justifyContent: 'center'
  },
  success: {
    fontWeight: '500',
    fontSize: getFont(22),
    lineHeight: getLineHeight(30),
    color: R.colors.black0,
    textAlign: 'justify'
  },
  username: {
    fontWeight: '500',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    paddingHorizontal: WIDTH(25),
    color: R.colors.blackChecked,
    textAlign: 'justify',
    alignSelf: 'flex-start'
  },
  flexRow: {
    flexGrow: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: HEIGHT(28)
  },
  btnExit: {
    width: WIDTH(108),
    height: HEIGHT(48),
    borderRadius: WIDTH(100),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: R.colors.darkBlue,
    marginRight: WIDTH(17)
  }
})
