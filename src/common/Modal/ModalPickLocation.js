/* eslint-disable camelcase */
/* eslint-disable import/no-unresolved */
import React, { Component } from 'react';
import { View, Text, Modal, StyleSheet, TouchableWithoutFeedback, ActivityIndicator } from 'react-native';
import PickerItem from 'common/Picker/PickerItemDark';
import { getTinh, getHuyen, getXa } from 'apis';
import { getFont, HEIGHT, popupOk, WIDTH } from '../../config/Function';
import R from '../../assets/R';
import { TextInput } from 'react-native';
import BaseButton from 'common/Button/BaseButton';
import { TouchableOpacity } from 'react-native';
import url from 'apis/url';


class ModalPickLocation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      miniLoading: false,
      reRender: false,
      ready: false,
      diachiChiTiet: ''
    };
    this.idTinh = []
    this.dataTinh = ['Đang tải']
    this.tinh = -1
    this.idHuyen = []
    this.dataHuyen = ['Đang tải']
    this.huyen = -1
    this.idXa = []
    this.dataXa = ['Đang tải']
    this.xa = -1
  }

  _reRender = () => this.setState({ reRender: !this.state.reRender })

  componentDidMount = () => {
    this.getMetaData()
  }

  setModalVisible = (visible) => {
    this.setState({
      modalVisible: visible
    })
  }

  getMetaData = async () => {
    let dataTinh = await getTinh(url.GET_TINH_DAT_LICH)
    this.dataTinh = []
    if (dataTinh.data) {
      dataTinh.data.map((item, index) => this.dataTinh.push(item.tenTinh))
      this.idTinh = dataTinh.data
      this._reRender()
    }
  }

  getMetaHuyen = async (index) => {
    this.setState({ miniLoading: true })
    let parram = `?IdTinh=${this.idTinh[index].iD_Tinh}`
    let dataHuyen = await getHuyen(parram, url.GET_HUYEN_DAT_LICH)
    this.dataHuyen = []
    if (dataHuyen.data) {
      dataHuyen.data.map((item, index) => this.dataHuyen.push(item.tenQuan))
      this.idHuyen = dataHuyen.data
    }
    this.setState({ miniLoading: false })
  }


  getMetaXa = async (index) => {
    this.setState({ miniLoading: true })
    let parram = `?IdQuan=${this.idHuyen[index].iD_Quan}`
    let dataXa = await getXa(parram, url.GET_XA_DAT_LICH)
    this.dataXa = []
    if (dataXa.data) {
      dataXa.data.map((item, index) => this.dataXa.push(item.tenPhuong))
      this.idXa = dataXa.data
    }
    this.setState({ miniLoading: false })
  }

  onButtonHoanThanh = () => {
    if (this.tinh < 0 || this.huyen < 0 || this.xa < 0 || !this.state.diachiChiTiet) {
      popupOk('Thông báo', 'Vui lòng nhập đầy đủ địa chỉ')
    } else {
      const ID_Tinh = this.idTinh?.[this.tinh]?.iD_Tinh || -1
      const ID_Quan = this.idHuyen?.[this.huyen]?.iD_Quan || -1
      const ID_Phuong = this.idXa?.[this.xa]?.iD_Phuong || -1
      const DuongPho = this.state.diachiChiTiet
      const DiaChi = `${this.state.diachiChiTiet}, ${this.dataXa[this.xa]}, ${this.dataHuyen[this.huyen]}, ${this.dataTinh[this.tinh]}`
      const dataDiachiDaydu = {
        ID_Tinh,
        ID_Quan,
        ID_Phuong,
        DuongPho,
        DiaChi
      }
      console.log('dataDiachiDaydu', dataDiachiDaydu)
      this.props.onSearch(dataDiachiDaydu)
      this.setModalVisible(false)
    }
  }

  render() {
    const { title } = this.props
    return (
      <Modal
        animationType="slide"
        transparent={true}
        onRequestClose={() => this.setModalVisible(false)}
        visible={this.state.modalVisible}
      >
        <TouchableWithoutFeedback
          onPress={() => { this.setModalVisible(false) }}
        >
          <View style={styles.opacity}>
            <TouchableWithoutFeedback>
              <View style={styles.modal}>
                <Text style={styles.title}>{title}</Text>
                <View style={styles.wrapper}>
                  <PickerItem
                    placeholder="Chọn tỉnh/TP"
                    width={WIDTH(270)}
                    data={this.dataTinh}
                    onChangeValue={(index, item) => { this.tinh = index; this.getMetaHuyen(index) }}
                  />
                </View>
                {this.tinh > -1
                  && (
                    <View style={styles.wrapper}>
                      <PickerItem
                        placeholder="Chọn quận/huyện"
                        width={WIDTH(270)}
                        data={this.dataHuyen}
                        onChangeValue={(index, item) => { this.huyen = index; this.getMetaXa(index) }}
                      />
                    </View>
                  )
                }
                {this.huyen > -1
                  && (
                    <View style={styles.wrapper}>
                      <PickerItem
                        placeholder="Chọn xã/phường/TT"
                        width={WIDTH(270)}
                        data={this.dataXa}
                        onChangeValue={(index, item) => { this.xa = index; this.setState({ ready: true }) }}
                      />
                    </View>
                  )}
                {this.state.ready && (
                  <View>
                    <TextInput
                      style={styles.textInput}
                      placeholder="Nhập số nhà, tên đường, phố"
                      value={this.state.diachiChiTiet}
                      onChangeText={(text) => { this.setState({ diachiChiTiet: text }) }}
                    />
                    <TouchableOpacity style={{ marginTop: HEIGHT(30) }} onPress={this.onButtonHoanThanh}>
                      <Text style={{ fontSize: 16, fontFamily: R.fonts.RobotoMedium, textDecorationLine: 'underline' }}>Hoàn thành</Text>
                    </TouchableOpacity>
                  </View>

                )}
                {this.state.miniLoading
                  && <ActivityIndicator
                    color={R.colors.black0}
                    animating
                    size="small"
                  />
                }
              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}

export default ModalPickLocation;

const styles = StyleSheet.create({
  textInput: {
    padding: 0,
    margin: 0,
    width: WIDTH(270),
    alignItems: 'center',
    alignSelf: 'center',
    borderBottomWidth: 2,
    borderBottomColor: R.colors.black0,
    height: HEIGHT(50)
  },
  opacity: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#rgba(0,0,0,0.7)'
  },
  modal: {
    backgroundColor: R.colors.white100,
    width: WIDTH(355),
    borderRadius: WIDTH(10),
    height: HEIGHT(400),
    paddingTop: HEIGHT(16),
    paddingBottom: HEIGHT(14),
    alignItems: 'center',
    paddingHorizontal: WIDTH(12)
  },
  body: {
    width: WIDTH(331)
  },
  title: {
    color: R.colors.black0,
    fontSize: getFont(16),
    fontWeight: 'bold',
    marginBottom: HEIGHT(8),
    marginTop: HEIGHT(12),
    alignSelf: 'center',
  },
  container: {
    marginTop: HEIGHT(6),
    flex: 1,
  },
  autocompleteContainer: {
    zIndex: 1,
    width: WIDTH(331),
  },
  itemText: {
    fontSize: 15,
    margin: 2,
    color: R.colors.black0
  },
  inputBox: {
    width: WIDTH(331),
    paddingVertical: HEIGHT(2),
    borderRadius: HEIGHT(8),
    minHeight: HEIGHT(52),
    paddingHorizontal: WIDTH(14),
    paddingRight: WIDTH(35),
    borderWidth: 1,
    borderColor: R.colors.black0,
    justifyContent: 'center'
  },

  iconDelText: {
    position: 'absolute',
    top: HEIGHT(3),
    right: WIDTH(8),
    zIndex: 1,
    height: HEIGHT(48),
    width: WIDTH(30),
    justifyContent: 'center',
    alignItems: 'center'
  },
  iconLoading: {
    position: 'absolute',
    top: HEIGHT(60),
    right: WIDTH(140),
    zIndex: 1,
    height: HEIGHT(48),
    width: WIDTH(50),
    justifyContent: 'center',
    alignItems: 'center'
  }
});
