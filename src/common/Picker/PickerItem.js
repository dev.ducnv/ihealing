import React from 'react';
import {
  View,
  StyleSheet,
  Text
} from 'react-native';
import { Picker } from 'native-base';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/Feather';
import FastImage from 'react-native-fast-image';
import FlatListPicker from 'react-native-flatlist-picker';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { HEIGHT, WIDTH, getFont } from '../../config/Function';
import R from '../../assets/R';

export default class PickerItem extends React.Component {
  state = {
    value: 0,
  }

  converData = (data) => {
    let arr = [];
    data.map((item, index) => {
      arr.push({ value: item, key: index },)
    })
    return arr
  }

  render() {
    const { width, data, isFilter, value, height, isHideBorder, sourceImage, placeholder, heightContainer } = this.props
    // console.log('data_data', data, this.converData(data))
    return (
      <View style={[styles.inputBox, width && { width }, isHideBorder && { borderWidth: 0 }]}>
        {sourceImage && (
          <FastImage
            style={styles.icon}
            source={sourceImage}
            resizeMode={FastImage.resizeMode.contain}
          />
        )}
        <FlatListPicker
          ref={ref => { this.FlatListPicker = ref }}
          animationIn="slideInDown"
          data={this.converData(data)}
          containerStyle={{
            borderWidth: 0,
            borderColor: R.colors.yellow37c,
            borderRadius: WIDTH(4),
            width: width || WIDTH(150),
            height: height || HEIGHT(50),
            alignSelf: 'center',
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: 'row',
            paddingVertical: HEIGHT(8),
            // paddingHorizontal: WIDTH(11)
          }}
          dropdownStyle={{ backgroundColor: R.colors.black3, height: heightContainer }}
          renderItem={({ item, index }) => (
            <Text style={[styles.textItem, { padding: WIDTH(12) }, { width: width - WIDTH(35) || WIDTH(115) }]}>{item.value}</Text>
          )}
          dropdownTextStyle={styles.textItem}
          // placeholder={placeholder}
          pickedTextStyle={[styles.textItem, { width: width - WIDTH(10) || WIDTH(115) }]}
          animated="none"
          onSelect={(item, index) => {
            // console.log('vv_vv', item, index, value)
            this.setState({ value: item })
            this.props.onChangeValue(item, index)
          }}
          defaultValue={placeholder}
          renderDropdownIcon={() => (<Icon name="chevron-down" color={R.colors.yellow254} size={15} />)}
        />
        {/* <Picker
            mode="dropdown"
            placeholder={placeholder}
            iosHeader="Chọn"
            headerBackButtonText="Quay lại"
            selectedValue={value || this.state.value}
            style={[styles.pickerStyle, { width: width || WIDTH(260), height: height || HEIGHT(48), color: colorText || R.colors.yellow254 }]}
            onValueChange={(itemValue, itemIndex) => {
              if (!_.isUndefined(isFilter)) {
                isFilter(itemIndex)
              }
              this.props.onChangeValue(itemIndex)
              this.setState({ value: itemIndex })
            }}
          >
            {
              data.map((item, index) => (<Picker.Item key={index} label={item} value={index} />))
            }
          </Picker> */}
        {/* <View style={{ marginRight: WIDTH(0) }}>
            <Icon size={HEIGHT(20)} color={R.colors.yellow254} name="chevron-down" />
          </View> */}
      </View>
    )
  }
}
const styles = StyleSheet.create({
  inputBox: {
    width: WIDTH(327),
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row'
  },
  icon: {
    width: WIDTH(20),
    height: HEIGHT(15),
    marginRight: WIDTH(10)
  },
  pickerStyle: {
    color: R.colors.yellow24C,
    backgroundColor: 'transparent'
  },
  textItem: {
    color: R.colors.yellow254,
    fontSize: getFont(15),
    fontFamily: R.fonts.Roboto
  }
})
