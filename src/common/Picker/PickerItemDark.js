import React from 'react';
import {
  View,
  StyleSheet,
  Text
} from 'react-native';
import { Picker } from 'native-base';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/Feather';
import FastImage from 'react-native-fast-image';
import FlatListPicker from 'react-native-flatlist-picker';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { HEIGHT, WIDTH, getFont } from '../../config/Function';
import R from '../../assets/R';

export default class PickerItem extends React.Component {
  state = {
    value: 0,
  }

  converData = (data) => {
    let arr = [];
    data.map((item, index) => {
      arr.push({ value: item, key: index },)
    })
    return arr
  }

  render() {
    const { width, data, isFilter, value, height, isHideBorder, sourceImage, placeholder, heightContainer } = this.props
    // console.log('data_data', data, this.converData(data))
    return (
      <View style={[styles.inputBox, width && { width }, isHideBorder && { borderWidth: 0 }]}>
        {sourceImage && (
          <FastImage
            style={styles.icon}
            source={sourceImage}
            resizeMode={FastImage.resizeMode.contain}
          />
        )}
        <FlatListPicker
          ref={ref => { this.FlatListPicker = ref }}
          animationIn="slideInDown"
          data={this.converData(data)}
          containerStyle={{
            borderWidth: 0,
            borderColor: R.colors.black0,
            borderRadius: WIDTH(4),
            width: width || WIDTH(150),
            height: height || HEIGHT(50),
            alignSelf: 'center',
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: 'row',
            paddingVertical: HEIGHT(8),
            // paddingHorizontal: WIDTH(11)
          }}
          dropdownStyle={{ backgroundColor: R.colors.whiteF2, height: heightContainer }}
          renderItem={({ item, index }) => (
            <Text style={[styles.textItem, { padding: WIDTH(12) }, { width: width - WIDTH(35) || WIDTH(115) }]}>{item.value}</Text>
          )}
          dropdownTextStyle={styles.textItem}
          // placeholder={placeholder}
          pickedTextStyle={[styles.textItem, { width: width - WIDTH(10) || WIDTH(115) }]}
          animated="none"
          onSelect={(item, index) => {
            // console.log('vv_vv', item, index, value)
            this.setState({ value: item })
            this.props.onChangeValue(item, index)
          }}
          defaultValue={placeholder}
          renderDropdownIcon={() => (<Icon name="chevron-down" color={R.colors.black0} size={15} />)}
        />
      </View>
    )
  }
}
const styles = StyleSheet.create({
  inputBox: {
    width: WIDTH(327),
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 2,
    borderBottomColor: R.colors.black0
  },
  icon: {
    width: WIDTH(20),
    height: HEIGHT(15),
    marginRight: WIDTH(10)
  },
  pickerStyle: {
    color: R.colors.black0,
    backgroundColor: 'transparent'
  },
  textItem: {
    color: R.colors.black0,
    fontSize: getFont(15),
    fontFamily: R.fonts.Roboto
  }
})
