// @flow
import React from 'react';
import Feather from 'react-native-vector-icons/Feather'
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import { StyleSheet } from 'react-native';
import { HEIGHT, WIDTH, getFont } from '../../config';
import R from '../../assets/R';

class DatePickerWithImage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      date: (this.props.defaultDate && new Date(this.props.defaultDate)) || new Date()
    }
  }

  render() {
    const { onChangeDate, mode, format, width, minDate, height } = this.props
    return (
      <DatePicker
        locale="vi"
        style={[styles.pickerStyle, { width: width || WIDTH(110), height: height || HEIGHT(48) }]}
        date={this.state.date}
        mode={mode || 'date'}
        placeholder="select date"
        format={format || 'DD/MM/YYYY'}
        git
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        iconComponent={
          <Feather size={15} color={R.colors.yellow254} name="chevron-down" />
        }
        customStyles={{
          dateInput: {
            marginLeft: WIDTH(0),
            borderWidth: 0,
            alignItems: 'flex-start'
          },
          dateText: {
            ...styles.textInfor,
            marginLeft: 0
          }
        }}
        onDateChange={(time) => {
          onChangeDate && onChangeDate(moment(time, 'DD/MM/YYYY HH:mm').toISOString());
          this.setState({ date: time })
        }}
        minDate={minDate}
      />
    )
  }
}

export default DatePickerWithImage;
const styles = StyleSheet.create({
  textInfor: {
    color: R.colors.yellow254,
    fontFamily: R.fonts.Roboto,
    lineHeight: HEIGHT(24),
    fontSize: getFont(17)
  },

  line: {
    backgroundColor: R.colors.backgroundColorMesseage,
    height: HEIGHT(8),
    width: WIDTH(360),
    marginTop: HEIGHT(24)
  },
  datePicker: {
    paddingHorizontal: 0,
    borderWidth: 0,
    width: WIDTH(70),
  },
  dateInput: {
    marginLeft: WIDTH(0),
    borderWidth: 0
  },
  flexRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  }
});
