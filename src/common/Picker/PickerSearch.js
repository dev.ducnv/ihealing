/* eslint-disable react/jsx-closing-tag-location */

import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign'
import _ from 'lodash';
import { WIDTH, HEIGHT, getFont } from '../../config/Function';
import ModalSearch from '../Modal/ModalSearch';
import R from '../../assets/R';

class PickerSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: _.has(this.props.value, 'tenDayDu') ? this.props.value : {}
    };
  }

  render() {
    const { value } = this.state;
    const { width, onCheck } = this.props;
    const isNguoiGTExists = _.has(value, 'tenDayDu');

    return (
      <View style={[styles.container, { width: width || WIDTH(270) }]}>
        <TouchableOpacity
          style={[styles.inputBox, { width: width || WIDTH(270) }]}
          onPress={() => { this.ModalSearch.setModalVisible(true) }}
        >
          <Text style={{ color: isNguoiGTExists ? R.colors.yellow254 : R.colors.yellow37c, fontSize: getFont(16) }}>{isNguoiGTExists ? value.maTaiKhoan : 'ID hoặc số điện thoại người giới thiệu (nếu có)'}</Text>
          {isNguoiGTExists
          && <TouchableOpacity
            style={styles.check}
            hitSlop={styles.hitSlop}
            onPress={() => {
              onCheck();
              this.setState({ value: {} })
            }}
          >
            <AntDesign name="check" size={WIDTH(18)} color={R.colors.yellow254} />
          </TouchableOpacity>}
        </TouchableOpacity>
        <ModalSearch
          ref={ref => { this.ModalSearch = ref }}
          data={this.props.data}
          type={this.props.type}
          onSearch={(value, code) => {
            this.setState({ value: { ...code } })
            this.props.onSearch(code)
          }}
          title={this.props.title}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemText: {
    fontSize: 15,
    margin: 2
  },
  inputBox: {
    paddingVertical: HEIGHT(2),
    height: HEIGHT(52),
    paddingHorizontal: WIDTH(14),
    paddingRight: WIDTH(30),
    justifyContent: 'space-between',
    color: '#000000',
    flexDirection: 'row',
    alignItems: 'center'
  },
  infoText: {
    textAlign: 'center'
  },
  directorText: {
    color: 'grey',
    fontSize: 12,
    marginBottom: 10,
    textAlign: 'center'
  },
  openingText: {
    textAlign: 'center'
  },
  check: {
    position: 'absolute',
    top: HEIGHT(14),
    right: 0
  },
  hitSlop: {
    top: 10,
    left: 10,
    right: 10,
    bottom: 10,
  }
});

export default PickerSearch;
