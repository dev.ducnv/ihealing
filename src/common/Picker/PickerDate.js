// @flow
import React from 'react';
import Feather from 'react-native-vector-icons/Feather'
import DatePicker from 'react-native-datepicker';
import { StyleSheet } from 'react-native'
import { HEIGHT, WIDTH, getFont } from '../../config';
import R from '../../assets/R';

const renderDatePicker = (props) => {
  const { date, onChangeDate, mode, format, width, minDate } = props
  const parseDate = date ? date.split('/') : null
  return (
    <DatePicker
      locale="vi"
      style={[styles.pickerStyle, { width: width || WIDTH(110) }]}
      date={date ? new Date(parseDate[2], parseDate[1] - 1, parseDate[0]) : new Date()}
      mode={mode || 'date'}
      placeholder="select date"
      format={format || 'DD/MM/YYYY'}
      git
      confirmBtnText="Confirm"
      cancelBtnText="Cancel"
      iconComponent={
        <Feather size={HEIGHT(20)} color={R.colors.yellow254} name="chevron-down" />
      }
      customStyles={{
        dateInput: {
          marginLeft: WIDTH(0),
          borderWidth: 0
        },
        dateText: {
          ...styles.textInfor,
          marginLeft: 0
        }
      }}
      onDateChange={(time) => {
        onChangeDate && onChangeDate(time)
      }}
      minDate={minDate}
    />
  )
}

export default renderDatePicker;
const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: R.colors.colorWhite,
    paddingBottom: HEIGHT(15)
  },

  textInfor: {
    color: R.colors.yellow254,
    fontFamily: R.fonts.Roboto,
    fontWeight: 'bold',
    lineHeight: HEIGHT(24),
    fontSize: getFont(17)
  },

  line: {
    backgroundColor: R.colors.backgroundColorMesseage,
    height: HEIGHT(8),
    width: WIDTH(360),
    marginTop: HEIGHT(24)
  },

  datePicker: {
    paddingHorizontal: 0,
    borderWidth: 0,
    width: WIDTH(70),
  },
  dateInput: {
    marginLeft: WIDTH(0),
    borderWidth: 0
  },


});
