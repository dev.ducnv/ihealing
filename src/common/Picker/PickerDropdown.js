import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { TouchableOpacity } from 'react-native-gesture-handler';
import FlatListPicker from 'react-native-flatlist-picker';
import R from 'assets/R';
import { WIDTH, HEIGHT, getFont } from '../../config';

export default class PickerDropdown extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount = () => {
    // To show or hide your picker component
    // this.PickerFlatlist.showPickerList()
  }

  render() {
    const { width, height, placeholder, onValueChange, data } = this.props
    return (
      <FlatListPicker
        ref={ref => { this.FlatListPicker = ref }}
        animationIn="slideInDown"
        data={this.props.data}
        containerStyle={{
          borderWidth: 0.5,
          borderColor: R.colors.yellow37c,
          borderRadius: WIDTH(4),
          width: width || WIDTH(150),
          height: height || HEIGHT(50),
          alignSelf: 'center',
          justifyContent: 'space-between',
          alignItems: 'center',
          flexDirection: 'row',
          paddingVertical: HEIGHT(8),
          paddingHorizontal: WIDTH(11)
        }}
        dropdownStyle={{ backgroundColor: R.colors.black3, height: data.length * HEIGHT(50) }}
        renderItem={({ item, index }) => (
          <Text style={[styles.textItem, { padding: WIDTH(12) }]}>{item.value}</Text>
        )}
        dropdownTextStyle={styles.textItem}
        pickedTextStyle={[styles.textItem, { width: width - WIDTH(35) || WIDTH(115) }]}
        animated="none"
        defaultValue={placeholder}
        onValueChange={onValueChange && onValueChange}
        // defaultIndex={1}
        renderDropdownIcon={() => (<AntDesign name="caretdown" color={R.colors.yellow254} size={15} />)}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    borderWidth: 0.5,
    borderColor: R.colors.yellow37c,
    borderRadius: WIDTH(4),
    alignSelf: 'center',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: HEIGHT(8),
    paddingHorizontal: WIDTH(11)
  },
  textItem: {
    color: R.colors.yellow254,
    fontSize: getFont(15),
    fontFamily: R.fonts.Roboto
  }
});
