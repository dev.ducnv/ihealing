import React, { PureComponent } from 'react';
import { StyleSheet, View, FlatList, TextInput, Text } from 'react-native';
import FastImage from 'react-native-fast-image';
import { HEIGHT, getFont, getLineHeight, WIDTH } from '../../config';
import R from '../../assets/R';
import PickerItem from '../Picker/PickerItem';
import PickerDate from '../Picker/PickerDateWithImage';
import PickerSearch from '../Picker/PickerSearch';

class FormInput extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {}
  }

  renderItem = ({ item, index }) => {
    if (item.hidden) return null
    if (item.isGender) {
      return (
        <View style={styles.wrapper}>
          <PickerItem
            sourceImage={R.images.ic_gender}
            placeholder="Giới tính"
            width={WIDTH(270)}
            data={['Nam', 'Nữ', 'Khác']}
            onChangeValue={item.func}
            value={item.value}
          />
        </View>
      )
    } else if (item.isPayment) {
      return (
        <View style={styles.wrapper}>
          <PickerItem
            sourceImage={item.image}
            placeholder="Thanh toán khi nhận hàng (COD)"
            width={WIDTH(270)}
            data={['Thanh toán khi nhận hàng (COD)', 'Chuyển khoản ATM nội địa', 'Thanh toán tiền mặt trực tiếp']}
            onChangeValue={item.func}
            value={item.value}
          />
        </View>
      )
    } else if (item.isPickDate) {
      return (
        <View style={[styles.wrapper, { paddingRight: WIDTH(5) }]}>
          <FastImage
            style={styles.icon}
            source={R.images.ic_birthday}
            resizeMode={FastImage.resizeMode.contain}
          />
          <PickerDate
            defaultDate={item.value}
            onChangeDate={(date) => item.func && item.func(date)}
            width={WIDTH(275)}
          />
        </View>
      )
    } else if (item.isSearch) {
      return (
        <View style={styles.wrapper}>
          <FastImage
            style={styles.icon}
            source={R.images.ic_person_id}
            resizeMode={FastImage.resizeMode.contain}
          />
          <PickerSearch
            width={WIDTH(300)}
            title="ID hoặc Số điện thoại người giới thiệu"
            onSearch={(body) => item.func && item.func(body)}
            checkRequire={this.state.checked}
            type="nguoiGioiThieu"
            data={this.props.dataSearch}
            upDateData={(key) => this.loadData(key)}
            onCheck={() => item.func && item.func({})}
            value={item.value}
          />
        </View>
      )
    }

    return (
      <View style={styles.wrapper}>
        <FastImage
          style={styles.icon}
          source={item.image}
          resizeMode={FastImage.resizeMode.contain}
        />
        {
            item.isRequire && (
              <Text style={styles.iconRequire}>*</Text>
            )}
        <TextInput
          secureTextEntry={item.isSecure}
          editable={!item.editable}
          placeholder={item.hint}
          autoCapitalize="none"
          style={styles.textInput}
          value={item.value}
          keyboardType={item.isKeyboadNum ? 'numeric' : 'default'}
          // autoCapitalize={item.isCharacters ? 'characters' : 'none'}
          placeholderTextColor={R.colors.yellow254}
          onChangeText={(text) => item.func && item.func(text)}
          defaultValue={item.value}
        />
        {
            item.doubleText
              ? <Text style={styles.text}>{item.title}</Text>
              : <View />
          }
      </View>
    )
  }

  renderProfileItem = ({ item, index }) => {
    const { onValueChange } = this.props
    if (item.isGender) {
      const dataGioitinh = ['Nam', 'Nữ', 'Khác']
      return (
        <View style={styles.wrapper}>
          <PickerItem
            sourceImage={R.images.ic_gender}
            width={WIDTH(270)}
            data={dataGioitinh}
            onChangeValue={(itemValue, itemIndex) => onValueChange && onValueChange(itemIndex, item.type)}
            // value={dataGioitinh[item.value]}
            placeholder={dataGioitinh[item.value]}
          />
        </View>
      )
    }
    return (
      <View style={styles.wrapper}>
        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
          <FastImage
            style={styles.icon}
            source={item.image}
            resizeMode={FastImage.resizeMode.contain}
          />
          <TextInput
            editable={item.editable}
            placeholder={item.hint}
            defaultValue={item.value.toString()}
            style={styles.textInput}
            keyboardType={item.isKeyboadNum ? 'numeric' : 'default'}
            onChangeText={(text) => onValueChange && onValueChange(text, item.type)}
            placeholderTextColor={R.colors.yellow24C}
          />
          {
            item.doubleText
              ? <Text style={styles.text}>{item.text}</Text>
              : <View />
          }
        </View>
      </View>
    )
  }

  render() {
    const { dataForm, type } = this.props;
    return (
      <FlatList
        data={dataForm}
        extraData={dataForm}
        renderItem={type === 'profile' ? this.renderProfileItem : this.renderItem}
        style={{ flexGrow: 0 }}
      />
    )
  }
}

export default FormInput;

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: HEIGHT(12),
    width: WIDTH(327),
    paddingLeft: WIDTH(10),
    borderBottomColor: R.colors.yellow254,
    borderBottomWidth: 1,
    alignSelf: 'center'
  },
  icon: {
    width: WIDTH(20),
    height: HEIGHT(15),
    marginBottom: HEIGHT(5)
  },
  textInput: {
    flex: 1,
    color: R.colors.yellow254,
    fontSize: getFont(15),
    lineHeight: getLineHeight(22),
    marginLeft: WIDTH(14),
    paddingBottom: HEIGHT(10),
    minHeight: HEIGHT(48)
    // paddingTop: Platform.OS === 'ios' ? 0 : HEIGHT(8)
  },
  info: {
    fontFamily: R.fonts.Roboto,
    fontWeight: '500',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.nameText,
    paddingVertical: HEIGHT(10),
    marginLeft: WIDTH(14)
  },
  text: {
    fontFamily: R.fonts.Roboto,
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.white,
    paddingVertical: HEIGHT(10),
  },
  iconRequire: {
    position: 'absolute',
    top: HEIGHT(10),
    color: R.colors.orange726,
    left: WIDTH(30),
    fontSize: getFont(16)
  }
})
