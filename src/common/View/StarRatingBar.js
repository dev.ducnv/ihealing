import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import FastImage from 'react-native-fast-image';
import Icon from 'react-native-vector-icons/AntDesign';
import { ratingById } from 'apis/Functions/users';
import BaseButton from '../Button/BaseButton';
import { WIDTH, getWidth, getLineHeight, getFont, HEIGHT, popupOk } from '../../config';
import R from '../../assets/R';


type Props = {
  styleContainer: Object
}

class StarRatingBar extends Component {
  constructor(props: Props) {
    super(props);
    this.state = {
      maxRating: 5,
      defaultRating: 2
    };
  }

  updateRating = (key) => {
    this.setState({ defaultRating: key })
  }


  postRating = async () => {
    this.props.setLoading(true)
    let querry = `?ID=${this.props.item.idKeHoach}&DanhGia=${this.state.defaultRating}`
    let resRaiting = await ratingById(querry)
    this.props.refresh()
    if (resRaiting.data && resRaiting.data.msg) {
      popupOk('Thông báo', resRaiting.data.msg)
    } else {
      popupOk('Thông báo', 'Đánh giá không thành công. Vui lòng kiểm tra kết nối')
      this.props.setLoading(false)
    }
  }

  render() {
    let bar = []
    for (let i = 1; i <= this.state.maxRating; i++) {
      bar.push(
        <TouchableOpacity
          activeOpacity={0.5}
          key={i}
          style={{ padding: 1, }}
          onPress={() => this.updateRating(i)}
        >
          {i <= this.state.defaultRating
            ? (<Icon size={WIDTH(32)} name="star" color={R.colors.yellow254} />)
            : (<Icon size={WIDTH(32)} name="staro" color={R.colors.yellow254} />)
          }
        </TouchableOpacity>
      )
    }
    return (

      <View style={{ paddingTop: HEIGHT(20) }}>
        <Text style={{
          fontSize: getFont(16),
          lineHeight: getLineHeight(24),
          fontWeight: '700',
          color: R.colors.white,
          alignSelf: 'center'
        }}
        >
          {this.props.item.tenDichVu}

        </Text>
        <View style={
          [{
            justifyContent: 'space-between',
            flexDirection: 'row',
            width: getWidth(),
            paddingHorizontal: WIDTH(45)
          }, this.props.styleContainer && this.props.styleContainer]}
        >
          {bar}
        </View>
        <BaseButton
          title="Đánh giá"
          width={WIDTH(140)}
          style={{ alignSelf: 'center', }}
          onButton={this.postRating}
        />
        <View style={{ backgroundColor: R.colors.yellow254, height: 1, width: WIDTH(300), alignSelf: 'center', marginTop: HEIGHT(20) }} />
      </View>
    );
  }
}

export default StarRatingBar;
