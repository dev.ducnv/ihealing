// @flow
import React from 'react'
import {
  Text, View, StyleSheet
} from 'react-native'

// import
import { getFont, getWidth, getLineHeight, WIDTH } from '../../config';
import R from '../../assets/R';

type Props = {
    text: string,
    yellowText: string,
    styleContainer: Object,
    styleText: Object,
}

const TextYellow = (props: Props) => {
  const { text, yellowText, styleContainer, styleText } = props;
  return (
    <View style={[styles.container, styleContainer && styleContainer]}>
      <Text style={[styles.text, styleText && styleText]}>
        {text}
        <Text style={[styles.text, { color: R.colors.yellow254 }]}>{` ${yellowText}`}</Text>
      </Text>
    </View>
  )
}

export default TextYellow;

const styles = StyleSheet.create({
  container: {
    width: getWidth(),
    alignItems: 'center',
    paddingHorizontal: WIDTH(16)
  },
  text: {
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.white,
    textAlign: 'center'
  }
})
