// @flow
import React from 'react'
import {
  View, StyleSheet
} from 'react-native'

// import
import Svg, { LinearGradient, Stop, Text } from 'react-native-svg';
import { getFont, getWidth, getLineHeight, WIDTH, HEIGHT } from '../../config';
import R from '../../assets/R';

type Props = {
  text: string,
  yellowText: string,
  styleContainer: Object,
  styleText: Object,
}

const TextGradient = (props: Props) => {
  const { text, fontSize, fontWeight, lineHeight, backgroundColor } = props;
  return (
    <View style={{ backgroundColor: R.colors.colorMain }}>
      <Svg
        height={lineHeight || 18}
        color={backgroundColor || R.colors.white}
      >
        <LinearGradient
          id="Gradient"
          x1="0%"
          y1="0%"
          x2="100%"
          y2="0%"
        >
          <Stop offset="0" stopColor={R.colors.yellow1c8} stopOpacity="1" />
          <Stop offset="0.5" stopColor={R.colors.yellow254} stopOpacity="1" />
          <Stop offset="1" stopColor={R.colors.yellow1c8} stopOpacity="1" />
        </LinearGradient>
        <Text
          fill="url(#Gradient)"
          // stroke="purple"
          // fill="#fff"
          // x="24"
          y="80%"
          fontFamily={R.fonts.TimesNewRomanBold}
          fontSize={fontSize || 14}
          fontWeight={fontWeight || 'bold'}
        >
          {text}
        </Text>
      </Svg>
    </View>
  )
}

export default TextGradient;

const styles = StyleSheet.create({
  container: {
    width: getWidth(),
    alignItems: 'center',
    paddingHorizontal: WIDTH(16)
  },
  text: {
    fontFamily: R.fonts.TimesNewRoman,
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.white,
    textAlign: 'center'
  }
})
