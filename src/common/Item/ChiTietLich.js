import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { LoadingComponent } from 'common/Loading/LoadingComponent';
import ItemChiTietLich from './ItemChiTietLich';
import ItemNull from './ItemNull';
import R from '../../assets/R';
import { getCalendarByDay } from '../../apis/Functions/users';

class ChiTietLich extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  renderItem = ({ item, index }) => (
    <ItemChiTietLich canChangeCelendar={this.props.canChangeCelendar} item={item} index={index} reloadFunc={this.props.reloadFunc} type={this.props.type} />
  )

  render() {
    console.log('this.props.data', this.props.data)
    if (!this.props.loading) {
      return (
        <FlatList
          data={this.props.data}
          renderItem={this.renderItem}
          ListEmptyComponent={<ItemNull text="Không có lịch" />}
        />
      );
    } else {
      return (<LoadingComponent isLoading={this.props.loading} />)
    }
  }
}

export default ChiTietLich;
