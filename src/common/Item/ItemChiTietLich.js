import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import _ from 'lodash'
import moment from 'moment'
import NavigationService from 'routers/NavigationService';
import { DoiLichChamSoc } from 'routers/screenNames';
import { connect } from 'react-redux';
import ModalConfirmGuest from 'common/Modal/ModalConfirmGuest';
import ModalConfirmEmployee from 'common/Modal/ModalConfirmEmployee';
import { LoadingComponent } from 'common/Loading/LoadingComponent';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { updateStateWorking } from 'apis';
import NumberOrderLich from './NumberOrderLich';
import { WIDTH, HEIGHT, getFont, getLineHeight, popupOk, formatVND } from '../../config';
import R from '../../assets/R';

export const TRANG_THAI_LICH = {
  DANG_CHO: 0,
  DA_NHAN_LICH: 1,
  DANG_LAM_LICH: 2,
  HOAN_THANH_LICH: 3,
  HUY_LICH: -1,
}


class ItemChiTietLich extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleGuest: false,
      visibleEmploy: false,
      trangThai: props.item.trangThai || 0,
      loading: false
    };
  }


  onConfirm = async (type) => {
    const { item } = this.props
    this.setState({ loading: true, visibleGuest: false, visibleEmploy: false })
    let querry = `xacnhanlich_tuchoi?ID_KeHoach=${item.idKeHoach}`

    switch (type) {
      case TRANG_THAI_LICH.DA_NHAN_LICH:
        querry = `xacnhanlich_nhanlich?ID_KeHoach=${item.idKeHoach}`
        break;
      case TRANG_THAI_LICH.DANG_LAM_LICH:
        querry = `xacnhanlich_batdaulam?ID_KeHoach=${item.idKeHoach}`
        break;
      case TRANG_THAI_LICH.HOAN_THANH_LICH:
        querry = `xacnhanlich_hoanthanh?ID_KeHoach=${item.idKeHoach}`
        break;
      default:
        break;
    }
    let resUpdate = await updateStateWorking(querry)
    // console.log(resUpdate)
    if (_.has(resUpdate, 'data.status')) {
      popupOk('Thông báo', 'Cập nhật lịch thành công')
      this.props.reloadFunc?.()
    } else {
      popupOk('Thông báo', 'Cập nhật không thành công, vui lòng kiểm tra kết nối')
    }
    this.setState({ loading: false, trangThai: type })
  }


  render() {
    const { index, item, reloadFunc, canChangeCelendar, Account } = this.props;
    let vaiTro = this.props.type === 'chuyengia' ? 'Tên khách' : 'Tên chuyên gia'
    return (
      <View style={[styles.container, item.text_color && { backgroundColor: item.text_color }]}>
        <NumberOrderLich index={index + 1} />
        {(canChangeCelendar && item.trangThai !== 3)
          && <ModalConfirmGuest
            onShowModal={() => this.setState({ visibleGuest: !this.state.visibleGuest })}
            visible={this.state.visibleGuest}
            onButton={this.onConfirm}
          />
        }
        {(!canChangeCelendar && item.trangThai !== 3)
          && <ModalConfirmEmployee
            onShowModal={() => this.setState({ visibleEmploy: !this.state.visibleEmploy })}
            visible={this.state.visibleEmploy}
            type={this.state.trangThai}
            onButton={this.onConfirm}
          />
        }
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: WIDTH(320) }}>
          <Text style={[styles.dateText, {
            width: (item.trangThai !== 3) ? (canChangeCelendar ? WIDTH(220) : WIDTH(180)) : WIDTH(180), paddingRight: WIDTH(5)
          }]}
          >
            {_.has(item, 'tenNhanVien') ? `${vaiTro}: ${this.props.type === 'chuyengia' ? item.tenKhachHang : item.tenNhanVien}` : 'Chưa xác định nhân viên'}
          </Text>

          {(item.trangThai !== 3)
            ? (
              <TouchableOpacity
                hitSlop={styles.hitSlop}
                style={{ zIndex: 10, flex: 1, alignItems: 'flex-end' }}
                onPress={() => {
                  if (canChangeCelendar) {
                    NavigationService.navigate(DoiLichChamSoc, { item, reloadFunc })
                  } else {
                    this.setState({ visibleEmploy: true })
                  }
                }}
              >
                <Text style={[styles.textChangeCal, { textAlign: 'center' }]}>{canChangeCelendar ? 'Đổi và hủy lịch' : 'Cập nhật lịch'}</Text>
              </TouchableOpacity>
            )
            : (
              <Text style={[styles.hotline, { fontSize: getFont(13) }]}>Đã hoàn thành</Text>
            )
          }
        </View>
        <View style={styles.line} />
        <View style={styles.textWrapper}>
          <View>
            <FastImage source={R.images.iconClock} style={styles.iconTime} />
          </View>
          <View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text style={styles.timeText}>
                {`Thời gian: ${moment(item.thoiGianCheckInDuKien).format('HH:mm')} - ${moment(item.thoiGianCheckOutDuKien).format('HH:mm')}`}
              </Text>
              {(this.props.type === 'chuyengia') && (
                <TouchableOpacity
                  onPress={() => { NavigationService.navigate('GuiThongBao', { ID_KhachHang: item?.ID_KhachHang }) }}
                  hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                  style={styles.notibutton}
                >
                  <AntDesign name="notification" size={WIDTH(20)} color={R.colors.backgroundBlack} />
                </TouchableOpacity>
              )}
            </View>
            <Text style={styles.title}>
              {item.tenDichVu || 'Trống'}
            </Text>
            {this.props.type !== 'chuyengia' ? (
              <Text style={[styles.title, { fontWeight: 'normal' }]}>
                {`SĐT chuyên gia: ${item.dienThoaiNhanVien || 'Trống'}`}
              </Text>
            ) : (
              <Text style={[styles.title, { fontWeight: 'normal' }]}>
                {`SĐT khách: ${item.dienThoaiKhach || 'Trống'}`}
              </Text>
            )}
            <Text style={[styles.title, { fontWeight: 'normal', color: R.colors.yellow254 }]}>
              {`Trạng thái: ${item.text_color_mota || 'Trống'}`}
            </Text>
            <Text style={[styles.title, { fontWeight: 'normal' }]}>
              {`Trạng thái thanh toán: ${item.trangThaiThanhToan || 'Chưa thanh toán'} (${item.tenPhuongThucThanhToan || 'trống'})`}
            </Text>
            <Text style={[styles.title, { fontWeight: 'normal' }]}>
              {`Giá tiền: ${formatVND(item.giaTien)}`}
            </Text>
            <Text style={[styles.title, { fontWeight: 'normal', color: 'blue' }]}>
              {`Địa chỉ làm DV: ${item.diaChiLam || 'Trống'}`}
            </Text>
            <Text style={styles.hotline}>
              {`Ghi chú: ${item.ghiChu || 'Trống'}`}
            </Text>
            {(item.trangThai === 3 && ((this.props.type === 'khach' && item.phuongThuc !== 1) || (this.props.type === 'chuyengia' && item.phuongThuc === 1))) && (
              <TouchableOpacity
                hitSlop={styles.hitSlop}
                style={{ paddingVertical: HEIGHT(5), paddingRight: WIDTH(50), zIndex: 10 }}
                onPress={() => this.setState({ visibleGuest: true })}
              >
                <Text style={[styles.hotline, { textDecorationLine: 'underline', alignSelf: 'flex-end', paddingRight: 0 }]}>
                  Hoàn thành
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    Account: state.userReducers.Account
  };
}

export default connect(mapStateToProps, {
  // addProduct
})(ItemChiTietLich);

const styles = StyleSheet.create({
  container: {
    width: WIDTH(343),
    borderRadius: WIDTH(4),
    borderWidth: 1,
    paddingHorizontal: WIDTH(8),
    paddingTop: WIDTH(12),
    backgroundColor: R.colors.colorMain,
    borderColor: R.colors.borderItemCart,
    alignSelf: 'center',
    marginBottom: HEIGHT(12)
  },
  hitSlop: {
    top: WIDTH(20),
    bottom: WIDTH(20),
    right: WIDTH(20),
    left: WIDTH(20)
  },
  dateText: {
    fontFamily: R.fonts.Roboto,
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.white,
    marginLeft: WIDTH(46),
    paddingBottom: HEIGHT(9),
    // width: WIDTH(200),
  },
  timeText: {
    fontFamily: R.fonts.Roboto,
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.yellow254,
    marginTop: HEIGHT(13),
    fontWeight: 'bold'
  },
  title: {
    fontFamily: R.fonts.Roboto,
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.white,
    fontWeight: '500',
    paddingRight: WIDTH(50),
    marginTop: HEIGHT(11),
  },
  hotline: {
    fontFamily: R.fonts.Roboto,
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    paddingRight: WIDTH(50),
    color: R.colors.yellow254,
    marginTop: HEIGHT(2),
    paddingBottom: HEIGHT(11)
  },
  textChangeCal: {
    fontFamily: R.fonts.Roboto,
    fontSize: getFont(13),
    lineHeight: getLineHeight(24),
    color: R.colors.yellow254,
    marginTop: HEIGHT(2),
    paddingBottom: HEIGHT(11)
  },
  line: {
    height: 1,
    backgroundColor: R.colors.yellow37c,
    width: WIDTH(343),
    alignSelf: 'center'
  },
  iconTime: {
    width: WIDTH(20),
    height: WIDTH(20),
    marginTop: HEIGHT(14),
    marginRight: WIDTH(8)
  },
  textWrapper: {
    flexDirection: 'row'
  },
  notibutton: {
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: WIDTH(28),
    marginTop: HEIGHT(10),
    borderRadius: WIDTH(3),
    padding: HEIGHT(8),
    paddingHorizontal: WIDTH(15),
    // borderWidth: 1,
    backgroundColor: R.colors.yellow254
  }
});
