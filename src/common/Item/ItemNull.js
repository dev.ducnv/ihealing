import React from 'react';
import { Text } from 'react-native';
import { getFont } from '../../config';

type Props = {
  text: string,
}

const ItemNull = (props: Props) => {
  const { text } = props;
  return (
    <Text style={{
      alignSelf: 'center',
      color: 'white',
      marginTop: 20,
      fontSize: getFont(15)
    }}
    >
      {text || 'Trống'}
    </Text>
  )
}

export default ItemNull;
