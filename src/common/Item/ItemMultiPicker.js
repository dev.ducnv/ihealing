// @flow
import React from 'react'
import {
  Text, View, StyleSheet, TextInput, Platform
} from 'react-native'

// import
import { TouchableOpacity } from 'react-native-gesture-handler';
import { getAllNVCapTren } from 'apis/Functions/users';
import KeyboardManager, {
  PreviousNextView,
} from 'react-native-keyboard-manager';
import { WIDTH, HEIGHT, getFont } from '../../config';
import R from '../../assets/R';
import PickerDate from '../Picker/PickerDate';
import PickerItem from '../Picker/PickerItem';

type Props = {
  stylesContainer: Object,
};

/* Default values */
if (Platform.OS === 'ios') {
  KeyboardManager.setEnable(true);
  KeyboardManager.setEnableDebugging(false);
  KeyboardManager.setKeyboardDistanceFromTextField(10);
  KeyboardManager.setPreventShowingBottomBlankSpace(true);
  KeyboardManager.setEnableAutoToolbar(true);
  KeyboardManager.setToolbarDoneBarButtonItemText('Done');
  KeyboardManager.setToolbarManageBehaviour(0);
  KeyboardManager.setShouldShowToolbarPlaceholder(true);
  KeyboardManager.setOverrideKeyboardAppearance(false);
  KeyboardManager.setShouldResignOnTouchOutside(true);
}

const dataLocation = ['Sử dụng tại nhà', 'Sử dụng tại cơ sở liên kết']

export default class ItemDatLich extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      mData: ['Đang tải dữ liệu', 'Đang tải dữ liệu', 'Đang tải dữ liệu'],
    };
    this.dataIdNhanVien = [];
  }

  componentDidMount = async () => {
    let nvCapTren = await getAllNVCapTren();
    if (nvCapTren.data && nvCapTren.data.length > 0) {
      this.dataIdNhanVien = nvCapTren.data;
      let mData = [];
      this.dataIdNhanVien.map((element, index) => {
        mData.push(element.tenDayDu);
      });
      // console.log(mData);
      this.setState({ mData });
    } else {
      this.setState({ mData: ['Bạn không có chuyên gia nào'] });
    }
  };

  _renderDataPickerHome = () => {
    const {
      stylesContainer,
      showModalLocation,
      data,
      showModalSearch,
    } = this.props;
    return (
      <View>
        <TouchableOpacity
          style={[
            styles.wrapper,
            { justifyContent: 'flex-start', height: HEIGHT(50) },
          ]}
          onPress={showModalLocation && showModalLocation}
        >
          <Text
            style={{
              color: R.colors.yellow254,
              fontSize: getFont(15),
              fontFamily: R.fonts.Roboto,
              lineHeight: HEIGHT(24),
              paddingLeft: 5,
            }}
          >
            {data.address.value ? data.address.value : 'Chọn địa chỉ'}
          </Text>
        </TouchableOpacity>

        <View style={styles.wrapper}>
          <PickerItem
            placeholder="Chuyên gia chăm sóc (mặc định)"
            width={WIDTH(290)}
            data={[
              'Chuyên gia chăm sóc (mặc định)',
              'Tìm chuyên gia theo tên, số điện thoại, mã chuyên gia',
              'Chuyên gia của bạn',
            ]}
            onChangeValue={(index, item) => data.employee.func(index)}
            colorText={R.colors.yellow254}
          />
        </View>
        {
          data.employee.value === 1 && (
            <TouchableOpacity
              style={[
                styles.wrapper,
                { justifyContent: 'flex-start', height: HEIGHT(50) },
              ]}
              onPress={() => showModalSearch && showModalSearch('Home')}
            >
              <Text
                style={{
                  color: R.colors.yellow254,
                  fontSize: getFont(15),
                  fontFamily: R.fonts.Roboto,
                  lineHeight: HEIGHT(24),
                  paddingLeft: 5,
                }}
              >
                {data.idEmployee.value ? data.idEmployee.value : 'Chọn chuyên gia'}
              </Text>
            </TouchableOpacity>
          )
        }
        {
          data.employee.value === 2 && (
            <View style={styles.wrapper}>
              <PickerItem
                placeholder="Chọn chuyên gia của bạn"
                width={WIDTH(290)}
                data={this.state.mData}
                onChangeValue={(index, item) => data.idEmployee.func(
                  item,
                  this.dataIdNhanVien[index] ? this.dataIdNhanVien[index] : null
                )
                }
                colorText={R.colors.yellow254}
              />
            </View>
          )
        }
        <View
          style={[
            styles.wrapper,
            { justifyContent: 'flex-start', height: HEIGHT(50) },
          ]}
        >
          <TextInput
            style={{
              color: R.colors.yellow254,
              fontSize: getFont(15),
              fontFamily: R.fonts.Roboto,
              lineHeight: HEIGHT(24),
              padding: 0,
              paddingLeft: 5,
              minWidth: WIDTH(280)
            }}
            value={data.note.value && data.note.value}
            placeholder="Ghi chú"
            placeholderTextColor={R.colors.yellow254}
            onChangeText={(text) => data.note.func && data.note.func(text)}
          />
        </View>
      </View>
    )
  }

  _renderDataPickerEmploy = () => {
    const {
      data,
      showModalSearch,
    } = this.props;
    return (
      <View>
        <TouchableOpacity
          style={[
            styles.wrapper,
            { justifyContent: 'flex-start', height: HEIGHT(50) },
          ]}
          onPress={() => showModalSearch && showModalSearch('Employ')}
        >
          <Text
            style={{
              color: R.colors.yellow254,
              fontSize: getFont(15),
              fontFamily: R.fonts.Roboto,
              lineHeight: HEIGHT(24),
              paddingLeft: 5,
            }}
          >
            {data.idEmployee.value ? data.idEmployee.value : 'Tìm kiếm cơ sở liên kết'}
          </Text>
        </TouchableOpacity>
        <View
          style={[
            styles.wrapper,
            { justifyContent: 'flex-start', height: HEIGHT(50) },
          ]}
          // onPress={showModalLocation && showModalLocation}
        >
          <TextInput
            style={{
              color: R.colors.yellow254,
              fontSize: getFont(15),
              fontFamily: R.fonts.Roboto,
              lineHeight: HEIGHT(24),
              padding: 0,
              paddingLeft: 5,
            }}
            value={data.note.value && data.note.value}
            placeholder="Ghi chú"
            placeholderTextColor={R.colors.yellow254}
            onChangeText={(text) => data.note.func && data.note.func(text)}
          />
        </View>
      </View>
    )
  }

  render() {
    const {
      stylesContainer,
      data
    } = this.props;
    return (
      <View style={stylesContainer && stylesContainer}>
        <View style={{ flexDirection: 'row' }}>
          <View style={[styles.wrapper, { width: WIDTH(151) }]}>
            <PickerItem
              placeholder={data.time.value ? data.time.value : 'Thời gian'}
              width={WIDTH(131)}
              data={R.strings.TIME_ARRAY}
              onChangeValue={(index, item) => {
                data.time && data.time.func(item, index);
              }}
              colorText={R.colors.yellow254}
            />
          </View>
          <View
            style={[
              styles.wrapper,
              { width: WIDTH(151), marginLeft: WIDTH(17) },
            ]}
          >
            <PickerDate
              onChangeDate={(date) => {
                data.date.func(date);
              }}
              date={data.date.value ? data.date.value : null}
              width={WIDTH(151)}
            />
          </View>
        </View>

        <View style={styles.wrapper}>
          <PickerItem
            placeholder={data.locationSelect.value >= 0 ? dataLocation[data.locationSelect.value] : 'Chọn địa điểm chăm sóc'}
            width={WIDTH(290)}
            heightContainer={HEIGHT(100)}
            data={dataLocation}
            onChangeValue={(index, item) => {
              data.locationSelect.func(item, index)
              // console.log('Chọn nơi sử dụng dịch vụ', index, item)
            }
              //   data.idEmployee.func(
              //   item,
              //   this.dataIdNhanVien[index] ? this.dataIdNhanVien[index] : null
              // )
            }
            colorText={R.colors.yellow254}
          />
        </View>

        {data.locationSelect.value === 0 && this._renderDataPickerHome()}
        {data.locationSelect.value === 1 && this._renderDataPickerEmploy()}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'center',
    marginTop: HEIGHT(12),
    width: WIDTH(319),
    paddingHorizontal: WIDTH(10),
    borderBottomColor: R.colors.yellow254,
    borderBottomWidth: 1,
  },
});
