
import React from 'react';
import { Animated } from 'react-native';
import R from 'assets/R';
import { WIDTH, getWidth } from '../../config';


export default (props) => {
  //   const animation = new Animated.Value(0)
  let { top, animation } = props
  return (
    <Animated.View
      style={{
        height: WIDTH(50),
        width: WIDTH(50),
        position: 'absolute',
        top: animation.interpolate({
          inputRange: [0, 1],
          outputRange: [top, 0]
        }),
        right: animation.interpolate({
          inputRange: [0, 1],
          outputRange: [getWidth() / 5 + WIDTH(10), -WIDTH(5)]
        }),
        zIndex: 10,
        opacity: animation.interpolate({
          inputRange: [0, 0.1, 0.8, 0.9, 1],
          outputRange: [0, 1, 0.8, 0.5, 0]
        }),
      }}
    >
      <Animated.Image
        style={{
          height: 15,
          width: 15,
        }}
        source={R.images.iconProduct}
      />
    </Animated.View>
  )
}
