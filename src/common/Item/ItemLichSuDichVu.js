// @flow
import React from 'react'
import {
  Text, View, StyleSheet, TouchableOpacity
} from 'react-native'
import _ from 'lodash'

// import
import NavigationService from 'routers/NavigationService';
import { ChiTietLichSuDonHang, DanhGiaDonHang } from 'routers/screenNames';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { getFont, getWidth, getLineHeight, WIDTH, HEIGHT, formatVND } from '../../config';
import R from '../../assets/R';
import i18n from '../../assets/languages/i18n'
import TextYellow from '../View/TextYellow';

type Props = {
  item: Object,
  icon: Object,
  status: string,
  yellowText: boolean
}
// 230510
const itemLichSuDonHang = (props: Props) => {
  const { item, icon, status, yellowText, onButton, refreshData } = props;
  const ItemDanhGia = () => {
    let isDanhGia = item?.danhGia;
    if (isDanhGia) {
      return (
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.textRate}>Đã đánh giá</Text>
          <AntDesign name="check" color={R.colors.borderItemCart} size={WIDTH(20)} style={{ marginLeft: WIDTH(5) }} />
        </View>
      )
    } else {
      return (
        <TouchableOpacity
          onPress={() => {
            NavigationService.navigate(DanhGiaDonHang, { item, refreshData })
          }}
          style={styles.rateButton}
        >
          <Text style={styles.textRate}>Đánh giá</Text>
        </TouchableOpacity>
      )
    }
  }
  return (
    <TouchableOpacity style={styles.container} onPress={onButton && onButton}>
      <View style={{ width: WIDTH(300) }}>
        <View style={styles.flexRow}>
          <Text style={[styles.text, { color: R.colors.white, fontSize: getFont(16), fontWeight: '700', width: WIDTH(200) }]}>
            {_.has(item, 'tenDichVu') ? item.tenDichVu : i18n.t('NULL_T')}
          </Text>
          <ItemDanhGia />
        </View>

        <Text style={styles.text}>{_.has(item, 'ngayTao') ? `Ngày đặt hàng: ${item.ngayTao}` : i18n.t('NULL_T')}</Text>
        <Text style={styles.text}>{_.has(item, 'giaTien') ? `Tổng tiền: ${formatVND(item.giaTien)} VNĐ` : i18n.t('NULL_T')}</Text>
        {
          yellowText && yellowText
            ? <TextYellow
                styleContainer={{ width: WIDTH(300), alignItems: 'flex-start', paddingHorizontal: 0 }}
                styleText={styles.text}
                text="Trạng thái: "
                yellowText={status}
            />
            : <Text style={styles.text}>{`Trạng thái: ${status}`}</Text>
        }
      </View>
      {icon}
    </TouchableOpacity>
  )
}

export default itemLichSuDonHang;

const styles = StyleSheet.create({
  container: {
    width: getWidth(),
    paddingHorizontal: WIDTH(16),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: HEIGHT(20),
    paddingTop: HEIGHT(16)
  },
  text: {
    fontSize: getFont(14),
    lineHeight: getLineHeight(20),
    color: R.colors.grey400,
  },
  flexRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
    width: WIDTH(350)
  },
  textRate: {
    lineHeight: getLineHeight(20),
    color: R.colors.borderItemCart,
    fontSize: getFont(14),
    // fontWeight: '700'
  },
  rateButton: {

  }
})
