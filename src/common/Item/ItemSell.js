import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Animated } from 'react-native';
import FastImage from 'react-native-fast-image';
import { PanGestureHandler } from 'react-native-gesture-handler';
import R from '../../assets/R';
import NavigationService from '../../routers/NavigationService';
import { CartProduct, DetailProduct } from '../../routers/screenNames';
import BaseButton from '../Button/BaseButton';
import { WIDTH, HEIGHT, getFont, getLineHeight, formatVND } from '../../config';

export default class ItemSell extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const { icon, title, price, isSellingFast, isNew, onButton, item } = this.props
    return (
      <View style={{ flexDirection: 'row', paddingVertical: HEIGHT(12), alignItems: 'center' }}>
        <View>
          <FastImage source={{ uri: icon }} style={{ width: WIDTH(124), height: HEIGHT(124), marginLeft: WIDTH(16) }} />
          {isSellingFast && (
            <View style={styles.banChay}>
              <Text style={styles.textBanChay}>
                Bán chạy
              </Text>
            </View>
          )}
          {isNew && (
            <View style={styles.moi}>
              <Text style={styles.textMoi}>
                Mới
              </Text>
            </View>
          )}
        </View>
        <View style={{ paddingHorizontal: WIDTH(16) }}>
          <Text style={styles.title} numberOfLines={2}>{title}</Text>
          <Text style={styles.price}>{`${formatVND(price)} VNĐ`}</Text>
          <View style={styles.subView}>
            <TouchableOpacity
              onPress={() => { item && NavigationService.navigate(DetailProduct, { item }) }}
              style={styles.textView}
            >
              <Text style={styles.txtBtn}>Xem</Text>
            </TouchableOpacity>
            <BaseButton
              title={item.isDichVu ? 'Đặt dịch vụ' : 'Thêm vào giỏ'}
              width={WIDTH(121)}
              height={HEIGHT(34)}
              ref={ref => { this.BaseButton = ref }}
              onButton={onButton && onButton}
            />
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: HEIGHT(12),
    alignItems: 'center'
  },
  title: {
    width: WIDTH(203),
    color: R.colors.white,
    fontWeight: '500',
    fontSize: getFont(14),
    fontFamily: R.fonts.Roboto
  },
  price: {
    width: WIDTH(203),
    marginTop: HEIGHT(5),
    color: R.colors.yellow254,
    fontWeight: '500',
    fontSize: getFont(20),
    fontFamily: R.fonts.Roboto
  },
  banChay: {
    height: HEIGHT(20),
    width: WIDTH(60),
    backgroundColor: R.colors.greentag,
    position: 'absolute',
    zIndex: 1,
    marginLeft: WIDTH(16),
    marginTop: HEIGHT(6),
    justifyContent: 'center',
    alignItems: 'center'
  },

  textBanChay: {
    fontFamily: R.fonts.Roboto,
    fontSize: getFont(13),
    lineHeight: getLineHeight(16),
    color: R.colors.white
  },
  moi: {
    height: HEIGHT(20),
    width: WIDTH(60),
    backgroundColor: R.colors.red646,
    position: 'absolute',
    zIndex: 1,
    marginLeft: WIDTH(16),
    marginTop: HEIGHT(6),
    justifyContent: 'center',
    alignItems: 'center'
  },
  textMoi: {
    fontFamily: R.fonts.Roboto,
    fontSize: getFont(13),
    lineHeight: getLineHeight(16),
    color: R.colors.white
  },
  subView: {
    flexDirection: 'row', justifyContent: 'space-between', marginTop: HEIGHT(13), height: HEIGHT(34)
  },
  textView: {
    borderWidth: 1, height: HEIGHT(34), borderRadius: WIDTH(50), borderColor: R.colors.yellow1c8, alignItems: 'center', justifyContent: 'center', paddingHorizontal: WIDTH(16), paddingVertical: HEIGHT(8)
  },
  txtBtn: {
    color: R.colors.yellow254, fontWeight: '500', fontSize: getFont(14), fontFamily: R.fonts.Roboto
  },

})
