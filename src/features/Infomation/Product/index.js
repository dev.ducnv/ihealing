import React from 'react';
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, FlatList, Animated, ActivityIndicator, Alert } from 'react-native';
import { connect } from 'react-redux'
import LinearGradient from 'react-native-linear-gradient';
import { getMetaDataProduct, getListProduct } from 'apis';
import _ from 'lodash';
import ItemNull from 'common/Item/ItemNull';
import HeaderSearch from '../../../common/Header/HeaderSearch';
import { LoadingComponent } from '../../../common/Loading/LoadingComponent';
import R from '../../../assets/R';
import HeaderBack from '../../../common/Header/HeaderBack';
import ItemSell from '../../../common/Item/ItemSell';
import { WIDTH, HEIGHT, getFont, getLineHeight, getWidth, getHeight, popupOk } from '../../../config';
import PickerDropdown from '../../../common/Picker/PickerDropdown'
import { addProduct } from '../../../actions';
import NavigationService from '../../../routers/NavigationService';
import { CartProduct } from '../../../routers/screenNames';

import IconAddAnimation from '../../../common/Item/IconAddAnimation'


const dataProduct = {
  sortPrice: [
    { value: 'Giá tăng dần', key: '1' },
    { value: 'Giá giảm dần', key: '2' },
    { value: 'Mức độ phổ biến', key: '3' }
  ],
  sortService: [
    { value: 'Combo chăm sóc toàn thân', key: 'key1' },
    { value: 'Combo chăm sóc toàn đầu', key: 'key2' },
    { value: 'Combo chăm sóc toàn bụng', key: 'key3' }
  ],
}

class Product extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      dataList: [],

      tagButton: [
        { value: 'Đang tải' },
        { value: 'Đang tải' },
        { value: 'Đang tải' },
      ],
      valueTab: null,
      sortService: [
        { value: 'Đang tải dữ liệu', key: 'key1' },
        { value: 'Đang tải dữ liệu', key: 'key2' },
        { value: 'Đang tải dữ liệu', key: 'key3' }
      ],
      valueService: 0,
      sortPrice: 0,

      page: 1,
      loading: true,
      showfooter: false,
      maxData: false,
      refreshing: false,

      searching: false,
      keySearch: '',
      top: 0,
    }
    this.animation = new Animated.Value(0)

    this.timeout;
    this.lastKeySearch = '';
  }

  componentDidMount = () => {
    this.getMetaData()
  }

  getMetaData = async () => {
    this.setState({ loading: true })
    const metaData = await getMetaDataProduct()
    // console.log('metaData', metaData)
    if (_.has(metaData, 'data.data.boloc')) {
      let sortService = []
      metaData.data.data.boloc.map((item, ind) => {
        sortService.push({
          ...item,
          value: item.tenDanhMuc && item.tenDanhMuc,
          key: item.iD_DANHMUC && item.iD_DANHMUC
        })
      })
      this.setState({ sortService }, async () => {
        await this.getList(sortService[0].key, '', 1, 1)
      })
    }
    if (_.has(metaData, 'data.data.locnhanh')) {
      let tagButton = []
      metaData.data.data.locnhanh.map((item, ind) => {
        tagButton.push({
          ...item,
          value: item.tenDanhMuc && item.tenDanhMuc,
          key: item.iD_DANHMUC && item.iD_DANHMUC
        })
      })
      this.setState({ tagButton })
    }
    this.setState({ loading: false })
  }

  getList = async (id, search, page, sortType) => {
    this.setState({ showfooter: true })
    let querry = `?ID_DanhMuc=${id}&Content=${search}&Page=${page}&LocType=${sortType}`
    let listProduct = await getListProduct(querry);
    if (_.has(listProduct, 'data.data')) {
      if (listProduct.data.data.length > 0) {
        this.setState({ dataList: [...this.state.dataList, ...listProduct.data.data], showfooter: false, loading: false })
      } else {
        this.setState({ maxData: true, showfooter: false, loading: false })
      }
    } else {
      this.setState({ showfooter: false, loading: false, maxData: true })
    }
    // console.log('listProduct', listProduct)
  }

  onRefresh = async () => {
    if (!this.state.loading) {
      this.setState({ page: 1, maxData: false, refreshing: true, valueTab: null, dataList: [], searching: false })
      await this.getList(
        this.state.sortService[this.state.valueService].key,
        '',
        1,
        dataProduct.sortPrice[this.state.sortPrice].key
      )
      this.setState({ refreshing: false })
    }
  }

  handleLoadMore = () => {
    // console.log('handleLoadMore,handleLoadMore')
    if (!this.state.loading && !this.state.maxData && !this.state.searching && !this.state.showfooter) {
      if (this.state.valueTab !== null) {
        this.getList(
          this.state.tagButton[this.state.valueTab].key,
          '',
          this.state.page + 1,
          dataProduct.sortPrice[this.state.sortPrice].key
        )
      } else {
        this.getList(
          this.state.sortService[this.state.valueService].key,
          '',
          this.state.page + 1,
          dataProduct.sortPrice[this.state.sortPrice].key
        )
      }
      this.setState({ page: this.state.page + 1 })
    }
  }


  addProduct = (y, item) => {
    this.props.addProduct(item)
    item.isDichVu && NavigationService.navigate(CartProduct)
  }

  renderItem = (item) => (
    <ItemSell
      onButton={(y) => {
        if (this.props.isDichVu === item.isDichVu || this.props.amount === 0) this.addProduct(y, item)
        else Alert.alert('Giỏ hàng không thế bao gồm cả dịch vụ và sản phẩm')
      }}
      icon={item.anhDaiDien && R.images.IMAGE + item.anhDaiDien}
      title={item.tenHang && item.tenHang}
      price={item.giaLe && item.giaLe}
      isSellingFast={item.isSellingFast}
      isNew={item.isNew}
      item={item}
    />
  )

  renderFooter = () => {
    // it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (!this.state.showfooter || this.state.loading || this.state.refreshing) return null;
    return (
      <ActivityIndicator
        color={R.colors.yellow254}
        animating
        size="small"
      />
    );
  };


  onValueChange = (value, index, key) => {
    this.setState({ maxData: false })

    switch (key) {
      case 'SERVICE':
        this.setState({ valueService: index, page: 1, valueTab: null, dataList: [] }, () => {
          this.getList(
            this.state.sortService[this.state.valueService].key,
            '',
            1,
            dataProduct.sortPrice[this.state.sortPrice].key
          )
        })
        break;
      default:
        this.setState({ sortPrice: index, page: 1, dataList: [] }, () => {
          this.getList(
            this.state.sortService[this.state.valueService].key,
            '',
            1,
            dataProduct.sortPrice[this.state.sortPrice].key
          )
        })
        break;
    }
  }

  onChangeText = (text) => {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(async () => {
      // console.log('======>>>>vao timeout', search, this.lastKeySearch);
      if (text === this.lastKeySearch && text !== '') {
        // this.getData(text)
        this.setState({ loading: true, dataList: [] })
        await this.getList(
          this.state.sortService[this.state.valueService].key,
          text,
          1,
          dataProduct.sortPrice[this.state.sortPrice].key
        )
      }
    }, 500);
    this.lastKeySearch = text;
    this.setState({ keySearch: text });
  }

  render() {
    if (!this.state.loading) {
      return (
        <View style={{ flex: 1, height: getHeight(), backgroundColor: R.colors.black3 }}>
          {!this.state.searching
            ? <HeaderBack
                onButtonCart={() => { NavigationService.navigate(CartProduct) }}
                onButtonSearch={() => this.setState({ searching: true })}
                title="Dược Liệu"
            />
            : <HeaderSearch
                title={this.state.keySearch}
                onChangeText={this.onChangeText}
                onButtonBack={() => {
                  this.onRefresh()
                  this.setState({ searching: false })
                }}
            />}
          {/* <IconAddAnimation
            top={this.state.top}
            animation={this.animation}
          /> */}

          <View
            style={{ flexDirection: 'row', paddingHorizontal: WIDTH(7), flexWrap: 'wrap', width: getWidth(), paddingBottom: HEIGHT(12), backgroundColor: R.colors.colorMain }}
          >
            {this.state.tagButton.map((item, index) => {
              if (index === this.state.valueTab) {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({ valueTab: null })
                    }}
                    style={{ paddingBottom: 1 }}
                    activeOpacity={0.6}
                  >
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 1 }}
                      colors={[R.colors.yellow1c8, R.colors.yellow254, R.colors.yellow1c8]}
                      style={styles.btnStyleNoborder}
                    >
                      <Text style={[styles.title, { color: R.colors.black0 }]}>{item.value}</Text>
                    </LinearGradient>
                  </TouchableOpacity>
                )
              } else {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({ valueTab: index, page: 1, dataList: [] })
                      this.getList(
                        this.state.tagButton[index].key,
                        '',
                        1,
                        dataProduct.sortPrice[this.state.sortPrice].key
                      )
                    }}
                    style={{ paddingBottom: 1 }}
                    activeOpacity={0.6}
                  >
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 1 }}
                      colors={[R.colors.colorMain, R.colors.colorMain, R.colors.colorMain]}
                      style={styles.btnStyle}
                    >
                      <Text style={styles.title}>{item.value}</Text>
                    </LinearGradient>
                  </TouchableOpacity>
                )
              }
            })}

          </View>

          <View style={{ flexDirection: 'row', paddingHorizontal: WIDTH(16), paddingVertical: HEIGHT(12), justifyContent: 'space-between' }}>
            <PickerDropdown
              data={this.state.sortService}
              placeholder={this.state.sortService[this.state.valueService].value}
              width={WIDTH(191)}
              onValueChange={(value, index) => this.onValueChange(value, index, 'SERVICE')}
            />
            <PickerDropdown
              data={dataProduct.sortPrice}
              placeholder={dataProduct.sortPrice[this.state.sortPrice].value}
              width={WIDTH(137)}
              onValueChange={(value, index) => this.onValueChange(value, index, 'PRICE')}
            />
          </View>

          <FlatList
            style={{ paddingBottom: HEIGHT(20) }}
            data={this.state.dataList}
            renderItem={({ item }) => this.renderItem(item)}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={0.1}
            showsVerticalScrollIndicator={false}
            ListFooterComponent={this.renderFooter}
            onRefresh={this.onRefresh}
            ListEmptyComponent={<ItemNull />}
            refreshing={this.state.refreshing}
          />
        </View>
      );
    } else {
      return (
        <View style={{ flex: 1, height: getHeight() }}>
          {!this.state.searching
            ? <HeaderBack
                onButtonCart={() => { NavigationService.navigate(CartProduct) }}
                onButtonSearch={() => this.setState({ searching: true })}
                title="Dược Liệu"
            />
            : <HeaderSearch
                title={this.state.keySearch}
                onChangeText={this.onChangeText}
                onButtonBack={() => {
                  this.onRefresh()
                  this.setState({ searching: false })
                }}
            />}
          <LoadingComponent isLoading={this.state.loading} />
        </View>
      )
    }
  }
}

function mapStateToProps(state) {
  return {
    cart: state.cartReducers.cart,
    amount: state.cartReducers.amount,
    isDichVu: state.cartReducers.isDichVu
  };
}

export default connect(mapStateToProps, {
  addProduct
})(Product);
const styles = StyleSheet.create({
  btnStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: WIDTH(4),
    borderWidth: 1,
    borderColor: R.colors.yellow254,
    marginTop: HEIGHT(12),
    marginLeft: WIDTH(9),
    paddingHorizontal: WIDTH(11),
    paddingVertical: HEIGHT(6)
    // height: HEIGHT(32)
  },
  btnStyleNoborder: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: WIDTH(4),
    marginTop: HEIGHT(12),
    marginLeft: WIDTH(9),
    paddingHorizontal: WIDTH(11) + 1,
    paddingVertical: HEIGHT(6) + 1
    // height: HEIGHT(32)
  },
  title: {
    fontSize: getFont(15),
    color: R.colors.yellow254,
    lineHeight: getLineHeight(22)
  },
  flexCenter: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: HEIGHT(6),
    paddingVertical: WIDTH(11)
  }
})
