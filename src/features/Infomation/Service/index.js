import React from 'react';
import { View, Text, ScrollView, FlatList, Animated, ActivityIndicator, Alert } from 'react-native';
import R from 'assets/R';
import NavigationService from 'routers/NavigationService';
import { CartProduct } from 'routers/screenNames';
import { connect } from 'react-redux';
import { getMetaDataService, getListProduct } from 'apis';
import _ from 'lodash'
import HeaderSearch from 'common/Header/HeaderSearch';
import { LoadingComponent } from 'common/Loading/LoadingComponent';
import ItemNull from 'common/Item/ItemNull';
import HeaderBack from '../../../common/Header/HeaderBack';
import IconAddAnimation from '../../../common/Item/IconAddAnimation';
import ItemSell from '../../../common/Item/ItemSell';
import { HEIGHT, WIDTH, getHeight } from '../../../config';
import PickerDropdown from '../../../common/Picker/PickerDropdown';
import { addProduct } from '../../../actions';


const dataService = {
  sortPrice: [
    { value: 'Giá tăng dần', key: '1' },
    { value: 'Giá giảm dần', key: '2' },
    { value: 'Mức độ phổ biến', key: '3' }
  ]
}

class Service extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      top: 0,

      dataList: [],

      sortService: [
        { value: 'Đang tải dữ liệu', key: 'key0' },
        { value: 'Đang tải dữ liệu', key: 'key1' },
        { value: 'Đang tải dữ liệu', key: 'key2' },
        // { value: 'Combo chăm sóc toàn đầu', key: 'key2' },
        // { value: 'Combo chăm sóc toàn bụng', key: 'key3' }
      ],
      valueService: 0,
      sortPrice: 0,

      page: 1,
      loading: true,
      showfooter: false,
      maxData: false,
      refreshing: false,

      searching: false,
      keySearch: '',
    }
    this.animation = new Animated.Value(0)
  }

  addProduct = (y, item) => {
    this.setState({ top: y })
    this.props.addProduct(item)
    item.isDichVu && NavigationService.navigate(CartProduct)
    // Animated.timing(this.animation, {
    //   toValue: 1,
    //   duration: 1500,
    //   useNativeDriver: false
    // }).start()
    // setTimeout(() => {
    //   this.animation = new Animated.Value(0)
    //   this.props.addProduct(item)
    // }, 1500)
  }

  componentDidMount = () => {
    this.getMetaData()
  }

  getMetaData = async () => {
    this.setState({ loading: true })
    const metaData = await getMetaDataService()
    // console.log('metaData', metaData)
    if (_.has(metaData, 'data.data.boloc')) {
      let sortService = []
      metaData.data.data.boloc.map((item, ind) => {
        sortService.push({
          ...item,
          value: item.tenDanhMuc && item.tenDanhMuc,
          key: item.iD_DANHMUC && item.iD_DANHMUC
        })
      })
      this.setState({ sortService }, async () => {
        await this.getList(sortService[0].key, '', 1, 1)
      })
    }
    this.setState({ loading: false })
  }

  getList = async (id, search, page, sortType) => {
    this.setState({ showfooter: true })
    let querry = `?ID_DanhMuc=${id}&Content=${search}&Page=${page}&LocType=${sortType}`
    // console.log('querry1', querry)
    let listProduct = await getListProduct(querry);
    if (_.has(listProduct, 'data.data')) {
      // console.log('querry', page, listProduct.data.data)
      if (listProduct.data.data.length > 0) {
        this.setState({ dataList: [...this.state.dataList, ...listProduct.data.data], showfooter: false, loading: false })
      } else {
        this.setState({ maxData: true, showfooter: false, loading: false })
      }
    } else {
      this.setState({ showfooter: false, maxData: true, loading: false })
    }
    // console.log('listProduct', listProduct)
  }


  handleLoadMore = () => {
    // console.log('handleLoadMore,handleLoadMore')
    if (!this.state.loading && !this.state.maxData && !this.state.searching) {
      this.getList(
        this.state.sortService[this.state.valueService].key,
        '',
        this.state.page + 1,
        dataService.sortPrice[this.state.sortPrice].key
      )
      this.setState({ page: this.state.page + 1 })
    }
  }

  onRefresh = async () => {
    if (!this.state.loading) {
      this.setState({ page: 1, maxData: false, refreshing: true, dataList: [], searching: false })
      await this.getList(
        this.state.sortService[this.state.valueService].key,
        '',
        1,
        dataService.sortPrice[this.state.sortPrice].key
      )
      this.setState({ refreshing: false })
    }
  }

  onValueChange = (value, index, key) => {
    this.setState({ maxData: false })
    switch (key) {
      case 'SERVICE':
        this.setState({ valueService: index, page: 1, dataList: [] }, () => {
          this.getList(
            this.state.sortService[this.state.valueService].key,
            '',
            1,
            dataService.sortPrice[this.state.sortPrice].key
          )
        })
        break;
      default:
        this.setState({ sortPrice: index, page: 1, dataList: [] }, () => {
          this.getList(
            this.state.sortService[this.state.valueService].key,
            '',
            1,
            dataService.sortPrice[this.state.sortPrice].key
          )
        })
        break;
    }
  }

  onChangeText = (text) => {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(async () => {
      // console.log('======>>>>vao timeout', search, this.lastKeySearch);
      if (text === this.lastKeySearch && text !== '') {
        // this.getData(text)
        this.setState({ loading: true, dataList: [] })
        await this.getList(
          this.state.sortService[this.state.valueService].key,
          text,
          1,
          dataService.sortPrice[this.state.sortPrice].key
        )
      }
    }, 500);
    this.lastKeySearch = text;
    this.setState({ keySearch: text });
  }

  renderFooter = () => {
    // it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (!this.state.showfooter || this.state.loading || this.state.refreshing) return null;
    return (
      <ActivityIndicator
        color={R.colors.yellow254}
        animating
        size="small"
      />
    );
  };

  render() {
    if (!this.state.loading) {
      return (
        <View style={{ flex: 1, height: getHeight(), backgroundColor: R.colors.black3 }}>
          {!this.state.searching
            ? <HeaderBack
                onButtonCart={() => { NavigationService.navigate(CartProduct) }}
                onButtonSearch={() => this.setState({ searching: true })}
                title="Dịch vụ"
            />
            : <HeaderSearch
                title={this.state.keySearch}
                onChangeText={this.onChangeText}
                onButtonBack={() => {
                  this.onRefresh()
                  this.setState({ searching: false })
                }}
            />}
          {/* <IconAddAnimation
            top={this.state.top}
            animation={this.animation}
          /> */}
          <View style={{ flexDirection: 'row', paddingHorizontal: WIDTH(16), paddingVertical: HEIGHT(12), justifyContent: 'space-between' }}>
            <PickerDropdown
              data={this.state.sortService}
              placeholder={this.state.sortService[this.state.valueService].value}
              width={WIDTH(191)}
              onValueChange={(value, index) => this.onValueChange(value, index, 'SERVICE')}
            />
            <PickerDropdown
              data={dataService.sortPrice}
              placeholder={dataService.sortPrice[this.state.sortPrice].value}
              width={WIDTH(137)}
              onValueChange={(value, index) => this.onValueChange(value, index, 'PRICE')}
            />
          </View>
          <FlatList
            style={{ paddingBottom: HEIGHT(20) }}
            data={this.state.dataList}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={0.1}
            showsVerticalScrollIndicator={false}
            ListFooterComponent={this.renderFooter}
            ListEmptyComponent={<ItemNull />}
            onRefresh={this.onRefresh}
            refreshing={this.state.refreshing}
            renderItem={({ item, index }) => (
              <ItemSell
                onButton={(y) => {
                  if (this.props.isDichVu === item.isDichVu || this.props.amount === 0) this.addProduct(y, item)
                  else Alert.alert('Giỏ hàng không thế bao gồm cả dịch vụ và sản phẩm')
                }}
                icon={item.anhDaiDien && R.images.IMAGE + item.anhDaiDien}
                title={item.tenHang && item.tenHang}
                price={item.giaLe && item.giaLe}
                isSellingFast={item.isSellingFast}
                isNew={item.isNew}
                item={item}
              />
            )}
          />
        </View>
      );
    } else {
      return (
        <View style={{ flex: 1, height: getHeight() }}>
          {!this.state.searching
            ? <HeaderBack
                onButtonCart={() => { NavigationService.navigate(CartProduct) }}
                onButtonSearch={() => this.setState({ searching: true })}
                title="Dịch vụ"
            />
            : <HeaderSearch
                title={this.state.keySearch}
                onChangeText={this.onChangeText}
                onButtonBack={() => {
                  this.onRefresh()
                  this.setState({ searching: false })
                }}
            />}
          <LoadingComponent isLoading={this.state.loading} />
        </View>
      )
    }
  }
}

function mapStateToProps(state) {
  return {
    amount: state.cartReducers.amount,
    isDichVu: state.cartReducers.isDichVu
  };
}

export default connect(mapStateToProps, {
  addProduct
})(Service);
