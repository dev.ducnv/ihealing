import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet, Linking } from 'react-native';
import BaseButton from '../../../../common/Button/BaseButton';
import R from '../../../../assets/R';
import NavigationService from '../../../../routers/NavigationService';
import {
  WIDTH,
  HEIGHT,
  getLineHeight,
  getFont,
  getWidth,
} from '../../../../config';

const dataHR = {
  quyenLoi: `-	Được đào tạo nghề Chăm sóc sức khỏe và sắc đẹp từ cơ bản tới nâng cao, thường xuyên cập nhật và đào tạo nâng cao tay nghề.
-	Được tham gia thi cấp chứng chỉ hành nghề sau khi hoàn thành khóa học.
-	Sau khi tham gia vào hệ thống, mỗi quản lý sẽ được cấp 01 mã ID và thẻ thành viên, tiện cho việc quản lý và hưởng các ưu đãi của hệ thống.
-	Được hưởng 20% tổng doanh thu sản phẩm từ các thành viên dưới hệ thống của mình.
-	Được chiết khấu 20% khi mua sản phẩm của hệ thống iHealing.
-	Xây dựng và sở hữu hệ thống khách hàng riêng biệt, có tính kế thừa và nhân rộng.
-	Hưởng 70% phí dịch vụ khi đăng ký nhận khách trên APP iHealing.
-	Hưởng 5% tổng doanh thu dịch vụ của các thành viên dưới hệ thống của mình.
-	Được đào tạo và Phát triển bản thân, xây dựng trau dồi kỹ năng giao tiếp, kỹ năng nghề nghiệp, nắm bắt tâm lý khách hàng,...
-	Được đào tạo, hỗ trợ Marketing, đẩy mạnh việc tìm kiếm khách hàng và có nguồn khách hàng thụ động.
-	Luôn có những ưu đãi, chính sách khen thưởng, thi đua từ công ty và hệ thống iHealing.
-	Được hỗ trợ phát triển nhân hiệu, khuếch trương vị thế, nâng cao uy tín trong hệ thống iHealing, cũng như trong ngành trị liệu và chăm sóc sắc đẹp.
-	Được hưởng hệ thống khách hàng lâu dài, có tính kế thừa, nhận tiền hoa hồng vào thứ 5 hàng tuần.
-	Được sự hỗ trợ và phát triển công việc từ trực tiếp giám đốc phát triển dự án, công ty.`,
  nghiaVu: `-	Đặt cọc 03 triệu đồng nếu là cá nhân
-	Đạt chuẩn quản lý trong 03 tháng:
  •	Có 10 (Chuyên gia , chuyên viên tư vấn)  /03 tháng và có doanh số đạt chuẩn 50 triệu/ 03 tháng.
  •	Sau 03 tháng không đạt tiêu chuẩn Quản lý, thành viên sẽ tự động chuyển xuống thành chuyên gia ( bổ sung chứng chỉ hành nghề và chứng chỉ iHealing). Hệ thống chuyên gia và chuyên viên tư vấn bên dưới sẽ chuyển cho 1 thành viên khác là quản lý đạt chuẩn trong cùng hệ thống do Giám đốc quyết định.
-	Những tháng tiếp theo sau khi đạt tiêu chuẩn Quản lý, thành viên phải đảm bảo doanh số hệ thống tối thiểu 20 triệu/01 tháng và phát triển mỗi tháng ít nhất 02 (chuyên gia , chuyên viên tư vấn mới) vào hệ thống đảm bảo được hưởng tất cả các quyền lợi của công ty. Trường hợp không đạt tiêu chuẩn duy trì Quản lý tỷ lệ hoa hồng được hưởng giảm từ 20% xuống 18% cho tới khi đạt chuẩn Quản lý tỷ lệ hoa hồng sẽ trở về mức 20%.
-	Tham gia đầy đủ các khóa đào tạo cùng công ty và đảm bảo 100% đội ngũ chuyên gia, chuyên viên tư vấn cùng tham gia.
-	Giám sát, đôn đốc, chịu trách nhiệm và hỗ trợ các chuyên gia, chuyên viên tư vấn thực hiện đúng luật pháp và đúng quy định của công ty.
-	Chăm sóc, đào tạo các chuyên gia, chuyên viên tư vấn nâng cao tay nghề, phổ cập kỹ năng chăm sóc khách hàng của hệ thống mình thường xuyên, định kỳ.
-	Trường hợp  doanh số nhóm không phát sinh trong 06 tháng ID của Quản lý sẽ tự động chuyển xuống thành chuyên gia. 
-	Trích 2.5% tổng doanh số sản phẩm của người được giới thiệu tham gia hệ thống iHealing với vị trí chuyên gia/ chuyên viên tư vấn cho chuyên gia/chuyên viên tư vấn giới thiệu vào thứ 5 hàng tuần.`,
};

class ThanhVien extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <Text style={styles.title}>ĐĂNG KÝ QUẢN LÝ</Text>
        <Text style={styles.key}>Quyền lợi</Text>
        <Text style={styles.detail}>{dataHR.quyenLoi}</Text>
        <Text style={styles.key}>Nghĩa vụ</Text>
        <Text style={styles.detail}>{dataHR.nghiaVu}</Text>
        <View style={styles.viewBtn}>
          <BaseButton
            title="Đăng kí"
            onButton={() => Linking.openURL(R.strings.URL_REGISTER)}
          />
        </View>
      </ScrollView>
    );
  }
}

export default ThanhVien;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: R.colors.black3,
  },
  title: {
    paddingHorizontal: WIDTH(16),
    marginTop: HEIGHT(24),
    fontWeight: '500',
    fontSize: getFont(20),
    lineHeight: getLineHeight(28),
    color: R.colors.yellow254,
    fontFamily: R.fonts.Roboto,
    letterSpacing: 0.25,
    alignSelf: 'center',
  },
  key: {
    fontSize: getFont(16),
    marginTop: HEIGHT(16),
    lineHeight: getLineHeight(24),
    marginLeft: WIDTH(16),
    fontFamily: R.fonts.Roboto,
    color: R.colors.yellow254,
  },
  detail: {
    marginTop: HEIGHT(8),
    fontSize: getFont(15),
    lineHeight: getLineHeight(22),
    color: R.colors.white,
    fontFamily: R.fonts.Roboto,
    marginLeft: WIDTH(16),
    letterSpacing: 0.25,
  },
  viewBtn: {
    width: getWidth(),
    alignSelf: 'center',
    paddingVertical: HEIGHT(32),
    alignItems: 'center',
  },
});
