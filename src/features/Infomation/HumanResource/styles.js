import { StyleSheet } from 'react-native';
import R from '../../../assets/R';
import { HEIGHT, getFont, getLineHeight } from '../../../config';

const styles = StyleSheet.create({
  styleTabbar: {
    backgroundColor: 'transparent'
  },
  tabStyle: {
    height: HEIGHT(44)
  },
  styleText: {
    fontSize: getFont(14),
    lineHeight: getLineHeight(18),
    fontFamily: R.fonts.Roboto,
    color: R.colors.black0,
    letterSpacing: 0.2,
    textAlign: 'center',
  },
  indicatorStyle: {
    backgroundColor: R.colors.black0,
    height: HEIGHT(2)
  }
});

export default styles;
