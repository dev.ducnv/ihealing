import React from 'react';
import { View, Text } from 'react-native';
import { TabView, TabBar } from 'react-native-tab-view';
import LinearGradient from 'react-native-linear-gradient';
import R from '../../../assets/R';
import { WIDTH, HEIGHT } from '../../../config';
import HeaderBack from '../../../common/Header/HeaderBack';
import styles from './styles';
import ChuyenGia from './ChuyenGia';
import ThanhVien from './ThanhVien';
import QuanLy from './QuanLy';
import GiamDoc from './GiamDoc';

const LazyPlaceholder = ({ route }: Object) => (
  <View style={styles.scene}>
    <Text>
      Loading
      {' '}
      {route.title}
      …
    </Text>
  </View>
);

export default class HumanResource extends React.PureComponent {
  constructor(props: any) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        { key: '0', title: 'Chuyên viên' },
        { key: '1', title: 'Chuyên gia' },
        { key: '2', title: 'Quản lý' },
        { key: '3', title: 'Cơ sở liên kết' },
      ],
    };
  }

  _renderLabel = ({ route }) => {
    let additionStyle = this.state.index.toString() === route.key ? { fontWeight: 'bold' } : { opacity: 0.5 }
    return (
      <Text style={[styles.styleText, additionStyle]}>{route.title}</Text>
    )
  };

  _renderTabBar = (props: any) => (
    <LinearGradient
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 1 }}
      colors={R.colors.linearButtonYellow}
    >
      <TabBar
        {...props}
        style={styles.styleTabbar}
        indicatorStyle={styles.indicatorStyle}
        renderLabel={this._renderLabel}
        tabStyle={styles.tabStyle}
      />
    </LinearGradient>
  );

  renderScene = ({ route }: Object) => {
    switch (route.key) {
      case '0':
        return <ThanhVien />;
      case '1':
        return <ChuyenGia />;
      case '2':
        return <QuanLy />;
      case '3':
        return <GiamDoc />;
      default:
        return null;
    }
  };

  _renderLazyPlaceholder = ({ route }: Object) => (<LazyPlaceholder route={route} />);

  render() {
    return (
      <View style={{ flex: 1 }}>
        <HeaderBack title="Tuyển dụng" iconSearch={false} iconCart={false} />
        <TabView
          navigationState={this.state}
          renderTabBar={this._renderTabBar}
          renderScene={this.renderScene}
          style={{ flex: 1 }}
          onIndexChange={index => this.setState({ index })}
          initialLayout={{ width: WIDTH(360) }}
          lazyPreloadDistance={1}
          renderLazyPlaceholder={this._renderLazyPlaceholder}
        />
      </View>
    );
  }
}
