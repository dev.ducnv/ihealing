import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet, Linking } from 'react-native';
import BaseButton from '../../../../common/Button/BaseButton';
import R from '../../../../assets/R';
import NavigationService from '../../../../routers/NavigationService';
import {
  WIDTH,
  HEIGHT,
  getLineHeight,
  getFont,
  getWidth,
} from '../../../../config';

const dataHR = {
  quyenLoi: `-  Được đào tạo nghề Chăm sóc sức khỏe và sắc đẹp từ cơ bản tới nâng cao, thường xuyên cập nhật và đào tạo nâng cao tay nghề đối với tất cả các chuyên gia thuộc cơ sở liên kết.
-  Được tham gia thi cấp chứng chỉ hành nghề trên toàn quốc sau khóa đào tạo và vượt qua bài kiểm tra của iHealing.
-  Được hưởng 40% giá trị sản phẩm từ sản phẩm tiêu hao và sản phẩm bán cho khách hàng.
-  Hưởng 75% phí dịch vụ khi đăng ký nhận khách trên APP iHealing.
-  Được đào tạo, hỗ trợ Marketing, đẩy mạnh việc tìm kiếm khách hàng và có nguồn khách hàng thụ động.
-  Được hỗ trợ phát triển nhân hiệu, khuếch trương vị thế, nâng cao uy tín trong hệ thống iHealing, cũng như trong ngành trị liệu và chăm sóc sắc đẹp.
-  Được hưởng hệ thống khách hàng lâu dài, nhận tiền hoa hồng vào thứ 5 hàng tuần.
-  Được sự hỗ trợ và phát triển công việc từ trực tiếp giám đốc điều hành và công ty.
-  Được hưởng hoa hồng, tiền thưởng từ các chương trình do công ty đưa ra.
-  Được quy chuẩn lại cơ sở theo hệ thống Ihealing : không gian, chuyên môn,
  dịch vụ….
  `,
  nghiaVu: `-  Đặt cọc đối soát tham gia hệ thống iHealing: 10.000.000 đồng.
-  Mua bộ công cụ từ iHealing trị giá 30.000.000 đồng bao gồm:
    01 máy đo sức khỏe
    04 chổi
    01 lược
    01 cốc trị liệu
    01 chiếu nóng
    01 túi đựng đồ
    01 tấm nhựa phủ
    01 bộ dược liệu
-  Thanh toán công nợ 25% phí dịch vụ của khách hàng làm dịch vụ iHealing (trong trường hợp khách hàng thanh toán tại cơ sở liên kết) bằng cách trừ vào khoản cọc đối soát. Khi trừ hết cọc đối soát, CSLK không được nhận khách làm dịch vụ iHealing.
-  Có trách nhiệm nộp cập nhật khoản cọc đối soát, không để khoản cọc đối soát dưới 10.000.00 đồng trước kỳ thanh toán hoa hồng thứ 5 hàng tuần.
-  Có ít nhất từ 3 – 5 chuyên gia đạt chuẩn tại cơ sở liên kết.
  `
};

class ThanhVien extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <Text style={styles.title}>ĐĂNG KÝ CƠ SỞ LIÊN KẾT</Text>
        <Text style={styles.key}>Quyền lợi</Text>
        <Text style={styles.detail}>{dataHR.quyenLoi}</Text>
        <Text style={styles.key}>Nghĩa vụ</Text>
        <Text style={styles.detail}>{dataHR.nghiaVu}</Text>
        <View style={styles.viewBtn}>
          <BaseButton
            title="Đăng kí"
            onButton={() => Linking.openURL(R.strings.URL_REGISTER)}
          />
        </View>
      </ScrollView>
    );
  }
}

export default ThanhVien;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: R.colors.black3,
  },
  title: {
    paddingHorizontal: WIDTH(16),
    marginTop: HEIGHT(24),
    fontWeight: '500',
    fontSize: getFont(20),
    lineHeight: getLineHeight(28),
    color: R.colors.yellow254,
    fontFamily: R.fonts.Roboto,
    letterSpacing: 0.25,
    alignSelf: 'center',
  },
  key: {
    fontSize: getFont(16),
    marginTop: HEIGHT(16),
    lineHeight: getLineHeight(24),
    marginLeft: WIDTH(16),
    fontFamily: R.fonts.Roboto,
    color: R.colors.yellow254,
  },
  detail: {
    marginTop: HEIGHT(8),
    fontSize: getFont(15),
    lineHeight: getLineHeight(22),
    color: R.colors.white,
    fontFamily: R.fonts.Roboto,
    marginLeft: WIDTH(16),
    letterSpacing: 0.25,
  },
  viewBtn: {
    width: getWidth(),
    alignSelf: 'center',
    paddingVertical: HEIGHT(32),
    alignItems: 'center',
  },
});
