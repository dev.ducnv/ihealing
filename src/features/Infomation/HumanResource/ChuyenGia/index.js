import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet, Linking } from 'react-native';
import BaseButton from '../../../../common/Button/BaseButton';
import R from '../../../../assets/R';
import NavigationService from '../../../../routers/NavigationService';
import {
  WIDTH,
  HEIGHT,
  getLineHeight,
  getFont,
  getWidth,
} from '../../../../config';

const dataHR = {
  quyenLoi: `-	Được đào tạo nghề Chăm sóc sức khỏe và sắc đẹp từ cơ bản tới nâng cao, thường xuyên cập nhật và đào tạo nâng cao tay nghề.
-	Được tham gia thi cấp chứng chỉ hành nghề sau khi hoàn thành khóa học.
-	Sau khi tham gia vào hệ thống, mỗi chuyên gia sẽ được cấp 01 mã ID và thẻ thành viên, tiện cho việc quản lý và hưởng các ưu đãi của hệ thống.
-	Được hưởng 20% giá trị sản phẩm từ sản phẩm tiêu hao và sản phẩm bán cho khách hàng.
-	Xây dựng và sở hữu hệ thống khách hàng riêng biệt, có tính kế thừa và nhân rộng.
-	Hưởng 70% phí dịch vụ khi đăng ký nhận khách trên APP iHealing.
-	Được đào tạo và Phát triển bản thân, xây dựng trau dồi kỹ năng giao tiếp, kỹ năng nghề nghiệp, nắm bắt tâm lý khách hàng,...
-	Được đào tạo, hỗ trợ Marketing, đẩy mạnh việc tìm kiếm khách hàng và có nguồn khách hàng thụ động.
-	Luôn có những ưu đãi, chính sách khen thưởng, thi đua từ công ty và hệ thống iHealing.
-	Được hỗ trợ phát triển nhân hiệu, khuếch trương vị thế, nâng cao uy tín trong hệ thống iHealing, cũng như trong ngành trị liệu và chăm sóc sắc đẹp.
-	Được hưởng hệ thống khách hàng lâu dài, có tính kế thừa, nhận tiền hoa hồng vào thứ 5 hàng tuần.`,
  nghiaVu: `-	Tham gia đúng, đủ các khóa học theo theo định của công ty.
-	Thi theo quy định và được cấp chứng chỉ hành nghề trên toàn quốc nếu vượt qua bài kiểm tra của iHealing.
-	Luôn đề cao và tôn trọng đạo đức nghề nghiệp, thực hiện đúng, đủ các quy định chung của iHealing.
-	Không dùng sản phẩm ngoài hệ thống iHealing, hoặc bất kỳ sự kết hợp nào giữa các sản phẩm ngoài và sản phẩm của iHealing trong quá trình trị liệu và chăm sóc khách hàng.
-	Đặt cọc 3 triệu đồng để đối soát doanh thu dịch vụ khi nhận khách trên App iHealing.
-	Mua bộ công cụ 10 triệu.
-	Không từ chối nhận dịch vụ khi hệ thống báo có dịch vụ, nếu hủy quá 5 lần hệ thống sẽ giảm lượt điều phối khách hàng.`,
};

class ThanhVien extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <Text style={styles.title}>ĐĂNG KÝ CHUYÊN GIA</Text>
        <Text style={styles.key}>Quyền lợi</Text>
        <Text style={styles.detail}>{dataHR.quyenLoi}</Text>
        <Text style={styles.key}>Nghĩa vụ</Text>
        <Text style={styles.detail}>{dataHR.nghiaVu}</Text>
        <View style={styles.viewBtn}>
          <BaseButton
            title="Đăng kí"
            onButton={() => Linking.openURL(R.strings.URL_REGISTER)}
          />
        </View>
      </ScrollView>
    );
  }
}

export default ThanhVien;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: R.colors.black3,
  },
  title: {
    paddingHorizontal: WIDTH(16),
    marginTop: HEIGHT(24),
    fontWeight: '500',
    fontSize: getFont(20),
    lineHeight: getLineHeight(28),
    color: R.colors.yellow254,
    fontFamily: R.fonts.Roboto,
    letterSpacing: 0.25,
    alignSelf: 'center',
  },
  key: {
    fontSize: getFont(16),
    marginTop: HEIGHT(16),
    lineHeight: getLineHeight(24),
    marginLeft: WIDTH(16),
    fontFamily: R.fonts.Roboto,
    color: R.colors.yellow254,
  },
  detail: {
    marginTop: HEIGHT(8),
    fontSize: getFont(15),
    lineHeight: getLineHeight(22),
    color: R.colors.white,
    fontFamily: R.fonts.Roboto,
    marginLeft: WIDTH(16),
    letterSpacing: 0.25,
  },
  viewBtn: {
    width: getWidth(),
    alignSelf: 'center',
    paddingVertical: HEIGHT(32),
    alignItems: 'center',
  },
});
