import React from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import R from 'assets/R';
import NavigationService from 'routers/NavigationService';
import { SearchArticle } from 'routers/screenNames';
import moment from 'moment'

import { connect } from 'react-redux';
import ItemNull from 'common/Item/ItemNull';
import HeaderBack from '../../../common/Header/HeaderBack';
import { WIDTH, HEIGHT, getLineHeight, getFont } from '../../../config';

class Training extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      num: 0
    };
  }


  render() {
    // console.log(this.state.num)
    return (
      <View style={{ flex: 1, backgroundColor: R.colors.black3 }}>
        <HeaderBack title="Đào tạo" onButtonSearch={() => NavigationService.navigate(SearchArticle)} />
        {this.state.num === 0 && <ItemNull text="Không có bài viết nào" />}
        <FlatList
          data={this.props.data}
          renderItem={({ item, index }) => {
            if (item.iD_DanhMucBaiViet === 8) {
              this.setState({ num: 1 })
              let image = item.anhDaiDien ? { uri: R.images.IMAGE + item.anhDaiDien } : R.images.logoHadoo
              return (
                <TouchableOpacity
                  onPress={() => NavigationService.navigate('DetailTraining', { item })}
                  style={styles.listDVContainer}
                  activeOpacity={0.6}
                >
                  <FastImage source={image} style={styles.itemDV} />
                  <View style={{ paddingHorizontal: WIDTH(16) }}>
                    <Text style={styles.title} numberOfLines={2}>{item.tenBaiViet && item.tenBaiViet}</Text>
                    <Text style={styles.date}>{item.ngayTao && item.ngayTao && moment(item.ngayTao).format('DD/MM/YYYY')}</Text>
                    <View style={styles.viewLike}>
                      <FastImage source={R.images.iconLike} style={styles.iconLike} resizeMode={FastImage.resizeMode.stretch} />
                      <Text style={styles.like}>{item.luotXem && item.luotXem}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              )
            }
          }}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    // cart: state.cartReducers.cart,
    // amount: state.cartReducers.amount,
    data: state.userReducers.listArticle
  };
}

export default connect(mapStateToProps, {
  // addProduct
})(Training);

const styles = StyleSheet.create({
  title: {
    width: WIDTH(203),
    fontWeight: '500',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.white,
    fontFamily: R.fonts.Roboto
  },

  iconLike: {
    width: WIDTH(17),
    height: HEIGHT(16)
  },
  listDVContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: WIDTH(16),
    paddingTop: HEIGHT(20)
  },
  itemDV: {
    width: WIDTH(124),
    height: WIDTH(124)
  },
  date: {
    fontSize: getFont(14),
    lineHeight: getLineHeight(18),
    color: R.colors.grey400,
    fontFamily: R.fonts.Roboto,
    marginTop: HEIGHT(12)
  },
  like: {
    fontSize: getFont(14),
    lineHeight: getLineHeight(18),
    color: R.colors.white,
    fontFamily: R.fonts.Roboto,
    marginLeft: WIDTH(9)
    // marginTop: HEIGHT(12)
  },
  viewLike: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: HEIGHT(13)
  }
})
