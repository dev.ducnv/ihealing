import React from 'react';
import { View, Text, ScrollView, StyleSheet, ActivityIndicator } from 'react-native';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image';
import R from 'assets/R';
import WebView from 'react-native-webview';
import NavigationService from 'routers/NavigationService';
import { SearchArticle, CartProduct } from 'routers/screenNames';
import { WIDTH, HEIGHT, getLineHeight, getFont, getWidth, getHeight } from '../../../config';

import HeaderBack from '../../../common/Header/HeaderBack';


export default class DetailTraining extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: true
    };
  }

  render() {
    const item = this.props.navigation.getParam('item')
    return (
      <View style={{ flex: 1, backgroundColor: R.colors.black3 }}>
        <HeaderBack title="Chi tiết" onButtonSearch={() => NavigationService.navigate(SearchArticle)} onButtonCart={() => NavigationService.navigate(CartProduct)} />
        <WebView
          onLoad={() => { this.setState({ visible: false }) }}
          style={{ flex: 1, backgroundColor: R.colors.black3 }}
          source={{ uri: item.contentHTML }}
        />
        {
          this.state.visible && (
            <View style={{ flex: 1, backgroundColor: R.colors.black3, position: 'absolute', top: HEIGHT(50), left: 0, height: getHeight(), width: getWidth(), zIndex: 1000 }}>
              <ActivityIndicator
                animating={this.state.visible}
                color={R.colors.yellow254}
                size={WIDTH(40)}
                style={{ position: 'absolute', top: HEIGHT(200), left: WIDTH(160) }}
                hidesWhenStopped={true}
              />
            </View>
          )
        }
      </View>
    );
  }
}
