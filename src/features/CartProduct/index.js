import React from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import { connect } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import AntDesign from 'react-native-vector-icons/AntDesign';
import i18n from 'assets/languages/i18n';
import moment from 'moment';
import _ from 'lodash';
import { postCreateInvoice, postBookCalendar } from 'apis';
import global from 'features/global';
import { Switch } from 'native-base'
import ModalBooking from '../../common/Modal/ModalBooking';
import { LoadingComponent } from '../../common/Loading/LoadingComponent';
import R from '../../assets/R';
import NavigationService from '../../routers/NavigationService';
import { PaymentOrders, Home } from '../../routers/screenNames';
import HeaderBack from '../../common/Header/HeaderBack';
import { WIDTH, popupOk, HEIGHT, formatVND } from '../../config';
import ItemCart from './Items/ItemCart';
import BaseButton from '../../common/Button/BaseButton';
import {
  addProduct,
  minusProduct,
  setProduct,
  deleteProduct,
  getLichDangCho,
  setAccount
} from '../../actions';
import { styles } from './styles';
import FormInput from '../../common/Form/FormInput';
import ItemInputPoint from './Items/ItemInputPoint'
import AsyncStorageUtils from '../../helpers/AsyncStorageUtils';

class CartProduct extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      reRender: false,
      loading: false,
      visible: false,
      maDon: '',
      tongtien: 0,
      isUsePoint: false,
      msg: ''
    };
    this.cPrice = 0;
    this.discount = 0;
    this.sale = 0
    this.nguoiGioiThieu = '';
    this.typePayment = 1;
    // console.log('props_props', props.Account)
    this.formPayment = [
      {
        image: R.images.ic_user,
        hint: props.Account.tenDayDu || 'Tên đầy đủ',
        text: props.Account.tenDayDu || '',
        func: this.changeName,
      },
      {
        image: R.images.ic_person_id,
        hint: `Điểm tích lũy: ${formatVND(props.Account?.tichDiem ?? 0)}`,
        text: `Điểm tích lũy: ${formatVND(props.Account?.tichDiem ?? 0)}`,
        func: this.changeName,
        editable: true,
        hidden: props.Account.tichDiem === undefined
      },
      {
        image: R.images.icAddress,
        hint: props.Account.diaChi || 'Địa chỉ nhận hàng',
        text: props.Account.diaChi || '',
        func: this.changeAdd,
      },
      {
        image: R.images.iconNote,
        hint: 'Ghi chú đơn hàng',
        text: '',
        func: this.changeNote,
      },
      {
        image: R.images.ic_phone,
        hint: props.Account.dienThoai || 'Số điện thoại',
        isKeyboadNum: true,
        func: this.changePhone,
        text: props.Account.dienThoai || '',
      },
      {
        image: R.images.ic_email,
        hint: props.Account.email || 'Email',
        func: this.changeMail,
        text: props.Account.email || '',
      },
      {
        image: R.images.icPayment,
        hint: 'Hình thức thanh toán',
        value: 'Thanh toán trực tiếp cho cấp trên',
        editable: true,
        func: this.changePicker,
      }
    ];
    this.listBooking = [];
  }

  changeName = (text) => {
    this.formPayment[0].text = text;
  };

  changeAdd = (text) => {
    this.formPayment[2].text = text;
  };

  changeNote = (text) => {
    this.formPayment[3].text = text;
  };

  changePhone = (text) => {
    this.formPayment[4].text = text;
  };

  changeMail = (text) => {
    this.formPayment[5].text = text;
  };

  changePicker = (valuePicker) => {
    // console.log('valuePicker', valuePicker)
    this.typePayment = valuePicker + 1;
  };

  usePointSave = (text) => {
    this.sale = text

    this._reRender();
  };

  componentWillMount() {
    // let cPrice = 0
    // this.props.cart.map((item, index) => {
    //   cPrice += item.item.giaLe * item.total
    // })
    // this.setState({ listBooking: this.props.cart })
    this.props.cart.map((item, index) => {
      this.listBooking.push({
        time: {
          value: '',
          func: (time) => {
            this._reRender();
            this.listBooking[index].time.value = time.value;
          },
        },
        date: {
          value: '',
          func: (date) => {
            this._reRender();
            this.listBooking[index].date.value = date;
          },
        },
        address: {
          value: '',
          item: null,
          func: (dataDiachiDaydu) => {
            this._reRender();
            this.listBooking[index].address.value = dataDiachiDaydu.DiaChi;
            this.listBooking[index].address.item = dataDiachiDaydu;
          },
        },
        employee: {
          value: '',
          func: (type) => {
            this._reRender();
            this.listBooking[index].employee.value = type;
          },
        },
        idEmployee: {
          value: '',
          item: null,
          func: (value, element) => {
            this._reRender();
            this.listBooking[index].idEmployee.value = value;
            this.listBooking[index].idEmployee.item = element;
          },
        },
        note: {
          value: '',
          func: (text) => {
            this._reRender();
            this.listBooking[index].note.value = text;
          },
        },
        locationSelect: { value: -1, func: (locationSelect, indexLocation) => { this._reRender(); this.listBooking[index].locationSelect.value = indexLocation } },

      });
    });
    // console.log(this.props.cart)
  }

  _reRender = () => {
    this.setState({ reRender: !this.state.reRender });
  };

  onIncreaseProduct = (item, indexOfPro) => {
    this.props.addProduct(item);
    this._reRender();
  };

  onAddNguoiGioiThieu = (people) => {
    this.nguoiGioiThieu = people;
    this._reRender();
  };

  onPressClose = (item, indexOfPro) => {
    this.props.deleteProduct(indexOfPro);
    this._reRender();
    // this.ItemCart.bookCalendar()
  };

  onDecreaseProduct = (item, indexOfPro) => {
    this.props.minusProduct(indexOfPro);
  };

  onBuyMore = () => { };

  bookCalendar = (ID_DonHang) => {
    console.log('vaodayy')
    this.props.cart.map(async (item, index) => {
      if (item.total === 1 && item.item.isDichVu === 1) {
        if (
          this.listBooking[index].time.value
        ) {
          const parseDate = this.listBooking[index].date.value
            ? this.listBooking[index].date.value.split('/')
            : moment(new Date())
              .format('DD/MM/YYYY')
              .split('/');
          const parseTime = this.listBooking[index].time.value.split('h');
          const BatDau = moment(new Date(parseDate[2], parseDate[1] - 1, parseDate[0], parseTime[0], parseTime[1], 0, 0)).format('YYYY-MM-DD HH:mm:ss');
          const KetThuc = moment(new Date(parseDate[2], parseDate[1] - 1, parseDate[0], parseInt(parseTime[0], 10) + 1, parseTime[1], 0, 0)).format('YYYY-MM-DD HH:mm:ss')
          const Ngay = moment(new Date(parseDate[2], parseDate[1] - 1, parseDate[0], parseTime[0], parseTime[1], 0, 0)).format('YYYY-MM-DD HH:mm:ss')
          let param;
          if (this.listBooking[index].locationSelect.value === 1) {
            param = {
              ID_DonHang,
              BatDau,
              GhiChu: this.listBooking[index].note.value,
              ID_DichVu: item.item.idMatHang,
              ID_KhachHang: this.props.Account.iD_QuanLy,
              ID_NhanVien: this.listBooking[index]?.idEmployee?.item?.iD_QuanLy || -1,
              KetThuc,
              Ngay,
              DiaChi: this.listBooking[index]?.idEmployee?.item?.diaChi || '',
              ViDo: this.listBooking[index]?.idEmployee?.item?.viDo || 0,
              KinhDo: this.listBooking[index]?.idEmployee?.item?.kinhDo || 0,
              ID_NguoiGioiThieu: this.nguoiGioiThieu?.iD_QuanLy ?? 0
            }
          } else {
            param = {
              ID_DonHang,
              BatDau,
              GhiChu: this.listBooking[index].note.value,
              ID_DichVu: item.item.idMatHang,
              ID_KhachHang: this.props.Account.iD_QuanLy,
              ID_NhanVien: this.listBooking[index].idEmployee.item
                ? this.listBooking[index].idEmployee.item.iD_QuanLy
                : -1,
              KetThuc,
              Ngay,
              ...this.listBooking[index].address.item,
              ViDo: 0,
              KinhDo: 0,
              ID_NguoiGioiThieu: this.nguoiGioiThieu?.iD_QuanLy ?? 0
            }
          }
          console.log('param', param)
          let resBooking = await postBookCalendar(`?${JSON.stringify(param)}`);
          let msg2 = _.has(resBooking, 'data.txt') ? resBooking.data.txt : 'Lịch của bạn vẫn chưa thể đặt, vui lòng kiểm tra tại lịch đang chờ';
          this.setState({ msg: msg2 })
        } else {
          this.setState({ msg: 'Lịch của bạn vẫn chưa thể đặt, vui lòng kiểm tra tại lịch đang chờ' })
        }
      }
    });
  };

  onBooking = async () => {
    const { formPayment } = this;
    // console.log('nguoiGioiThieu_nguoiGioiThieu', this.nguoiGioiThieu)
    if (parseFloat(this.sale) > this.props.Account?.tichDiem ?? 0) {
      popupOk('Thông báo', 'Điểm tích lũy sử dụng phải nhỏ hơn điểm tích lũy hiện có!');
    } else {
      this.setState({ loading: true, tongtien: this.cPrice - this.discount });
      let chitietdonhang = [];
      this.props.cart.map((item, index) => {
        chitietdonhang.push({
          giaban: item.item.giaLe,
          giabuon: item.item.giaLe,
          giale: item.item.giaLe,
          hinhthucban: 0,
          idhanghoa: item.item.idMatHang,
          soluong: item.total,
          isDichVu: item.item.isDichVu,
        });
      });
      let body = {
        chietKhauPhanTramKhac: 0,
        chietKhauTienKhac: 0,
        chietkhauphantram: 0,
        chietkhauphantram_theoctkm: 0,
        chietkhautien: 0,
        chietkhautien_theoctkm: 0,
        chitietdonhang,
        email: formPayment[5].text,
        ghichu: formPayment[3].text,
        idctkm: 0,
        phuongthuc: this.typePayment,
        sodienthoai: formPayment[4].text,
        tenkhachhang: formPayment[0].text,
        diachitao: formPayment[2].text,
        truTichDiem: this.state.isUsePoint ? parseFloat(this.sale) : 0,
        // thoigiantao: '2020/07/23 15:33:59',
        thoigiantao: moment(new Date()).format('YYYY/MM/DD HH:mm:ss'),
        tienHang: 0,
        tiendathanhtoan: 0,
        tongtien: this.cPrice,
        tongtienchietkhau: 0,
      };
      let res = await postCreateInvoice(body);
      // let res = null
      await this.bookCalendar(_.get(res, 'data.data.iddonhang', ''));
      this.setState({ loading: false });
      if (
        _.has(res, 'data.success')
        && _.has(res, 'data.data.mathamchieu')
      ) {
        this.props.getLichDangCho(
          `?idKhachHang=${this.props.Account.iD_QuanLy}`
        );
        this.onRefreshAccount()
        global.reloadHilight && global.reloadHilight();
        if (body.phuongthuc === 2) {
          NavigationService.navigate(PaymentOrders, {
            mathamchieu: res.data.data.mathamchieu,
          });
          this.props.setProduct([], 0);
        } else {
          this.setState(
            { maDon: res.data.data.mathamchieu, tongtien: res.data.data.tongtien, visible: true },
            () => {
              this.props.setProduct([], 0);
            }
          );
        }
      } else {
        popupOk('Thông báo', _.get(res, 'data.txt', 'Không thể đặt hàng, vui lòng kiểm tra kết nối'));
      }
    }

    // NavigationService.navigate(PaymentOrders)
  };

  onRefreshAccount = async () => {
    let account = {
      ...this.props.Account,
      tichDiem: this.props.Account?.tichDiem - (this.state.isUsePoint ? parseFloat(this.sale) : 0)
    }

    this.props.setAccount(account)
    await AsyncStorageUtils.multiSet([
      [R.strings.INIT_STORGE, JSON.stringify(account)],
    ]);
  }

  onShowModal = () => {
    if (this.state.visible) NavigationService.navigate(Home);
    this.setState({ visible: !this.state.visible });
  };

  checkDiemTichLuy = () => {
    if (this.props.isDichVu) return this.props.Account?.suDungDiem_DichVu === 1
    else return this.props.Account?.suDungDiem_HangHoa === 1
  }

  render() {
    this.cPrice = 0;
    this.props.cart.map((item, index) => {
      this.cPrice += item.item.giaLe * item.total;
    });
    if (this.props.Account.iD_NhomTaiKhoan && !this.props.isDichVu) {
      this.discount = this.cPrice * 10 / 100;
    }
    // console.log('Account_Account', this.props.Account)

    // console.log('sale_sale', sale, this.cPrice, sale !== '', this.cPrice - (sale !== '' ? parseFloat(sale) : 0))

    // this.formPayment[8].value = String(this.cPrice - 200 - (sale !== '' ? parseFloat(sale) : 0));
    if (!this.state.loading) {
      return (
        <ScrollView showsVerticalScrollIndicator={false}>
          <HeaderBack title="Giỏ hàng" iconSearch={false} iconCart={false} />
          <ModalBooking
            visible={this.state.visible}
            onShowModal={this.onShowModal}
            maDon={this.state.maDon}
            tongTien={this.state.tongtien}
            msg={this.state.msg}
            thanhToan={this.typePayment - 1}
          />

          <View style={styles.container}>
            <FlatList
              data={this.props.cart}
              extraData={this.state}
              renderItem={({ item, index }) => (
                <ItemCart
                  item={item}
                  dataBooking={this.listBooking[index]}
                  image={item.item.anhDaiDien}
                  name={item.item.tenHang}
                  price={item.item.giaLe}
                  total={item.total}
                  unit={item.item.tenDonVi}
                  typeProduct={item.item.isDichVu}
                  onPressClose={() => this.onPressClose(item.item, index)}
                  onDecreaseProduct={() => this.onDecreaseProduct(item.item, index)
                  }
                  onIncreaseProduct={() => this.onIncreaseProduct(item.item, index)
                  }
                  onAddNguoiGioiThieu={this.onAddNguoiGioiThieu}
                />
              )}
            />
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 1 }}
              colors={[
                R.colors.yellow1c8,
                R.colors.yellow254,
                R.colors.yellow1c8,
              ]}
              style={[{ width: WIDTH(375) }]}
            >
              <View style={[styles.flexRow, { width: WIDTH(375) }]}>
                <Text style={styles.textTotalMoney}>Tổng tiền:</Text>
                <Text style={styles.textBigPrice}>
                  {`${formatVND(this.cPrice)} VNĐ`}
                </Text>
              </View>
              {
                this.checkDiemTichLuy() && (
                  <>
                    <View style={styles.viewTichLuy}>
                      <Text style={styles.textTotalMoney}>Dùng điểm tích lũy:</Text>
                      <Switch onValueChange={val => this.setState({ isUsePoint: val })} value={this.state.isUsePoint} />
                    </View>
                    {
                      this.state.isUsePoint && (
                        <ItemInputPoint onChangeText={this.usePointSave} item={this.formPayment[7]} totalMoney={this.cPrice - this.discount} />
                      )
                    }
                  </>
                )
              }

            </LinearGradient>
            {!this.props.isDichVu
              && (
                <Text style={styles.textInforPayment}>Thông tin thanh toán</Text>
              )}
            {/* this.props.Account.iD_QuanLy && !this.props.isDichVu && <Text style={styles.textDesmore}>Bạn nhận được 10% chiết khấu từ iHealing!</Text>
            !this.props.Account.iD_QuanLy && !this.props.isDichVu && <Text style={styles.textDesmore}>Tạo tài khoản để nhận được 10% chiết khấu từ iHealing!</Text> */}

            {!this.props.isDichVu
              && (
                <View style={{ paddingLeft: WIDTH(24) }}>
                  <FormInput dataForm={this.formPayment} />
                </View>
              )}
            <View style={styles.bottomButton}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  width: WIDTH(343),
                }}
              >
                <View style={styles.flexCenter} />
                {this.cPrice > 0 && (
                  <BaseButton
                    title="Đặt hàng"
                    onButton={this.onBooking}
                    width={WIDTH(163)}
                    height={HEIGHT(53)}
                    hasImage
                  />
                )}
              </View>
            </View>
          </View>
        </ScrollView>
      );
    } else {
      return (
        <View
          showsVerticalScrollIndicator={false}
          style={{ flex: 1, backgroundColor: R.colors.background }}
        >
          <HeaderBack title="Giỏ hàng" iconSearch={false} iconCart={false} />
          <LoadingComponent isLoading={this.state.loading} />
        </View>
      );
    }
  }
}

function mapStateToProps(state) {
  return {
    cart: state.cartReducers.cart,
    amount: state.cartReducers.amount,
    Account: state.userReducers.Account,
    isDichVu: state.cartReducers.isDichVu
  };
}

export default connect(mapStateToProps, {
  addProduct,
  minusProduct,
  setProduct,
  deleteProduct,
  getLichDangCho,
  setAccount
})(CartProduct);
