import { StyleSheet } from 'react-native';
import R from 'assets/R';

import { WIDTH, getFont, HEIGHT, getLineHeight } from '../../config';


export const styles = StyleSheet.create({
  container: {
    width: WIDTH(375),
    paddingVertical: WIDTH(12),
    backgroundColor: R.colors.backgroundBlack,
    alignSelf: 'center'
  },
  viewTichLuy: {
    width: WIDTH(375),
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: HEIGHT(15),
    alignItems: 'center',
    paddingBottom: HEIGHT(10),
    marginTop: HEIGHT(15)
  },
  textPrice: {
    color: R.colors.yellow254,
    fontSize: getFont(16),
    fontFamily: R.fonts.Roboto,
    marginBottom: HEIGHT(6)
  },
  itemProduct: {
    width: WIDTH(323),
    flexDirection: 'row',
    alignItems: 'center',

  },
  imageProduct: {
    height: WIDTH(96),
    width: WIDTH(96)
  },
  textDes: {
    color: R.colors.white,
    fontSize: getFont(14),
    fontFamily: R.fonts.Roboto,
    width: WIDTH(207)
  },
  textBigPrice: {
    color: R.colors.black0,
    fontSize: getFont(22),
    fontFamily: R.fonts.Roboto,
  },
  textTotalMoney: {
    color: R.colors.black0,
    fontSize: getFont(16),
    fontFamily: R.fonts.Roboto,
  },
  viewInfor: {
    marginLeft: WIDTH(16)
  },
  textInforPayment: {
    color: R.colors.white,
    fontSize: getFont(16),
    fontFamily: R.fonts.Roboto,
    marginTop: HEIGHT(20),
    marginLeft: WIDTH(16)
  },
  flexRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: HEIGHT(8),
    paddingHorizontal: HEIGHT(15),
    alignItems: 'center',
    // borderWidth: 1
  },
  btnStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: R.colors.yellow254,
    borderRadius: WIDTH(100)
  },
  title: {
    fontSize: getFont(16),
    color: R.colors.yellow254,
    fontWeight: '500',
    lineHeight: getLineHeight(24)
  },

  flexCenter: {
    flexDirection: 'row',
    alignItems: 'center',
    width: WIDTH(163),
    paddingVertical: HEIGHT(12),
    paddingHorizontal: WIDTH(26),
    height: HEIGHT(48),
    // borderColor: R.colors.yellow254,
    // borderWidth: 1,
    // borderRadius: WIDTH(100),
    justifyContent: 'space-between'
  },
  bottomButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: R.colors.black050,
    height: HEIGHT(250),
    marginTop: HEIGHT(10)
  },
  textDesmore: {
    color: R.colors.yellow254,
    fontSize: getFont(13),
    fontFamily: R.fonts.Roboto,
    marginTop: HEIGHT(3),
    marginLeft: WIDTH(16),
    fontStyle: 'italic'
  }
})
