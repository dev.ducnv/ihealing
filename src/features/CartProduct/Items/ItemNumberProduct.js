import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import R from '../../../assets/R';
import { HEIGHT, WIDTH, getFont } from '../../../config';

export default class Home extends React.PureComponent {
  render() {
    const { onPressMinus, onPressAdd, onPressClose, total, unit, } = this.props
    return (
      <View style={styles.container}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 1 }}
          colors={R.colors.linearButtonYellow}
          style={styles.linearView}
        >
          <TouchableOpacity
            onPress={onPressMinus}
            hitSlop={styles.hitSlop}
          >
            <Entypo size={HEIGHT(26)} color={R.colors.colorIconBlack} name="minus" />
          </TouchableOpacity>
          <View style={styles.boxNumber}>
            <Text style={styles.textNumber}>
              {total}
            </Text>
          </View>
          <TouchableOpacity
            onPress={onPressAdd}
            hitSlop={styles.hitSlop}
            style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Entypo size={HEIGHT(26)} color={R.colors.colorIconBlack} name="plus" />
          </TouchableOpacity>
        </LinearGradient>
        <View style={styles.viewRight} />
        <TouchableOpacity
          onPress={onPressClose}
          style={styles.boxClose}
          hitSlop={{ top: 10, left: 10, right: 10, bottom: 10 }}
        >
          <EvilIcons name="close" color={R.colors.gray82} size={HEIGHT(25)} />
        </TouchableOpacity>
      </View>

    );
  }
}
const styles = StyleSheet.create({
  container: {
    width: WIDTH(207),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  boxNumber: {
    backgroundColor: R.colors.colorIconBlack,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    width: WIDTH(36),
    height: HEIGHT(25)
  },
  textNumber: {
    color: R.colors.yellow254,
    fontSize: getFont(14),
    fontFamily: R.fonts.Roboto
  },
  hitSlop: {
    top: 20,
    left: 10,
    right: 10,
    bottom: 20
  },
  linearView: {
    height: HEIGHT(28),
    width: WIDTH(92),
    borderRadius: HEIGHT(4),
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  viewRight: {
    flexDirection: 'row',
    marginLeft: WIDTH(16),
    justifyContent: 'space-between'
  },
  textDes: {
    color: R.colors.grey300,
    fontSize: getFont(14),
    fontFamily: R.fonts.Roboto,
  },
  boxClose: {
    height: WIDTH(28),
    width: WIDTH(28),
    borderColor: R.colors.gray82,
    borderWidth: 0.5,
    borderRadius: WIDTH(4),
    justifyContent: 'center',
    alignItems: 'center'
  }
})
