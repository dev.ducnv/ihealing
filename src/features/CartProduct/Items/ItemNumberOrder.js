import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Svg, {
  Path,
  LinearGradient,
  Stop,
} from 'react-native-svg';
import R from '../../../assets/R';
import { WIDTH, getFont, HEIGHT } from '../../../config';


export default class SvgExample extends React.PureComponent {
  render() {
    const { total } = this.props
    return (
      <View style={{ height: WIDTH(40), width: WIDTH(40), position: 'absolute', left: 0, top: 0, zIndex: 1 }}>
        <Svg width={WIDTH(40)} height={WIDTH(40)}>
          <LinearGradient
            id="Gradient"
            //   gradientUnits="userSpaceOnUse"
            x1="0%"
            y1="0%"
            x2="100%"
            y2="0%"
          >
            <Stop offset="0" stopColor={R.colors.yellow1c8} stopOpacity="0.5" />
            <Stop offset="0.5" stopColor={R.colors.yellow254} stopOpacity="0.8" />
            <Stop offset="1" stopColor={R.colors.yellow1c8} stopOpacity="1" />
          </LinearGradient>
          <Path
            d={`M0 0 L${WIDTH(40)} 0 C ${WIDTH(40)} 0, ${WIDTH(40)} ${WIDTH(30)} 0 ${WIDTH(40)}`}
            fill="url(#Gradient)"
            stroke="none"
          />
          <Text style={styles.textDes}>
            {total && total}
          </Text>
        </Svg>
      </View>

    );
  }
}
const styles = StyleSheet.create({
  textDes: {
    color: R.colors.black0,
    fontSize: getFont(14),
    fontFamily: R.fonts.Roboto,
    marginTop: HEIGHT(8),
    marginLeft: HEIGHT(15)
  },
})
