import React from 'react';
import { View, Text, StyleSheet, FlatList, Image } from 'react-native';
import ModalPickLocation from 'common/Modal/ModalPickLocation';
import ModalSearch from 'common/Modal/ModalSearch';
import R from '../../../assets/R';
import ItemMultiPicker from '../../../common/Item/ItemMultiPicker';
import { WIDTH, getFont, HEIGHT, formatVND } from '../../../config';
import ItemNumberProduct from './ItemNumberProduct'
import ItemNumberOrder from './ItemNumberOrder'
import PickerSearch from '../../../common/Picker/PickerSearch';

function ItemProduct(props) {
  const { name, image, price, unit, total, onPressClose, onIncreaseProduct, onDecreaseProduct } = props
  return (
    <View style={styles.itemProduct}>
      <Image source={{ uri: image && R.images.IMAGE + image }} style={styles.imageProduct} />
      <View style={styles.viewInfor}>
        <Text style={styles.textDes}>
          {name && name}
        </Text>
        <Text style={styles.textPrice}>
          {`${price && formatVND(price)} VNĐ`}
        </Text>
        <ItemNumberProduct
          unit={unit}
          total={total}
          onPressMinus={onDecreaseProduct}
          onPressAdd={onIncreaseProduct}
          onPressClose={onPressClose}
        />
      </View>

    </View>
  )
}

export default class ItemCart extends React.PureComponent {
  renderMoreInfor = (typeProduct) => {
    const { onAddNguoiGioiThieu } = this.props
    switch (typeProduct) {
      case 0: {
        return (<View style={{ height: HEIGHT(12) }} />)
      }
      case 1: {
        if (this.props.total === 1) {
          return (<FormProductInfor
            unit={this.props.unit}
            data={this.props.dataBooking}
            onAddNguoiGioiThieu={onAddNguoiGioiThieu}
            showModalLocation={() => this.ModalPickLocation.setModalVisible(true)}
            showModalSearch={() => this.ModalSearch.setModalVisible(true, this.props.dataBooking.locationSelect.value)}
          />)
        } else return (<TextPromotion />)
      }
      case 2: {
        return (<View style={{ height: HEIGHT(12) }} />)
      }
      default:
        return (<View style={{ height: HEIGHT(12) }} />)
    }
  }

  bookCalendar = () => {
  }

  render() {
    const { typeProduct, image, name, price, total, unit, onPressClose, onIncreaseProduct, onDecreaseProduct, dataBooking } = this.props
    return (
      <View style={styles.container}>
        <ModalPickLocation
          ref={ref => { this.ModalPickLocation = ref }}
          onSearch={(dataDiachiDaydu) => { dataBooking.address.func(dataDiachiDaydu) }}
          title="Tìm địa chỉ"
        />

        <ModalSearch
          ref={ref => { this.ModalSearch = ref }}
          onSearch={(value, item) => { dataBooking.idEmployee.func(value, item) }}
          title={this.props.title}
        />
        <ItemNumberOrder total={total} />
        <ItemProduct
          name={name}
          image={image}
          price={price}
          unit={unit}
          total={total}
          onPressClose={onPressClose}
          onIncreaseProduct={onIncreaseProduct}
          onDecreaseProduct={onDecreaseProduct}
        />
        {this.renderMoreInfor(typeProduct)}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    width: WIDTH(343),
    borderRadius: WIDTH(4),
    borderWidth: 1,
    paddingHorizontal: WIDTH(12),
    paddingTop: WIDTH(12),
    backgroundColor: R.colors.backgroundBlack,
    borderColor: R.colors.borderItemCart,
    alignSelf: 'center',
    marginBottom: HEIGHT(12)
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: HEIGHT(12),
    width: WIDTH(319),
    paddingLeft: WIDTH(10),
    borderBottomColor: R.colors.yellow254,
    borderBottomWidth: 1,
    alignSelf: 'center'
  },
  textPrice: {
    color: R.colors.yellow254,
    fontSize: getFont(16),
    fontFamily: R.fonts.Roboto,
    marginBottom: HEIGHT(6)
  },
  itemProduct: {
    width: WIDTH(323),
    flexDirection: 'row',
    alignItems: 'center',

  },
  imageProduct: {
    height: WIDTH(96),
    width: WIDTH(96)
  },
  textDes: {
    color: R.colors.white,
    fontSize: getFont(14),
    fontFamily: R.fonts.Roboto,
    width: WIDTH(207)
  },
  viewInfor: {
    marginLeft: WIDTH(16)
  },
  textBooking: {
    color: R.colors.white,
    fontSize: getFont(14),
    fontFamily: R.fonts.Roboto,
    marginTop: HEIGHT(12),
  },
  boxBooking: {
    marginTop: HEIGHT(16),

  },
  line: {
    width: WIDTH(343),
    borderTopColor: R.colors.white,
    borderTopWidth: 0.5,
    opacity: 0.3,
    marginLeft: -WIDTH(12)
  },
  boxPromotion: {
    borderBottomLeftRadius: HEIGHT(4),
    borderBottomRightRadius: HEIGHT(4),
    backgroundColor: R.colors.black050,
    paddingVertical: HEIGHT(12),
    paddingHorizontal: WIDTH(18),
    marginTop: HEIGHT(16),
    width: WIDTH(341),
    marginLeft: -WIDTH(12)
  }
})
function TextPromotion() {
  return (
    <View style={styles.boxPromotion}>
      <Text style={[styles.textDes, { width: WIDTH(311) }]}>
        Ghi chú: Khi sử dụng nhiều, khách hàng sẽ nhận được những ưu đãi về giá từ
        <Text style={{ color: R.colors.yellow254 }}> ihealing.vn. </Text>
        Khách hàng sẽ được chọn lịch chăm sóc sau khi thanh toán tổng chi phí của tất cả các buổi.
      </Text>
    </View>
  )
}
function FormProductInfor(props) {
  // console.log('object', props.data.locationSelect.value)
  return (
    <View style={styles.boxBooking}>
      <View style={styles.line} />
      <Text style={styles.textBooking}>
        Đặt lịch
      </Text>
      <View style={styles.wrapper}>
        <PickerSearch
          width={WIDTH(300)}
          title="ID hoặc Số điện thoại người giới thiệu"
          onSearch={(body) => {
            console.log('result_search_ne', body);
            props.onAddNguoiGioiThieu(body)
          }}
          checkRequire={false}
          data={[]}
          upDateData={(key) => {}}
          onCheck={() => {}}
          value=""
        />
      </View>
      <ItemMultiPicker
        data={props.data}
        showModalLocation={props.showModalLocation}
        showModalSearch={props.showModalSearch}
      />
      <View style={{ height: HEIGHT(16) }} />
    </View>
  )
}
