import React, { useState } from 'react';
import { StyleSheet, View, FlatList, TextInput, Text } from 'react-native';
import FastImage from 'react-native-fast-image';
import _, { min } from 'lodash';
import { HEIGHT, getFont, getLineHeight, WIDTH, formatVND, numberWithCommas, slitNumber } from '../../../config';
import R from '../../../assets/R';


export default ({ totalMoney, onChangeText }) => {
  const [cPrice] = useState(1000);
  const [point, setPoint] = useState(0);
  const [value, setValue] = useState('');
  const onChange = (text) => {
    console.log('text_text', text)
    if (text !== '') {
      if (parseFloat(text) > totalMoney) {
        setValue(String(totalMoney));
        setPoint(totalMoney)
      } else {
        setValue(text);
        setPoint(text)
      }
    } else {
      setValue(text);
      setPoint(0)
    }
  }
  return (
    <View>
      <View style={styles.wrapper}>
        {/* <FastImage
          style={styles.icon}
          source={R.images.ic_person_id}
          resizeMode={FastImage.resizeMode.contain}
        /> */}
        <Text style={styles.text}>Điểm tích lũy: </Text>
        <TextInput
          placeholder="Nhập điểm tích lũy"
          autoCapitalize="none"
          style={styles.textInput}
          value={numberWithCommas(value)}
          keyboardType="numeric"
                // autoCapitalize={item.isCharacters ? 'characters' : 'none'}
          placeholderTextColor={R.colors.black5}
          onChangeText={(text) => {
            let newText = slitNumber(text)
            onChange(newText);
            onChangeText(newText)
          }}
        //   defaultValue={item.value}
        />
      </View>
      <View style={[styles.flexRow, { width: WIDTH(375) }]}>
        <Text style={styles.textTotalMoney}>Tổng tiền cần thanh toán:</Text>
        <Text style={styles.textBigPrice}>
          {`${formatVND(
            Math.max(totalMoney - point, 0)
          )} VNĐ`}
        </Text>
      </View>
    </View>

  )
}
const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: HEIGHT(12),
    width: WIDTH(327),
    paddingLeft: WIDTH(10),
    borderBottomColor: R.colors.black0,
    borderBottomWidth: 1,
    alignSelf: 'center'
  },
  textTotalMoney: {
    color: R.colors.black0,
    fontSize: getFont(16),
    fontFamily: R.fonts.Roboto,
  },
  flexRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: HEIGHT(8),
    padding: HEIGHT(15),
    alignItems: 'center'
  },
  textBigPrice: {
    color: R.colors.black0,
    fontSize: getFont(22),
    fontFamily: R.fonts.Roboto,
  },
  icon: {
    width: WIDTH(20),
    height: HEIGHT(15),
    marginBottom: HEIGHT(5)
  },
  textInput: {
    flex: 1,
    color: R.colors.black0,
    fontSize: getFont(15),
    lineHeight: getLineHeight(22),
    marginLeft: WIDTH(14),
    paddingBottom: HEIGHT(10),
    height: HEIGHT(48)
    // paddingTop: Platform.OS === 'ios' ? 0 : HEIGHT(8)
  },
  info: {
    fontFamily: R.fonts.Roboto,
    fontWeight: '500',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.nameText,
    paddingVertical: HEIGHT(10),
    marginLeft: WIDTH(14)
  },
  text: {
    fontFamily: R.fonts.Roboto,
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.black0,
    paddingVertical: HEIGHT(10),
  },
  iconRequire: {
    position: 'absolute',
    top: HEIGHT(10),
    color: R.colors.orange726,
    left: WIDTH(30),
    fontSize: getFont(16)
  }
})
