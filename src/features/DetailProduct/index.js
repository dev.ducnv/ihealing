import React from 'react';
import { View, Text, ScrollView, StyleSheet, Alert } from 'react-native';
import R from 'assets/R';
import NavigationService from 'routers/NavigationService';
import { CartProduct } from 'routers/screenNames';
import { getUserInfo, postUpdateInfo, getDetailProductById, getOtherProduct } from 'apis';
import _ from 'lodash'
import i18n from 'assets/languages/i18n';
import AutoHeightWebView from 'react-native-autoheight-webview'
import { connect } from 'react-redux';
import { addProduct } from 'actions';
import { LoadingComponent } from '../../common/Loading/LoadingComponent';
import HeaderBack from '../../common/Header/HeaderBack';
import { getWidth, HEIGHT, WIDTH, getFont, formatVND } from '../../config';
import ImageViewer from './Items/ImageViewer'
import GeneralInformation from './Items/GeneralInformation'
import OtherInformation from './Items/OtherInformation'


class DetailProduct extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      reRender: false,
      loading: true,
      total: 1,
      heightWebview: '0',

      otherSize: [
      ],
      selectedSize: null,
      selectedItem: null,
    };
    this.detailProduct = {
      image: [
        {
          uri: 'https://thuthuatnhanh.com/wp-content/uploads/2019/07/anh-girl-xinh-facebook-tuyet-dep-387x580.jpg'
        },
        {
          uri: 'https://thuthuatnhanh.com/wp-content/uploads/2019/07/anh-girl-xinh-facebook-tuyet-dep-387x580.jpg',
        },
        {
          uri: 'https://thuthuatnhanh.com/wp-content/uploads/2019/07/anh-girl-xinh-facebook-tuyet-dep-387x580.jpg',
        }
      ],
      name: 'Thải độc dành cho đau vai gáy và đốt sống cổ',
      price: '1,380,000',
      typeProduct: 0,
      total: 1,
      unit: 'buổi',
      code: 'Mã: MHDV192c',
      detailInformation: {
        congDung: 'Làm sạch và thu nhỏ lỗ chân lông',
        doiTuong: 'Dành cho người có lỗ chân lông to, lỗ chân lông nhiều bụi bẩn.',
        thanhPhan: ['Tinh dầu Xô thơm (Clary sage Essential oil)', 'Tinh dầu hạt cà rốt (Carrot seed)', 'Tinh dầu bạc hà (Peppermint)', 'Tinh dầu Đinh hương (Eugenia caryophyllata Oil)', 'Tinh dầu Long não (Aetheroleum Cinnamomi camphorae)']
      }
    }
  }

  _reRender = () => {
    this.setState({ reRender: !this.state.reRender })
  }

  componentDidMount = () => {
    let item = this.props.navigation.getParam('item')
    this.getData(item.idMatHang)
    // console.log('item', item.maHang)
    if (item.maHang.includes('a') || item.maHang.includes('b') || item.maHang.includes('c')
      || item.maHang.includes('d') || item.maHang.includes('e') || item.maHang.includes('f')) {
      this.getOtherById(item.idMatHang)
    }
  }

  getData = async (id) => {
    let querry = `?ID_MatHang=${id}`
    let detailed = await getDetailProductById(querry)
    // console.log('getDetailProductById', detailed)
    if (_.has(detailed, 'data')) {
      this.detailProduct = detailed.data;
      this.setState({ loading: false, selectedItem: detailed.data })
    }
  }

  getOtherById = async (id) => {
    let querry = `?ID_MatHang=${id}`
    let otherProduct = await getOtherProduct(querry)
    if (_.has(otherProduct, 'data')) {
      this.setState({ otherSize: otherProduct.data })
    }
  }

  onIncreaseProduct = () => {
    this.setState({ total: this.state.total + 1 })
  }

  onDecreaseProduct = () => {
    this.setState({ total: this.state.total - 1 })
  }

  onAddFavorites = () => {

  }

  onAddToCart = () => {
    for (let index = 0; index < this.state.total; index++) {
      if (this.props.isDichVu === this.state.selectedItem?.isDichVu || this.props.amount === 0) {
        this.props.addProduct({
          ...this.state.selectedItem,
          tenHang: this.detailProduct.tenHang.substring(0, this.detailProduct.tenHang.indexOf('(')) + this.state.selectedItem.tenHang
        })
        this.state.selectedItem?.isDichVu && NavigationService.navigate(CartProduct)
      } else Alert.alert('Giỏ hàng không thế bao gồm cả dịch vụ và sản phẩm')
    }
  }


  render() {
    if (!this.state.loading) {
      return (
        <View style={{ flex: 1, backgroundColor: R.colors.black3 }}>
          <HeaderBack title="Sản phẩm chăm sóc" onButtonCart={() => NavigationService.navigate(CartProduct)} iconSearch={false} />
          <ScrollView style={{ flex: 1 }}>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: R.colors.black3 }}>
              <ImageViewer listImage={_.has(this.state.selectedItem, 'danhSachAnh') ? this.state.selectedItem.danhSachAnh.split(';') : []} />
              <GeneralInformation
                name={_.has(this.detailProduct, 'tenHang') ? this.detailProduct.tenHang : i18n.t('NULL_T')}
                price={_.has(this.state.selectedItem, 'giaLe') ? formatVND(this.state.selectedItem.giaLe) : i18n.t('NULL_T')}
                typeProduct={_.has(this.detailProduct, 'isDichVu') ? this.detailProduct.isDichVu : i18n.t('NULL_T')}
                total={this.state.total}
                unit={_.has(this.detailProduct, 'tenDonVi') ? this.detailProduct.tenDonVi : i18n.t('NULL_T')}
                code={_.has(this.state.selectedItem, 'maHang') ? this.state.selectedItem.maHang : i18n.t('NULL_T')}
                onIncreaseProduct={this.onIncreaseProduct}
                onDecreaseProduct={this.onDecreaseProduct}
                onAddFavorites={this.onAddFavorites}
                onAddToCart={this.onAddToCart}
                otherSize={this.state.otherSize}
                selectedSize={this.state.selectedSize}
                onButtonChangeSize={(item, index) => { this.setState({ selectedSize: index, selectedItem: item }) }}
                isDichVu={this.props.isDichVu}
              />


              <Text style={styles.textLabel}>Thông tin sản phẩm</Text>

              <AutoHeightWebView
                style={{
                  backgroundColor: R.colors.black3,
                  // marginTop: 5,
                  // marginBottom: 5,
                  marginHorizontal: WIDTH(16),
                  marginTop: HEIGHT(16),
                  // height: parseInt(this.state.heightWebview, 10),
                  // height: 600,
                  width: WIDTH(343),
                  // flex: 1
                }} // customScript={'document.body.style.background = \'lightyellow\';'}
                // onSizeUpdated={({ size => console.log(size.height)})},
                source={{ html: _.has(this.detailProduct, 'moTa') ? `<body style="background-color:#0D0D31;">${this.detailProduct.moTa}</body>` : 'Trống' }}
                // scalesPageToFit={true}
                customStyle={`* {color: white; font-size: ${getFont(16)}px;}`}
                viewportContent="width=device-width, user-scalable=no"
              // originWhitelist={['*']}
              />

              <View style={styles.lineBlack} />


              <OtherInformation
                detailInformation={this.detailProduct.detailInformation}
              />

            </View>
          </ScrollView>
        </View>
      );
    } else {
      return (
        <View style={{ flex: 1 }}>
          <HeaderBack title="Sản phẩm chăm sóc" iconSearch={false} />
          <LoadingComponent isLoading={this.state.loading} />
        </View>
      )
    }
  }
}
function mapStateToProps(state) {
  return {
    // cart: state.cartReducers.cart,
    amount: state.cartReducers.amount,
    isDichVu: state.cartReducers.isDichVu

  };
}

export default connect(mapStateToProps, {
  addProduct
})(DetailProduct);
const styles = StyleSheet.create({
  lineBlack: {
    height: HEIGHT(8),
    backgroundColor: R.colors.colorIconBlack,
    width: getWidth()
  },
  textLabel: {
    color: R.colors.yellow254,
    fontSize: getFont(16),
    fontFamily: R.fonts.Roboto,
    paddingHorizontal: WIDTH(16),
    alignSelf: 'flex-start',
    paddingTop: HEIGHT(16)
  },
})
