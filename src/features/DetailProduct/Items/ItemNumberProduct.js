import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import R from 'assets/R';
import Entypo from 'react-native-vector-icons/Entypo'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { HEIGHT, WIDTH, getFont } from '../../../config';

export default class ItemNumberProduct extends React.PureComponent {
  render() {
    const { onPressMinus, onPressAdd, total } = this.props
    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 1 }}
        colors={R.colors.linearButtonYellow}
        style={{
          height: HEIGHT(48),
          width: WIDTH(130),
          borderRadius: HEIGHT(4),
          flexDirection: 'row',
          justifyContent: 'space-around',
          alignItems: 'center'
        }}
      >
        <TouchableOpacity
          onPress={onPressMinus}
          hitSlop={styles.hitSlop}
        >
          <Entypo size={HEIGHT(30)} color={R.colors.colorIconBlack} name="minus" />
        </TouchableOpacity>
        <View style={styles.boxNumber}>
          <Text style={styles.textNumber}>
            {/* {numOfProduct} */}
            {total && total}
          </Text>
        </View>
        <TouchableOpacity
          onPress={onPressAdd}
          hitSlop={styles.hitSlop}
          style={{
            // width: WIDTH(45),
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <Entypo size={HEIGHT(30)} color={R.colors.colorIconBlack} name="plus" />
        </TouchableOpacity>
      </LinearGradient>
    );
  }
}
const styles = StyleSheet.create({
  boxNumber: {
    backgroundColor: R.colors.colorIconBlack,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    width: WIDTH(40),
    height: HEIGHT(44)
  },
  textNumber: {
    color: R.colors.yellow254,
    fontSize: getFont(20),
    fontFamily: R.fonts.Roboto
  },
  hitSlop: {
    top: 20,
    left: 10,
    right: 10,
    bottom: 20
  }
})
