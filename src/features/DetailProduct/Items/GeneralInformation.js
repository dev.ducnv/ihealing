import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import R from 'assets/R';
import AntDesign from 'react-native-vector-icons/AntDesign'
import BaseButton from 'common/Button/BaseButton';
import LinearGradient from 'react-native-linear-gradient';
import { WIDTH, HEIGHT, getWidth, getFont, getLineHeight } from '../../../config';
import ItemNumberProduct from './ItemNumberProduct'

export default class GeneralInformation extends React.PureComponent {
  render() {
    const { name, price, typeProduct, otherSize, selectedSize, onButtonChangeSize, total, unit, code, onIncreaseProduct, onDecreaseProduct, onAddFavorites, onAddToCart } = this.props
    return (
      <View style={styles.container}>
        <Text style={styles.textName}>{name && name.substring(0, name.indexOf('('))}</Text>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: HEIGHT(8) }}>
          <Text style={styles.textNormal}>{`Mã: ${code && code}`}</Text>
        </View>
        <Text style={styles.textPrice}>{`Giá: ${price && price} / ${unit}`}</Text>
        <View style={{ flexDirection: 'row', justifyContent: typeProduct ? 'center' : 'space-between', marginTop: HEIGHT(20) }}>
          {!typeProduct && <ItemNumberProduct
            onPressMinus={onDecreaseProduct}
            onPressAdd={onIncreaseProduct}
            total={total}
          />}
          <BaseButton
            title={typeProduct ? 'Đặt dịch vụ' : 'Thêm vào giỏ'}
            onButton={onAddToCart}
            style={{ width: WIDTH(196) }}
          />
        </View>
        <View style={{ flexDirection: 'row', paddingHorizontal: WIDTH(7), flexWrap: 'wrap', width: getWidth(), paddingTop: HEIGHT(10), paddingBottom: HEIGHT(12), backgroundColor: R.colors.black3 }}>
          {otherSize.map((item, index) => {
            if (index === selectedSize) {
              return (
                <TouchableOpacity
                  onPress={() => { }}
                  style={{ paddingBottom: 1 }}
                  activeOpacity={0.6}
                >
                  <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 1 }}
                    colors={[R.colors.yellow1c8, R.colors.yellow254, R.colors.yellow1c8]}
                    style={styles.btnStyleNoborder}
                  >
                    <Text style={[styles.title, { color: R.colors.black0 }]}>{item.tenHang}</Text>
                  </LinearGradient>
                </TouchableOpacity>
              )
            } else {
              return (
                <TouchableOpacity
                  onPress={() => { onButtonChangeSize && onButtonChangeSize(item, index) }}
                  style={{ paddingBottom: 1 }}
                  activeOpacity={0.6}
                >
                  <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 1 }}
                    colors={[R.colors.black3, R.colors.black3, R.colors.black3]}
                    style={styles.btnStyle}
                  >
                    <Text style={styles.title}>{item.tenHang}</Text>
                  </LinearGradient>
                </TouchableOpacity>
              )
            }
          })}
        </View>
        <View style={{
          paddingHorizontal: WIDTH(12),
          paddingVertical: HEIGHT(12),
          backgroundColor: R.colors.colorIconBlack,
          borderRadius: WIDTH(4),
          marginTop: HEIGHT(12),
          marginBottom: HEIGHT(16)
        }}
        >
          <Text style={styles.textNormal}>
            Tri ân: Khi mua nhiều sản phẩm, khách hàng sẽ nhận được những ưu đãi về giá từ
            <Text style={{ color: R.colors.yellow254 }}> ihealing.vn</Text>
          </Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    width: getWidth(),
    paddingHorizontal: WIDTH(16),
    marginTop: HEIGHT(24),
    paddingBottom: HEIGHT(16)
  },
  textName: {
    color: R.colors.yellow254,
    fontSize: getFont(20),
    fontFamily: R.fonts.Roboto
  },
  textPrice: {
    color: R.colors.yellow254,
    fontSize: getFont(24),
    fontFamily: R.fonts.Roboto
  },
  textNormal: {
    color: R.colors.white,
    fontSize: getFont(14),
    fontFamily: R.fonts.Roboto
  },
  btnStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: WIDTH(4),
    borderWidth: 1,
    borderColor: R.colors.yellow254,
    marginTop: HEIGHT(12),
    marginLeft: WIDTH(9),
    paddingHorizontal: WIDTH(11),
    paddingVertical: HEIGHT(6)
    // height: HEIGHT(32)
  },
  btnStyleNoborder: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: WIDTH(4),
    marginTop: HEIGHT(12),
    marginLeft: WIDTH(9),
    paddingHorizontal: WIDTH(11) + 1,
    paddingVertical: HEIGHT(6) + 1
    // height: HEIGHT(32)
  },
  title: {
    fontSize: getFont(15),
    color: R.colors.yellow254,
    lineHeight: getLineHeight(22)
  },
})
