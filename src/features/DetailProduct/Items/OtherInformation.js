import React from 'react';
import { View, Text, StyleSheet, Image, FlatList } from 'react-native';
import R from 'assets/R';
import FastImage from 'react-native-fast-image';
import { WIDTH, HEIGHT, getFont, getWidth } from '../../../config';

const listTypeInformation = ['Effectivenss/ Công dụng', 'Target user/ Đối tượng sử dụng', 'Ingredient/Thành phần']

// const textPaymentMethod = `I. HÌNH THỨC THANH TOÁN
// Để thuận tiện nhất cho khách hàng mua sắm online tại iHealing, chúng tôi hỗ trợ các hình thức thanh toán sau:
// 1. Thanh toán tiền mặt khi nhận hàng.
// 2. Thanh toán trước bằng các hình thức:
// - Thẻ ATM Nội Địa
// - Thẻ tín dụng / Thẻ ghi nợ / VISA / MASTER / JCB / AMEX`

const textPaymentMethod = `II. HỦY ĐƠN HÀNG
Đơn hàng của khách hàng sẽ được xử lý ngay khi thực hiện xác nhận thanh toán. Để được hỗ trợ về việc hủy đơn hàng, vui lòng liên hệ chúng tôi:
- Tổng đài: 0975820880
- Email: vedephoanggia116@gmail.com`;
const textTransProduct = '1. THỜI GIAN VÀ PHÍ GIAO HÀNG\n Chúng tôi luôn cố gắng để giao đơn hàng sớm nhất đến khách hàng với cách hình thức giao hàng sau:\n 1. Giao hàng tận nơi:\n - Miễn phí giao hàng cho đơn hàng trên 449.000 VNĐ\n - Đơn đặt hàng dưới 449.000 VND sẽ có phí giao hàng như sau:\n ▪ TP. Hồ Chí Minh hoặc Hà Nội: 19.000 VND. Thời gian giao hàng dự kiến từ 1-3 ngày làm việc.\n ▪ Các tỉnh thành khác của Việt Nam: 49.000 VND. Thời gian giao hàng dự kiến từ 3 - 7 ngày làm việc.\n 2. Nhận hàng tại các store:\n - Quý khách tới trực tiếp các store và spa của hệ thống để được tư vấn và mua hàng.\n 2. CHÍNH SÁCH TRẢ HÀNG & HOÀN TIỀN\n Chúng tôi luôn cố gắng giao đến khách hàng sản phẩm trong tình trạng tốt nhất và chính xác theo đơn hàng đã đặt. Những trường hợp trả hàng và hoàn tiền được xem xét như sau:\n - Sản phẩm bị lỗi, sai loại sản phẩm, sai kích cỡ, sai màu sắc, thiếu sản phẩm so với đơn hàng được đặt\n - Sản phẩm bị hết hạn sử dụng\n - Sản phẩm hư hỏng trong quá trình vận chuyển Khách hàng có thể trả hàng trong vòng 14 ngày kể từ ngày nhận được hàng và thực hiện theo hướng dẫn trên Phiếu đính kèm trong kiện hàng:\n - Đơn hàng trả cần hoàn trả đầy đủ sản phẩm đã đặt, các quà tặng, phiếu mua hàng và phiếu giảm giá đính kèm (nếu có).\n - Sản phẩm chưa có dấu hiệu sử dụng, không bị hư hỏng, trầy xước hoặc có vết dơ trên bao bì sản phẩm.\n - Sau khi kiện hàng được hoàn trả thành công và được kiểm tra đầy đủ, khoản tiền hoàn lại sẽ được hoàn trong vòng 30 ngày làm việc.\n Mọi thông tin cần hỗ trợ vui lòng liên hệ tổng đài 093.652.5858 (9:00-18:00) hoặc Email: contactus@hadoo.vn'
export default class OtherInformation extends React.PureComponent {
  renderTransMethod = () => (
    <View style={styles.container}>
      <Text style={styles.textLabel}>Quy trình mua hàng</Text>
      <FastImage
        style={{ width: WIDTH(340), height: HEIGHT(190), marginTop: HEIGHT(16) }}
        resizeMode={FastImage.resizeMode.stretch}
        source={R.images.muaHang}
      />
    </View>
  )

  renderProducInformation = (detailInformation) => (
    <View style={styles.container}>
      <Text style={styles.textLabel}>Thông tin sản phẩm</Text>
      <Text style={styles.textNormal}>
        Overal Information/ Thông tin chung:
      </Text>
      <Text style={styles.textNormal}>
        {`${listTypeInformation[0]}: ${detailInformation.congDung}`}
      </Text>
      <Text style={styles.textNormal}>
        {`${listTypeInformation[1]}: ${detailInformation.doiTuong}`}
      </Text>
      <Text style={styles.textNormal}>
        {`${listTypeInformation[2]}: `}
      </Text>
      <FlatList
        data={detailInformation.thanhPhan}
        renderItem={({ item, index }) => (
          <Text style={styles.textNormal}>
            {`   - ${item}`}
          </Text>
        )}
      />
    </View>
  )

  renderPaymentMethod = () => (
    <View style={styles.container}>
      <Text style={styles.textLabel}>Hình thức thanh toán</Text>
      <Text style={styles.textNormal}>I. HÌNH THỨC THANH TOÁN</Text>
      <FastImage
        style={{ width: WIDTH(340), height: HEIGHT(190), marginVertical: HEIGHT(16) }}
        resizeMode={FastImage.resizeMode.stretch}
        source={R.images.thanhToan}
      />
      <Text style={styles.textNormal}>{textPaymentMethod}</Text>
    </View>
  )

  renderProducTrans = () => (
    <View style={styles.container}>
      <Text style={styles.textLabel}>Vận chuyển hàng hoá</Text>
      <Text style={styles.textNormal}>{textTransProduct}</Text>

    </View>
  )

  render() {
    const { detailInformation } = this.props
    return (
      <View style={{ flex: 1 }}>
        {this.renderTransMethod()}
        <View style={styles.lineBlack} />
        {this.renderPaymentMethod()}
        <View style={styles.lineBlack} />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.black3,
    paddingHorizontal: WIDTH(16),
    paddingVertical: HEIGHT(16),
    width: getWidth()

  },
  textLabel: {
    color: R.colors.yellow254,
    fontSize: getFont(16),
    fontFamily: R.fonts.Roboto,
  },
  textNormal: {
    color: R.colors.whiteF2,
    fontSize: getFont(16),
    fontFamily: R.fonts.Roboto
  },
  textTypeTrans: {
    color: R.colors.white,
    fontSize: getFont(15),
    fontFamily: R.fonts.Roboto,
    marginLeft: WIDTH(14)
  },
  icon: {
    height: HEIGHT(17),
    width: WIDTH(20)
  },
  viewTypeTrans: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: HEIGHT(17),
    marginBottom: HEIGHT(9)
  },
  lineBlack: {
    height: HEIGHT(8),
    backgroundColor: R.colors.colorIconBlack,
    width: getWidth()
  }

})
