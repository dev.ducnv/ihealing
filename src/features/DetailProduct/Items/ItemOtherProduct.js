import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity } from 'react-native';
import R from 'assets/R';
import { WIDTH, getFont, HEIGHT } from '../../../config';

const dataProduct = [
  { image: 'https://thuthuatnhanh.com/wp-content/uploads/2019/07/anh-girl-xinh-facebook-tuyet-dep-387x580.jpg', name: 'Combo chăm sóc vai gáy (50ml)' },
  { image: 'https://thuthuatnhanh.com/wp-content/uploads/2019/07/anh-girl-xinh-facebook-tuyet-dep-387x580.jpg', name: 'Combo chăm sóc vai gáy (50ml)' }
]
export default class ItemOtherProduct extends React.PureComponent {
    renderItem=({ item, index }) => (
      <TouchableOpacity style={styles.itemProduct}>
        <Image source={{ uri: item.image }} style={styles.imageProduct} />
        <Text style={styles.textDes}>
          {item.name}
        </Text>
      </TouchableOpacity>
    )

    render() {
      return (
        <View style={styles.container}>
          <Text style={styles.textLabel}>Sản phẩm cùng loại</Text>
          <FlatList
            renderItem={this.renderItem}
            data={dataProduct}
            horizontal
          />
        </View>
      );
    }
}
const styles = StyleSheet.create({
  container: {
    width: WIDTH(343),
    borderRadius: WIDTH(4),
    borderWidth: 1,
    borderColor: R.colors.white,
    padding: WIDTH(12)
  },
  textLabel: {
    color: R.colors.yellow254,
    fontSize: getFont(14),
    fontFamily: R.fonts.Roboto,
    marginBottom: HEIGHT(16)
  },
  itemProduct: {
    width: WIDTH(152),
    flexDirection: 'row',
    alignItems: 'center',

  },
  imageProduct: {
    height: WIDTH(48),
    width: WIDTH(48)
  },
  textDes: {
    color: R.colors.white,
    fontSize: getFont(12),
    fontFamily: R.fonts.Roboto,
    marginLeft: WIDTH(6),
    width: WIDTH(82)
  }

})
