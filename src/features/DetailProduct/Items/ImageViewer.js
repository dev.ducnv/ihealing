import React from 'react';
import { View, Text, Image, FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import { WIDTH, HEIGHT } from '../../../config';
import R from '../../../assets/R'


export default class ImageViewer extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      imageChoosen: '',
      indexOfImageChoosen: 0
    }
  }

  onPressImage = (index, item) => {
    this.setState({
      imageChoosen: item,
      indexOfImageChoosen: index
    })
  }

  renderItem = ({ item, index }) => {
    let isChoose = this.state.indexOfImageChoosen === index
    return (
      <TouchableOpacity
        onPress={() => this.onPressImage(index, item)}
        style={{ marginLeft: WIDTH(12), borderColor: R.colors.yellow254, borderWidth: isChoose ? 1 : 0, height: WIDTH(61) }}
      >
        <Image style={styles.smallImage} source={{ uri: R.images.IMAGE + item }} />
      </TouchableOpacity>
    )
  }

  componentDidMount() {
    const { listImage } = this.props
    if (listImage.length > 0) {
      this.setState({ imageChoosen: listImage[0], indexOfImageChoosen: 0 })
    }
  }

  render() {
    const { listImage } = this.props

    return (
      <View style={{ flex: 1 }}>
        <Image style={styles.bigImage} source={{ uri: R.images.IMAGE + this.state.imageChoosen }} />
        <FlatList
          horizontal
          data={listImage}
          keyExtractor={({ item, index }) => { index }}
          renderItem={this.renderItem}
          extraData={this.state.indexOfImageChoosen}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  smallImage: {
    width: WIDTH(59),
    height: WIDTH(59)
  },
  bigImage: {
    width: WIDTH(375),
    height: WIDTH(375),
    marginBottom: HEIGHT(11)
  },
})
