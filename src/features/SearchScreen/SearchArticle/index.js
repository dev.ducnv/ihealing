import React from 'react';
import { View, Text, ScrollView, FlatList, TouchableOpacity, StyleSheet } from 'react-native';
import R from 'assets/R';
import FastImage from 'react-native-fast-image';
import NavigationService from 'routers/NavigationService';
import { searchArticle } from 'apis';
import moment from 'moment';
import { LoadingComponent } from '../../../common/Loading/LoadingComponent';
import ItemSell from '../../../common/Item/ItemSell';
import PickerDropdown from '../../../common/Picker/PickerDropdown';

import HeaderSearch from '../../../common/Header/HeaderSearch';
import { HEIGHT, WIDTH, getFont, getLineHeight } from '../../../config';

const dataSearchArticle = {
  sortPrice: [
    { value: 'Thấp đến cao', key: 'key1' },
    { value: 'Cao đến thấp', key: 'key2' },
    { value: 'Ngẫu nhiên', key: 'key3' }
  ],
  dataItem: [
    { image: 'http://112.78.1.190:9090/FileUpload/Images/dv1.jpg', title: 'Cách tắm trắng toàn thân tại nhà từ thiên nhiên', date: '12/07/2020', like: 325 },
    { image: 'http://112.78.1.190:9090/FileUpload/Images/dv4.jpg', title: 'Cách massage toàn thân bằng sữa', date: '13/07/2020', like: 300 },
    { image: 'http://112.78.1.190:9090/FileUpload/Images/dv1.jpg', title: 'Cách tắm trắng toàn thân tại nhà từ thiên nhiên', date: '12/07/2020', like: 325 },
    { image: 'http://112.78.1.190:9090/FileUpload/Images/dv4.jpg', title: 'Cách massage toàn thân bằng sữa', date: '13/07/2020', like: 365 },
    { image: 'http://112.78.1.190:9090/FileUpload/Images/dv1.jpg', title: 'Cách tắm trắng toàn thân tại nhà từ thiên nhiên', date: '12/07/2020', like: 325 },

  ],
}

export default class SearchArticle extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      search: '',
      dataSearch: [],
      loading: true
    }
    this.timeout;
    this.lastKeySearch1 = '';
  }

  componentDidMount = () => {
    this.getData('')
  }

  getData = async (keySearch) => {
    this.setState({ loading: true })
    // console.log('vao search', keySearch)
    let dataSearch = await searchArticle(keySearch)
    if (dataSearch.data) {
      this.setState({ dataSearch: dataSearch.data, loading: false })
    } else {
      this.setState({ loading: false })
    }
  }

  onChangeText = (text) => {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      // console.log('======>>>>vao timeout', search, this.lastKeySearch1);
      if (text === this.lastKeySearch1 && text !== '') {
        this.getData(text)
      }
    }, 500);
    this.lastKeySearch1 = text;
    this.setState({ search: text });
  }

  render() {
    if (!this.state.loading) {
      return (
        <ScrollView style={{ flex: 1, backgroundColor: R.colors.black3 }}>
          <HeaderSearch title={this.state.search} onChangeText={this.onChangeText} />
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', padding: WIDTH(16) }}>
            <Text style={{ fontSize: getFont(20), color: R.colors.yellow254, fontFamily: R.fonts.Roboto, fontWeight: '500' }}>
              {this.state.dataSearch ? this.state.dataSearch.length : 0}
              <Text style={{ fontSize: getFont(15), color: R.colors.white, fontFamily: R.fonts.Roboto, fontWeight: 'normal' }}>  kết quả tìm kiếm phù hợp</Text>
            </Text>
          </View>
          <FlatList
            data={this.state.dataSearch}
            renderItem={({ item, index }) => {
              let thumnai = item.anhDaiDien ? { uri: R.images.IMAGE + item.anhDaiDien } : R.images.logoHadoo
              return (
                <TouchableOpacity
                  // onPress={() => NavigationService.navigate('DetailTraining', { item })}
                  style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: WIDTH(16), paddingTop: HEIGHT(20) }}
                  key={index}
                >
                  <FastImage source={thumnai} style={{ width: WIDTH(124), height: WIDTH(124) }} />
                  <View style={{ paddingHorizontal: WIDTH(16) }}>
                    <Text style={styles.title} numberOfLines={2}>{item.tenBaiViet && item.tenBaiViet}</Text>
                    <Text style={styles.date}>{item.ngayTao && moment(item.ngayTao).format('DD/MM/YYYY')}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: HEIGHT(13) }}>
                      <FastImage source={R.images.iconLike} style={{ width: WIDTH(17), height: HEIGHT(16), }} resizeMode={FastImage.resizeMode.stretch} />
                      <Text style={styles.like}>{item.luotXem && item.luotXem}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              )
            }}
          />
        </ScrollView>
      );
    } else {
      return (
        <View style={{ flex: 1 }}>
          <HeaderSearch title={this.state.search} onChangeText={this.onChangeText} />
          <LoadingComponent isLoading={this.state.loading} />
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  title: {
    width: WIDTH(203),
    fontWeight: '500',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.white,
    fontFamily: R.fonts.Roboto
  },
  date: {
    fontSize: getFont(14),
    lineHeight: getLineHeight(18),
    color: R.colors.grey400,
    fontFamily: R.fonts.Roboto,
    marginTop: HEIGHT(12)
  },
  like: {
    fontSize: getFont(14),
    lineHeight: getLineHeight(18),
    color: R.colors.white,
    fontFamily: R.fonts.Roboto,
    marginLeft: WIDTH(9)
    // marginTop: HEIGHT(12)
  }
})
