import React from 'react';
import { View, Text, ScrollView, FlatList } from 'react-native';
import R from 'assets/R';
import ItemSell from '../../../common/Item/ItemSell';
import PickerDropdown from '../../../common/Picker/PickerDropdown';
import HeaderSearch from '../../../common/Header/HeaderSearch';
import { HEIGHT, WIDTH, getFont } from '../../../config';

const dataSearchProduct = {
  sortPrice: [
    { value: 'Thấp đến cao', key: 'key1' },
    { value: 'Cao đến thấp', key: 'key2' },
    { value: 'Ngẫu nhiên', key: 'key3' }
  ],
  dataItem: [
    {
      icon: 'http://ihealing.vn/ihealing/images/haddoo_image/wweb-04.jpg',
      title: 'Thải độc dành cho đau vai gáy và đốt sống cổ',
      price: '1,380,000',
      isSellingFast: true
    },
    {
      icon: 'http://112.78.1.190:9090/FileUpload/Images/dv6.jpg',
      title: 'Combo chăm sóc chăm sóc bệnh Gout (50ml)',
      price: '4,260,000',
      isSellingFast: true
    },
    {
      icon: 'http://112.78.1.190:9090/FileUpload/Images/dv4.jpg',
      title: 'Combo chăm sóc bong gân (15ml)',
      price: '1,070,000',
      isSellingFast: false
    },
    {
      icon: 'http://ihealing.vn/ihealing/images/haddoo_image/wweb-04.jpg',
      title: 'Thải độc dành cho đau vai gáy và đốt sống cổ',
      price: '1,380,000',
      isSellingFast: true
    },
    {
      icon: 'http://112.78.1.190:9090/FileUpload/Images/dv6.jpg',
      title: 'Combo chăm sóc chăm sóc bệnh Gout (50ml)',
      price: '4,260,000',
      isSellingFast: true
    },
    {
      icon: 'http://112.78.1.190:9090/FileUpload/Images/dv4.jpg',
      title: 'Combo chăm sóc bong gân (15ml)',
      price: '1,070,000',
      isSellingFast: false
    },
  ],
}

export default class SearchProduct extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      search: 'Vai gáy và đốt sống cổ'
    }
  }

  render() {
    return (
      <ScrollView style={{ flex: 1, backgroundColor: R.colors.black3 }}>
        <HeaderSearch title={this.state.search} onChangeText={(text) => this.setState({ search: text })} />
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', padding: WIDTH(16) }}>
          <Text style={{ fontSize: getFont(20), color: R.colors.yellow254, fontFamily: R.fonts.Roboto, fontWeight: '500' }}>
            5
            <Text style={{ fontSize: getFont(15), color: R.colors.white, fontFamily: R.fonts.Roboto, fontWeight: 'normal' }}>  kết quả tìm kiếm phù hợp</Text>
          </Text>
          <PickerDropdown data={dataSearchProduct.sortPrice} placeholder="Sắp xếp giá" width={WIDTH(137)} />
        </View>
        <FlatList
          data={dataSearchProduct.dataItem}
          renderItem={({ item, index }) => (<ItemSell icon={item.icon} title={item.title} price={item.price} isSellingFast={item.isSellingFast} />
          )}
        />
      </ScrollView>
    );
  }
}
