import React, { Component } from 'react';
import { View, Text, StatusBar, TouchableOpacity, ScrollView, Keyboard, Alert } from 'react-native';
import FastImage from 'react-native-fast-image';
import Icon from 'react-native-vector-icons/AntDesign';
import NavigationService from 'routers/NavigationService';
import DeviceInfo from 'react-native-device-info';
import _ from 'lodash';
import axios from 'axios';
import { connect } from 'react-redux';
import { Home } from 'routers/screenNames';
import { setAccount } from '../../actions/actionCreators';
import { WIDTH } from '../../config';
import R from '../../assets/R';
import { login, getUserInfo, forgotPass, pushDeviceToken } from '../../apis/Functions/users';
import FormInput from '../../common/Form/FormInput';
import BaseButton from '../../common/Button/BaseButton';
import styles from './styles';
import AsyncStorageUtils from '../../helpers/AsyncStorageUtils';
import Modal from './items/ModalForgotPass';
import { LoadingComponent } from '../../common/Loading/LoadingComponent';
import { popupOk } from '../../config/Function';
import Analytics from '../../helpers/Analytics';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      username: '',
      password: '',
      loading: false
    };
    this.textForgot = ''
  }

  deviceId = '';

  deviceName = '';

  deviceToken = '';

  keyboardWillShowListener = () => { };

  async componentDidMount() {
    this.deviceId = await DeviceInfo.getUniqueId();
    this.deviceName = await DeviceInfo.getDeviceName();
  }

  componentWillMount() {
    this.keyboardWillShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardWillShow);
    this.keyboardWillShowListener = Keyboard.addListener('keyboardDidHide', this._keyboardWillHide);
  }

  componentWillUnmount() {
    this.keyboardWillShowListener.remove();
    this.keyboardWillShowListener.remove();
  }

  onShowModal = async () => {
    const { username } = this.state
    if (username === '') {
      popupOk('Thông báo', 'Vui lòng nhập tên đăng nhập của bạn!');
    } else {
      let resForgotPass = await forgotPass({
        Username: username
      })

      if (resForgotPass && resForgotPass.data) {
        if (resForgotPass.data.msg === 'label_daguiemailpassword') {
          this.textForgot = resForgotPass.data.txt
          this.setState({ visible: !this.state.visible })
        } else {
          popupOk('Thông báo', resForgotPass.data.txt);
        }
      } else {
        popupOk('Thông báo', 'Đã có lỗi xảy ra vui lòng thử lại sau!');
      }
      //
    }
  }

  onChangeUsername = (value) => {
    this.setState({ username: value });
  }

  onChangePassword = (value) => {
    this.setState({ password: value });
  }

  skip = async (routeName) => {
    const { dispatch } = this.props.navigation;
    // AsyncStorageUtils.get(R.strings.SAVE_TOKEN_NOTI).then(async values => {
    //   if (!_.isNull(values)) {
    //     // let res = await putToken({
    //     //   deviceID: this.deviceId,
    //     //   oneSignalID: values,
    //     //   deviceName: this.deviceName,
    //     // })
    //   }
    // })
    dispatch({
      type: 'Navigation/RESET',
      actions: [{
        type: 'Navigate',
        routeName
      }],
      index: 0
    });
  }

  onLogin = async () => {
    const account = {
      Username: this.state.username.toLowerCase(),
      PassWord: this.state.password,
    };
    if (account.Username.length > 20) {
      Alert.alert(
        'Thông báo',
        'Tên đăng nhập không được vượt quá 20 ký tự!',
        [
          {
            text: 'OK',
            onPress: () => {
              this.setState({ username: '', password: '' });
            }
          }
        ],
        { cancelable: false }
      )
    } else if (account.Username === '' || account.PassWord === '') {
      Alert.alert(
        'Thông báo',
        'Vui lòng điền đầy đủ thông tin đăng nhập!',
        [
          { text: 'OK' }
        ],
        { cancelable: false }
      )
    } else {
      this.setState({ loading: true }, async () => {
        let checkLogin = await login(account);
        if (_.has(checkLogin, 'data.loginSuccess') && checkLogin.data.loginSuccess === true) {
          let userinfo = await getUserInfo(account)
          console.log(userinfo)
          if (_.has(userinfo, 'data.userinfo')) {
            this.setState({ loading: false })
            let mAccount = {
              ...userinfo.data.userinfo,
              token: userinfo.data.token
            };
            this.setAccount(mAccount);
            this.skip(Home);
          } else {
            this.setState({ loading: false })
            Alert.alert('Đăng nhập thất bại');
          }
        } else {
          this.setState({ loading: false })
          Alert.alert('Đăng nhập thất bại');
        }
      })
    }
  }

  pushDeviceToken = async (idTaiKhoan) => {
    let value = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.DEVICE_TOKEN)
    const deviceToken = `?ID_TaiKhoan=${idTaiKhoan}&IDPUSH=${value}`


    // {
    //   idTaiKhoan,
    //   idPush: value,
    // };
    let mess = await pushDeviceToken(deviceToken);
    // console.log('messssssssssss', mess)
  }

  setAccount = async (Account) => {
    // console.log('luu tai khoan', Account.iD_QuanLy)
    this.props.setAccount(Account)
    // const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImloZWFsaW5nIiwiVXNlcm5hbWUiOiJxdWFubHkxIiwibmJmIjoxNjAwNDEwMDQwLCJleHAiOjE2MDEwMTQ4NDAsImlhdCI6MTYwMDQxMDA0MCwiaXNzIjoic29udHEiLCJhdWQiOiJzb250cSJ9.cPlLzK_RTvGeQ28ozA_eEqgY7WWFy1czK9nLnVjCm-4';
    axios.defaults.headers.common.Authorization = `${_.has(Account, 'token') ? Account.token : null}`;
    this.pushDeviceToken(Account.iD_QuanLy);

    await AsyncStorageUtils.multiSet([
      [R.strings.INIT_STORGE, JSON.stringify(Account)],
    ]);
  }


  FormSignIn = [
    { image: R.images.ic_user_lock, hint: 'Tên đăng nhập', func: this.onChangeUsername, isCharacters: true },
    { image: R.images.ic_lock, hint: 'Mật khẩu', isSecure: true, func: this.onChangePassword, isCharacters: true }
  ]

  render() {
    if (!this.state.loading) {
      return (
        <View style={styles.container}>
          <StatusBar backgroundColor={R.colors.background} />
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.container}>
              <FastImage
                style={styles.logoStyle}
                source={R.images.logoHadoo}
                resizeMode={FastImage.resizeMode.contain}
              />
              <FormInput dataForm={this.FormSignIn} />
              <View style={styles.wrapForgot}>
                <TouchableOpacity style={styles.btnForgot} onPress={this.onShowModal}>
                  <Text style={styles.textForgot}>Quên mật khẩu?</Text>
                </TouchableOpacity>
              </View>

              <BaseButton
                title="Đăng nhập"
                onButton={this.onLogin}
              />
              <View style={[styles.navigate, styles.contentBelowButton]}>
                <TouchableOpacity
                  style={styles.navigate}
                  onPress={async () => {
                    NavigationService.navigate('Home');
                    // await Analytics.onSignIn({ id: '1', username: this.state.username })
                  }}
                >
                  <Icon name="arrowleft" size={WIDTH(16)} color={R.colors.white} />
                  <Text style={[styles.text, { color: R.colors.white, marginLeft: WIDTH(10) }]}>Trang chủ</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => NavigationService.navigate('Register')}>
                  <Text style={[styles.text, { color: R.colors.yellow254 }]}>Đăng ký tài khoản</Text>
                </TouchableOpacity>
              </View>

              <Text style={styles.hotline}>Hotline: 093.652.5858 – 097.463.1299</Text>
            </View>
            <Modal
              visible={this.state.visible}
              onShowModal={this.onShowModal}
              text={this.textForgot}
            />
          </ScrollView>
        </View>
      )
    } return <LoadingComponent isLoading={this.state.loading} />
  }
}

function mapStateToProps(state) {
  return {
    Account: state.userReducers.Account
  };
}

export default connect(mapStateToProps, {
  setAccount
})(Login);
