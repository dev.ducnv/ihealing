import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash'
import { View, StyleSheet, FlatList } from 'react-native';
import FastImage from 'react-native-fast-image';
import NavigationService from 'routers/NavigationService';
import { ChiTietLichSuDonHang } from 'routers/screenNames';
import moment from 'moment';
import { getLichSuMuaDuocLieu } from '../../../apis';
import { LoadingComponent } from '../../../common/Loading/LoadingComponent';
import { HEIGHT, WIDTH, getWidth } from '../../../config';
import R from '../../../assets/R';
import ItemLichSuDonHang from '../../../common/Item/ItemLichSuDonHang';

const data = require('../Item/data.json');

class DaDat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
    };
  }

  getData = async () => {
    let tomorrow = moment().add(1, 'days');
    tomorrow = moment(tomorrow).format('YYYY/MM/DD')
    let body = {
      from: '2020/01/01',
      idKhachHang: this.props.Account.iD_QuanLy,
      to: tomorrow,
      trangthaixem: -1,
      ttht: -1
    }
    let resAccept = await getLichSuMuaDuocLieu(body);
    // console.log('resAccept_resAccept', resAccept)
    if (_.has(resAccept, 'data')) {
      this.setState({ data: resAccept.data, loading: false })
    }
  }

  componentDidMount = () => {
    this.getData()
  }

  renderItem = (item, index, d) => (
    <View>
      <ItemLichSuDonHang
        item={item}
        status={item.tenTrangThaiDongHang}
        onButton={() => NavigationService.navigate(ChiTietLichSuDonHang, { item: { ...item, canCancel: item.iD_TrangThaiDonHang < 3, refresh: this.getData } })}
        yellowText={false}
        icon={<FastImage style={styles.icon} source={R.images.check} resizeMode={FastImage.resizeMode.contain} />}
      />
      {
        (index !== d.length - 1) && <View style={styles.line} />
      }
    </View>
  )


  render() {
    if (!this.state.loading) {
      return (
        <View style={styles.container}>
          <FlatList
            data={this.state.data}
            renderItem={({ item, index }) => this.renderItem(item, index, data.data)}
          />
        </View>
      );
    } else return (<LoadingComponent isLoading={this.state.loading} />)
  }
}
function mapStateToProps(state) {
  return {
    Account: state.userReducers.Account
  };
}
export default connect(mapStateToProps, {
  // setAccount
})(DaDat);
const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.colorMain,
    flex: 1,
  },
  line: {
    height: HEIGHT(1),
    width: getWidth(),
    backgroundColor: R.colors.yellow1c8
  },
  icon: {
    width: WIDTH(21),
    height: WIDTH(21),
  }
});
