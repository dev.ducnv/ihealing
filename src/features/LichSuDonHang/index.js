import React from 'react';
import { View } from 'react-native';
import HeaderBack from '../../common/Header/HeaderBack';
import R from '../../assets/R';
import { getFont, WIDTH, getLineHeight } from '../../config';
import HeaderTabHome from '../../common/Header/HeaderTabHome';
import TabCategory from './Item/TabLichSuDonHang';

export default class LichSuDonHang extends React.PureComponent {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <HeaderBack
          title="Lịch sử đơn hàng"
          iconSearch={false}
          iconCart={false}
        />
        <TabCategory />
      </View>
    );
  }
}
