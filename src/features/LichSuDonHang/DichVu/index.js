import React, { Component } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import FastImage from 'react-native-fast-image';
import { getLichSuDichVu } from 'apis/Functions/users';
import { connect } from 'react-redux';
import NavigationService from 'routers/NavigationService';
import { ChiTietLichSuDonHang } from 'routers/screenNames';
import { LoadingComponent } from '../../../common/Loading/LoadingComponent';
import { HEIGHT, WIDTH, getWidth } from '../../../config';
import R from '../../../assets/R';
import ItemLichSuDichVu from '../../../common/Item/ItemLichSuDichVu';

const data = require('../Item/data.json');

class DangGiao extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
    };
  }


  getData = async () => {
    let toDay = new Date();
    let start = '2020/01/01';
    let end = toDay.toISOString()
    // let body = {
    //   from: '2020/01/01',
    //   idKhachHang: this.props.Account.iD_QuanLy,
    //   to: toDay.toISOString(),
    //   trangthaixem: -1,
    //   ttht: 1
    // }
    let querry = `?${JSON.stringify({ start, end })}`;

    let resAccept = await getLichSuDichVu(querry);
    console.log('resAccept_resAccept', resAccept)
    if (resAccept.data) {
      this.setState({ data: resAccept.data, loading: false })
    }
  }

  componentDidMount = () => {
    this.getData()
  }

  renderItem = (item, index, d) => {
    let t = item.trangThaiThanhToan || 'Đang chờ'
    // switch (item.status) {
    //   case 1:
    //     t = 'Đang giao hàng'
    //     break;
    //   default:
    //     t = 'Đang chờ sử dụng'
    //     break;
    // }
    return (
      <View>
        <ItemLichSuDichVu
          item={item}
          status={t}
          yellowText={true}
          onButton={() => NavigationService.navigate(ChiTietLichSuDonHang, { item, getData: this.getData })}
          refreshData={this.getData}
          icon={<FastImage style={styles.icon} source={R.images.iconCar} resizeMode={FastImage.resizeMode.contain} />}
        />
        {
          (index !== d.length - 1) && <View style={styles.line} />
        }
      </View>
    )
  }

  render() {
    if (!this.state.loading) {
      return (
        <View style={styles.container}>
          <FlatList
            data={this.state.data}
            renderItem={({ item, index }) => this.renderItem(item, index, data.data)}
          />
        </View>
      );
    } else return (<LoadingComponent isLoading={this.state.loading} />)
  }
}
function mapStateToProps(state) {
  return {
    Account: state.userReducers.Account
  };
}
export default connect(mapStateToProps, {
  // setAccount
})(DangGiao);
const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.colorMain,
    flex: 1,
  },
  line: {
    height: HEIGHT(1),
    width: getWidth(),
    backgroundColor: R.colors.yellow1c8
  },
  icon: {
    width: WIDTH(21),
    height: WIDTH(21),
  }
});
