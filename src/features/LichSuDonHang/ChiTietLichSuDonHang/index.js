import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, ScrollView, TextInput } from 'react-native';
import { getDetailInvoice, getXoa } from 'apis';
import ItemNull from 'common/Item/ItemNull';
import { LoadingComponent } from 'common/Loading/LoadingComponent';
import HeaderBack from 'common/Header/HeaderBack';
import FastImage from 'react-native-fast-image';
import _ from 'lodash'
import i18n from 'assets/languages/i18n';
import BaseButton from 'common/Button/BaseButton';
import NavigationService from 'routers/NavigationService';
import StarRatingBar from '../../../common/View/StarRatingBar';
import TextYellow from '../../../common/View/TextYellow';
import R from '../../../assets/R';
import { getWidth, getFont, getLineHeight, HEIGHT, WIDTH, formatVND, popupOk } from '../../../config';


class ChiTietLichSuDonHang extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: [],
      note: '',
      isNote: false
    };
  }


  componentDidMount = () => {
    this.getData()
  }

  setLoading = (loading) => {
    this.setState({ loading })
  }

  getData = async () => {
    this.setLoading(true)
    const item = this.props.navigation.getParam('item')
    let param = `?idDonHang=${item.iD_DonHang}`
    let res = await getDetailInvoice(param)
    if (res.data) {
      this.setState({ data: res.data })
    }
    // console.log(res)
    this.setLoading(false)
  }

  cancelInvoice = async (item) => {
    this.setState({ loading: true })
    let param = `?id_donhang=${item.iD_DonHang}&lydo=${this.state.note}`
    let res = await getXoa(param)
    // console.log(res)
    if (res.data && res.data.txt) {
      popupOk('Thông báo', res.data.txt)
      item.refresh()
      NavigationService.pop()
    } else {
      popupOk('Thông báo', 'Hủy đơn không thành công. Vui lòng kiểm tra kết nối!')
    }
    this.setState({ loading: false })
  }

  render() {
    const item = this.props.navigation.getParam('item')
    if (!this.state.loading) {
      return (
        <ScrollView style={styles.container}>
          <HeaderBack title="Chi tiết đơn hàng" iconCart={false} iconSearch={false} />
          <FlatList
            data={this.state.data}
            ListEmptyComponent={() => (<ItemNull text="Không có đơn hàng nào đang sử dụng" />)}
            renderItem={({ item, index }) => (
              <View style={{
                width: getWidth(),
                paddingHorizontal: WIDTH(16),
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingBottom: HEIGHT(20),
                paddingTop: HEIGHT(16)
              }}
              >
                <View style={{ width: WIDTH(300) }}>
                  <Text style={[styles.text, { color: R.colors.white, fontSize: getFont(16), fontWeight: '700' }]}>{_.has(item, 'tenhienthi') ? item.tenhienthi : i18n.t('NULL_T')}</Text>
                  <Text style={[styles.text, { color: R.colors.grey400 }]}>{_.has(item, 'mahang') ? `Mã: ${item.mahang}` : i18n.t('NULL_T')}</Text>
                  <Text style={[styles.text, { color: R.colors.grey400 }]}>{_.has(item, 'tendonvi') ? `Đơn vị tính: ${item.tendonvi}` : i18n.t('NULL_T')}</Text>
                  <Text style={[styles.text, { color: R.colors.grey400 }]}>{_.has(item, 'soluong') ? `Số lượng: ${item.soluong}` : i18n.t('NULL_T')}</Text>
                  <Text style={[styles.text, { color: R.colors.grey400 }]}>{_.has(item, 'giaban') ? `Đơn giá: ${formatVND(item.giaban)} VNĐ` : i18n.t('NULL_T')}</Text>
                  <Text style={styles.text}>{_.has(item, 'tongTien') ? `Tổng tiền: ${formatVND(item.tongTien)} VNĐ` : i18n.t('NULL_T')}</Text>
                  <Text style={[styles.text, { color: 'red', fontStyle: 'italic' }]}>{_.has(item, 'ghichugia') ? item.ghichugia : i18n.t('NULL_T')}</Text>

                </View>
              </View>
            )}
          />

          {this.state.isNote && <View style={[styles.wrapper, { justifyContent: 'flex-start', height: HEIGHT(50) }]}>
            <TextInput
              style={{
                color: R.colors.yellow254,
                fontSize: getFont(15),
                fontFamily: R.fonts.Roboto,
                lineHeight: HEIGHT(24),
                padding: 0,
                paddingLeft: 5,
              }}
              value={this.state.note}
              autoFocus={true}
              placeholder="Lý do hủy đơn"
              placeholderTextColor={R.colors.yellow254}
              onChangeText={(text) => this.setState({ note: text })}
            />
          </View>}

          {item.canCancel
            && <View style={{ alignSelf: 'center' }}>
              <BaseButton
                title={this.state.isNote ? 'Xác nhận hủy đơn' : 'Hủy đơn'}
                width={WIDTH(220)}
                onButton={() => {
                  if (this.state.isNote) {
                    this.cancelInvoice(item)
                  } else {
                    this.setState({ isNote: true })
                  }
                }}
              />
            </View>}
        </ScrollView>
      );
    } else return (<LoadingComponent isLoading={this.state.loading} />)
  }
}

export default ChiTietLichSuDonHang;
const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.colorMain,
    flex: 1,
  },
  viewTxt: {
    width: getWidth(),
    alignItems: 'center',
    marginTop: HEIGHT(20)
  },
  text: {
    fontSize: getFont(14),
    lineHeight: getLineHeight(20),
    color: R.colors.yellow254,
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'center',
    marginTop: HEIGHT(12),
    width: WIDTH(319),
    paddingHorizontal: WIDTH(10),
    borderBottomColor: R.colors.yellow254,
    borderBottomWidth: 1,
    alignSelf: 'center',
    marginBottom: HEIGHT(20)
  },
});
