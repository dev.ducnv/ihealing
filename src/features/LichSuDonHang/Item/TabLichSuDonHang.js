// @flow
import React, { PureComponent } from 'react';
import { StyleSheet, View, TouchableOpacity, Text, Alert, BackHandler } from 'react-native';
// import { Text } from 'react-native-paper';
import { TabView } from 'react-native-tab-view';
import LinearGradient from 'react-native-linear-gradient';
import R from '../../../assets/R';
import { HEIGHT, getFont, getWidth } from '../../../config';
import DuocLieu from '../DuocLieu';
import DichVu from '../DichVu';
// import DangSuDung from '../DangSuDung';

const LazyPlaceholder = ({ route }: Object) => (
  <View style={styles.scene}>
    <Text>
      Loading
      {' '}
      {route.title}
      …
    </Text>
  </View>
);

export default class TabLichSuDonHang extends PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        { key: '1', title: 'Lịch sử mua dược liệu' },
        { key: '2', title: 'Lịch sử dịch vụ' },
        // { key: '3', title: 'Đang giao' },
      ],
    };
  }


  _renderTabBar = (props: any) => (
    <LinearGradient
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 1 }}
      style={styles.styleTabbar}
      colors={[R.colors.yellow1c8, R.colors.yellow254, R.colors.yellow1c8]}
    >
      {props.navigationState.routes.map((route, i) => {
        let opacity = this.state.index === i ? 1 : 0.5
        return (
          <TouchableOpacity style={this.state.index === i ? styles.stylePressed : styles.styleunPress} onPress={() => this.setState({ index: i })} key={i}>
            <Text style={[styles.styleText, { opacity }]}>{route.title}</Text>
          </TouchableOpacity>
        );
      })}
    </LinearGradient>
  );

  _renderLazyPlaceholder = ({ route }: Object) => (<LazyPlaceholder route={route} />)
    ;


  renderScene = ({ route }: Object) => {
    switch (route.key) {
      case '1':
        return <DuocLieu />;
      // case '2':
      //   return <DangSuDung />;
      case '2':
        return <DichVu />;
      default:
        return null;
    }
  };


  render() {
    return (
      <TabView
        navigationState={this.state}
        renderTabBar={this._renderTabBar}
        renderScene={this.renderScene}
        style={{ flex: 1 }}
        onIndexChange={index => this.setState({ index })}
        initialLayout={{ width: getWidth() }}
        // tabBarPosition="bottom"
        // lazy={true}
        lazyPreloadDistance={1}
        renderLazyPlaceholder={this._renderLazyPlaceholder}
      />
    );
  }
}
const styles = StyleSheet.create({
  scene: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  styleText: {
    fontSize: getFont(16),
    color: R.colors.black0,
    // marginTop: HEIGHT(2),
    fontFamily: R.fonts.Roboto,
    fontWeight: 'bold'
  },
  styleunPress: {
    flex: 1,
    width: getWidth() / 2,
    alignItems: 'center',
    justifyContent: 'center'
  },
  stylePressed: {
    flex: 1,
    width: getWidth() / 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: R.colors.black0
  },
  styleTabbar: {
    height: HEIGHT(44),
    width: getWidth(),
    backgroundColor: R.colors.colorMain,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  container: {
    flex: 1,
    backgroundColor: R.colors.white100,
    justifyContent: 'flex-end',
  }
});
