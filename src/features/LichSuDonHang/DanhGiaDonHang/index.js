import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, ScrollView, TextInput } from 'react-native';
import { getDetailInvoice, getXoa } from 'apis/Functions/users';
import ItemNull from 'common/Item/ItemNull';
import { LoadingComponent } from 'common/Loading/LoadingComponent';
import HeaderBack from 'common/Header/HeaderBack';
import FastImage from 'react-native-fast-image';
import _ from 'lodash'
import i18n from 'assets/languages/i18n';
import BaseButton from 'common/Button/BaseButton';
import NavigationService from 'routers/NavigationService';
import StarRatingBar from '../../../common/View/StarRatingBar';
import TextYellow from '../../../common/View/TextYellow';
import R from '../../../assets/R';
import { getWidth, getFont, getLineHeight, HEIGHT, WIDTH, formatVND, popupOk } from '../../../config';


class DanhGiaDonHang extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: [],
      note: '',
      isNote: false
    };
  }


  componentDidMount = () => {
    this.getData()
  }

  setLoading = (loading) => {
    this.setState({ loading })
  }

  getData = async () => {
    this.props.navigation?.state?.params?.refreshData?.()
  }

  sendRate = async (item) => {
    this.setState({ loading: true })
    let param = `?id_donhang=${item.iD_DonHang}&lydo=${this.state.note}`
    let res = await getXoa(param)
    // console.log(res)
    if (res.data && res.data.txt) {
      popupOk('Thông báo', res.data.txt)
      item.refresh()
      NavigationService.pop()
    } else {
      popupOk('Thông báo', 'Hủy đơn không thành công. Vui lòng kiểm tra kết nối!')
    }
    this.setState({ loading: false })
  }

  render() {
    const item = this.props.navigation.getParam('item')
    console.log('item_item', item)
    if (!this.state.loading) {
      return (
        <ScrollView style={styles.container}>
          <HeaderBack title="Đánh giá đơn hàng" iconCart={false} iconSearch={false} />
          <StarRatingBar
            item={item}
            setLoading={this.setLoading}
            refresh={() => {
              NavigationService.pop()
              this.getData()
            }}
            styleContainer={{
              marginTop: HEIGHT(20),
              marginBottom: HEIGHT(20),
            }}
          />
        </ScrollView>
      );
    } else return (<LoadingComponent isLoading={this.state.loading} />)
  }
}

export default DanhGiaDonHang;
const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.colorMain,
    flex: 1,
  },
  viewTxt: {
    width: getWidth(),
    alignItems: 'center',
    marginTop: HEIGHT(20)
  },
  text: {
    fontSize: getFont(14),
    lineHeight: getLineHeight(20),
    color: R.colors.yellow254,
  },
  wrapper: {
    flexDirection: 'row',
    // alignItems: 'center',
    // justifyContent: 'center',
    marginTop: HEIGHT(60),
    width: WIDTH(319),
    paddingHorizontal: WIDTH(10),
    borderColor: R.colors.yellow254,
    borderWidth: 1,
    alignSelf: 'center',
    marginBottom: HEIGHT(20),
    borderRadius: WIDTH(10),
  },
});
