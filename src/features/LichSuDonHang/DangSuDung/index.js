import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, ScrollView } from 'react-native';
import { getListRating } from 'apis';
import ItemNull from '../../../common/Item/ItemNull';
import { LoadingComponent } from '../../../common/Loading/LoadingComponent';
import StarRatingBar from '../../../common/View/StarRatingBar';
import TextYellow from '../../../common/View/TextYellow';
import R from '../../../assets/R';
import { getWidth, getFont, getLineHeight, HEIGHT } from '../../../config';

class DangSuDung extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: []
    };
  }


  componentDidMount = () => {
    this.getData()
  }

  setLoading = (loading) => {
    this.setState({ loading })
  }

  getData = async () => {
    let res = await getListRating()
    if (res.data && res.data.data) {
      this.setState({ data: res.data.data })
    }
    console.log(res)
    this.setLoading(false)
  }

  render() {
    if (!this.state.loading) {
      return (
        <ScrollView style={styles.container}>
          <View style={styles.viewTxt}>
            <Text style={[styles.text, { flex: 1 }]}>Đánh giá chất lượng</Text>
          </View>
          <TextYellow
            styleContainer={{ marginTop: HEIGHT(16), }}
            text="Bằng cách vote sao, chúng tôi sẽ biết được sự hài lòng của bạn khi sử dụng dịch vụ chăm sóc sức khoẻ của"
            yellowText="iHealing"
          />
          <FlatList
            data={this.state.data}
            ListEmptyComponent={() => (<ItemNull text="Không có đơn hàng nào đang sử dụng" />)}
            renderItem={({ item, index }) => (
              <StarRatingBar
                item={item}
                setLoading={this.setLoading}
                refresh={this.getData}
                styleContainer={{
                  marginTop: HEIGHT(20),
                  marginBottom: HEIGHT(20),
                }}
              />
            )}
          />
        </ScrollView>
      );
    } else return (<LoadingComponent isLoading={this.state.loading} />)
  }
}

export default DangSuDung;
const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.colorMain,
    flex: 1,
  },
  viewTxt: {
    width: getWidth(),
    alignItems: 'center',
    marginTop: HEIGHT(20)
  },
  text: {
    color: R.colors.yellow254,
    fontSize: getFont(22),
    lineHeight: getLineHeight(30),
    fontWeight: '700'
  }
});
