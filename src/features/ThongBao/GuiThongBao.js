/* eslint-disable class-methods-use-this */
/* eslint-disable react/no-array-index-key */
// @flow

import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Alert,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import _ from 'lodash';
import { connect } from 'react-redux';
import HeaderBack from 'common/Header/HeaderBack';
import ItemNull from 'common/Item/ItemNull';
import { LoadingComponent } from 'common/Loading/LoadingComponent';
import NavigationService from '../../routers/NavigationService';
import { getFont, HEIGHT, popupOk, WIDTH } from '../../config';
import { getFormNotif, postNotigication } from '../../apis';

import styles from './styles';
// import { fetchGet, fetchPut, fetchThongBao, logOut } from '../../actions';
import R from '../../assets/R';
import BaseButton from '../../common/Button/BaseButton';

// type Props = {
//   Account: Object,
//  FetchGet: Function,
//   FetchPut: Function,
//   FetchThongBao: Function,
//   logOut: Function,
//   dsThongBao: Object,
// };

class GuiThongBao extends PureComponent<Props, State> {
  state = {
    loading: false,
    listFormNotif: [
    ],
  };


  componentDidMount = async () => {
    this.setState({ loading: true })
    let dataTB = await getFormNotif()
    console.log(dataTB.data)
    if (dataTB?.data) {
      this.setState({
        listFormNotif: dataTB.data,
        loading: false
      })
    } else this.setState({ loading: false })
  }

  senNotification = async (NoiDung = '') => {
    const khachHang = this.props.navigation.getParam('ID_KhachHang')
    console.log('ID_KhachHang', khachHang)
    const body = {
      NoiDung,
      ID_KhachHang: khachHang
    }
    this.setState({ loading: true })
    let dataTB = await postNotigication(body)
    this.setState({ loading: false });
    console.log('dataTB', dataTB)
    if (dataTB) {
      popupOk('Thông báo', 'Gửi thành công', () => NavigationService.pop())
    } else {
      popupOk('Thông báo', 'Lỗi kết nối, không thể gửi thông báo', () => NavigationService.pop())
    }
  }


  renderItem = ({ item }) => (
    <TouchableOpacity
      onPress={() => { }}
      style={[styles.containerItem, { backgroundColor: R.colors.black3 }]}
    >
      <View
        style={{
          marginLeft: WIDTH(5),
          width: WIDTH(340),
        }}
      >
        <Text
          style={[
            styles.tieuDe,
            { fontSize: getFont(16), width: WIDTH(340) }
          ]}
        >
          {_.get(item, 'noiDung', 'Chưa có nội dung')}
        </Text>
        <View style={{ height: HEIGHT(3) }} />
        <BaseButton
          title="Gửi thông báo"
          style={{ marginLeft: WIDTH(200) }}
          width={WIDTH(120)}
          height={HEIGHT(35)}
          titleStyle={{ fontSize: getFont(14) }}
          onButton={() => this.senNotification(item.noiDung)}
        />
        <View style={[styles.line, { width: WIDTH(350) }]} />
      </View>
    </TouchableOpacity>
  );

  render() {
    const { listFormNotif, loading } = this.state;
    if (!loading) {
      return (
        <View style={styles.container}>
          <HeaderBack title="Gửi thông báo cho khách hàng" iconCart={false} iconSearch={false} />
          <FlatList
            data={listFormNotif}
            extraData={listFormNotif}
            ListEmptyComponent={<ItemNull text="Không có thông báo nào" />}
            keyExtractor={item => item._id}
            style={{ width: WIDTH(360), paddingVertical: HEIGHT(16) }}
            renderItem={this.renderItem}
          />
        </View>
      );
    } else {
      return (
        <LoadingComponent isLoading={loading} />
      )
    }
  }
}
function mapStateToProps(state) {
  return {

  };
}

export default connect(mapStateToProps, {
  // FetchGet: (payload, skip) => fetchGet(payload, skip),
  // FetchPut: payload => fetchPut(payload),
  // FetchThongBao: (content, skip) => fetchThongBao(content, skip),
  // logOut,
})(GuiThongBao);
