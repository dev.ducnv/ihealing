/**
 * This component to show details news
 * @huanhtm
 */
// @flow
import React, { Component } from "react";
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  Linking,
  Alert,
  TouchableOpacity,
} from "react-native";
import FastImage from "react-native-fast-image";
import { connect } from "react-redux";
import _ from "lodash";
import moment from "moment";
// import HyperLink from 'react-native-hyperlink';
import HeaderBack from "common/Header/HeaderBack";
import HeaderReal from "../../common/Header/HeaderReal";
import i18n from "../../assets/languages/i18n";
import { HEIGHT, WIDTH, getFont, getLineHeight, getWidth } from "../../config";
import R from "../../assets/R";

type Props = {
  navigation: Object,
};
class ChiTietTB extends Component<Props> {
  pressBack = () => {
    this.props.navigation.state.params.funGoBack();
    this.props.navigation.goBack();
  };

  onContact = (phoneNum) => {
    Linking.openURL(`tel:${phoneNum}`);
  };

  render() {
    const item = this.props.navigation.getParam("item");
    const currentTime = new Date();
    const timeC = `${currentTime.getHours()}:${currentTime.getMinutes()} - ${currentTime.getDate()}/${currentTime.getMonth()}/${currentTime.getFullYear()}`;
    return (
      <View style={{ flex: 1, backgroundColor: R.colors.black3 }}>
        <HeaderBack
          title="Chi tiết thông báo"
          iconCart={false}
          iconSearch={false}
        />

        <ScrollView>
          <View
            style={{
              flexDirection: "row",
              width: getWidth(),
              marginTop: WIDTH(16),
            }}
          >
            <FastImage
              style={styles.img}
              source={R.images.logoMain}
              resizeMode="cover"
            />
            <View style={{ justifyContent: "center" }}>
              <Text style={styles.textTittle}>
                {_.has(item, "tenTaiKhoan")
                  ? item.tenTaiKhoan
                  : i18n.t("NULL_T")}
              </Text>
              <Text style={styles.textTime}>
                {!_.has(item, "ngayTao")
                  ? timeC
                  : moment(item.ngayTao).format("DD/MM/YYYY")}{" "}
              </Text>
            </View>
          </View>

          <Text style={styles.textDetail}>
            {_.has(item, "noiDungThongBao")
              ? item.noiDungThongBao
              : i18n.t("NULL_T")}
          </Text>
        </ScrollView>
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    // Account: state.userinforReducer.Account,
  };
}

export default connect(mapStateToProps, {})(ChiTietTB);
const styles = StyleSheet.create({
  textTittle: {
    fontFamily: R.fonts.RobotoRegular,
    fontWeight: "500",
    fontSize: getFont(15),
    color: R.colors.yellow254,
    width: WIDTH(270),
    flexWrap: "wrap",
  },
  textDetail: {
    fontFamily: R.fonts.RobotoRegular,
    fontSize: getFont(14),
    color: R.colors.white,
    marginHorizontal: HEIGHT(15),
    lineHeight: getLineHeight(26),
  },
  textAuthor: {
    fontFamily: R.fonts.RobotoRegular,
    fontSize: getFont(14),
    color: R.colors.orange726,
    paddingHorizontal: HEIGHT(15),
    lineHeight: getLineHeight(26),
    fontWeight: "500",
    marginTop: HEIGHT(15),
  },
  textTime: {
    fontFamily: R.fonts.RobotoRegular,
    fontSize: getFont(14),
    color: R.colors.white,
  },
  btn: {
    width: WIDTH(172),
    height: HEIGHT(44),
    backgroundColor: R.colors.orange726,
    borderRadius: WIDTH(24),
    alignSelf: "center",
    marginTop: HEIGHT(20),
    justifyContent: "center",
  },
  btnText: {
    textAlign: "center",
    fontSize: getFont(17),
    color: R.colors.white100,
  },
  img: {
    height: WIDTH(60),
    width: WIDTH(60),
    borderRadius: WIDTH(120),
    borderWidth: 1,
    borderColor: R.colors.orange726,
    margin: WIDTH(9),
    marginLeft: WIDTH(15),
  },
});
