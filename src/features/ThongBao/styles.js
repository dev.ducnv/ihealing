import {
  StyleSheet,
} from 'react-native'

// import
import { HEIGHT, WIDTH, getHeight, getWidth, getFont, getLineHeight } from '../../config';
import R from '../../assets/R';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: R.colors.black3,
    flex: 1,
  },
  containerSearchBar: {
    backgroundColor: R.colors.orange726,
    width: WIDTH(370),
    height: HEIGHT(60),
    borderWidth: 0,
    borderTopWidth: 0,
    borderBottomWidth: 0,
    borderRadius: WIDTH(5),
    borderColor: R.colors.orange726,
    justifyContent: 'center',
    padding: WIDTH(18)
  },
  containerItem: {
    width: getWidth(),
    minHeight: 65 * getHeight() / 640,
    flexDirection: 'row',
    paddingHorizontal: WIDTH(16)
  },
  time: {
    fontFamily: R.fonts.Roboto,
    marginVertical: HEIGHT(2),
    fontSize: getFont(13),
    lineHeight: getLineHeight(20),
    textAlign: 'right',
    color: R.colors.white
  },
  new: {
    width: WIDTH(25),
    height: WIDTH(20),
    backgroundColor: R.colors.orange726,
    borderRadius: WIDTH(100),
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: HEIGHT(2),
    marginRight: WIDTH(3)
  },
  img: {
    width: WIDTH(45),
    height: WIDTH(45),
    borderRadius: WIDTH(100),
    borderWidth: 1,
    borderColor: R.colors.orange726,
    marginTop: HEIGHT(3)
  },
  tieuDe: {
    fontFamily: R.fonts.Roboto,
    width: WIDTH(180),
    fontSize: getFont(15),
    lineHeight: getLineHeight(24),
    color: R.colors.yellow254,
    marginVertical: HEIGHT(1)
  },
  noiDung: {
    fontFamily: R.fonts.Roboto,
    width: WIDTH(180),
    fontSize: getFont(14),
    lineHeight: getLineHeight(20),
    marginVertical: HEIGHT(1),
    color: R.colors.white
  },
  line: {
    height: 1,
    width: WIDTH(275),
    backgroundColor: R.colors.yellow254,
    marginTop: HEIGHT(15)
  },
  emptyNoti: {
    fontFamily: R.fonts.Roboto,
    fontSize: getFont(15),
    padding: HEIGHT(12)
  },
});
export default styles;
