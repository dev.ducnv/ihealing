/* eslint-disable class-methods-use-this */
/* eslint-disable react/no-array-index-key */
// @flow

import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
  Alert,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import _ from 'lodash';
import { connect } from 'react-redux';
import moment from 'moment';
import HeaderBack from 'common/Header/HeaderBack';
import ItemNull from 'common/Item/ItemNull';
import { LoadingComponent } from 'common/Loading/LoadingComponent';
import NavigationService from '../../routers/NavigationService';
import { HEIGHT, WIDTH } from '../../config';
import { getNotif } from '../../apis';

import styles from './styles';
// import { fetchGet, fetchPut, fetchThongBao, logOut } from '../../actions';
import R from '../../assets/R';

// type Props = {
//   Account: Object,
//  FetchGet: Function,
//   FetchPut: Function,
//   FetchThongBao: Function,
//   logOut: Function,
//   dsThongBao: Object,
// };

type State = {
  search: string,
  loading: boolean,
  showfooter: boolean,
  isLoading: boolean,
  refresh: boolean,
  numOfPage: number,
  page: number,
  dsThongBao: Object,
  maxdata: boolean,
};

class ThongBao extends PureComponent<Props, State> {
  state = {
    loading: false,
    dsThongBao: [

    ],
  };


  componentDidMount = async () => {
    this.setState({ loading: true })
    let dataTB = await getNotif()
    // console.log(dataTB)
    if (dataTB.data && dataTB.data.data) {
      this.setState({ dsThongBao: dataTB.data.data, loading: false })
      this.onReadNotif()
    } else this.setState({ loading: false })
  }

  onReadNotif = item => {
    this.props.navigation?.state?.params?.updateDidRead();
    console.log('item_item', item)
    // NavigationService.navigate('ChiTietTB', { item });
  };


  renderItem = ({ item }) => (
    <TouchableOpacity
      onPress={() => this.onReadNotif(item)}
      style={[styles.containerItem, { backgroundColor: R.colors.black3 }]}
    >
      <View style={{ height: HEIGHT(66) }}>
        <FastImage
          source={R.images.logoMain}
          resizeMode={FastImage.resizeMode.cover}
          style={styles.img}
        />
      </View>
      <View
        style={{
          marginLeft: WIDTH(20),
          width: WIDTH(180),
          height: HEIGHT(66),
        }}
      >
        <Text
          style={[
            styles.tieuDe,
          ]}
          numberOfLines={1}
        >
          {_.has(item, 'tenTaiKhoan') ? item.tenTaiKhoan : 'Trống'}
        </Text>
        <Text
          style={[
            styles.noiDung,
          ]}
          numberOfLines={1}
        >
          {_.has(item, 'noiDungThongBao') ? item.noiDungThongBao : 'Trống'}
        </Text>

        <View style={styles.line} />
      </View>

      <View
        style={{
          height: HEIGHT(66),
          paddingRight: WIDTH(3),
          padding: HEIGHT(1),
          alignItems: 'flex-end',
          flex: 1,
        }}
      >
        <Text
          style={[
            styles.time,
          ]}
          numberOfLines={1}
        >
          {item.ngayTao && moment(item.ngayTao).format('HH:mm DD/MM/YYYY')}

        </Text>
      </View>
    </TouchableOpacity>
  );

  render() {
    const { dsThongBao, loading } = this.state;
    if (!loading) {
      return (
        <View style={styles.container}>
          <HeaderBack title="Thông báo" iconCart={false} iconSearch={false} />
          <FlatList
            data={dsThongBao}
            extraData={dsThongBao}
            ListEmptyComponent={<ItemNull text="Không có thông báo nào" />}
            keyExtractor={item => item._id}
            style={{ width: WIDTH(360), paddingVertical: HEIGHT(16) }}
            renderItem={this.renderItem}
          />
        </View>
      );
    } else {
      return (
        <LoadingComponent isLoading={loading} />
      )
    }
  }
}
function mapStateToProps(state) {
  return {

  };
}

export default connect(mapStateToProps, {
  // FetchGet: (payload, skip) => fetchGet(payload, skip),
  // FetchPut: payload => fetchPut(payload),
  // FetchThongBao: (content, skip) => fetchThongBao(content, skip),
  // logOut,
})(ThongBao);
