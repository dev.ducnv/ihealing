import React, { Component } from 'react';
import { ScrollView, StyleSheet, TextInput, View, Text, TouchableOpacity, Platform, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-picker';
import { postUpdateInfo, postUpdateAvatar, putImage, getTinh, getHuyen, getXa } from 'apis';
import _ from 'lodash'
import { setAccount } from 'actions';
import { LoadingComponent } from 'common/Loading/LoadingComponent';
import NavigationService from 'routers/NavigationService';
import AsyncStorageUtils from 'helpers/AsyncStorageUtils';
import PickerItem from 'common/Picker/PickerItem';
import R from '../../assets/R';
import { WIDTH, HEIGHT, getLineHeight, getFont, getWidth, popupOk, formatVND } from '../../config';
import HeaderBack from '../../common/Header/HeaderBack';
import FormInput from '../../common/Form/FormInput';
import BaseButton from '../../common/Button/BaseButton';


class ChangeInfo extends Component {
  constructor(props) {
    let { Account } = props
    super(props);
    this.state = {
      reRender: true,
      miniLoading: false,

      avatar: Account.anhDaiDien ? { uri: R.images.IMAGE + Account.anhDaiDien } : R.images.logoHadoo,
      username: Account.username ? Account.username : '',
      hoTen: Account.tenDayDu ? Account.tenDayDu : '',
      email: Account.email ? Account.email : '',
      phone: Account.dienThoai ? Account.dienThoai : '',
      tichDiem: formatVND(Account?.tichDiem ?? 0),
      id: Account.$id ? Account.$id : '',
      maTK: Account.maTaiKhoan ? Account.maTaiKhoan : '',
      cmt: Account.cmt ? Account.cmt : '',
      gioiTinh: Account.gioiTinh ? Account.gioiTinh : 0,
      stk: Account.soTaiKhoan ? Account.soTaiKhoan : '',
      diaChi: Account.diaChi ? Account.diaChi : 'Chưa có địa chỉ',
      showPickerAdd: false,


      formSignUp: [
        { image: R.images.ic_user_lock, hint: 'Tài khoản đăng nhập', doubleText: false, editable: false, value: '', type: 'username' },
        { image: R.images.ic_user, hint: 'Tên đầy đủ', doubleText: false, editable: true, value: '', type: 'hoTen' },
        { image: R.images.ic_email, hint: 'Email', doubleText: false, editable: true, value: '', type: 'email' },
        { image: R.images.ic_gender, hint: 'Giới tính', isGender: true, editable: false, value: 0, type: 'gender' },
        { image: R.images.ic_phone, hint: 'Số điện thoại', doubleText: false, editable: true, value: '', type: 'phone', isKeyboadNum: true },
        { image: R.images.ic_person_id, hint: 'Điểm tích lũy', text: '(Điểm tích lũy)', doubleText: true, editable: false, value: '', type: 'tichDiem', isKeyboadNum: true },
        { image: R.images.ic_person_id, hint: 'Mã tài khoản', text: '(Mã tài khoản)', doubleText: true, editable: false, value: '', type: 'maTK' },
        { image: R.images.ic_person_id, hint: 'Số CMT/CCCD', text: '(Số CMT/ CCCD)', doubleText: true, editable: true, value: '', type: 'cmt', isKeyboadNum: true },
        { image: R.images.techcombank, hint: 'Số tài khoản', text: '(Số tài khoản)', doubleText: true, editable: true, value: '', type: 'stk', isKeyboadNum: true },
      ],

      loading: false
    };
    this.imageit = null
    this.idTinh = []
    this.dataTinh = ['Đang tải']
    this.tinh = -1
    this.idHuyen = []
    this.dataHuyen = ['Đang tải']
    this.huyen = -1
    this.idXa = []
    this.dataXa = ['Đang tải']
    this.xa = -1
  }

  componentWillMount() {
    let arr = this.state.formSignUp
    arr[0].value = this.state.username
    arr[1].value = this.state.hoTen
    arr[2].value = this.state.email
    arr[3].value = this.state.gioiTinh
    arr[4].value = this.state.phone
    arr[5].value = this.state.tichDiem
    arr[6].value = this.state.maTK
    arr[7].value = this.state.cmt
    arr[8].value = this.state.stk
    this.setState({ formSignUp: arr })
  }

  _reRender = () => {
    this.setState({ reRender: !this.state.reRender })
  }


  onValueChange = (text, type) => {
    switch (type) {
      case 'hoTen':
        this.setState({ hoTen: text })
        break;
      case 'email':
        this.setState({ email: text })
        break;
      case 'phone':
        this.setState({ phone: text })
        break;
      case 'cmt':
        this.setState({ cmt: text })
        break;
      case 'stk':
        this.setState({ stk: text })
        break;
      case 'gender':
        this.setState({ gioiTinh: text.key })
        break;
      default:
        break;
    }
  }

  launchImageLibrary = async () => {
    let options = {
      quality: 0.04,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.didCancel) {
      } else if (response.error) {
      } else {
        this.setState({
          avatar: { uri: response.uri }
        });
        // console.log('quality', response.fileSize)
        this.imageit = {
          uri: Platform.OS === 'android' ? response.uri : response.uri.replace('file://', ''),
          type: response.type,
          name: response.fileName || response.uri
        }
      }
    });
  }

  onPostData = async () => {
    this.setState({ loading: true })
    let resDeny
    let body
    let urlImage = ''
    // console.log('call api nes')
    if (this.imageit !== null) {
      let formData = new FormData()
      formData.append('', this.imageit)
      urlImage = await putImage(formData)
      // console.log('xong up anh', urlImage, this.imageit)
      if (urlImage) {
        body = {
          ...this.props.Account,
          email: this.state.email,
          tenDayDu: this.state.hoTen,
          gioiTinh: this.state.gioiTinh,
          dienThoai: this.state.phone,
          cmt: this.state.cmt,
          anhDaiDien: urlImage,
          soTaiKhoan: this.state.stk,
          phuongXa: this.state.showPickerAdd ? this.idXa[this.xa].iD_Phuong : 0,
          quanHuyen: this.state.showPickerAdd ? this.idHuyen[this.huyen].iD_Quan : 0,
          tinhThanh: this.state.showPickerAdd ? this.idTinh[this.tinh].iD_Tinh : 0,
          diaChi: this.state.diaChi,
        };
        resDeny = await postUpdateInfo(body);
        // console.log('xong up infor 1', urlImage)
      }
    } else {
      body = {
        ...this.props.Account,
        email: this.state.email,
        tenDayDu: this.state.hoTen,
        gioiTinh: this.state.gioiTinh,
        dienThoai: this.state.phone,
        cmt: this.state.cmt,
        phuongXa: this.state.showPickerAdd ? this.idXa[this.xa].iD_Phuong : 0,
        quanHuyen: this.state.showPickerAdd ? this.idHuyen[this.huyen].iD_Quan : 0,
        tinhThanh: this.state.showPickerAdd ? this.idTinh[this.tinh].iD_Tinh : 0,
        diaChi: this.state.diaChi,
        soTaiKhoan: this.state.stk,
      };
      resDeny = await postUpdateInfo(body);
    }

    this.setState({ loading: false })
    if (_.has(resDeny, 'data.success')) {
      this.props.setAccount(body)
      await AsyncStorageUtils.multiSet([
        [R.strings.INIT_STORGE, JSON.stringify(body)],
      ]);
      if (urlImage !== '') {
        this.setState({
          avatar: R.images.IMAGE + body.anhDaiDien
        })
      }
      popupOk('Thông báo', 'Cập nhật thành công')
      NavigationService.pop()
    } else {
      popupOk('Thông báo', 'Không thể cập nhật, vui lòng kiểm tra kết nối')
    }
  }

  checkBeforeUpdate = () => {
    if (this.state.showPickerAdd && this.xa < 0) {
      popupOk('Thông báo', 'Vui lòng chọn địa chỉ')
    } else {
      this.onPostData()
    }
  }

  componentDidMount = () => {
    this.getMetaData()
  }

  getMetaData = async () => {
    let dataTinh = await getTinh()
    this.dataTinh = []
    if (dataTinh.data) {
      dataTinh.data.map((item, index) => this.dataTinh.push(item.tenTinh))
      this.idTinh = dataTinh.data
      this._reRender()
    }
  }

  getMetaHuyen = async (index) => {
    this.setState({ miniLoading: true })
    let parram = `?IdTinh=${this.idTinh[index].iD_Tinh}`
    let dataHuyen = await getHuyen(parram)
    this.dataHuyen = []
    if (dataHuyen.data) {
      dataHuyen.data.map((item, index) => this.dataHuyen.push(item.tenQuan))
      this.idHuyen = dataHuyen.data
    }
    this.setState({ miniLoading: false })
  }


  getMetaXa = async (index) => {
    this.setState({ miniLoading: true })
    let parram = `?IdQuan=${this.idHuyen[index].iD_Quan}`
    let dataXa = await getXa(parram)
    this.dataXa = []
    if (dataXa.data) {
      dataXa.data.map((item, index) => this.dataXa.push(item.tenPhuong))
      this.idXa = dataXa.data
    }
    this.setState({ miniLoading: false })
  }

  setAdd = () => {
    this.setState({ diaChi: `${this.dataXa[this.xa]} - ${this.dataHuyen[this.huyen]} - ${this.dataTinh[this.tinh]}` })
  }

  renderPickerAdd = () =>
    // console.log('vaody')
    (
      <View>
        <View style={styles.wrapper}>
          <PickerItem
            placeholder="Chọn tỉnh/TP"
            width={WIDTH(270)}
            data={this.dataTinh}
            onChangeValue={(index, item) => { this.tinh = index; this.getMetaHuyen(index) }}
          />
        </View>
        {this.tinh > -1 && <View style={styles.wrapper}>
          <PickerItem
            placeholder="Chọn quận/huyện"
            width={WIDTH(270)}
            data={this.dataHuyen}
            onChangeValue={(index, item) => { this.huyen = index; this.getMetaXa(index) }}
          />
        </View>}
        {this.huyen > -1 && <View style={styles.wrapper}>
          <PickerItem
            placeholder="Chọn xã/phường/TT"
            width={WIDTH(270)}
            data={this.dataXa}
            onChangeValue={(index, item) => { this.xa = index; this.setAdd() }}
          />
        </View>}
        {this.state.miniLoading
          && <ActivityIndicator
            color={R.colors.yellow254}
            animating
            size="small"
          />
        }
      </View>
    )


  render() {
    if (!this.state.loading) {
      return (
        <ScrollView style={{ flex: 1, backgroundColor: R.colors.black3 }}>
          <HeaderBack title="Đổi thông tin" iconSearch={false} iconCart={false} />
          <View style={styles.viewTitle}>
            <Text style={styles.title}>Đổi thông tin tài khoản</Text>
          </View>

          <TouchableOpacity
            onPress={() => this.launchImageLibrary()}
            disabled={this.state.loading}
          >
            <View style={{ alignItems: 'center', }}>
              <FastImage
                source={this.state.avatar}
                style={styles.avatar}
                resizeMode={FastImage.resizeMode.stretch}
              />
            </View>
            <FastImage
              source={R.images.iconAdd}
              style={styles.img}
              resizeMode={FastImage.resizeMode.stretch}
            />
          </TouchableOpacity>
          <FormInput dataForm={this.state.formSignUp} type="profile" onValueChange={this.onValueChange} />


          <View style={styles.wrapper}>
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
              <FastImage
                style={styles.icon}
                source={R.images.iconCar}
                resizeMode={FastImage.resizeMode.contain}
              />
              <Text style={[styles.textInput, { color: R.colors.yellow254, paddingRight: WIDTH(10) }]}>{this.state.diaChi}</Text>
              <TouchableOpacity onPress={() => { this.setState({ showPickerAdd: true }) }}>
                <Text style={[styles.text, { textDecorationLine: 'underline' }]}>Thay đổi</Text>
              </TouchableOpacity>

            </View>
          </View>
          {this.state.showPickerAdd && this.renderPickerAdd()}


          <View style={{
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: HEIGHT(23),
            marginBottom: HEIGHT(23)
          }}
          >
            <BaseButton
              title="Đổi thông tin"
              onButton={() => this.checkBeforeUpdate()}
            />
          </View>
        </ScrollView>
      );
    } else {
      return (
        <View style={{ flex: 1, backgroundColor: R.colors.black3 }}>
          <HeaderBack title="Đổi thông tin" iconSearch={false} iconCart={false} />
          <LoadingComponent isLoading={this.state.loading} />
        </View>
      )
    }
  }
}

function mapStateToProps(state) {
  return {
    Account: state.userReducers.Account
  };
}

export default connect(mapStateToProps, {
  setAccount
})(ChangeInfo);

const styles = StyleSheet.create({
  avatar: {
    width: WIDTH(120),
    height: WIDTH(120),
    marginTop: HEIGHT(32),
    borderRadius: WIDTH(60),
  },
  img: {
    marginTop: HEIGHT(-30),
    width: WIDTH(28),
    height: WIDTH(28),
    marginLeft: WIDTH(213),
    borderWidth: 2,
    borderColor: R.colors.black0,
    borderRadius: WIDTH(14)
  },
  textInput: {
    flex: 1,
    color: R.colors.yellow254,
    fontSize: getFont(15),
    marginLeft: WIDTH(14),
  },
  viewTitle: {
    width: getWidth(),
    alignItems: 'center',
    marginTop: HEIGHT(24),
    justifyContent: 'center'
  },
  title: {
    color: R.colors.white,
    fontSize: getFont(20),
    fontWeight: '700',
    lineHeight: getLineHeight(28),
    width: getWidth(),
    textAlign: 'center'
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: HEIGHT(12),
    width: WIDTH(327),
    paddingLeft: WIDTH(10),
    borderBottomColor: R.colors.yellow254,
    borderBottomWidth: 1,
    alignSelf: 'center'
  },
  text: {
    fontFamily: R.fonts.Roboto,
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.white,
    paddingVertical: HEIGHT(10),
  },
  icon: {
    width: WIDTH(20),
    height: HEIGHT(15),
    marginBottom: HEIGHT(5)
  },
})
