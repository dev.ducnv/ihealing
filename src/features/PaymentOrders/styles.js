import { StyleSheet } from 'react-native';
import { WIDTH, HEIGHT, getFont, getLineHeight, getWidth } from '../../config';
import R from '../../assets/R';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: R.colors.backgroundBlack,
    alignItems: 'center'
  },
  logoStyle: {
    width: WIDTH(110),
    height: HEIGHT(86),
    marginTop: HEIGHT(64),
    marginBottom: HEIGHT(28)
  },
  logoBank: {
    width: WIDTH(40),
    height: WIDTH(40),
    marginRight: WIDTH(12)
  },
  text: {
    fontWeight: '500',
    fontSize: getFont(15),
    lineHeight: getLineHeight(22),
    color: R.colors.white,
    width: WIDTH(193)
  },
  navigate: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  contentBelowButton: {
    width: getWidth(),
    paddingHorizontal: WIDTH(24),
    marginTop: HEIGHT(25)
  },

  boxNote: {
    borderBottomLeftRadius: HEIGHT(4),
    borderBottomRightRadius: HEIGHT(4),
    backgroundColor: R.colors.black050,
    paddingVertical: HEIGHT(12),
    paddingHorizontal: WIDTH(18),
    marginTop: HEIGHT(16),
    width: WIDTH(343),
    marginLeft: -WIDTH(12)
  },
  textDes: {
    color: R.colors.white,
    fontSize: getFont(14),
    fontFamily: R.fonts.Roboto,
    width: WIDTH(319),

  },
  textLabel: {
    color: R.colors.yellow254,
    fontSize: getFont(14),
    fontFamily: R.fonts.Roboto,
    fontWeight: '500',
    marginRight: WIDTH(25)
  },
  viewBottomButton: {
    backgroundColor: R.colors.black050,
    paddingVertical: HEIGHT(16),
    alignItems: 'center',
    width: WIDTH(375),
    position: 'absolute',
    bottom: 0
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: HEIGHT(12),
    width: WIDTH(375),
    paddingHorizontal: WIDTH(16),
    paddingVertical: HEIGHT(16),
    borderBottomColor: R.colors.yellow254,
    borderBottomWidth: 1
  },
})
export default styles;
