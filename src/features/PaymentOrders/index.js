import React, { Component } from 'react';
import { View, Text, StatusBar, TouchableOpacity, ScrollView, Image, Clipboard } from 'react-native';
import FastImage from 'react-native-fast-image';
import Icon from 'react-native-vector-icons/AntDesign';
import NavigationService from 'routers/NavigationService';
import { LichSuDonHang } from 'routers/screenNames';
import { WIDTH, getFont, HEIGHT } from '../../config';
import R from '../../assets/R';
import FormInput from '../../common/Form/FormInput';
import BaseButton from '../../common/Button/BaseButton';
import styles from './styles';
import Modal from './Items/ModalBookingSuccess';

const FormSignIn = [
  { image: R.images.ic_user_lock, hint: 'Tên đăng nhập' },
  { image: R.images.ic_lock, hint: 'Mật khẩu' }
]
const informationPayment = {
  bank: {
    icon: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRND8ZVRka7qOE8Va2v-neRYV9aNVF_SeFDWg&usqp=CAU',
    name: 'ACB Ngân hàng á châu'
  },
  cardNumberID: '590 558 888',
  receiver: 'Cao Thị Thương - Ngân hàng ACB',
}
function PaymentInformation(props) {
  return (
    <View>
      <View style={styles.wrapper}>
        <Text style={styles.textLabel}>Ngân hàng:</Text>
        <View style={{ flexDirection: 'row', alignItems: 'center', width: WIDTH(250) }}>
          <Image source={{ uri: informationPayment.bank.icon }} style={styles.logoBank} resizeMode="stretch" />
          <Text style={styles.text}>{informationPayment.bank.name}</Text>
        </View>
      </View>
      <View style={styles.wrapper}>
        <Text style={styles.textLabel}>Số tài khoản:</Text>
        <View style={{ flexDirection: 'row', alignItems: 'center', width: WIDTH(250) }}>
          <Text style={styles.text}>{informationPayment.cardNumberID}</Text>
          <TouchableOpacity
            onPress={() => { Clipboard.setString(informationPayment.cardNumberID) }}
          >
            <Text style={[styles.text, { color: R.colors.yellow254, fontSize: getFont(12) }]}>Copy</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.wrapper}>
        <Text style={styles.textLabel}>Người thụ hưởng:</Text>
        <Text style={styles.text}>{informationPayment.receiver}</Text>
      </View>
      <View style={styles.wrapper}>
        <Text style={styles.textLabel}>Nội dung:</Text>
        <Text style={styles.text}>{`Vui lòng chuyển khoản với nội dung: "Thanh toan ${props.mathamchieu}"`}</Text>
      </View>
    </View>
  )
}
class PaymentOrders extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false
    };
  }

  onShowModal = () => {
    this.setState({ visible: !this.state.visible })
  }

  render() {
    let mathamchieu = this.props.navigation.getParam('mathamchieu')
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor={R.colors.background} />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
            <FastImage
              style={styles.logoStyle}
              source={R.images.iconCard}
              resizeMode={FastImage.resizeMode.contain}
            />
            <PaymentInformation mathamchieu={mathamchieu} />
            <View style={styles.boxNote}>
              <Text style={[styles.textDes, { width: WIDTH(311) }]}>
                Quý khách hàng vui lòng nhập đúng nội dung chuyển khoản để định danh đúng đơn hàng của mình!
              </Text>
              <Text style={[styles.textDes, { color: R.colors.yellow254 }]}>Hotline hỗ trợ: 093.652.5858 – 097.463.1299</Text>
            </View>
          </View>
          <Modal
            visible={this.state.visible}
            onShowModal={this.onShowModal}
          />
        </ScrollView>
        <View style={styles.viewBottomButton}>
          <BaseButton
            title="Hoàn thành đặt hàng"
            onButton={() => {
              this.onShowModal();
            }}
            hasImage
          />
        </View>
      </View>
    )
  }
}

export default PaymentOrders;
