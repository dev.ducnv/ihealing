import { StyleSheet } from 'react-native';
import R from '../../assets/R';
import { getFont, getLineHeight, HEIGHT, WIDTH, getWidth } from '../../config';

const styles = StyleSheet.create({
  tabbar: {
    flexDirection: 'row',
    elevation: 1,
    height: HEIGHT(44)
  },
  activeTabTextColor: {
    color: R.colors.colorMain,
    fontWeight: '500',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    fontFamily: R.fonts.Roboto
  },
  tabTextColor: {
    color: R.colors.black0,
    fontWeight: '500',
    fontSize: getFont(17),
    lineHeight: getLineHeight(22),
    fontFamily: R.fonts.Roboto
  },
  flex: {
    flex: 1
  },
  tab: {
    flexDirection: 'row',
    backgroundColor: R.colors.colorMain,
    elevation: 1,
    height: HEIGHT(44)
  },
  tabBar: {
    justifyContent: 'center',
    width: getWidth() / 2,
    alignItems: 'center',
    borderBottomWidth: 2,
    height: HEIGHT(44)
  },
  textTab: {
    color: R.colors.black0,
    fontSize: getFont(16),
    fontFamily: R.fonts.RobotoRegular
  },
  container: {
    flex: 1
  }
});

export default styles;
