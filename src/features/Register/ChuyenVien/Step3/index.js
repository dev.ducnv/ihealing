/* eslint-disable camelcase */
import React, { Component } from 'react';
import { View, Text, StatusBar, TouchableOpacity, ScrollView, FlatList } from 'react-native';
import FastImage from 'react-native-fast-image';
import _ from 'lodash';
import moment from 'moment';
import { connect } from 'react-redux'
import { register } from '../../../../apis/Functions/users';
import { popupOk, getFont } from '../../../../config/Function';
import { HEIGHT, WIDTH } from '../../../../config';
import PickerItem from '../../../../common/Picker/PickerItem';
import styles from './styles';
import R from '../../../../assets/R';
import BaseButton from '../../../../common/Button/BaseButton';
import Modal from '../../items/ModalRegisterSuccess';
import { LoadingComponent } from '../../../../common/Loading/LoadingComponent';
import FakeData from './FakeData';
import ModalSearchRegiter from '../../../../common/Modal/ModalSearchRegiter'
// const NguoiBaoTro = ['HDS2006000050 - Nguyễn Thị Thuỳ Dương - SDT: 0982319129']

class CVStep3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      position: this.props.iD_CapTren,
      nameBaoTro: null,
      itemBaoTro: null,
      visible: false,
      loading: false
    };
  }

  onChangePosition = (position) => {
    this.setState({ position })
    this.props.onChangeIdCapTren(position)
  }

  onShowModal = () => {
    this.setState({ visible: !this.state.visible })
  }

  onChangeStep = (step) => {
    this.props.onChangeStep(step);
  }

  renderItemInfo = ({ item }) => (
    <View style={styles.flexRow}>
      <FastImage
        style={styles.icon}
        source={item.image}
        resizeMode={FastImage.resizeMode.contain}
      />
      <Text style={styles.textContent}>{item.lable}</Text>
    </View>
  )

  renderItem = (item) => {
    const DataInfo = [
      { image: R.images.ic_address, lable: _.get(item, 'diaChi', '') },
      { image: R.images.ic_qr, lable: _.get(item, 'iD_QuanLy', '') },
      { image: R.images.ic_phone, lable: _.get(item, 'dienThoai', '') },
      { image: R.images.ic_email, lable: _.get(item, 'email', '') },
      { image: R.images.ic_birthday, lable: _.has(item, 'ngayPheDuyet') ? moment(new Date(item.ngayPheDuyet)).format('DD/MM/YYYY') : moment(new Date()).format('DD/MM/YYYY') }
    ];
    return (
      <View>
        <Text style={styles.textName}>{_.get(item, 'tenDayDu', 'Chưa có người bảo trợ cho bạn')}</Text>
        {item && <FlatList
          data={DataInfo}
          renderItem={this.renderItemInfo}
        />}
      </View>
    )
  }

  dangKyTaiKhoan = async (account) => {
    this.setState({ loading: true }, async () => {
      let res = await register(account);
      if (!_.isUndefined(res)) {
        this.setState({ loading: false })
        if (_.has(res, 'data')) {
          if (res.data.success) {
            // popupOk('Thông báo', 'Đăng ký tài khoản thành công!');
            this.onShowModal();
            // this.skip('Login');
          } else {
            popupOk('Thông báo', `Đăng ký thất bại! ${res.data.txt}`);
          }
        } else {
          popupOk('Thông báo', 'Đăng ký thất bại. Vui lòng kiểm tra kết nối');
        }
      } else {
        this.setState({ loading: false })
        popupOk('Thông báo', 'Đăng ký thất bại!');
      }
    });
  }

  onRegister = () => {
    if (this.state.itemBaoTro === null) {
      popupOk('Thông báo', 'Vui lòng chọn người bảo trợ!');
    } else {
      const account = this.props.getAccount();
      this.dangKyTaiKhoan({ ...account, ID_CapTren: this.state.position });
    }
  }

  converData = (data) => {
    let arr = [];
    data.map(item => {
      arr.push(`${item.maTaiKhoan} - ${item.tenDayDu} - SDT: ${item.dienThoai}`)
    })
    return arr
  }

  render() {
    const { NguoiBaoTro, position, Account } = this.props
    if (!this.state.loading) {
      return (
        <View style={styles.container}>
          <StatusBar backgroundColor={R.colors.background} />
          <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
            <View style={styles.container}>
              <Text style={styles.title}>Bước 3: Người bảo trợ</Text>
              <Text style={styles.content}>
                Hoàn thành quy trình để trở thành thành viên hệ thống
                {' '}
                <Text style={{ color: R.colors.yellow254 }}>IHEALING</Text>
              </Text>

              {/* <PickerItem
                  colorText={R.colors.yellow254}
                  sourceImage={R.images.ic_people}
                  value={this.state.position}
                  placeholder="Người bảo trợ"
                  width={WIDTH(265)}
                  data={this.converData(NguoiBaoTro)}
                  onChangeValue={this.onChangePosition}
                /> */}
              <TouchableOpacity
                style={[
                  styles.wrapper,
                  { justifyContent: 'flex-start', height: HEIGHT(50) },
                ]}
                onPress={() => this.ModalSearchRegiter.setModalVisible(true, this.props.iD_NhomTaiKhoan_ChoDuyet)}
              >
                <Text style={styles.textPicker}>
                  {this.state.itemBaoTro?.tenDayDu || 'Tìm kiếm người bảo trợ'}
                </Text>
              </TouchableOpacity>

              <View style={styles.infomation}>
                {this.renderItem(this.state.itemBaoTro)}
              </View>

              <View style={{ marginTop: HEIGHT(24) }}>
                <BaseButton
                  title="Hoàn tất đăng ký"
                  onButton={this.onRegister}
                />
              </View>
              <TouchableOpacity style={styles.wrapLogin} onPress={() => this.onChangeStep(2)}>
                <Text style={styles.text}>Quay lại</Text>
              </TouchableOpacity>
            </View>
            <Modal
              visible={this.state.visible}
              onShowModal={this.onShowModal}
              account={this.props.getAccount()}
            />
            <ModalSearchRegiter
              ref={ref => { this.ModalSearchRegiter = ref }}
              iD_NhomTaiKhoan_ChoDuyet={this.props.iD_NhomTaiKhoan_ChoDuyet}
              iD_QuanLy={Account?.iD_QuanLy ?? 0}
              onSearch={(value, item) => {
                this.setState({ itemBaoTro: item })
                this.onChangePosition(item?.iD_QuanLy)
                console.log('item', item)
              }}
              title="Tìm kiếm người bảo trợ"
            />
          </ScrollView>
        </View>
      )
    } return <LoadingComponent isLoading={this.state.loading} />
  }
}

function mapStateToProps(state) {
  return {
    Account: state.userReducers.Account
  };
}

export default connect(mapStateToProps, {})(CVStep3);
