/* eslint-disable camelcase */
/* eslint-disable react/jsx-closing-tag-location */
import React, { Component } from 'react';
import { View, Text, StatusBar, TouchableOpacity, ScrollView, Keyboard } from 'react-native';
import NavigationService from 'routers/NavigationService';
import moment from 'moment';
import _ from 'lodash';
import FastImage from 'react-native-fast-image';
import { getNguoiBaoTro } from 'apis/Functions/users'
import { connect } from 'react-redux';
import { popupOk, Xoadau, validateEmail, WIDTH } from '../../../config/Function';
import { HEIGHT } from '../../../config';
import FormInput from '../../../common/Form/FormInput';
import styles from './styles';
import R from '../../../assets/R';
import Step2 from './Step2';
import Step3 from './Step3';
import BaseButton from '../../../common/Button/BaseButton';

class CVStep1 extends Component {
  constructor(props) {
    super(props);
    const { Account } = props
    // console.log(Account)
    this.state = {
      step: 1,
      cmt: Account.cmt ? Account.cmt : '',
      dienThoai: Account.dienThoai ? Account.dienThoai : '',
      email: Account.email ? Account.email : '',
      gioiTinh: Account.gioiTinh ? Account.gioiTinh : 0,
      nguoiGT: {},
      matKhau: '',
      ngaySinh: moment(new Date(), 'DD/MM/YYYY HH:mm').toISOString(),
      tenDayDu: Account.tenDayDu ? Account.tenDayDu : '',
      username: Account.username ? Account.username : '',
      xacNhanMatKhau: '',

      iD_CapTren: 0,
      iD_NhomTaiKhoan_ChoDuyet: 2,
      NguoiBaoTro: [],
      loading: false
    };
  }

  onChangeStep = (step) => {
    const { cmt, dienThoai, email, gioiTinh, nguoiGT, matKhau, ngaySinh, tenDayDu, username, xacNhanMatKhau } = this.state;
    Keyboard.dismiss();
    let account = {
      cmt: cmt.trim(),
      dienThoai: dienThoai.trim(),
      email: email.trim(),
      gioiTinh,
      iD_NguoiGioiThieu: _.get(nguoiGT, 'maTaiKhoan', null),
      matKhau,
      ngaySinh,
      tenDayDu: tenDayDu.trim(),
      username: username.trim(),
      xacNhanMatKhau,
      iD_QuanLy: this.props.Account.iD_QuanLy || 0
    }
    // this.setState({ step })

    if (matKhau === '' || account.tenDayDu === '' || account.username === '' || xacNhanMatKhau === '') {
      popupOk('Thông báo', 'Vui lòng nhập đầy đủ các trường đánh dấu!')
    } else if (matKhau !== xacNhanMatKhau) {
      popupOk('Thông báo', 'Mật khẩu không khớp')
    } else {
      this.setState({ step })
    }
  }

  onChangeCmt = (cmt) => {
    this.setState({ cmt })
  }

  onChangeDienThoai = (dienThoai) => {
    this.setState({ dienThoai })
  }

  onChangeEmail = (email) => {
    this.setState({ email })
  }

  onChangeGioiTinh = (gioiTinh) => {
    this.setState({ gioiTinh })
  }

  onSearch = (nguoiGT) => {
    this.setState({ nguoiGT: { ...nguoiGT } })
  }

  onChangeMatKhau = (matKhau) => {
    this.setState({ matKhau })
  }

  onChangeXacNhanMatKhau = (xacNhanMatKhau) => {
    this.setState({ xacNhanMatKhau })
  }

  onChangeNgaySinh = (ngaySinh) => {
    this.setState({ ngaySinh })
  }

  onChangeTenDayDu = (tenDayDu) => {
    this.setState({ tenDayDu })
  }

  onChangeUsername = (username) => {
    this.setState({ username: username?.trim()?.replace(/[^a-z0-9_@\s]/gi, '') })
  }

  onChangeIdCapTren = (iD_CapTren) => {
    this.setState({ iD_CapTren })
    // this.getNguoiBaoTro()
  }

  onChangeIdNhomTaiKhoanChoDuyet = (iD_NhomTaiKhoan_ChoDuyet) => {
    this.setState({ iD_NhomTaiKhoan_ChoDuyet, iD_CapTren: 0 }, () => {
      this.getNguoiBaoTro()
    })
    // console.log('iD_NhomTaiKhoan_ChoDuyet_iD_NhomTaiKhoan_ChoDuyet', iD_NhomTaiKhoan_ChoDuyet)
  }

  componentDidMount() {
    this.getNguoiBaoTro()
  }

  getNguoiBaoTro = async () => {
    const { iD_NhomTaiKhoan_ChoDuyet } = this.state
    let resBaoTro = await getNguoiBaoTro(iD_NhomTaiKhoan_ChoDuyet, this.props.Account.iD_QuanLy || 0)
    // console.log(resBaoTro)
    if (resBaoTro && resBaoTro.data) {
      this.setState({
        NguoiBaoTro: resBaoTro.data
      })
    }
  }

  getAccount = () => {
    const { cmt, dienThoai, email, gioiTinh, nguoiGT, matKhau, ngaySinh, tenDayDu, username, xacNhanMatKhau, iD_CapTren, iD_NhomTaiKhoan_ChoDuyet } = this.state;
    let account = {
      cmt: cmt.trim() || '',
      dienThoai: dienThoai.trim() || '',
      email: email.trim(),
      gioiTinh: gioiTinh || '',
      ID_NguoiGioiThieu: _.get(nguoiGT, 'maTaiKhoan', null),
      matKhau,
      ngaySinh,
      tenDayDu: tenDayDu.trim(),
      username: username.trim(),
      xacNhanMatKhau,
      ID_CapTren: _.get(this.state.NguoiBaoTro[iD_CapTren], 'iD_QuanLy', 0),
      ID_NhomTaiKhoan_ChoDuyet: iD_NhomTaiKhoan_ChoDuyet,
      ID_QuanLy: this.props.Account.iD_QuanLy || 0
    }
    return account;
  }

  getFormSignUp = () => [
    { image: R.images.ic_user_lock, hint: 'Tài khoản đăng nhập', isCharacters: true, func: this.onChangeUsername, value: _.get(this.state, 'username', ''), isRequire: true },
    { image: R.images.ic_user, hint: 'Tên đầy đủ', func: this.onChangeTenDayDu, value: _.get(this.state, 'tenDayDu', ''), isRequire: true },
    { image: R.images.ic_phone, hint: 'Số điện thoại', isKeyboadNum: true, func: this.onChangeDienThoai, value: _.get(this.state, 'dienThoai', '') },
    { image: R.images.ic_lock, hint: 'Mật khẩu', isSecure: true, func: this.onChangeMatKhau, value: _.get(this.state, 'matKhau', ''), isRequire: true },
    { image: R.images.ic_lock, hint: 'Nhập lại mật khẩu', isSecure: true, func: this.onChangeXacNhanMatKhau, value: _.get(this.state, 'xacNhanMatKhau', ''), isRequire: true },
    { image: R.images.ic_person_id, hint: 'ID hoặc Số điện thoại người giới thiệu (nếu có)', isSearch: true, func: this.onSearch, value: _.get(this.state, 'nguoiGT', {}) },
  ]

  getPhoneSecure = (phone) => {
    let securePhone = '';
    for (let i = 0; i < phone.length; i += 1) {
      if (i <= 2 || i >= phone.length - 3) { securePhone += phone[i]; } else { securePhone += '*'; }
    }
    return securePhone;
  }

  renderNguoiGT = (item) => (
    <View style={styles.nguoiGTContainer}>
      <Text style={styles.lable}>Thông tin người giới thiệu</Text>
      <View style={styles.flexRow}>
        <FastImage
          style={styles.imgAvatar}
          source={{ uri: R.images.IMAGE + item.anhDaiDien }}
          resizeMode={FastImage.resizeMode.contain}
        />
        <View style={styles.wrapContent}>
          <Text style={styles.lableName}>{item.tenDayDu}</Text>
          <Text style={styles.lablePhone}>{this.getPhoneSecure(item.dienThoai)}</Text>
        </View>
      </View>
    </View>
  )

  render() {
    const { nguoiGT, NguoiBaoTro, iD_CapTren, iD_NhomTaiKhoan_ChoDuyet } = this.state;
    let FormSignUp = this.getFormSignUp();
    return (
      <View style={{ flex: 1 }}>
        {this.state.step === 1 && <View style={styles.container}>
          <StatusBar backgroundColor={R.colors.background} />
          <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
            <View style={styles.container}>
              <Text style={styles.title}>Bước 1: Nhập thông tin tài khoản</Text>
              <Text style={styles.content}>
                Hoàn thành quy trình để trở thành thành viên hệ thống
                {' '}
                <Text style={{ color: R.colors.yellow254 }}>IHEALING</Text>
              </Text>
              <FormInput dataForm={FormSignUp} />
              {!_.isUndefined(nguoiGT.tenDayDu) ? this.renderNguoiGT(nguoiGT) : <View />}
              <View style={{ marginTop: HEIGHT(24) }}>
                <BaseButton
                  title="Đăng ký"
                  onButton={() => this.onChangeStep(2)}
                />
              </View>
              <View style={styles.wrapLogin}>
                <Text style={styles.text}>Bạn đã có tài khoản?  </Text>
                <TouchableOpacity onPress={() => NavigationService.navigate('Login')}>
                  <Text style={[styles.text, { fontWeight: 'bold', textAlign: 'center', width: WIDTH(150) }]}>Đăng nhập</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </View>}
        {this.state.step === 2 && <Step2
          onChangeStep={this.onChangeStep}
          onChangeIdNhomTaiKhoanChoDuyet={this.onChangeIdNhomTaiKhoanChoDuyet}
          iD_NhomTaiKhoan_ChoDuyet={iD_NhomTaiKhoan_ChoDuyet}
        />}
        {this.state.step === 3 && <Step3
          onChangeStep={this.onChangeStep}
          NguoiBaoTro={NguoiBaoTro}
          onChangeIdCapTren={this.onChangeIdCapTren}
          iD_NhomTaiKhoan_ChoDuyet={iD_NhomTaiKhoan_ChoDuyet}
          getAccount={this.getAccount}
          iD_CapTren={iD_CapTren}
        />}
      </View>
    )
  }
}

function mapStateToProps(state) {
  return {
    Account: state.userReducers.Account
  };
}

export default connect(mapStateToProps, {
})(CVStep1);
