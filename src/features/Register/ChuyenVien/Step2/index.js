import React, { Component } from 'react';
import { View, Text, StatusBar, TouchableOpacity, ScrollView } from 'react-native';
import NavigationService from 'routers/NavigationService';
import PickerDropdown from '../../../../common/Picker/PickerDropdown';
import { HEIGHT, WIDTH } from '../../../../config';
import PickerItem from '../../../../common/Picker/PickerItem';
import styles from './styles';
import R from '../../../../assets/R';
import BaseButton from '../../../../common/Button/BaseButton';
import FakeInfo from './FakeInfo';

const Role = ['Chuyên gia', 'Chuyên viên tư vấn', 'Quản lý']

class CVStep2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      position: this.props.iD_NhomTaiKhoan_ChoDuyet - 2
    };
  }

  onChangePosition = (position) => {
    this.setState({ position })
    this.props.onChangeIdNhomTaiKhoanChoDuyet(position + 2)
  }

  onChangeStep = (step) => {
    this.props.onChangeStep(step);
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor={R.colors.background} />
        <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
          <View style={styles.container}>
            <Text style={styles.title}>Bước 2: Chọn vị trí phù hợp</Text>
            <Text style={styles.content}>
              Hoàn thành quy trình để trở thành thành viên hệ thống
              {' '}
              <Text style={{ color: R.colors.yellow254 }}>IHEALING</Text>
            </Text>

            <View style={styles.wrapper}>
              <PickerItem
                colorText={R.colors.yellow254}
                sourceImage={R.images.ic_people}
                value={this.state.position}
                placeholder="Chuyên gia"
                width={WIDTH(265)}
                data={Role}
                onChangeValue={this.onChangePosition}
              />
            </View>

            <View style={styles.infomation}>
              <ScrollView style={{ flex: 1 }}>
                <Text style={styles.textInfo}>{FakeInfo[this.state.position]}</Text>
              </ScrollView>
            </View>

            <View style={{ marginTop: HEIGHT(24) }}>
              <BaseButton
                title="Tiếp theo"
                onButton={() => this.onChangeStep(3)}
              />
            </View>
            <TouchableOpacity style={styles.wrapLogin} onPress={() => this.onChangeStep(1)}>
              <Text style={styles.text}>Quay lại</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    )
  }
}

export default CVStep2;
