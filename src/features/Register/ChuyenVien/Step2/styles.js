import { StyleSheet } from 'react-native';
import R from '../../../../assets/R';
import { getFont, getLineHeight, HEIGHT, WIDTH } from '../../../../config';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: R.colors.background,
    alignItems: 'center'
  },
  title: {
    marginTop: HEIGHT(18),
    fontWeight: '500',
    fontSize: getFont(20),
    lineHeight: getLineHeight(28),
    color: R.colors.white,
    alignSelf: 'center'
  },
  content: {
    marginTop: HEIGHT(4),
    marginBottom: HEIGHT(20),
    color: R.colors.grey300,
    fontWeight: 'normal',
    fontSize: getFont(15),
    lineHeight: getLineHeight(22),
    textAlign: 'center',
    width: WIDTH(327),
    alignSelf: 'center'
  },
  wrapLogin: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginVertical: HEIGHT(20)
  },
  text: {
    fontSize: getFont(15),
    lineHeight: getLineHeight(22),
    fontWeight: 'normal',
    color: R.colors.grey400,
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: HEIGHT(12),
    width: WIDTH(327),
    paddingHorizontal: WIDTH(10),
    borderBottomColor: R.colors.yellow254,
    borderBottomWidth: 1
  },
  infomation: {
    width: WIDTH(351),
    height: HEIGHT(376),
    padding: WIDTH(16),
    backgroundColor: R.colors.blackChecked,
    borderRadius: WIDTH(4),
    elevation: 3,
    marginTop: HEIGHT(36),
    marginBottom: HEIGHT(24)
  },
  textInfo: {
    color: R.colors.white,
    fontSize: getFont(15),
    lineHeight: getLineHeight(22),
    fontWeight: 'normal',
    textAlign: 'justify'
  }
});

export default styles;
