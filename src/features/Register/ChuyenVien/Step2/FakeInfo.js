const info = [
  'Quyền lợi:\nĐược đào tạo nghề Chăm sóc sức khỏe và sắc đẹp từ cơ bản tới nâng cao, thường xuyên cập nhật và đào tạo nâng cao tay nghề. \nĐược cấp chứng chỉ hành nghề trên toàn quốc sau khóa đào tạo và vượt qua bài kiểm tra của iHealing. \nSau khi tham gia vào hệ thống, mỗi chuyên gia sẽ được cấp 01 mã ID và thẻ thành viên, tiện cho việc quản lý và hưởng các ưu đãi của hệ thống. \nĐược hưởng 20% giá trị sản phẩm từ sản phẩm tiêu hao và sản phẩm bán cho khách hàng.',
  'Tư vấn viên tiếp xúc trực tiếp hoặc gián tiếp với khách hàng, có trách nhiệm tư vấn về sản phẩm/dịch vụ, giải quyết các thắc mắc tạm thời của khách hàng, giúp họ lựa chọn và sử dụng các sản phẩm, dịch vụ một cách hiệu quả.',
  'Quản lý điều hành và hỗ trợ hệ thống các chuyên gia/chuyên viên tư vấn do mình đảm nhận, giúp họ phát triển và nâng cao kỹ năng, đồng thời hỗ trợ tìm kiếm khách hàng, giải quyết những khúc mắc phát sinh trong quá trình chuyên gia/chuyên viên tư vấn gặp phải trong khâu chăm sóc khách hàng.'
];
export default info;
