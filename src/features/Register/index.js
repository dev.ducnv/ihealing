// @flow
import React, { Component } from 'react';
import { SafeAreaView, Dimensions, Text, View, StatusBar, TouchableHighlight, Animated } from 'react-native';
import { TabView } from 'react-native-tab-view';
import LinearGradient from 'react-native-linear-gradient';
import KhachHang from './KhachHang';
import ChuyenVien from './ChuyenVien/Step1';
import R from '../../assets/R';
import styles from './styles';

type Props = {
}

type State = {
  index: number,
  routes: Array<{ key: number, title: string }, >,
  loading: boolean
}

class TabCalendar extends Component<Props, State> {
  state = {
    index: this.props.navigation.getParam('index') || 0,
    routes: [
      { key: 1, title: 'Khách hàng' },
      { key: 2, title: 'Chuyên viên' }
    ],
    loading: true
  };

  componentDidMount() {
    setTimeout(() => { this.setState({ loading: false }) }, 100)
  }

  _renderScene = ({ route }: Object) => {
    switch (route.key) {
      case 1: {
        return <KhachHang />;
      }
      case 2:
        return <ChuyenVien />;
      default:
        return null;
    }
  };

  renderLabel = ({ route, focused }: Object) => (
    <View>
      <Text
        style={[focused ? styles.activeTabTextColor : styles.tabTextColor]}
      >
        {route.title}
      </Text>
    </View>
  )

  renderTabBar = props => {
    const inputRange = props.navigationState.routes.map((key, i) => i)
    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 1 }}
        colors={[R.colors.yellow1c8, R.colors.yellow254, R.colors.yellow1c8]}
        style={styles.tab}
      >
        {props.navigationState.routes.map((route, i) => {
          const opacity = props.position.interpolate({
            inputRange,
            outputRange: inputRange.map(
              inputIndex => (inputIndex === i ? 1 : 0.5)
            ),
          });
          const bcolor = props.position.interpolate({
            inputRange,
            outputRange: inputRange.map(
              inputIndex => (inputIndex === i ? '#000' : '#fff')
            ),
          });
          return (
            <TouchableHighlight
              activeOpacity={0.6}
              underlayColor="transparent"
              onPress={() => { this.setState({ index: i }) }}
            >
              <Animated.View style={[styles.tabBar, { borderBottomColor: bcolor }]}>
                <Animated.Text style={[styles.textTab, { opacity }]}>{route.title}</Animated.Text>
              </Animated.View>
            </TouchableHighlight>
          );
        })}

      </LinearGradient>
    )
  };

  render() {
    return (
      <SafeAreaView style={styles.flex}>
        <StatusBar backgroundColor={R.colors.background} />
        <KhachHang />
      </SafeAreaView>
    );
  }
}

export default TabCalendar;
