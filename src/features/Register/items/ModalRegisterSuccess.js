import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import BottomModal, { ModalContent, SlideAnimation } from 'react-native-modals';
import Icon from 'react-native-vector-icons/Ionicons';
import NavigationService from 'routers/NavigationService';
import BaseButton from '../../../common/Button/BaseButton';
import { WIDTH, getLineHeight, getFont, HEIGHT } from '../../../config';
import R from '../../../assets/R'

const getPasswordSecure = (pass) => {
  let securePass = '';
  for (let i = 0; i < pass.length; i += 1) {
    if (i <= 1 || i >= pass.length - 2) { securePass += pass[i]; } else { securePass += '*'; }
  }
  return securePass;
}

const ModalChuyenVien = (props) => {
  const { onShowModal, visible, account } = props;
  return (
    <BottomModal
      modalAnimation={new SlideAnimation({
        initialValue: 0,
        slideFrom: 'bottom',
        useNativeDriver: true,
      })}
      visible={visible}
      onTouchOutside={() => {
        NavigationService.navigate('Login')
        onShowModal();
      }}
      onHardwareBackPress={() => {
        NavigationService.navigate('Login')
        onShowModal();
      }}
    >
      <ModalContent>
        <View style={styles.container}>
          <Icon
            name="ios-checkmark-circle"
            size={WIDTH(92)}
            color={R.colors.blackChecked}
          />
          <Text style={styles.success}>Đăng ký thành công</Text>
          <Text style={[styles.username, { marginTop: HEIGHT(20) }]}>{`Tên tài khoản: ${account.username}`}</Text>
          <Text style={[styles.username, { marginBottom: HEIGHT(24) }]}>{`Mật khẩu: ${account.matKhau && getPasswordSecure(account.matKhau)}`}</Text>
          <Text style={[styles.username, { color: R.colors.black0 }]}>Đối với chuyên gia vui lòng thanh toán đặt cọc theo điều kiện đã nêu trong trách nhiệm của chuyên gia để có thể hoàn tất thủ tục đăng ký!</Text>

          <View style={styles.flexRow}>
            <TouchableOpacity
              style={styles.btnExit}
              onPress={() => {
                NavigationService.navigate('Login'); onShowModal()
              }}
            >
              <Text style={[styles.username, { color: R.colors.yellow254 }]}>Thoát</Text>
            </TouchableOpacity>
            <BaseButton
              title="Đăng nhập ngay"
              width={WIDTH(186)}
              height={HEIGHT(48)}
              onButton={() => {
                NavigationService.navigate('Login');
                onShowModal();
              }}
            />
          </View>
        </View>
      </ModalContent>
    </BottomModal>
  )
}

export default ModalChuyenVien;

const styles = StyleSheet.create({
  container: {
    width: WIDTH(320),
    height: HEIGHT(450),
    paddingHorizontal: WIDTH(8),
    paddingVertical: HEIGHT(28),
    alignItems: 'center',
    justifyContent: 'center'
  },
  success: {
    fontWeight: '500',
    fontSize: getFont(22),
    lineHeight: getLineHeight(30),
    color: R.colors.black0,
    textAlign: 'justify'
  },
  username: {
    fontWeight: '500',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.blackChecked,
    textAlign: 'justify'
  },
  flexRow: {
    flexGrow: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: HEIGHT(28)
  },
  btnExit: {
    width: WIDTH(108),
    height: HEIGHT(48),
    borderRadius: WIDTH(100),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: R.colors.darkBlue,
    marginRight: WIDTH(17)
  }
})
