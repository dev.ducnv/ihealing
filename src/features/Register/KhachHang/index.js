/* eslint-disable camelcase */
import React, { Component } from 'react';
import { View, Text, StatusBar, TouchableOpacity, ScrollView, Keyboard } from 'react-native';
import NavigationService from 'routers/NavigationService';
import _ from 'lodash';
import moment from 'moment';
import FastImage from 'react-native-fast-image';
import { connect } from 'react-redux';
import { HEIGHT } from '../../../config';
import FormInput from '../../../common/Form/FormInput';
import styles from './styles';
import { register } from '../../../apis/Functions/users';
import { popupOk, Xoadau, validateEmail, WIDTH } from '../../../config/Function';
import R from '../../../assets/R';
import Modal from '../items/ModalRegisterSuccess';
import BaseButton from '../../../common/Button/BaseButton';
import { LoadingComponent } from '../../../common/Loading/LoadingComponent';

class KHRegister extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cmt: '',
      dienThoai: '',
      email: '',
      gioiTinh: 0,
      matKhau: '',
      ngaySinh: moment(new Date(), 'DD/MM/YYYY HH:mm').toISOString(),
      tenDayDu: '',
      username: '',
      xacNhanMatKhau: '',
      nguoiGT: {},
      loading: false,
      visible: false
    };
  }

  onChangeCmt = (cmt) => {
    this.setState({ cmt })
  }

  onChangeDienThoai = (dienThoai) => {
    this.setState({ dienThoai })
  }

  onChangeEmail = (email) => {
    this.setState({ email })
  }

  onChangeGioiTinh = (gioiTinh) => {
    this.setState({ gioiTinh })
  }

  onChangeMatKhau = (matKhau) => {
    this.setState({ matKhau })
  }

  onChangeXacNhanMatKhau = (xacNhanMatKhau) => {
    this.setState({ xacNhanMatKhau })
  }

  onChangeNgaySinh = (ngaySinh) => {
    this.setState({ ngaySinh })
  }

  onChangeTenDayDu = (tenDayDu) => {
    this.setState({ tenDayDu })
  }

  onChangeUsername = (username) => {
    this.setState({ username: username?.trim()?.replace(/[^a-z0-9_@\s]/gi, '') })
  }

  onSearch = (nguoiGT) => {
    this.setState({ nguoiGT: { ...nguoiGT } })
  }

  onShowModal = () => {
    this.setState({ visible: !this.state.visible })
  }

  getPhoneSecure = (phone) => {
    let securePhone = '';
    for (let i = 0; i < phone.length; i += 1) {
      if (i <= 2 || i >= phone.length - 3) { securePhone += phone[i]; } else { securePhone += '*'; }
    }
    return securePhone;
  }

  onButton = async () => {
    const { cmt, dienThoai, email, gioiTinh, nguoiGT, matKhau, ngaySinh, tenDayDu, username, xacNhanMatKhau } = this.state;
    // console.log('object', nguoiGT)
    Keyboard.dismiss();
    let account = {
      cmt: cmt.trim() || '',
      dienThoai: dienThoai.trim() || '',
      email: email.trim(),
      gioiTinh: gioiTinh || '',
      iD_NguoiGioiThieu: _.get(nguoiGT, 'iD_QuanLy', null),
      maNguoiGioiThieu: _.get(nguoiGT, 'maTaiKhoan', null),
      iD_CapTren: _.get(nguoiGT, 'iD_QuanLy', null),
      matKhau,
      ngaySinh,
      tenDayDu: tenDayDu.trim(),
      username: username.trim(),
      xacNhanMatKhau
    }
    if (matKhau === '' || account.tenDayDu === '' || account.username === '' || xacNhanMatKhau === '') {
      popupOk('Thông báo', 'Vui lòng nhập đầy đủ các trường đánh dấu!')
    } else if (matKhau !== xacNhanMatKhau) {
      popupOk('Thông báo', 'Mật khẩu không khớp')
    } else {
      this.dangKyTaiKhoan(account);
    }
  }

  skip = (routeName) => {
    NavigationService.reset(routeName);
  }

  dangKyTaiKhoan = async (account) => {
    this.setState({ loading: true }, async () => {
      let res = await register(account);
      if (!_.isUndefined(res)) {
        this.setState({ loading: false })
        if (_.has(res, 'data')) {
          if (res.data.success) {
            this.onShowModal();
          } else {
            popupOk('Thông báo', `Đăng ký thất bại! ${res.data.txt}`);
          }
        } else {
          popupOk('Thông báo', 'Đăng ký thất bại! Vui lòng kiểm tra kết nối');
        }
      } else {
        this.setState({ loading: false })
        popupOk('Thông báo', 'Đăng ký thất bại!');
      }
    });
  }

  renderNguoiGT = (item) => (
    <View style={styles.nguoiGTContainer}>
      <Text style={styles.lable}>Thông tin người giới thiệu</Text>
      <View style={styles.flexRow}>
        <FastImage
          style={styles.imgAvatar}
          source={{ uri: R.images.IMAGE + item.anhDaiDien }}
          resizeMode={FastImage.resizeMode.contain}
        />
        <View style={styles.wrapContent}>
          <Text style={styles.lableName}>{item.tenDayDu}</Text>
          <Text style={styles.lablePhone}>{this.getPhoneSecure(item.dienThoai)}</Text>
        </View>
      </View>
    </View>
  )

  getFormSignUp = () => [
    { image: R.images.ic_user_lock, hint: 'Tài khoản đăng nhập', isCharacters: true, func: this.onChangeUsername, value: _.get(this.state, 'username', ''), isRequire: true },
    { image: R.images.ic_user, hint: 'Tên đầy đủ', func: this.onChangeTenDayDu, value: _.get(this.state, 'tenDayDu', ''), isRequire: true },
    { image: R.images.ic_phone, hint: 'Số điện thoại', isKeyboadNum: true, func: this.onChangeDienThoai, value: _.get(this.state, 'dienThoai', '') },
    { image: R.images.ic_lock, hint: 'Mật khẩu', isSecure: true, func: this.onChangeMatKhau, value: _.get(this.state, 'matKhau', ''), isRequire: true },
    { image: R.images.ic_lock, hint: 'Nhập lại mật khẩu', isSecure: true, func: this.onChangeXacNhanMatKhau, value: _.get(this.state, 'xacNhanMatKhau', ''), isRequire: true },
    { image: R.images.ic_person_id, hint: 'ID hoặc Số điện thoại người giới thiệu (nếu có)', isSearch: true, func: this.onSearch, value: _.get(this.state, 'nguoiGT', {}) },
  ]

  getAccount = () => {
    const { cmt, dienThoai, email, gioiTinh, nguoiGT, matKhau, ngaySinh, tenDayDu, username, xacNhanMatKhau } = this.state;
    let account = {
      cmt: cmt.trim(),
      dienThoai: dienThoai.trim(),
      email: email.trim(),
      gioiTinh,
      ID_NguoiGioiThieu: _.get(nguoiGT, 'maTaiKhoan', null),
      matKhau,
      ngaySinh,
      tenDayDu: tenDayDu.trim(),
      username: username.trim(),
      xacNhanMatKhau
    }
    return account;
  }

  render() {
    const { nguoiGT } = this.state;
    let FormSignUp = this.getFormSignUp();
    if (!this.props.Account.tenDayDu) {
      if (!this.state.loading) {
        return (
          <View style={styles.container}>
            <StatusBar backgroundColor={R.colors.background} />
            <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
              <View style={styles.container}>
                <Text style={styles.title}>Tạo tài khoản</Text>
                <Text style={styles.content}>Tạo tài khoản miễn phí để trải nghiệm dịch vụ của chúng tôi.</Text>
                <FormInput dataForm={FormSignUp} />

                {!_.isUndefined(nguoiGT.tenDayDu) ? this.renderNguoiGT(nguoiGT) : <View />}
                <View>
                  <View style={{ marginTop: HEIGHT(24) }}>
                    <BaseButton
                      title="Đăng ký"
                      disable={this.state.visible}
                      onButton={this.onButton}
                    />
                  </View>
                  <View style={styles.wrapLogin}>
                    <Text style={styles.text}>Bạn đã có tài khoản?  </Text>
                    <TouchableOpacity onPress={() => NavigationService.navigate('Login')}>
                      <Text style={[styles.text, { fontWeight: 'bold', textAlign: 'center', width: WIDTH(150) }]}>Đăng nhập</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <Modal
                visible={this.state.visible}
                onShowModal={this.onShowModal}
                account={this.getAccount()}
              />
            </ScrollView>
          </View>
        )
      } return <LoadingComponent isLoading={this.state.loading} />
    } else {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: R.colors.black3 }}>
          <Text style={[styles.title, { color: R.colors.yellow254, textAlign: 'center', paddingHorizontal: WIDTH(25) }]}>Tài khoản của bạn hiện đã là khách hàng của iHealing</Text>
        </View>
      )
    }
  }
}

function mapStateToProps(state) {
  return {
    Account: state.userReducers.Account
  };
}

export default connect(mapStateToProps, {
})(KHRegister);
