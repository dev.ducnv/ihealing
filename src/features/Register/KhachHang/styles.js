import { StyleSheet } from 'react-native';
import R from '../../../assets/R';
import { getFont, getLineHeight, HEIGHT, WIDTH } from '../../../config';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: R.colors.background,
    alignItems: 'center'
  },
  title: {
    marginTop: HEIGHT(18),
    fontWeight: '500',
    fontSize: getFont(20),
    lineHeight: getLineHeight(28),
    color: R.colors.white,
    alignSelf: 'center'
  },
  content: {
    marginTop: HEIGHT(4),
    marginBottom: HEIGHT(20),
    color: R.colors.grey300,
    fontWeight: 'normal',
    fontSize: getFont(15),
    lineHeight: getLineHeight(22),
    textAlign: 'center',
    width: WIDTH(327),
    alignSelf: 'center'
  },
  wrapLogin: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginVertical: HEIGHT(20)
  },
  text: {
    fontSize: getFont(15),
    lineHeight: getLineHeight(22),
    fontWeight: 'normal',
    color: R.colors.orange37C,
  },
  nguoiGTContainer: {
    width: WIDTH(327),
    height: HEIGHT(130),
    padding: WIDTH(16),
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    alignSelf: 'center',
    backgroundColor: R.colors.black050,
    marginTop: HEIGHT(12),
    borderRadius: WIDTH(4)
  },
  lable: {
    color: R.colors.white,
    fontSize: getFont(14),
    lineHeight: getLineHeight(18),
    fontWeight: '500'
  },
  imgAvatar: {
    width: WIDTH(64),
    height: WIDTH(64),
    borderRadius: WIDTH(4)
  },
  wrapContent: {
    marginLeft: WIDTH(16),
    height: HEIGHT(64),
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  lableName: {
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.yellow254,
    fontWeight: '500'
  },
  lablePhone: {
    fontSize: getFont(15),
    lineHeight: getLineHeight(22),
    color: R.colors.yellow254,
    fontWeight: 'normal'
  },
  flexRow: {
    flexDirection: 'row'
  }
});

export default styles;
