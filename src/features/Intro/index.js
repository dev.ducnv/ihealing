// @flow
import { Image, StatusBar, View, Text, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import * as React from 'react';
import * as Animatable from 'react-native-animatable';
import { connect } from 'react-redux';
import axios from 'axios';
import AsyncStorageUtils from 'helpers/AsyncStorageUtils';
import _ from 'lodash';
import NavigationService from '../../routers/NavigationService';
import { setAccount } from '../../actions/actionCreators';
import styles from './styles';
import i18n, { setLocation } from '../../assets/languages/i18n';
import R from '../../assets/R';

type Props = {

}

export class Intro extends React.PureComponent<Props> {
  timeout = '';

  async componentDidMount() {
    let routeName = 'Home';
    setLocation(i18n, 'vi')
    await AsyncStorage.multiGet([
      AsyncStorageUtils.KEY.INIT_STORGE
    ], async (err, results) => {
      // console.log('results', results)
      let account = JSON.parse(results[0][1]);
      // console.log('token==============>', account.token)
      if (account !== null) {
        axios.defaults.headers.common.Authorization = `${_.has(account, 'token') ? account.token : null}`;

        // axios.defaults.headers.common.Authorization = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImloZWFsaW5nIiwiVXNlcm5hbWUiOiJxdWFubHkxIiwibmJmIjoxNjAwNDEwMDQwLCJleHAiOjE2MDEwMTQ4NDAsImlhdCI6MTYwMDQxMDA0MCwiaXNzIjoic29udHEiLCJhdWQiOiJzb250cSJ9.cPlLzK_RTvGeQ28ozA_eEqgY7WWFy1czK9nLnVjCm-4';
        this.props.setAccount(account)
        routeName = 'Home';
      } else {
        this.props.setAccount({})

        // axios.defaults.headers.common.Authorization = '';
      }
    });
    this.changeScreen(routeName);
  }

  changeScreen(routeName: string) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      NavigationService.reset(routeName)
    }, 3500);
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor={R.colors.black3} />
        <Animatable.View
          animation="bounceIn"
          direction="alternate"
          duration={4000}
          style={styles.logoContainer}
        >
          <Image
            resizeMode="contain"
            source={R.images.logoMain}
            style={styles.image}
          />
        </Animatable.View>
      </View>
    );
  }
}
function mapStateToProps() {
  return {

  };
}

export default connect(mapStateToProps, {
  setAccount
})(Intro);
