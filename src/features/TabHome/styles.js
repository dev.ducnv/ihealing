import { StyleSheet } from 'react-native';
import R from '../../assets/R';
import { HEIGHT, getFont, getWidth, WIDTH } from '../../config';

const styles = StyleSheet.create({
  scene: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  numCart: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: R.colors.red646,
    right: WIDTH(32),
    top: WIDTH(9),
    width: WIDTH(22),
    height: WIDTH(22),
    borderRadius: WIDTH(20)
  },
  styleText: {
    fontSize: getFont(12),
    color: R.colors.colorMain,
    marginTop: HEIGHT(2),
    fontFamily: R.fonts.Roboto,
    fontWeight: '500'
  },
  styleunPress: {
    flex: 1,
    width: getWidth() / 4,
    alignItems: 'center',
    justifyContent: 'center'
  },
  stylePressed: {
    flex: 1,
    width: getWidth() / 4,
    alignItems: 'center',
    justifyContent: 'center'
  },
  styleTabbar: {
    height: HEIGHT(83),
    width: getWidth(),
    backgroundColor: R.colors.colorMain,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  container: {
    flex: 1,
    backgroundColor: R.colors.white100,
    justifyContent: 'flex-end',
  }
});

export default styles;
