// @flow
import React, { PureComponent } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Alert,
  BackHandler,
} from 'react-native';
// import { Text } from 'react-native-paper';
import Icon0 from 'react-native-vector-icons/Fontisto';
import Icon1 from 'react-native-vector-icons/FontAwesome5';
import { TabView } from 'react-native-tab-view';
import { NavigationEvents } from 'react-navigation';
import FastImage from 'react-native-fast-image';
import itemLichSuDonHang from 'common/Item/ItemLichSuDonHang';
import { connect } from 'react-redux';
import i18n from '../../assets/languages/i18n';
import R from '../../assets/R';
import { HEIGHT, WIDTH } from '../../config';
import Home from './Item/Home';
import Personal from './Item/Personal';
import Management from './Item/Management';
import WorkingCalendar from './Item/WorkingCalendar';
import styles from './styles';
import Target from './Item/Target';

const LazyPlaceholder = ({ route }: Object) => (
  <View style={styles.scene}>
    <Text>
Loading
      {route.title}
…
    </Text>
  </View>
);

const guestTab = [
  { key: 'Home', title: 'Trang chủ', icon: R.images.iconHome, typeIcon: 0 },
  {
    key: 'Calendar',
    title: 'Lịch chăm sóc',
    icon: R.images.iconLichLam,
    typeIcon: 1,
  },
  { key: 'Personal', title: 'Cá nhân', icon: R.images.iconCaNhan, typeIcon: 0 },
];
const employeeTab = [
  { key: 'Home', title: 'Trang chủ', icon: R.images.iconHome, typeIcon: 0 },
  // { key: 'Manager', title: 'Quản trị', icon: R.images.iconAdmin, typeIcon: 1 },
  // { key: 'Target', title: 'Mục tiêu', icon: R.images.iconMucTieu, typeIcon: 1 },
  {
    key: 'Calendar',
    title: 'Lịch làm',
    icon: R.images.iconLichLam,
    typeIcon: 1,
  },
  { key: 'Personal', title: 'Cá nhân', icon: R.images.iconCaNhan, typeIcon: 0 },
];

class Tabbar extends PureComponent<Props, State> {
  constructor(props: any) {
    super(props);
    this.state = {
      index: 0,
      routes: this.getTabArray(props.Account.iD_NhomTaiKhoan),
    };
  }

  getNumberCalendar = () => {
    switch (this.props.Account.iD_NhomTaiKhoan) {
      case R.strings.ROLE_USER.khachHang:
        return this.props.numberCalendarCl;
      default:
        return this.props.numberCalendarEm;
    }
  };

  getTabArray = (role) => {
    switch (role) {
      case R.strings.ROLE_USER.khachHang:
        return guestTab;
      case R.strings.ROLE_USER.chuyenGia:
        return employeeTab;
      case R.strings.ROLE_USER.chuyenVienTuVan:
        return employeeTab;
      case R.strings.ROLE_USER.quanLy:
        return employeeTab;
      case R.strings.ROLE_USER.giamDoc:
        return employeeTab;
      default:
        return guestTab;
    }
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    if (this.state.index === 0) {
      Alert.alert(
        'Thông báo',
        'Bạn có muốn thoát ứng dụng',
        [
          { text: 'Không', onPress: () => {}, style: 'cancel' },
          { text: 'Có', onPress: () => BackHandler.exitApp() },
        ],
        { cancelable: true }
      );
    } else {
      this.setState({ index: 0 });
    }
    return true;
  };

  _renderTabBar = (props: any) => (
    <View>
      <View style={styles.styleTabbar}>
        {props.navigationState.routes.map((route, i) => {
          let color = R.colors.yellow254;
          let opacity = this.state.index === i ? 1 : 0.65;
          return (
            <TouchableOpacity
              style={styles.stylePressed}
              onPress={() => this.setState({ index: i })}
              key={i}
            >
              <FastImage
                source={route.icon}
                style={{ width: WIDTH(20), height: WIDTH(20), opacity }}
              />
              <Text style={[styles.styleText, { color, opacity }]}>
                {route.title}
              </Text>
              {this.getNumberCalendar() > 0 && route.key === 'Calendar' && (
                <View style={styles.numCart}>
                  <Text style={{ color: R.colors.white, fontWeight: '500' }}>
                    {this.getNumberCalendar()}
                  </Text>
                </View>
              )}
            </TouchableOpacity>
          );
        })}
      </View>
    </View>
  );

  _renderLazyPlaceholder = ({ route }: Object) => (
    <LazyPlaceholder route={route} />
  );

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    // this.refresh()
  }

  renderScene = ({ route }: Object) => {
    switch (route.key) {
      case 'Home':
        return <Home />;
      case 'Manager':
        return <Management />;
      case 'Target':
        return <Target />;
      case 'Calendar':
        return <WorkingCalendar />;
      case 'Personal':
        return <Personal />;
      default:
        return null;
    }
  };

  render() {
    return (
      <>
        <NavigationEvents
          onWillFocus={(payload) => {
            // this.refresh()
            BackHandler.addEventListener(
              'hardwareBackPress',
              this.handleBackPress
            );
          }}
          onDidFocus={(payload) => {
            // this.refresh()
            BackHandler.addEventListener(
              'hardwareBackPress',
              this.handleBackPress
            );
          }}
          onWillBlur={(payload) => {
            BackHandler.removeEventListener(
              'hardwareBackPress',
              this.handleBackPress
            );
          }}
          onDidBlur={(payload) => {
            BackHandler.removeEventListener(
              'hardwareBackPress',
              this.handleBackPress
            );
          }}
        />
        <TabView
          navigationState={this.state}
          renderTabBar={this._renderTabBar}
          renderScene={this.renderScene}
          style={{ flex: 1 }}
          onIndexChange={(index) => this.setState({ index })}
          initialLayout={{ width: WIDTH(360) }}
          tabBarPosition="bottom"
          // lazy={true}
          lazyPreloadDistance={1}
          renderLazyPlaceholder={this._renderLazyPlaceholder}
        />
      </>
    );
  }
}

function mapStateToProps(state) {
  return {
    Account: state.userReducers.Account,
    numberCalendarEm: state.userReducers.numberCalendarEm,
    numberCalendarCl: state.userReducers.numberCalendarCl,
  };
}

export default connect(mapStateToProps, {
  // setAccount
})(Tabbar);
