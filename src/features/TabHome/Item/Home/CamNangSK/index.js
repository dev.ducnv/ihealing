import React, { Component } from 'react';
import { View, TouchableOpacity, FlatList, Text, StyleSheet } from 'react-native';
import FastImage from 'react-native-fast-image';
import moment from 'moment'
import { connect } from 'react-redux';
import ItemNull from 'common/Item/ItemNull';
import { WIDTH, getWidth, HEIGHT, getFont, getLineHeight } from '../../../../../config';
import R from '../../../../../assets/R';
import NavigationService from '../../../../../routers/NavigationService';


class GioiThieu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      num: 0
    };
  }


  renderListDV = (item) => {
    if (item.iD_DanhMucBaiViet === 2) {
      this.setState({ num: this.state.num + 1 })
      let image = item.anhDaiDien ? { uri: R.images.IMAGE + item.anhDaiDien } : R.images.logoHadoo
      return (
        <TouchableOpacity
          onPress={() => NavigationService.navigate('DetailTraining', { item })}
          style={styles.listDVContainer}
          activeOpacity={0.6}
        >
          <FastImage source={image} style={styles.itemDV} />
          <View style={{ paddingHorizontal: WIDTH(16) }}>
            <Text style={styles.title} numberOfLines={2}>{item.tenBaiViet && item.tenBaiViet}</Text>
            <Text style={styles.date}>{item.ngayTao && item.ngayTao && moment(item.ngayTao).format('DD/MM/YYYY')}</Text>
            <View style={styles.viewLike}>
              <FastImage source={R.images.iconLike} style={styles.iconLike} resizeMode={FastImage.resizeMode.stretch} />
              <Text style={styles.like}>{item.luotXem && item.luotXem}</Text>
            </View>
          </View>
        </TouchableOpacity>
      )
    }
  }

  render() {
    // console.log(this.props.data)
    return (
      <View style={styles.container}>
        <View style={{ marginTop: HEIGHT(12), flex: 1 }}>
          {this.state.num === 0 && <ItemNull text="Không có bài viết nào" />}
          <FlatList
            data={this.props.data}
            renderItem={({ item }) => this.renderListDV(item)}
            showsVerticalScrollIndicator={false}
          />
        </View>
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    // cart: state.cartReducers.cart,
    // amount: state.cartReducers.amount,
    data: state.userReducers.listArticle
  };
}

export default connect(mapStateToProps, {
  // addProduct
})(GioiThieu);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: R.colors.black3
  },
  paginationStyle: {
    alignSelf: 'center'
  },
  dotStyle: {
    borderWidth: WIDTH(1),
    borderColor: R.colors.gray4642,
    backgroundColor: 'transparent'
  },
  imgSwiper: {
    width: getWidth(),
    height: HEIGHT(120),
  },
  swiper: {
    height: HEIGHT(125),
    width: getWidth(),
  },
  iconMenu: {
    width: WIDTH(28),
    height: HEIGHT(21)
  },
  itemContainer: {
    width: (getWidth() - WIDTH(20)) / 4,
    height: HEIGHT(66),
    justifyContent: 'center',
    alignItems: 'center',
  },
  nameIcon: {
    fontSize: getFont(12),
    lineHeight: getLineHeight(16),
    fontFamily: R.fonts.Roboto,
    color: R.colors.yellow254,
    marginTop: HEIGHT(9)
  },
  lineLinear: {
    width: getWidth(),
    height: HEIGHT(1)
  },
  listDVContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: WIDTH(16),
    paddingTop: HEIGHT(20)
  },
  itemDV: {
    width: WIDTH(124),
    height: WIDTH(124)
  },
  title: {
    width: WIDTH(203),
    fontWeight: '500',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.white,
    fontFamily: R.fonts.Roboto
  },
  date: {
    fontSize: getFont(14),
    lineHeight: getLineHeight(18),
    color: R.colors.grey400,
    fontFamily: R.fonts.Roboto,
    marginTop: HEIGHT(12)
  },
  like: {
    fontSize: getFont(14),
    lineHeight: getLineHeight(18),
    color: R.colors.white,
    fontFamily: R.fonts.Roboto,
    marginLeft: WIDTH(9)
  },
  iconLike: {
    width: WIDTH(17),
    height: HEIGHT(16)
  },
  viewLike: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: HEIGHT(13)
  }
});
