import { StyleSheet } from 'react-native';
import R from '../../../../assets/R';
import { HEIGHT, getFont, getLineHeight, getWidth, WIDTH } from '../../../../config';

const styles = StyleSheet.create({
  styleTabbar: {
    backgroundColor: 'transparent'
  },
  tabStyle: {
    minHeight: HEIGHT(44),
  },
  styleText: {
    fontSize: getFont(12),
    lineHeight: getLineHeight(16),
    fontFamily: R.fonts.Roboto,
    color: R.colors.black0,
    // letterSpacing: 0.2,
    // flex: 1,
    textAlign: 'center',
  },
  indicatorStyle: {
    backgroundColor: R.colors.black0,
    height: HEIGHT(2)
  },
  imgSwiper: {
    width: getWidth(),
    height: HEIGHT(120),
  },
  swiper: {
    height: HEIGHT(125),
    width: getWidth(),
  },
  iconMenu: {
    width: WIDTH(28),
    height: HEIGHT(21)
  },


  paginationStyle: {
    alignSelf: 'center'
  },
  dotStyle: {
    borderWidth: WIDTH(1),
    borderColor: R.colors.gray4642,
    backgroundColor: 'transparent'
  },

  itemContainer: {
    height: HEIGHT(66),
    justifyContent: 'center',
    alignItems: 'center',
  },
  nameIcon: {
    fontSize: getFont(12),
    lineHeight: getLineHeight(16),
    fontFamily: R.fonts.Roboto,
    color: R.colors.yellow254,
    marginTop: HEIGHT(9)
  },
  lineLinear: {
    width: getWidth(),
    height: HEIGHT(1)
  },

  title: {
    width: WIDTH(203),
    fontWeight: '500',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.white,
    fontFamily: R.fonts.Roboto
  },
});

export default styles;
