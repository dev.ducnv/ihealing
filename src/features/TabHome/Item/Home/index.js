import React from 'react';
import { View, Text, FlatList, TouchableOpacity, Platform } from 'react-native';
import { TabView, TabBar } from 'react-native-tab-view';
import LinearGradient from 'react-native-linear-gradient';
import Swiper from 'react-native-swiper';
import FastImage from 'react-native-fast-image';
import NavigationService from 'routers/NavigationService';
import { Training, Product, Service, Register, CartProduct } from 'routers/screenNames';
import { connect } from 'react-redux';
import LoadingImage from '../../../../common/Loading/LoadingImage';
import { WIDTH, HEIGHT, getWidth } from '../../../../config';
import HeaderHomeNew from '../../../../common/Header/HeaderHomeNew';
import R from '../../../../assets/R';
import DichVuHome from './GioiThieu';
import CamNangSK from './CamNangSK';
import SuKien from './SuKien';
import ChiaSe from './ChiaSe';
import MultiMedia from './MultiMedia';
import styles from './styles';

const imgSwiper = [
  { uri: 'https://ihealing.vn/assets/img/banner-app/banner-app1.jpg' },
  { uri: 'https://ihealing.vn/assets/img/banner-app/banner-app2.jpg' },
]

const menu = [
  // {
  //   name: 'Đào tạo',
  //   icon: R.images.daoTao
  // },
  {
    name: 'Dược liệu',
    icon: R.images.sanPham
  },
  {
    name: 'Dịch vụ',
    icon: R.images.dichVu
  },
  {
    name: 'Đăng kí thành viên',
    icon: R.images.tuyenDung
  }
]


const menuManager = [
  {
    name: 'Đào tạo',
    icon: R.images.daoTao
  },
  {
    name: 'Dược liệu',
    icon: R.images.sanPham
  }
]

const LazyPlaceholder = ({ route }: Object) => (
  <View style={styles.scene}>
    <Text>
      Loading
      {' '}
      {route.title}
      …
    </Text>
  </View>
);

class Home extends React.PureComponent {
  constructor(props: any) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        { key: '0', title: 'Giới thiệu' },
        { key: '1', title: 'Cẩm nang sức khỏe' },
        { key: '2', title: 'Sự kiện' },
        { key: '3', title: 'Chia sẻ' },
        { key: '4', title: 'Kho dữ liệu' },
      ],
    };
  }

  onButton = (index) => {
    switch (index) {
      case 1:
        break;
      case 2:
        break;
      default:
        break;
    }
  }

  _renderLabel = ({ route }) => {
    let additionStyle = this.state.index.toString() === route.key ? { fontWeight: 'bold' } : { opacity: 0.5 }
    return (
      <Text style={[styles.styleText, additionStyle]}>{route.title}</Text>
    )
  };

  onPressMenu = (name) => {
    switch (name) {
      case 'Đào tạo':
        NavigationService.navigate(Training)
        break;
      case 'Dược liệu':
        NavigationService.navigate(Product)
        break;
      case 'Dịch vụ':
        NavigationService.navigate(Service)
        break;
      case 'Đăng kí thành viên':
        NavigationService.navigate(Register)
        break;
      default:
        break;
    }
  }

  renderItem = (item, index) => (
    <LoadingImage item={item} index={index} />
  )

  renderMenu = (item, index) => (
    <TouchableOpacity
      activeOpacity={0.6}
      style={[styles.itemContainer, { width: this.props.Account.iD_NhomTaiKhoan > 1 ? (getWidth() - WIDTH(20)) / 3 : (getWidth() - WIDTH(20)) / 3 }]}
      onPress={() => this.onPressMenu(item.name)}
    >
      <FastImage
        source={item.icon}
        resizeMode={FastImage.resizeMode.contain}
        style={styles.iconMenu}
      />
      <Text style={styles.nameIcon}>{item.name}</Text>
    </TouchableOpacity>
  )

  renderScene = ({ route }: Object) => {
    switch (route.key) {
      case '0':
        return <DichVuHome />;
      case '1':
        return <CamNangSK />;
      case '2':
        return <SuKien />;
      case '3':
        return <ChiaSe />;
      case '4':
        return <MultiMedia />;
      default:
        return null;
    }
  };

  _renderLazyPlaceholder = ({ route }: Object) => (<LazyPlaceholder route={route} />);

  _renderListOptions = () => (
    <View style={{ backgroundColor: R.colors.black3 }}>
      <View style={[styles.swiper, { height: HEIGHT(180) }]}>
        <Swiper
          key={imgSwiper}
          autoplay={true}
          showsButtons={false}
          dotStyle={styles.dotStyle}
          activeDotStyle={[styles.dotStyle, { borderColor: R.colors.orange37C }]}
          removeClippedSubviews
          style={styles.swiper}
          paginationStyle={styles.paginationStyle}
          loop={Platform.OS === 'android'}
          autoplayDirection={true}
        >
          {imgSwiper.map((item, index) => this.renderItem(imgSwiper[index]))}
        </Swiper>
      </View>
      <LinearGradient
        colors={R.colors.linearButton2}
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        style={[styles.lineLinear, { marginTop: -HEIGHT(20) }]}
      />
      <View style={{ alignItems: 'center' }}>
        <FlatList
          data={(this.props.Account.iD_NhomTaiKhoan > 1) ? menuManager : menu}
          renderItem={({ item, index }) => this.renderMenu(item, index)}
          horizontal
        />
      </View>
      <LinearGradient
        colors={R.colors.linearButton2}
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        style={styles.lineLinear}
      />
    </View>
  )

  render() {
    return (
      <View style={{ flex: 1 }}>
        <HeaderHomeNew
          title="iHealing"
          onButtonCart={() => NavigationService.navigate(CartProduct)}
          onButton={this.onButton}
        />
        {
          this._renderListOptions()
        }
        <DichVuHome />
        {/* <TabView
          navigationState={this.state}
          renderTabBar={this._renderTabBar}
          renderScene={this.renderScene}
          style={{ flex: 1 }}
          onIndexChange={index => this.setState({ index })}
          initialLayout={{ width: WIDTH(360) }}
          lazyPreloadDistance={1}
          renderLazyPlaceholder={this._renderLazyPlaceholder}
        /> */}
      </View>
    );
  }
}


function mapStateToProps(state) {
  return {
    // cart: state.cartReducers.cart,
    // amount: state.cartReducers.amount,
    Account: state.userReducers.Account
  };
}

export default connect(mapStateToProps, {
  // addProduct
})(Home);
