import React, { Component } from 'react';
import { View, TouchableOpacity, FlatList, Text, Alert } from 'react-native';
import FastImage from 'react-native-fast-image';
import moment from 'moment'
import { connect } from 'react-redux';
import { getAllArticle, addProduct } from 'actions';
import ItemNull from 'common/Item/ItemNull';
import { LoadingComponent } from 'common/Loading/LoadingComponent';
import BaseButton from 'common/Button/BaseButton';
import { CartProduct, DetailProduct } from 'routers/screenNames';
import { getDichVuHome } from 'apis';
import { formatVND, HEIGHT, WIDTH } from '../../../../../config';
import styles from './styles';
import R from '../../../../../assets/R';
import NavigationService from '../../../../../routers/NavigationService';


class GioiThieu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      num: 0,
      listService: []
    };
  }

  componentDidMount = async () => {
    // this.setState({ loading: true })
    // this.props.getAllArticle('', () => {
    //   this.setState({ loading: false })
    // })
    try {
      this.setState({
        loading: true,

      })
      let resService = await getDichVuHome()
      this.setState({
        loading: false,
        listService: resService?.data ?? []
      })
      // console.log('resService_resService', resService)
    } catch (error) {
      console.log('error_error', error)
      this.setState({
        loading: false
      })
    }
  }

  onButton = (y, item) => {
    if (this.props.isDichVu === item.isDichVu || this.props.amount === 0) this.addProduct(y, item)
    else Alert.alert('Giỏ hàng không thế bao gồm cả dịch vụ và sản phẩm')
  }

  addProduct = (y, item) => {
    this.setState({ top: y })
    this.props.addProduct(item)
    item.isDichVu && NavigationService.navigate(CartProduct)
    // Animated.timing(this.animation, {
    //   toValue: 1,
    //   duration: 1500,
    //   useNativeDriver: false
    // }).start()
    // setTimeout(() => {
    //   this.animation = new Animated.Value(0)
    //   this.props.addProduct(item)
    // }, 1500)
  }

  renderListDV = (item) => {
    // if (item.iD_DanhMucBaiViet === 1) {
    // this.setState({ num: this.state.num + 1 })
    let image = item.anhDaiDien ? { uri: R.images.IMAGE + item.anhDaiDien } : R.images.logoHadoo
    return (
      <TouchableOpacity
        onPress={() => { item && NavigationService.navigate(DetailProduct, { item }) }}
        style={styles.listDVContainer}
        activeOpacity={0.6}
      >
        <FastImage source={image} style={styles.itemDV} />
        <View style={{ paddingHorizontal: WIDTH(16) }}>
          <Text style={styles.title} numberOfLines={2}>{item?.tenHang}</Text>
          <Text style={styles.date}>{`${formatVND(item?.giaLe)} VNĐ`}</Text>
        </View>
        <BaseButton
          title="Đặt dịch vụ"
          width={WIDTH(121)}
          height={HEIGHT(34)}
          onButton={(y) => { this.onButton(y, item) }}
          style={{
            borderTopRightRadius: 0,
            borderBottomRightRadius: WIDTH(10),
            borderBottomLeftRadius: 0,
            position: 'absolute',
            bottom: 0,
            right: 0,
            borderTopLeftRadius: WIDTH(10)
          }}
        />
      </TouchableOpacity>
    )
    // }
  }

  render() {
    const { listService } = this.state
    if (!this.state.loading) {
      return (
        <View style={styles.container}>
          <View style={{ marginTop: HEIGHT(12), flex: 1 }}>
            {(listService?.length === 0 && !this.state.loading) && <ItemNull text="Không có dịch vụ nào" />}
            <FlatList
              data={listService}
              numColumns={2}
              renderItem={({ item }) => this.renderListDV(item)}
              showsVerticalScrollIndicator={false}
            />
          </View>
        </View>
      );
    } else return (<LoadingComponent isLoading={this.state.loading} />)
  }
}
function mapStateToProps(state) {
  return {
    // cart: state.cartReducers.cart,
    amount: state.cartReducers.amount,
    data: state.userReducers.listArticle,
    isDichVu: state.cartReducers.isDichVu

  };
}

export default connect(mapStateToProps, {
  // addProduct
  getAllArticle,
  addProduct
})(GioiThieu);
