import { StyleSheet } from 'react-native';
import R from '../../../../../assets/R';
import { WIDTH, getWidth, HEIGHT, getFont, getLineHeight } from '../../../../../config';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: R.colors.black3,
    alignItems: 'center'
  },
  paginationStyle: {
    alignSelf: 'center'
  },
  dotStyle: {
    borderWidth: WIDTH(1),
    borderColor: R.colors.gray4642,
    backgroundColor: 'transparent'
  },
  imgSwiper: {
    width: getWidth(),
    height: HEIGHT(120),
  },
  swiper: {
    height: HEIGHT(125),
    width: getWidth(),
  },
  iconMenu: {
    width: WIDTH(28),
    height: HEIGHT(21)
  },
  itemContainer: {
    width: (getWidth() - WIDTH(20)) / 4,
    height: HEIGHT(66),
    justifyContent: 'center',
    alignItems: 'center',
  },
  nameIcon: {
    fontSize: getFont(12),
    lineHeight: getLineHeight(16),
    fontFamily: R.fonts.Roboto,
    color: R.colors.yellow254,
    marginTop: HEIGHT(9)
  },
  lineLinear: {
    width: getWidth(),
    height: HEIGHT(1)
  },
  listDVContainer: {
    // flexDirection: 'row',
    alignItems: 'center',
    // paddingLeft: WIDTH(16),
    // paddingTop: HEIGHT(20),
    width: WIDTH(170),
    height: HEIGHT(270),
    backgroundColor: R.colors.white,
    marginLeft: WIDTH(9),
    borderRadius: WIDTH(10),
    marginTop: HEIGHT(20)
  },
  itemDV: {
    width: WIDTH(170),
    height: WIDTH(137),
    borderTopLeftRadius: WIDTH(9),
    borderTopRightRadius: WIDTH(9),
    backgroundColor: R.colors.colorBackgroundDetail
  },
  title: {
    width: WIDTH(155),
    fontWeight: '700',
    fontSize: getFont(14),
    lineHeight: getLineHeight(18),
    color: R.colors.black0,
    fontFamily: R.fonts.Roboto
  },
  date: {
    fontSize: getFont(20),
    lineHeight: getLineHeight(24),
    color: R.colors.yellow1c8,
    fontFamily: R.fonts.RobotoMedium,
    marginTop: HEIGHT(5)
  },
  like: {
    fontSize: getFont(14),
    lineHeight: getLineHeight(18),
    color: R.colors.white,
    fontFamily: R.fonts.Roboto,
    marginLeft: WIDTH(9)
  },
  iconLike: {
    width: WIDTH(17),
    height: HEIGHT(16)
  },
  viewLike: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: HEIGHT(13)
  }
});

export default styles;
