import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Swiper from 'react-native-swiper'
import R from 'assets/R';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { HEIGHT, getFont, getLineHeight, WIDTH, getWidth } from '../../../../../config';
import ItemSell from '../../../../../common/Item/ItemSell'

export default class SwiperItem extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const { data, titleSwiper } = this.props;
    return (
      <View style={{ backgroundColor: R.colors.black3, height: HEIGHT(400) }}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: WIDTH(16) }}>
          <Text style={styles.text}>{titleSwiper}</Text>
          <TouchableOpacity>
            <Text style={styles.textMore}>Xem thêm</Text>
          </TouchableOpacity>
        </View>
        <View style={{ height: 1, width: getWidth(), backgroundColor: R.colors.yellow37c, marginTop: HEIGHT(4) }} />
        <Swiper style={styles.wrapper} autoplay={true} showsButtons={false}>
          {data.map((item, index) => {
            if (index % 2 === 0) {
              return (
                <View style={styles.slide}>
                  <ItemSell icon={data[index].icon} title={data[index].title} price={data[index].price} isSellingFast={data[index].isSellingFast} />
                  <ItemSell icon={data[index + 1].icon} title={data[index + 1].title} price={data[index + 1].price} isSellingFast={data[index + 1].isSellingFast} />
                </View>
              )
            }
          })}
        </Swiper>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  wrapper: {},
  slide: {
    justifyContent: 'center',
    paddingVertical: HEIGHT(4),
    backgroundColor: R.colors.black3
  },
  text: {
    fontSize: getFont(22),
    fontWeight: 'bold',
    lineHeight: getLineHeight(30),
    color: R.colors.yellow254,
    fontFamily: R.fonts.Roboto
  },
  textMore: {
    color: R.colors.grey300,
    fontSize: getFont(14),
    lineHeight: getLineHeight(18),
    fontFamily: R.fonts.Roboto

  }
})
