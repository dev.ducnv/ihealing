import React from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import R from 'assets/R';
import NavigationService from 'routers/NavigationService';
import { Register } from 'routers/screenNames';
import { WIDTH, HEIGHT, getFont, getWidth, getLineHeight } from '../../../../../config';

const dataCooperate = [
  { title: 'Chuyên viên tư vấn', icon: R.images.iconPerson, description: ' Tư vấn viên tiếp xúc trực tiếp hoặc gián tiếp với khách hàng, có trách nhiệm tư vấn về sản phẩm/dịch vụ, giải quyết các thắc mắc tạm thời của khách hàng, giúp họ lựa chọn và sử dụng các sản phẩm, dịch vụ một cách hiệu quả.' },
  { title: 'Chuyên gia', icon: R.images.iconExpert, description: 'Chuyên gia là người tiếp xúc trực tiếp với khách hàng, là người nắm rõ hồ sơ khách hàng, tình trạng của khách hàng và các bệnh lý nền. Từ đó đưa ra phác đồ trị liệu cụ thể, chính xác, nhanh gọn và hiệu quả cho người bệnh.' },
  { title: 'Quản lý', icon: R.images.iconManager, description: 'Quản lý điều hành và hỗ trợ hệ thống các chuyên gia/chuyên viên tư vấn do mình đảm nhận, giúp họ phát triển và nâng cao kỹ năng, đồng thời hỗ trợ tìm kiếm khách hàng, giải quyết những khúc mắc phát sinh trong quá trình chuyên gia/chuyên viên tư vấn gặp phải trong khâu chăm sóc khách hàng.' },
  { title: 'Giám đốc', icon: R.images.iconChief, description: 'Người đứng đầu hệ thống iHealing, có trách nhiệm liên kết với Ban lãnh đạo, luôn tìm kiếm phương thức mới giúp iHealing ngày một hoàn thiện và phát triển. Ngoài ra, nhiệm vụ xuyên suốt quá trình làm việc là làm sao liên kết và xây dựng hệ thống phía dưới do mình quản lý, chăm sóc và hỗ trợ các quản lý hoàn thành mục tiêu ngắn hạn, dài hạn, phát huy khả năng từng thành viên.' },
]

export default class Cooperate extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    // const { icon, title, description } = this.props
    return (
      <View style={{ backgroundColor: R.colors.black3 }}>
        <Text style={styles.text}>Cơ hội hợp tác</Text>
        <View style={{ height: 1, width: getWidth(), backgroundColor: R.colors.yellow37c, marginTop: HEIGHT(4) }} />
        <FlatList
          data={dataCooperate}
          style={{ paddingBottom: HEIGHT(13) }}
          numColumns={2}
          renderItem={({ item, index }) => (
            <View style={{ width: getWidth() / 2, alignItems: 'center', justifyContent: 'center' }}>
              <TouchableOpacity
                onPress={() => NavigationService.navigate(Register)}
                style={styles.container}
              >
                <FastImage
                  source={item.icon}
                  style={{ width: WIDTH(25), height: HEIGHT(29), marginLeft: WIDTH(16), marginTop: HEIGHT(19) }}
                  resizeMode={FastImage.resizeMode.stretch}
                />
                <Text style={styles.title}>{item.title}</Text>
                <Text style={styles.description} numberOfLines={4}>{item.description}</Text>
              </TouchableOpacity>
            </View>
          )}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  title: {
    fontWeight: '500',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    paddingHorizontal: WIDTH(12),
    marginVertical: HEIGHT(9),
    color: R.colors.white
  },
  description: {
    opacity: 0.5,
    fontSize: getFont(12),
    paddingHorizontal: WIDTH(12),
    lineHeight: getLineHeight(18),
    color: R.colors.white
  },
  text: {
    fontSize: getFont(22),
    paddingHorizontal: WIDTH(16),
    fontWeight: 'bold',
    lineHeight: getLineHeight(30),
    color: R.colors.yellow254,
    fontFamily: R.fonts.Roboto
  },
  container: {
    width: WIDTH(165),
    minHeight: HEIGHT(174),
    // marginHorizontal: WIDTH(13),
    marginTop: HEIGHT(8),
    paddingBottom: HEIGHT(16),
    borderWidth: 1,
    borderColor: 'rgba(255, 255, 255, 0.25)'
  }
})
