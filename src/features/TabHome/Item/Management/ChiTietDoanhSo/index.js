import React from 'react';
import { View, FlatList, Text, StyleSheet } from 'react-native';
import i18n from 'assets/languages/i18n';
import _ from 'lodash'
import moment from 'moment'
import { Table, Row, Rows, TableWrapper, Cell, Col } from 'react-native-table-component';
import { ScrollView } from 'react-native-gesture-handler';
import BaseButton from 'common/Button/BaseButton';
import { LoadingComponent } from '../../../../../common/Loading/LoadingComponent';
import ItemNull from '../../../../../common/Item/ItemNull';
import HeaderBack from '../../../../../common/Header/HeaderBack';
import R from '../../../../../assets/R';
import { getFont, WIDTH, getLineHeight, getWidth, HEIGHT, formatVND } from '../../../../../config';
import { getDetailKPI } from '../../../../../apis'
import PickerDate from '../../../../../common/Picker/PickerDate'


export default class ChiTietDoanhSo extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: [],
      tableHead: ['Người mua hàng', 'Đơn hàng', 'Trạng thái', 'Lợi nhuận'],
      tableData: [
      ],
      fromDate: moment(new Date()).format('DD/MM/YYYY'),
      toDate: moment(new Date()).format('DD/MM/YYYY')
    };
  }

  parseData = (data) => {
    let arr = []
    // let arrloiNhuan = []
    data.map((item, index) => {
      arr.push([
        item.tenKhachHang || 'Trống',
        item.maThamChieu ? `${item.maThamChieu}\nNgày lập: ${item.ngayLap}\nTổng tiền: ${formatVND(item.tongTien)}` : '0',
        item?.isProcess_Name ?? 'Hủy',
        item.loiNhuan ? formatVND(item.loiNhuan) : '0'
      ])
      // arrloiNhuan.push(item.tienDaThanhToan ? formatVND(item.tienDaThanhToan) : 'Trống')
    })
    // return arr
    this.setState({ loading: false, tableData: arr })
  }


  // var prevMonday = new Date();
  // prevMonday.setDate(prevMonday.getDate() - (prevMonday.getDay() +3) % 7 - 7);
  // document.getElementById("demo").innerHTML = prevMonday;

  getDon = async () => {
    this.setState({ loading: true })
    // let year = new Date().getFullYear();
    // let fromdate = new Date(year, thang, 1, 0, 0, 0);
    // let toDate = new Date(year, thang + 1, 0, 23, 59, 59);
    let parseFrom = this.state.fromDate;
    parseFrom = parseFrom.split('/')

    let parseTo = this.state.toDate;
    parseTo = parseTo.split('/')

    let param = {
      from: moment(new Date(parseFrom[2], parseFrom[1] - 1, parseFrom[0])).format('YYYY/MM/DD HH:mm:ss'),
      to: moment(new Date(parseTo[2], parseTo[1] - 1, parseTo[0])).format('YYYY/MM/DD HH:mm:ss'),
      trangthaixem: -1,
      ttht: -1
    }
    let res = await getDetailKPI(param);
    // console.log(param, res)
    if (res.data) {
      // this.setState({ data: res.data, loading: false })
      // this.setState({ loading: false, tableData: this.parseData(res.data) })
      this.parseData(res.data)
    } else this.setState({ loading: false })
  }

  render() {
    const { state } = this
    if (!this.state.loading) {
      return (
        <ScrollView style={{ flex: 1, backgroundColor: R.colors.black3, paddingHorizontal: WIDTH(5) }}>

          <View style={{ marginVertical: HEIGHT(15), flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: WIDTH(10) }}>
            <View style={{ borderBottomWidth: 1, borderColor: R.colors.yellow254 }}>
              <Text style={[styles.text, { fontSize: getFont(16) }]}>Từ ngày</Text>
              <PickerDate
                onChangeDate={(date) => {
                  // data.date.func(date);
                  // console.log(date)
                  this.setState({ fromDate: date })
                  this.setState({ toDate: date })
                }}
                // minDate
                date={state.fromDate}
                width={WIDTH(120)}
              />
            </View>
            <View style={{ borderBottomWidth: 1, borderColor: R.colors.yellow254 }}>
              <Text style={[styles.text, { fontSize: getFont(16) }]}>Đến ngày</Text>
              <PickerDate
                onChangeDate={(date) => {
                  // data.date.func(date);
                  // console.log(date)
                  this.setState({ toDate: date })
                }}
                minDate={state.fromDate}
                date={state.toDate}
                width={WIDTH(120)}
              />
            </View>
          </View>
          <View style={{ paddingBottom: HEIGHT(15), alignItems: 'center' }}>
            <BaseButton title="Lấy danh sách đơn" onButton={this.getDon} width={WIDTH(170)} />
          </View>
          <Table borderStyle={{ borderWidth: 1, borderColor: R.colors.yellow254 }}>
            <Row data={state.tableHead} style={styles.head} textStyle={styles.textHead} flexArr={[1, 2, 1, 1]} />
            <Rows data={state.tableData} textStyle={styles.text} flexArr={[1, 2, 1, 1]} />
          </Table>
        </ScrollView>
      );
    } else {
      return (
        <View style={{ flex: 1, backgroundColor: R.colors.black3 }}>
          <LoadingComponent isLoading={true} />
        </View>

      )
    }
  }
}
const styles = StyleSheet.create({
  // container: {
  //   flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff'
  // },
  head: {
    height: HEIGHT(80), backgroundColor: '#050021'
  },
  textHead: {
    fontSize: getFont(16),
    fontWeight: 'bold',
    lineHeight: getLineHeight(20),
    color: R.colors.yellow254,
    margin: WIDTH(3),
    marginLeft: WIDTH(3),
    textAlign: 'center',
    marginVertical: HEIGHT(7),

  },
  viewTxt: {
    width: getWidth(),
    alignItems: 'center',
    marginTop: HEIGHT(20)
  },
  text: {
    fontSize: getFont(14),
    lineHeight: getLineHeight(20),
    color: R.colors.yellow254,
    margin: WIDTH(3),
    marginVertical: HEIGHT(5)
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'center',
    marginTop: HEIGHT(12),
    width: WIDTH(319),
    paddingHorizontal: WIDTH(10),
    borderBottomColor: R.colors.yellow254,
    borderBottomWidth: 1,
    alignSelf: 'center',
    marginBottom: HEIGHT(20)
  },
});
