// @flow
import React, { PureComponent } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Alert,
  BackHandler,
} from 'react-native';
// import { Text } from 'react-native-paper';
import { TabView } from 'react-native-tab-view';
import _ from 'lodash';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import R from '../../../../../assets/R';
import { HEIGHT, getFont, getWidth, WIDTH } from '../../../../../config';
import PhanCapNhanSu from '../PhanCapNhanSu';
import PheDuyetCapDuoi from '../PheDuyetCapDuoi';
import ChiTietDoanhSo from '../ChiTietDoanhSo';
import YeuCauTuVan from '../YeuCauTuVan';

const LazyPlaceholder = ({ route }: Object) => (
  <View style={styles.scene}>
    <Text>
      Loading
      {' '}
      {route.title}
      …
    </Text>
  </View>
);

class TabManagement extends PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    const { Account } = this.props;
    this.state = {
      index: 0,
      routes: [
        { key: '1', title: 'Phân cấp nhân sự' },
        { key: '2', title: 'Phê duyệt thành viên' },
        { key: '3', title: 'Chi tiết doanh số' },
      ],
      iDNhomTaiKhoan: _.get(Account, 'iD_NhomTaiKhoan', 0)
    };
  }

  componentDidMount = () => {
    const { iDNhomTaiKhoan } = this.state;
    if (iDNhomTaiKhoan === 2 || iDNhomTaiKhoan === 3) {
      this.setState({
        ...this.state,
        routes: [
          { key: '1', title: 'Phân cấp nhân sự' },
          { key: '2', title: 'Yêu cầu tư vấn' },
        ]
      })
    }
  }

  _renderTabBar = (props: any) => {
    const { iDNhomTaiKhoan } = this.state;
    const tabWidth = iDNhomTaiKhoan === 2 || iDNhomTaiKhoan === 3 ? getWidth() / 2 : getWidth() / 3;
    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 1 }}
        style={styles.styleTabbar}
        colors={[R.colors.yellow1c8, R.colors.yellow254, R.colors.yellow1c8]}
      >
        {props.navigationState.routes.map((route, i) => {
          let opacity = this.state.index === i ? 1 : 0.5;
          return (
            <TouchableOpacity
              style={
                this.state.index === i ? [styles.stylePressed, { width: tabWidth }] : [styles.styleunPress, { width: tabWidth }]
              }
              onPress={() => this.setState({ index: i })}
              key={i}
            >
              <Text style={[styles.styleText, { opacity, width: tabWidth }]}>{route.title}</Text>
            </TouchableOpacity>
          );
        })}
      </LinearGradient>
    );
  }

  _renderLazyPlaceholder = ({ route }: Object) => (
    <LazyPlaceholder route={route} />
  );

  renderScene = ({ route }: Object) => {
    const { iDNhomTaiKhoan } = this.state;
    if (iDNhomTaiKhoan === 2 || iDNhomTaiKhoan === 3) {
      switch (route.key) {
        case '1':
          return <PhanCapNhanSu />;
        case '2':
          return <YeuCauTuVan />;
        default:
          return null;
      }
    } else {
      switch (route.key) {
        case '1':
          return <PhanCapNhanSu />;
        case '2':
          return <PheDuyetCapDuoi />;
        case '3':
          return <ChiTietDoanhSo />;
        default:
          return null;
      }
    }
  };

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderTabBar={this._renderTabBar}
        renderScene={this.renderScene}
        style={{ flex: 1 }}
        onIndexChange={(index) => this.setState({ index })}
        initialLayout={{ width: getWidth() }}
        // tabBarPosition="bottom"
        // lazy={true}
        lazyPreloadDistance={1}
        renderLazyPlaceholder={this._renderLazyPlaceholder}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    Account: state.userReducers.Account
  };
}

export default connect(mapStateToProps, {})(TabManagement);

const styles = StyleSheet.create({
  scene: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  styleText: {
    fontSize: getFont(16),
    color: R.colors.black0,
    flexWrap: 'wrap',
    width: getWidth() / 3,
    paddingHorizontal: WIDTH(18),
    textAlign: 'center',
    paddingVertical: HEIGHT(5),
    // marginTop: HEIGHT(2),
    fontFamily: R.fonts.Roboto,
    fontWeight: 'bold',
  },
  styleunPress: {
    // flex: 1,
    width: getWidth() / 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  stylePressed: {
    // flex: 1,
    width: getWidth() / 3,
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 2,
    borderBottomColor: R.colors.black0,
  },
  styleTabbar: {
    minHeight: HEIGHT(44),
    width: getWidth(),
    backgroundColor: R.colors.colorMain,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  container: {
    flex: 1,
    backgroundColor: R.colors.white100,
    justifyContent: 'flex-end',
  },
});
