import React from 'react';
import { View } from 'react-native';
import HeaderTabHome from '../../../../common/Header/HeaderTabHome';
import TabManagement from './Item/TabManagement';

export default class Management extends React.PureComponent {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <HeaderTabHome
          title="Quản trị"
          searchEnable={false}
          cartEnable={false}
          enableBack={false}
        />
        <TabManagement />
      </View>
    );
  }
}
