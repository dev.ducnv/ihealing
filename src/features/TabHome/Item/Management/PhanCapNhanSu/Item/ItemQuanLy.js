// @flow
import React, { Component } from 'react'
import {
  Text, View, StyleSheet, TouchableOpacity, FlatList
} from 'react-native'
import Entypo from 'react-native-vector-icons/Entypo';
import _ from 'lodash'

// import
import { WIDTH, HEIGHT, getFont, getLineHeight, getWidth } from '../../../../../../config';
import i18n from '../../../../../../assets/languages/i18n';
import R from '../../../../../../assets/R';
import ItemChuyenGia from './ItemChuyenGia';

type Props = {
  changeExpand: Function,
  value: Object,
  num: number,
  expand: boolean,
}

class ItemQuanLy extends Component<Props> {
  constructor(props: Props) {
    super(props);
    this.state = {
      expand: []
    };
  }

  componentDidMount() {
    const { value } = this.props;
    let arr = []
    for (let i = 0; i < value.lstCapDuoi.length; i++) {
      arr.push(false)
    }
    this.setState({ expand: arr })
  }

  changeExpand = (i) => {
    let arr = this.state.expand
    arr[i] = !arr[i]
    this.setState({ expand: arr })
  }

  renderItem = (item, index) => (
    <View
      style={{ paddingLeft: WIDTH(48), }}
      key={index}
    >
      <ItemChuyenGia value={item} />
      {
        (index !== this.props.value.lstCapDuoi.length - 1) && <View style={[styles.line, { width: WIDTH(279), height: HEIGHT(0.4) }]} />
      }
    </View>
  )

  render() {
    const { changeExpand, value, num, expand } = this.props;
    return (
      <View style={styles.itemView}>
        <TouchableOpacity
          style={styles.titleView}
          onPress={() => changeExpand(num)}
        >
          <View>
            <Text style={styles.title}>{`${!_.has(value, 'iD_NhomTaiKhoan') ? i18n.t('NULL_T') : R.strings.GROUP_USER[value.iD_NhomTaiKhoan]} - ${!_.has(value, 'tenDayDu') ? i18n.t('NULL_T') : value.tenDayDu}`}</Text>
            <Text style={[styles.title, { color: R.colors.yellow254 }]}>{!_.has(value, 'maTaiKhoan') ? i18n.t('NULL_T') : value.maTaiKhoan}</Text>
          </View>
          <View>
            <Entypo
              name={(expand) ? 'chevron-small-down' : 'chevron-right'}
              size={HEIGHT(32)}
              color={R.colors.yellow254}
            />
          </View>
        </TouchableOpacity>
        {
          (expand && value.lstCapDuoi.length > 0)
            ? (
              <View>
                <View style={styles.line} />
                <FlatList
                  data={!_.has(value, 'lstCapDuoi') ? [] : value.lstCapDuoi}
                  extraData={this.state}
                  renderItem={({ item, index }) => this.renderItem(item, index)}
                  listKey={(item, index) => `D${item.id}`}
                />
                <View style={styles.line} />
              </View>
            )
            : <View />
        }
      </View>
    )
  }
}

export default ItemQuanLy;
const styles = StyleSheet.create({
  itemView: {
    backgroundColor: R.colors.colorMain,
    width: getWidth(),
    paddingHorizontal: WIDTH(24),
    marginTop: HEIGHT(16),
    paddingBottom: HEIGHT(16)
  },
  line: {
    height: HEIGHT(1),
    width: WIDTH(327),
    backgroundColor: R.colors.yellow1c8,
    marginTop: HEIGHT(16)
  },
  titleView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 0
  },
  title: {
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.white
  },
})
