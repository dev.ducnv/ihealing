// @flow
import React, { Component } from 'react'
import {
  Text, View, StyleSheet
} from 'react-native'
import _ from 'lodash'

// import
import { WIDTH, HEIGHT, getFont, getLineHeight, getWidth } from '../../../../../../config';
import i18n from '../../../../../../assets/languages/i18n';
import R from '../../../../../../assets/R';

type Props = {
  value: Object,
}

class ItemChuyenGia extends Component<Props> {
  constructor(props: Props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { value } = this.props;
    return (
      <View style={styles.itemView}>
        <Text style={styles.title}>{`${!_.has(value, 'iD_NhomTaiKhoan') ? i18n.t('NULL_T') : R.strings.GROUP_USER[value.iD_NhomTaiKhoan]} - ${!_.has(value, 'tenDayDu') ? i18n.t('NULL_T') : value.tenDayDu}`}</Text>
        <Text style={[styles.title, { color: R.colors.yellow254 }]}>{!_.has(value, 'maTaiKhoan') ? i18n.t('NULL_T') : value.maTaiKhoan}</Text>
      </View>
    )
  }
}

export default ItemChuyenGia;
const styles = StyleSheet.create({
  itemView: {
    backgroundColor: R.colors.colorMain,
    width: getWidth(),
    marginTop: HEIGHT(16),
    paddingBottom: HEIGHT(16)
  },
  titleView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: WIDTH(16),
    paddingBottom: 0
  },
  title: {
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.white
  },
})
