import React, { Component } from 'react';
import { View, StyleSheet, FlatList, Text } from 'react-native';
import _ from 'lodash'
import { getListEmployee } from 'apis';
import { LoadingComponent } from '../../../../../common/Loading/LoadingComponent';
import R from '../../../../../assets/R';
import ItemGiamDoc from './Item/ItemGiamDoc';
import { HEIGHT, getWidth, getFont, getHeight } from '../../../../../config';

const data = require('./data.json');

class PhanCapNhanSu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expand: [],
      isLoading: true,
      item: {},
      listEmployee: [],
    };
  }

  componentDidMount() {
    this.getData()
  }

  getData = async () => {
    let res = await getListEmployee()
    // console.log('getListEmployee', res)
    if (_.has(res, 'data')) {
      this.setState({ listEmployee: res.data, isLoading: false }, () => {
        let arr = []
        for (let i = 0; i < this.state.listEmployee.length; i++) {
          arr.push(false)
        }
        this.setState({ expand: arr })
      })
    }
  }

  changeExpand = (i) => {
    let arr = this.state.expand
    arr[i] = !arr[i]
    this.setState({ expand: arr })
  }

  renderItem = (item, index) => (
    <ItemGiamDoc
      value={item}
      num={index}
      expand={this.state.expand[index]}
      changeExpand={this.changeExpand}
    />
  )

  render() {
    if (!this.state.isLoading) {
      return (
        <View style={styles.container}>
          <FlatList
            listKey={(item, index) => `D${item.id}`}
            data={this.state.listEmployee}
            extraData={this.state}
            ListEmptyComponent={<Text style={{
              alignSelf: 'center',
              color: 'white',
              marginTop: 20,
              fontSize: getFont(15)
            }}
            >
              Trống
                                </Text>}
            renderItem={({ item, index }) => this.renderItem(item, index)}
          />
        </View>
      );
    } else return (<LoadingComponent isLoading={this.state.isLoading} />)
  }
}

export default PhanCapNhanSu;
const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.backgroundBlack,
    flex: 1,
  },
  line: {
    height: HEIGHT(2),
    width: getWidth(),
    backgroundColor: R.colors.yellow254,
    marginTop: HEIGHT(16)
  },
});
