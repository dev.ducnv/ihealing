// @flow
import React from 'react'
import {
  Text, View, StyleSheet, TouchableOpacity
} from 'react-native'
import AntDesign from 'react-native-vector-icons/AntDesign';
import _ from 'lodash'

// import
import { WIDTH, HEIGHT, getFont, getLineHeight, getWidth } from '../../../../../../config';
import i18n from '../../../../../../assets/languages/i18n';
import R from '../../../../../../assets/R';
import BaseButton from '../../../../../../common/Button/BaseButton';

type Props = {
  item: Object,
  onAccept: Function,
  onDeny: Function,
}

const ItemPheDuyet = (props: Props) => {
  const { item, onAccept, onDeny } = props;
  return (
    <View>
      <View style={styles.titleView}>
        <View>
          <Text style={styles.title}>{!_.has(item, 'iD_NhomTaiKhoan') ? i18n.t('NULL_T') : R.strings.GROUP_USER[item.iD_NhomTaiKhoan]}</Text>
          <Text style={[styles.title, { color: R.colors.white }]}>{!_.has(item, 'tenDayDu') ? i18n.t('NULL_T') : item.tenDayDu}</Text>
          <Text style={styles.title}>{!_.has(item, 'maTaiKhoan') ? i18n.t('NULL_T') : item.maTaiKhoan}</Text>
        </View>
        <TouchableOpacity
          onPress={() => onDeny && onDeny(item)}
          style={{ marginLeft: WIDTH(32) }}
        >
          <AntDesign
            name="closecircleo"
            size={HEIGHT(32)}
            color={R.colors.yellow254}
          />
        </TouchableOpacity>
        <BaseButton
          title="Duyệt"
          width={WIDTH(85)}
          height={HEIGHT(34)}
          onButton={() => onAccept && onAccept(item)}
        />
      </View>
      <View style={styles.line} />
    </View>
  )
}

export default ItemPheDuyet;

const styles = StyleSheet.create({
  itemView: {
    backgroundColor: R.colors.colorMain,
    width: getWidth(),
    marginTop: HEIGHT(16),
    paddingBottom: HEIGHT(16)
  },
  line: {
    height: HEIGHT(1),
    width: getWidth(),
    backgroundColor: R.colors.yellow1c8,
    marginTop: HEIGHT(12)
  },
  titleView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: WIDTH(16),
    paddingBottom: 0
  },
  title: {
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.yellow254
  },
})
