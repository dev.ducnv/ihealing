import React, { Component } from 'react';
import { View, FlatList, StyleSheet, Text } from 'react-native';
import { getListAccept, postDeny, postAccept } from 'apis';
import _ from 'lodash'
import { connect } from 'react-redux';
import ItemNull from 'common/Item/ItemNull';
import { popupOk, getFont, getHeight } from '../../../../../config';
import { LoadingComponent } from '../../../../../common/Loading/LoadingComponent';
import R from '../../../../../assets/R';
import ItemPheDuyet from './Item/ItemPheDuyet'

const data = require('./data.json');

class PheDuyenCapDuoi extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataAccept: [],
      loading: true,
    };
  }

  renderItem = (item, index) => (
    <ItemPheDuyet
      item={item}
      onDeny={this.onDeny}
      onAccept={this.onAccept}
    />
  )

  onDeny = (item) => {
    popupOk('Thông báo', `Bạn có muốn từ chối phê duyệt nhân viên ${item.tenDayDu}`, async () => {
      this.setState({ loading: true })
      let body = {
        ID_NhanVien: item.iD_QuanLy,
        ID_NhanVienCapTren: this.props.Account.iD_QuanLy

      }
      let resDeny = await postDeny(body)
      if (_.has(resDeny, 'data.msg')) {
        popupOk('Thông báo', resDeny.data.msg, () => { this.getData() });
      } else {
        popupOk('Thông báo', 'Không thể thực hiện yêu cầu, vui lòng kiểm tra kết nối', () => { })
      }
    })
  }

  onAccept = async (item) => {
    popupOk('Thông báo', `Bạn có muốn phê duyệt nhân viên ${item.tenDayDu}`, async () => {
      this.setState({ loading: true })
      let body = {
        ID_NhanVien: item.iD_QuanLy,
        ID_NhanVienCapTren: this.props.Account.iD_QuanLy,
        TrangThai: 1

      }
      let resDeny = await postAccept(body)
      if (_.has(resDeny, 'data.msg')) {
        popupOk('Thông báo', resDeny.data.msg, () => { this.getData() });
      } else {
        popupOk('Thông báo', 'Không thể thực hiện yêu cầu, vui lòng kiểm tra kết nối', () => { })
      }
    })
  }

  getData = async () => {
    let resAccept = await getListAccept()
    if (_.has(resAccept, 'data')) {
      this.setState({ dataAccept: resAccept.data, loading: false })
    }
  }

  componentDidMount = () => {
    this.getData()
  }

  render() {
    if (!this.state.loading) {
      return (
        <View style={styles.container}>
          <FlatList
            data={this.state.dataAccept}
            ListEmptyComponent={<ItemNull />}
            renderItem={({ item, index }) => this.renderItem(item, index)}
          />
        </View>
      );
    } else return (<LoadingComponent isLoading={this.state.loading} />)
  }
}

function mapStateToProps(state) {
  return {
    Account: state.userReducers.Account
  };
}

export default connect(mapStateToProps, {
})(PheDuyenCapDuoi);

const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.backgroundBlack,
    flex: 1,
  },
});
