import React, { Component } from 'react';
import { View, FlatList, StyleSheet, Text } from 'react-native';
import _ from 'lodash'
import { connect } from 'react-redux';
import { getYeuCauTuVan, updateYeuCauTuVan, postAccept } from '../../../../../apis';
import ItemNull from '../../../../../common/Item/ItemNull';
import { popupOk, getFont, getHeight } from '../../../../../config';
import { LoadingComponent } from '../../../../../common/Loading/LoadingComponent';
import R from '../../../../../assets/R';
import ItemYeuCau from './Item/ItemYeuCau'


class YeuCauTuVan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
    };
  }

  renderItem = (item, index) => (
    <ItemYeuCau
      item={item}
      onUpdateStatus={this.onUpdateStatus}
    />
  )

  onUpdateStatus = async (item) => {
    popupOk('Thông báo', 'Bạn chắc chắn đã hoàn thành tư vấn?', async () => {
      this.setState({ loading: true })
      let body = {
        ID: item.id,
        TrangThai: 2,
        ID_NhanVien: item.iD_NhanVien

      }
      let res = await updateYeuCauTuVan(body)
      if (_.has(res, 'data.msg')) {
        popupOk('Thông báo', res.data.msg, () => { this.getData() });
      } else {
        popupOk('Thông báo', 'Không thể thực hiện yêu cầu, vui lòng kiểm tra kết nối', () => { })
      }
    })
  }

  getData = async () => {
    let res = await getYeuCauTuVan()
    if (_.has(res, 'data')) {
      this.setState({ data: res.data, loading: false })
    }
  }

  componentDidMount = () => {
    this.getData()
  }

  render() {
    if (!this.state.loading) {
      return (
        <View style={styles.container}>
          <FlatList
            data={this.state.data}
            ListEmptyComponent={<ItemNull />}
            renderItem={({ item, index }) => this.renderItem(item, index)}
          />
        </View>
      );
    } else return (<LoadingComponent isLoading={this.state.loading} />)
  }
}

function mapStateToProps(state) {
  return {
    Account: state.userReducers.Account
  };
}

export default connect(mapStateToProps, {
})(YeuCauTuVan);

const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.backgroundBlack,
    flex: 1,
  },
});
