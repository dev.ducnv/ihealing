/* eslint-disable react/jsx-indent */
// @flow
import React from 'react'
import {
  Text, View, StyleSheet, TouchableOpacity
} from 'react-native'
import AntDesign from 'react-native-vector-icons/AntDesign';
import _ from 'lodash'

// import
import { WIDTH, HEIGHT, getFont, getLineHeight, getWidth } from '../../../../../../config';
import i18n from '../../../../../../assets/languages/i18n';
import R from '../../../../../../assets/R';
import BaseButton from '../../../../../../common/Button/BaseButton';

type Props = {
  item: Object,
  onUpdateStatus: Function,
}

const ItemYeuCau = (props: Props) => {
  const { item, onUpdateStatus } = props;
  return (
    <View>
      <View style={styles.titleView}>
        <View style={{ width: _.get(item, 'trangThai', 2) !== 2 ? WIDTH(222) : getWidth() - WIDTH(32) }}>
          <Text style={[styles.title, { color: R.colors.white }]} numberOfLines={1}>{`ID khách hàng: ${_.get(item, 'iD_KhachHang', i18n.t('NULL_T'))}`}</Text>
          <Text style={[styles.title, { color: R.colors.white }]} numberOfLines={2}>{`Tên khách hàng: ${_.get(item, 'tenKhachHang', i18n.t('NULL_T'))}`}</Text>
          <Text style={[styles.title, { color: R.colors.white }]} numberOfLines={1}>{`Điện thoại: ${_.get(item, 'dienThoai', i18n.t('NULL_T'))}`}</Text>
          <Text style={[styles.title, { color: R.colors.white }]} numberOfLines={1}>{`Địa chỉ: ${_.get(item, 'diaChi', i18n.t('NULL_T'))}`}</Text>
          <Text style={[styles.title, { color: R.colors.yellow254, fontStyle: 'italic' }]} numberOfLines={1}>{`Ghi chú: ${_.get(item, 'maSanPhamTuVan', i18n.t('NULL_T'))}`}</Text>
          <Text style={[styles.title, { color: R.colors.white }]} numberOfLines={1}>
            {'Trạng thái: '}
            <Text style={[styles.title, { fontWeight: 'bold', color: _.get(item, 'trangThai', 2) === 2 ? 'green' : 'red' }]}>{_.get(item, 'trangThai', 2) === 2 ? 'Đã tư vấn' : 'Chưa tư vấn'}</Text>
          </Text>
        </View>
        {_.get(item, 'trangThai', 2) !== 2
          && <BaseButton
            title="Đã tư vấn"
            width={WIDTH(120)}
            height={HEIGHT(34)}
            onButton={() => onUpdateStatus && onUpdateStatus(item)}
          />
        }
      </View>
      <View style={styles.line} />
    </View>
  )
}

export default ItemYeuCau;

const styles = StyleSheet.create({
  itemView: {
    backgroundColor: R.colors.colorMain,
    width: getWidth(),
    marginTop: HEIGHT(16),
    paddingBottom: HEIGHT(16)
  },
  line: {
    height: HEIGHT(1),
    width: getWidth(),
    backgroundColor: R.colors.yellow1c8,
    marginTop: HEIGHT(12)
  },
  titleView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: WIDTH(16),
    paddingBottom: 0
  },
  title: {
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.yellow254
  },
})
