import React from 'react';
import { View, Text, ScrollView, Platform, StyleSheet, TouchableOpacity, Switch } from 'react-native';
import R from 'assets/R';
import ImagePicker from 'react-native-image-picker';
import FastImage from 'react-native-fast-image';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import NavigationService from 'routers/NavigationService';
import { LichChamSoc, Login, ChangePassword, ThongBao } from 'routers/screenNames';
import { connect } from 'react-redux';
import AsyncStorageUtils from 'helpers/AsyncStorageUtils';
import BaseButton from '../../../../common/Button/BaseButton';
import { setAccount } from '../../../../actions/actionCreators';
import HeaderTabHome from '../../../../common/Header/HeaderTabHome';
import { WIDTH, HEIGHT, getFont, getLineHeight } from '../../../../config';
import LichHen from './Item/LichHen';
import MenuProfile from './Item/MenuProfile';
import { pushDeviceToken } from '../../../../apis/Functions/users';
import ItemActiveStatus from './Item/ItemActiveStatus'

const MenuItem = [
  { image: R.images.iconSetting, title: 'Chỉnh sửa thông tin thành viên', destination: 'ChangeInfo' },
  { image: R.images.iconTime, title: 'Lịch sử đơn hàng', destination: 'LichSuDonHang' },
]

class Personal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      fileData: '',
      fileUri: ''
    }
  }

  launchImageLibrary = () => {
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.didCancel) {
      } else if (response.error) {
      } else {
        this.setState({
          fileData: response.data,
          fileUri: response.uri
        });
        this.imageit = {
          uri: Platform.OS === 'android' ? response.uri : response.uri.replace('file://', ''),
          type: response.type,
          name: response.fileName || response.uri
        }
      }
    });
  }

  logOut = async (iD_QuanLy) => {
    this.pushDeviceToken(iD_QuanLy)
    this.props.setAccount({})
    await AsyncStorageUtils.multiSet([
      [R.strings.INIT_STORGE, JSON.stringify(null)],
    ]);
    NavigationService.reset(Login)
  }

  pushDeviceToken = async (idTaiKhoan) => {
    const deviceToken = `?ID_TaiKhoan=${idTaiKhoan}&IDPUSH=`
    await pushDeviceToken(deviceToken);
  }

  render() {
    console.log('Account ne', this.props.Account.iD_QuanLy)
    const { Account } = this.props
    let avatar = Account.anhDaiDien ? { uri: R.images.IMAGE + Account.anhDaiDien } : R.images.logoHadoo
    if (this.props.Account.iD_QuanLy) {
      return (
        <ScrollView style={{ flex: 1, backgroundColor: R.colors.black3 }}>
          <HeaderTabHome title="Hồ sơ" searchEnable={false} />
          <View style={{ alignItems: 'center', }}>
            <FastImage
              source={avatar}
              style={styles.avatar}
              resizeMode={FastImage.resizeMode.stretch}
            />
          </View>
          <Text style={styles.fullName}>{Account.tenDayDu}</Text>
          {/** <LichHen time="8h30" type="Spa" /> */}
          <View style={{ marginTop: HEIGHT(48) }}>
            <MenuProfile data={MenuItem} />
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: WIDTH(24) }}>

            <TouchableOpacity
              onPress={() => NavigationService.navigate(ChangePassword)}
              disabled={this.state.loading}
              style={styles.logOutContainer}
            >
              <Text style={styles.logOut}>Đổi mật khẩu</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.logOut(Account.iD_QuanLy)}
              disabled={this.state.loading}
              style={styles.logOutContainer}
            >
              <Entypo name="log-out" size={HEIGHT(20)} color={R.colors.grey400} style={{ marginRight: WIDTH(20) }} />
              <Text style={styles.logOut}>Đăng xuất</Text>
            </TouchableOpacity>
            {/* <TouchableOpacity
              onPress={() => NavigationService.navigate(ThongBao)}
              disabled={this.state.loading}
              style={styles.logOutContainer}
            >
              <MaterialIcons name="notifications" size={HEIGHT(20)} color={R.colors.grey400} style={{ marginRight: WIDTH(20) }} />
              <Text style={styles.logOut}>Thông báo</Text>
            </TouchableOpacity> */}


          </View>
          {/* <View style={{ flexDirection: 'row', justifyContent: 'flex-end', paddingHorizontal: WIDTH(24) }}>
            <TouchableOpacity
              onPress={() => this.logOut(Account.iD_QuanLy)}
              disabled={this.state.loading}
              style={styles.logOutContainer}
            >
              <Entypo name="log-out" size={HEIGHT(20)} color={R.colors.grey400} style={{ marginRight: WIDTH(20) }} />
              <Text style={styles.logOut}>Đăng xuất</Text>
            </TouchableOpacity>
          </View> */}
          {
            this.props.Account.iD_NhomTaiKhoan >= R.strings.ROLE_USER.chuyenGia && <ItemActiveStatus />
          }
        </ScrollView>
      );
    } else {
      return (
        <View style={{ flex: 1, backgroundColor: R.colors.black3, alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{
            marginHorizontal: WIDTH(24),
            fontFamily: R.fonts.Roboto,
            fontWeight: '500',
            fontSize: getFont(16),
            lineHeight: getLineHeight(24),
            color: R.colors.grey400,
            marginBottom: HEIGHT(20),
            textAlign: 'center'
          }}
          >
            Bạn chưa đăng nhập. Vui lòng đăng nhập để có thể trải nghiệm hết các tính năng

          </Text>
          <BaseButton title="Đăng nhập" onButton={() => { NavigationService.navigate(Login) }} width={WIDTH(160)} />
        </View>
      )
    }
  }
}

function mapStateToProps(state) {
  return {
    Account: state.userReducers.Account
  };
}

export default connect(mapStateToProps, {
  setAccount
})(Personal);

const styles = StyleSheet.create({
  avatar: {
    width: WIDTH(120),
    height: WIDTH(120),
    marginTop: HEIGHT(32),
    borderRadius: WIDTH(60),
  },
  img: {
    marginTop: HEIGHT(-30),
    width: WIDTH(28),
    height: WIDTH(28),
    marginLeft: WIDTH(213),
    borderWidth: 2,
    borderColor: R.colors.black0,
    borderRadius: WIDTH(14)
  },
  fullName: {
    fontFamily: R.fonts.Roboto,
    fontWeight: '500',
    fontSize: getFont(28),
    lineHeight: getLineHeight(36),
    color: R.colors.yellow254,
    paddingVertical: HEIGHT(16),
    textAlign: 'center'
  },
  logOut: {
    fontFamily: R.fonts.Roboto,
    fontWeight: '500',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.grey400,
  },
  logOutContainer: {
    alignSelf: 'flex-end',
    flexDirection: 'row',
    marginTop: HEIGHT(20),
    justifyContent: 'center',
    alignItems: 'center'
  }
})
