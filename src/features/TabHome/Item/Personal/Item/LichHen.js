import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { HEIGHT, WIDTH, getFont, getLineHeight } from '../../../../../config';
import R from '../../../../../assets/R';

class LichHen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { time, type } = this.props;
    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 1 }}
        colors={[R.colors.yellow1c8, R.colors.yellow254, R.colors.yellow1c8]}
        style={styles.container}
      >
        <Text style={styles.text}>
          Lịch hẹn hôm nay:
          <Text style={[styles.text, { fontWeight: '700' }]}>{` ${time} - ${type}`}</Text>
        </Text>
      </LinearGradient>

    );
  }
}

export default LichHen;

const styles = StyleSheet.create({
  container: {
    width: WIDTH(258),
    height: HEIGHT(40),
    borderRadius: WIDTH(8),
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontFamily: R.fonts.Roboto,
    fontSize: getFont(15),
    lineHeight: getLineHeight(22),
    paddingVertical: HEIGHT(8),
    textAlign: 'center'
  }
});
