import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import NavigationService from '../../../../../routers/NavigationService';
import { HEIGHT, WIDTH, getFont, getLineHeight, getWidth } from '../../../../../config';
import R from '../../../../../assets/R';

class MenuProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

renderItem=({ item, index }) => (
  <TouchableOpacity
    style={[styles.container, { borderTopWidth: index === 0 ? 1 : 0 }]}
    onPress={() => NavigationService.navigate(item.destination)}
  >
    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
      <FastImage source={item.image} resizeMode={FastImage.resizeMode.contain} style={styles.img} />
      <Text style={styles.title}>{item.title}</Text>
    </View>
  </TouchableOpacity>
)

render() {
  return (
    <FlatList
      data={this.props.data}
      renderItem={this.renderItem}
    />
  );
}
}

export default MenuProfile;

const styles = StyleSheet.create({
  img: {
    width: WIDTH(20),
    height: WIDTH(20),
    marginLeft: WIDTH(26),
    marginRight: WIDTH(18)
  },
  title: {
    fontFamily: R.fonts.Roboto,
    fontWeight: '500',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.yellow254,
    paddingVertical: HEIGHT(20),
  },
  number: {
    fontFamily: R.fonts.Roboto,
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.white,
    paddingVertical: HEIGHT(20),
    marginRight: WIDTH(24),
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderColor: R.colors.nameText,
    alignItems: 'center',
    width: getWidth(),
  }
});
