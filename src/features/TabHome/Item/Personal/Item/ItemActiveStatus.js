import React, { useState, useEffect } from 'react';
import { View, Switch, StyleSheet, Text } from 'react-native';
import { getTrangThaiDV, setTrangThaiDV } from 'apis/Functions/users';
import { TRANG_THAI_NHAN_DV } from 'apis/constant';
import { fallbacks } from 'i18n-js';
import { HEIGHT, WIDTH, getFont, getLineHeight, getWidth, popupOk } from '../../../../../config';
import R from '../../../../../assets/R';


const ItemActiveStatus = () => {
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => {
    setIsEnabled(previousState => !previousState);
  }
  useEffect(() => {
    getStatus()
  }, [])
  async function getStatus() {
    try {
      let resStatus = await getTrangThaiDV()
      if (resStatus.data === 1) {
        setIsEnabled(true)
      } else {
        setIsEnabled(false)
      }
      // console.log('resStatus_resStatus', resStatus)
    } catch (error) {
      // console.log('error_error', error)
    }
  }
  const setTrangThaiDichVu = async (trangThai) => {
    try {
      let status = {
        TrangThai: 0
      }
      if (trangThai) {
        status = {
          TrangThai: TRANG_THAI_NHAN_DV.Active
        }
      }
      console.log('resSetTrangThai_resSetTrangThai', status)

      let resSetTrangThai = await setTrangThaiDV(status)
      // console.log('resSetTrangThai_resSetTrangThai', resSetTrangThai)
      if (resSetTrangThai) {
        popupOk('Thông báo', 'Đã cập nhật trạng thái!')
      } else {
        popupOk('Thông báo', 'Lỗi kết nối, không thể cập nhật trạng thái')
      }
    } catch (error) {
      popupOk('Thông báo', 'Lỗi kết nối, không thể cập nhật trạng thái')
    }
  }
  return (
    <View style={styles.container}>
      <Text style={styles.label}>Trạng thái nhận dịch vụ</Text>
      <Switch
        trackColor={{ false: '#767577', true: R.colors.whitergba }}
        thumbColor={isEnabled ? R.colors.greentag : '#f4f3f4'}
        ios_backgroundColor="#3e3e3e"
        onValueChange={(val) => {
          setTrangThaiDichVu(val)
          toggleSwitch()
        }}
        value={isEnabled}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: WIDTH(24),
    marginTop: HEIGHT(10)
  },
  label: {
    fontFamily: R.fonts.Roboto,
    fontWeight: '500',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.grey400,
  },
});

export default ItemActiveStatus;
