import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import moment from 'moment';
import { getKPI } from 'apis';
import { LoadingComponent } from 'common/Loading/LoadingComponent';
import R from 'assets/R';
import { ChiTietDoanhSo } from 'routers/screenNames';
import NavigationService from 'routers/NavigationService';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { HEIGHT, getFont, WIDTH } from '../../../../config';
import styles from './styles';
import BoardInfo from './ItemTarget/BoardInfo';
import EditTarget from './ItemTarget/EditTarget'

const dataFake = require('./fakeData.json');

class Target extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: moment(new Date()).format('M') - 1,
      loading: false,
      visible: false
    };
    this.dataItem = [
      {
        iD_KhachHang: 126,
        tenKhachHang: 'Trống',
        soTaiKhoan: '0',
        tongDonHang: 0,
        iD_NhomTaiKhoan: 0,
        tenNhom: 'Quản lý',
        donThanhCong: 0,
        tongTien: 0.0,
        daThanhToan: 0.0,
        loiNhuan: 0.0,
        nhanVienMoi: 0,
        luotDichVu: 0,
      },
    ];
  }

  componentDidMount = () => {
    // console.log('dissssssssss', this.state.selected)
    this.getData();
  };

  onShowModal=() => {
    this.setState({
      visible: !this.state.visible
    })
  }

  getData = async () => {
    this.setState({ loading: true });
    let year = new Date().getFullYear();
    let fromdate = new Date(year, this.state.selected, 1, 0, 0, 0);
    let toDate = new Date(year, this.state.selected + 1, 0, 23, 59, 59);
    let param = `?fromdate=${moment(fromdate).format(
      'YYYY/MM/DD HH:mm:ss'
    )}&todate=${moment(toDate).format('YYYY/MM/DD HH:mm:ss')}`;
    // console.log('paramparamparamparam', param)
    let res = await getKPI(encodeURI(param));
    // console.log('object', res)
    if (res.data && res.data[0]) {
      this.dataItem = res.data;
    }
    this.setState({ loading: false });
  };

  renderItem = (item, index) => (
    <TouchableOpacity
      activeOpacity={0.6}
      onPress={() => this.setState({ selected: index }, () => {
        this.getData();
      })
      }
      style={styles.itemContainer}
    >
      <Text
        style={[
          styles.itemTxt,
          {
            color:
              parseInt(this.state.selected, 10) === index
                ? 'white'
                : R.colors.yellow254,
            fontWeight: this.state.selected === index ? 'bold' : 'normal',
          },
        ]}
      >
        {item.name}
      </Text>
      <Text style={[styles.itemTxt, { fontWeight: '500' }]}>{item?.value}</Text>
    </TouchableOpacity>
  );

  render() {
    // const { selected } = this.state
    // console.log('day_nhe', this.dataItem)
    if (!this.state.loading) {
      return (
        <View style={styles.container}>
          <View style={styles.flexRow}>
            <Text style={styles.headerTitle}>Mục Tiêu</Text>
            <TouchableOpacity
              activeOpacity={0.6}
              onPress={this.onShowModal}
            >
              {/* <AntDesign
                name="pluscircleo"
                color={R.colors.yellow1c8}
                size={HEIGHT(30)}
              /> */}
            </TouchableOpacity>
          </View>
          <View style={styles.subView}>
            <BoardInfo data={this.dataItem?.[0]} />
            {/* <TouchableOpacity
              onPress={this.onShowModal}
              style={styles.buttonAddTarget}
            >
              <Text style={[styles.textAddTarget]}>Đặt mục tiêu</Text>
            </TouchableOpacity> */}
            <FlatList
              data={dataFake.data}
              scrollEnabled
              renderItem={({ item, index }) => this.renderItem(item, index)}
              showsVerticalScrollIndicator={false}
            />
          </View>
          <EditTarget
            visible={this.state.visible}
            onShowModal={this.onShowModal}
            text={this.state.selected + 1}
          />
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <Text style={styles.headerTitle}>Mục Tiêu</Text>
          <LoadingComponent isLoading={this.state.loading} />
        </View>
      );
    }
  }
}

export default Target;
