import React from 'react';
import { View, FlatList, Text, StyleSheet } from 'react-native';
import i18n from 'assets/languages/i18n';
import _ from 'lodash'
import moment from 'moment'
import { LoadingComponent } from 'common/Loading/LoadingComponent';
import ItemNull from '../../../../common/Item/ItemNull';
import HeaderBack from '../../../../common/Header/HeaderBack';
import R from '../../../../assets/R';
import { getFont, WIDTH, getLineHeight, getWidth, HEIGHT, formatVND } from '../../../../config';
import { getDetailKPI } from '../../../../apis'

export default class ChiTietDoanhSo extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: []
    };
  }

  componentDidMount = async () => {
    const thang = this.props.navigation.getParam('thang')
    let year = new Date().getFullYear();
    let fromdate = new Date(year, thang, 1, 0, 0, 0);
    let toDate = new Date(year, thang + 1, 0, 23, 59, 59);
    let param = {
      from: moment(fromdate).format('YYYY/MM/DD HH:mm:ss'),
      to: moment(toDate).format('YYYY/MM/DD HH:mm:ss'),
      trangthaixem: -1,
      ttht: -1
    }
    let res = await getDetailKPI(param);
    // console.log(param, res)
    if (res.data) {
      this.setState({ data: res.data, loading: false })
    } else this.setState({ loading: false })
  }

  render() {
    if (!this.state.loading) {
      return (
        <View style={{ flex: 1, backgroundColor: R.colors.black3 }}>
          <HeaderBack
            title={`Chi tiết doanh số tháng ${this.props.navigation.getParam('thang') + 1}`}
            iconSearch={false}
            iconCart={false}
          />
          <FlatList
            data={this.state.data}
            ListEmptyComponent={() => (<ItemNull text="Không có doanh số" />)}
            renderItem={({ item, index }) => (
              <View style={{
                width: getWidth(),
                paddingHorizontal: WIDTH(16),
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingBottom: HEIGHT(10),
                paddingTop: HEIGHT(16)
              }}
              >
                <View style={{ width: WIDTH(320), borderBottomWidth: 2, borderColor: R.colors.yellow254 }}>
                  <Text style={[styles.text, { color: R.colors.white, fontSize: getFont(16), fontWeight: '700' }]}>{_.has(item, 'tenKhachHang') ? `${item.tenKhachHang} - ${item.maKH}` : i18n.t('NULL_T')}</Text>
                  <Text style={[styles.text, { color: R.colors.grey400 }]}>{_.has(item, 'diaChi') ? `Địa chỉ: ${item.diaChi}` : i18n.t('NULL_T')}</Text>
                  <Text style={[styles.text, { color: R.colors.grey400 }]}>{_.has(item, 'dienThoai') ? `Số điện thoại: ${item.dienThoai}` : i18n.t('NULL_T')}</Text>
                  <Text style={[styles.text, { color: R.colors.grey400 }]}>{_.has(item, 'maThamChieu') ? `Mã: ${item.maThamChieu}` : i18n.t('NULL_T')}</Text>
                  <Text style={[styles.text, { color: R.colors.grey400 }]}>{_.has(item, 'ngayLap') ? `Ngày khởi tạo: ${item.ngayLap}` : i18n.t('NULL_T')}</Text>
                  <Text style={[styles.text, { color: R.colors.grey400 }]}>{_.has(item, 'tenTrangThaiDongHang') ? `Trạng thái đơn: ${item.tenTrangThaiDongHang}` : i18n.t('NULL_T')}</Text>
                  <Text style={[styles.text, { fontWeight: 'bold' }]}>{_.has(item, 'tongTien') ? `Tổng tiền: ${formatVND(item.tongTien)} VNĐ` : i18n.t('NULL_T')}</Text>
                  <Text style={[styles.text, { color: 'green', fontWeight: 'bold' }]}>{_.has(item, 'tienDaThanhToan') ? `Đã TT: ${formatVND(item.tienDaThanhToan)} VNĐ` : i18n.t('NULL_T')}</Text>
                  <Text style={[styles.text, { color: 'red', fontWeight: 'bold' }]}>{_.has(item, 'loiNhuan') ? `Lợi nhuận: ${formatVND(item.loiNhuan)} VNĐ` : i18n.t('NULL_T')}</Text>
                  <Text style={[styles.text, { color: R.colors.grey400, fontStyle: 'italic' }]}>{item.ghiChu || 'Không có ghi chú'}</Text>
                </View>
              </View>
            )}
          />

        </View>
      );
    } else {
      return (
        <View style={{ flex: 1, backgroundColor: R.colors.black3 }}>
          <HeaderBack
            title={`Chi tiết doanh số tháng ${this.props.navigation.getParam('thang')}`}
            iconSearch={false}
            iconCart={false}
          />
          <LoadingComponent isLoading={true} />
        </View>

      )
    }
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.colorMain,
    flex: 1,
  },
  viewTxt: {
    width: getWidth(),
    alignItems: 'center',
    marginTop: HEIGHT(20)
  },
  text: {
    fontSize: getFont(14),
    lineHeight: getLineHeight(20),
    color: R.colors.yellow254,
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'center',
    marginTop: HEIGHT(12),
    width: WIDTH(319),
    paddingHorizontal: WIDTH(10),
    borderBottomColor: R.colors.yellow254,
    borderBottomWidth: 1,
    alignSelf: 'center',
    marginBottom: HEIGHT(20)
  },
});
