import React from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {
  WIDTH,
  HEIGHT,
  getFont,
  getLineHeight,
  formatVND,
} from '../../../../../config';
import R from '../../../../../assets/R';

const BoardInfo = (props) => {
  const { data } = props;
  return (
    <LinearGradient
      colors={R.colors.linearButtonYellow}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 0 }}
      style={styles.container}
    >
      <View style={styles.firstLine}>
        <Text style={[styles.doanhSo, { fontWeight: 'bold' }]}>
          {`${data.tenKhachHang && data.tenKhachHang}`}
        </Text>
        <Text style={styles.doanhSo}>
          {`Lợi nhuận: ${data.loiNhuan && formatVND(data.loiNhuan)} VNĐ`}
        </Text>
      </View>
      <View style={[styles.firstLine, { alignItems: 'center' }]}>
        <Text style={styles.doanhSo}>Tổng:</Text>
        <Text style={styles.money}>
          {`${data.tongTien && formatVND(data.tongTien)} VNĐ`}
        </Text>
      </View>
      <View style={styles.line} />
      <View style={[styles.firstLine, { alignItems: 'center' }]}>
        <Text style={styles.doanhSo}>ĐÃ TT:</Text>
        <Text style={styles.total}>
          {`${data.daThanhToan && formatVND(data.daThanhToan)} VNĐ`}
        </Text>
      </View>
      <View style={[styles.firstLine, { alignItems: 'center' }]}>
        <Text style={styles.doanhSo}>Nhân viên mới:</Text>
        <Text style={styles.doanhSo}>
          {data.nhanVienMoi || 0}
        </Text>
      </View>
      <View style={[styles.firstLine, { alignItems: 'center' }]}>
        <Text style={styles.doanhSo}>Lượt dịch vụ:</Text>
        <Text style={styles.doanhSo}>
          {data.luotDichVu || 0}
        </Text>
      </View>
    </LinearGradient>
  );
};

export default BoardInfo;

const styles = StyleSheet.create({
  container: {
    width: WIDTH(327),
    backgroundColor: R.colors.colorBackgroundDetail,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: WIDTH(16),
    borderRadius: WIDTH(4),
    paddingTop: HEIGHT(12),
    paddingBottom: HEIGHT(9),
    marginVertical: HEIGHT(24),
  },
  firstLine: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: WIDTH(295),
  },
  doanhSo: {
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.backgroundBlack,
    fontFamily: R.fonts.Roboto,
  },
  money: {
    fontSize: getFont(32),
    lineHeight: getLineHeight(40),
    color: R.colors.black050,
    fontWeight: 'bold',
    fontFamily: R.fonts.TimesNewRomanBold,
    marginVertical: HEIGHT(8),
  },
  line: {
    width: WIDTH(295),
    height: HEIGHT(1),
    backgroundColor: R.colors.black050,
  },
  total: {
    fontFamily: R.fonts.Roboto,
    fontSize: getFont(20),
    lineHeight: getLineHeight(28),
    color: R.colors.black050,
    marginTop: HEIGHT(12),
  },
});
