import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, TextInput } from 'react-native';
import BottomModal, { ModalContent, SlideAnimation } from 'react-native-modals';
import Icon from 'react-native-vector-icons/Ionicons';
import NavigationService from 'routers/NavigationService';
import BaseButton from '../../../../../common/Button/BaseButton';
import { WIDTH, getLineHeight, getFont, HEIGHT, numberWithCommas, slitNumber } from '../../../../../config';
import R from '../../../../../assets/R';

const ModalForgotPass = (props) => {
  const { onShowModal, visible, text } = props;
  const [note, setNote] = useState('')
  return (
    <BottomModal
      modalAnimation={new SlideAnimation({
        initialValue: 0,
        slideFrom: 'bottom',
        useNativeDriver: true,
      })}
      visible={visible}
      onTouchOutside={() => {
        onShowModal();
      }}
      onHardwareBackPress={() => {
        onShowModal();
      }}
    >
      <ModalContent>
        <View style={styles.container}>
          <Text style={styles.success}>Đặt mục tiêu</Text>
          <Text style={[styles.username, { color: R.colors.black0, marginTop: HEIGHT(16) }]}>{`Mục tiêu cho tháng ${text}`}</Text>
          <View style={[styles.wrapper, { justifyContent: 'flex-start', height: HEIGHT(50), marginTop: HEIGHT(10) }]}>
            <TextInput
              style={styles.textInput}
              value={numberWithCommas(note)}
            //   autoFocus={true}
              placeholder="Nhập số tiền"
              keyboardType="numeric"
              placeholderTextColor={R.colors.grey500a}
              onChangeText={(text) => {
                console.log('text', text, slitNumber(text))
                setNote(slitNumber(text))
              }}
            />
            <Text style={styles.textVND}>VNĐ</Text>
          </View>

          <View style={styles.flexRow}>
            <TouchableOpacity style={styles.btnExit} onPress={() => onShowModal()}>
              <Text style={[styles.username, { color: R.colors.yellow254 }]}>Thoát</Text>
            </TouchableOpacity>
            <BaseButton
              title="Lưu mục tiêu"
              width={WIDTH(160)}
              height={HEIGHT(48)}
              onButton={() => {
                NavigationService.navigate('Home');
                onShowModal();
              }}
            />
          </View>
        </View>
      </ModalContent>
    </BottomModal>
  )
}

export default ModalForgotPass;

const styles = StyleSheet.create({
  container: {
    width: WIDTH(300),
    height: HEIGHT(300),
    paddingHorizontal: WIDTH(8),
    paddingVertical: HEIGHT(15),
    // alignItems: 'center',
    // justifyContent: 'center'
  },
  textInput: {
    color: R.colors.backgroundBlack,
    fontSize: getFont(20),
    fontFamily: R.fonts.Roboto,
    lineHeight: HEIGHT(29),
    padding: 0,
    width: WIDTH(230),
    marginRight: WIDTH(10),
  },
  textVND: {
    color: R.colors.backgroundBlack,
    fontSize: getFont(20),
    fontFamily: R.fonts.Roboto,
    lineHeight: HEIGHT(29),
    padding: 0,
  },
  success: {
    fontWeight: 'bold',
    fontSize: getFont(28),
    lineHeight: getLineHeight(35),
    color: R.colors.black0,
    textAlign: 'justify',
    alignSelf: 'center'
  },
  username: {
    fontWeight: '500',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.blackChecked,
    textAlign: 'justify'
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'center',
    marginTop: HEIGHT(12),
    width: WIDTH(286),
    paddingHorizontal: WIDTH(10),
    borderBottomColor: R.colors.darkBlue,
    borderBottomWidth: 1,
    alignSelf: 'center',
    marginBottom: HEIGHT(20),
  },
  flexRow: {
    flexGrow: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: HEIGHT(28)
  },
  btnExit: {
    width: WIDTH(108),
    height: HEIGHT(48),
    borderRadius: WIDTH(100),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: R.colors.darkBlue,
    marginRight: WIDTH(17)
  }
})
