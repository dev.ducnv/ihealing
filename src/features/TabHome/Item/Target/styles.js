import { StyleSheet } from 'react-native';
import R from '../../../../assets/R';
import { WIDTH, getFont, getLineHeight, HEIGHT } from '../../../../config';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: R.colors.black050
  },
  flexRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: WIDTH(16),
    alignItems: 'center',
    paddingTop: HEIGHT(16),

  },

  headerTitle: {
    fontSize: getFont(32),
    lineHeight: getLineHeight(40),
    fontFamily: R.fonts.TimesNewRomanBold,
    color: R.colors.yellow1c8
  },
  subView: {
    flex: 1,
    backgroundColor: R.colors.background,
    marginTop: HEIGHT(12)
  },
  itemContainer: {
    width: WIDTH(327),
    height: HEIGHT(56),
    borderRadius: WIDTH(4),
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: WIDTH(16),
    borderWidth: WIDTH(1),
    borderColor: R.colors.yellow254,
    marginBottom: HEIGHT(16),
    alignSelf: 'center'
  },
  itemTxt: {
    fontFamily: R.fonts.Roboto,
    color: R.colors.yellow254,
    fontSize: getFont(16),
    lineHeight: getLineHeight(24)
  },
  buttonAddTarget: { alignSelf: 'flex-end', paddingVertical: HEIGHT(15), paddingRight: WIDTH(24) },
  textAddTarget: {
    fontFamily: R.fonts.Roboto,
    color: R.colors.yellow254,
    lineHeight: getLineHeight(24),
    fontSize: getFont(18),
    fontStyle: 'italic',
    textDecorationLine: 'underline'
  },
});

export default styles;
