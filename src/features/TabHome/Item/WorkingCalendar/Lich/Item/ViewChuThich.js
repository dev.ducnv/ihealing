import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, StyleSheet, Image, Linking, TouchableOpacity } from 'react-native';
import { WIDTH, getWidth, getFont, HEIGHT } from '../../../../../../config';
import R from '../../../../../../assets/R';
import { getChuThichLich } from '../../../../../../apis/Functions/users'

const weekday = ['T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'CN'];
const SIZE_DOT = WIDTH(10)
const renderChuThich = (item, index) => {
  let color = item?.color ?? R.colors.grey400
  return (
    <View
      key={index.toString()}
      style={styles.viewChuThich}
    >
      <Text style={[styles.text, { color }]}>{item?.text ?? ''}</Text>
      <View style={[styles.dot, { backgroundColor: color }]} />
    </View>
  )
}

const ViewChuThich = () => {
  const [listChuThich, setChuThich] = useState([])
  useEffect(() => {
    async function getChuThich() {
      try {
        let resChuThich = await getChuThichLich();
        console.log('resChuThich_resChuThich', resChuThich)
        setChuThich(resChuThich.data)
      } catch (error) {

      }
    }
    getChuThich()
  }, [])
  return (
    <View style={{ flexDirection: 'row', width: getWidth(), justifyContent: 'space-between', paddingHorizontal: WIDTH(20) }}>
      <TouchableOpacity
        onPress={() => { Linking.openURL('tel:0979756668') }}
        style={{ flexDirection: 'row', alignItems: 'center' }}
      >
        <Image style={styles.icHotline} source={R.images.hotline} />
        <View>
          <Text style={styles.hotLine}>Hotline</Text>
          <Text style={styles.phoneNumber}>0979756668</Text>
        </View>
      </TouchableOpacity>
      <View style={[styles.container, { height: (listChuThich?.length ?? 1) * HEIGHT(40) }]}>
        <FlatList
          data={listChuThich}
        // horizontal
          renderItem={({ item, index }) => renderChuThich(item, index)}
        />
      </View>
    </View>

  )
}
export default ViewChuThich;

const styles = StyleSheet.create({
  container: {
    // backgroundColor: R.colors.colorMain,
    // width: getWidth(),\
    paddingHorizontal: WIDTH(20),
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-end',
    paddingVertical: HEIGHT(10),
    borderRadius: WIDTH(20),
    borderWidth: 1,
    borderColor: R.colors.white,
    marginTop: HEIGHT(10)
  },
  phoneNumber: {
    color: '#fff',
    fontSize: getFont(18),
    marginLeft: WIDTH(5)
  },
  hotLine: {
    color: R.colors.orange726,
    fontSize: getFont(18),
    marginLeft: WIDTH(5),
    fontFamily: R.fonts.RobotoMedium
  },
  icHotline: {
    height: HEIGHT(40),
    width: HEIGHT(40)
  },
  dot: {
    height: SIZE_DOT,
    width: SIZE_DOT,
    borderRadius: SIZE_DOT / 2,
    marginLeft: WIDTH(5)
  },
  text: {
    color: R.colors.colorBlack,
    fontSize: getFont(12),
    textAlign: 'center',
    textAlignVertical: 'center'
  },
  viewChuThich: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: HEIGHT(5),
    width: WIDTH(120),
    alignSelf: 'flex-end',
    justifyContent: 'space-between'
    // width: WIDTH(47),
  }
});
