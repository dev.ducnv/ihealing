// @flow
import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

// import
import ViewMonth from './ViewMonth';
import i18n from '../../../../../../assets/languages/i18n';
import { HEIGHT, WIDTH, getWidth } from '../../../../../../config';
import R from '../../../../../../assets/R';

type Props = {
  month: string | number,
  year: string | number,
  onButtonLeft: Function,
  onButtonRight: Function
}
const BackGroundCLD = (props: Props) => {
  const { month, year, onButtonLeft, onButtonRight, onChange } = props;
  let textMonth = parseInt(month, 10) < 10 ? `0${month.toString()}` : month.toString();
  return (
    <View style={styles.container}>
      <ViewMonth
        onChange={onChange}
        onButtonLeft={onButtonLeft}
        onButtonRight={onButtonRight}
        title={`${textMonth}/${year}`}
      />
    </View>
  )
}

export default BackGroundCLD;

const styles = StyleSheet.create({
  container: {
    paddingVertical: HEIGHT(15),
    width: getWidth()
  },
  btnAdd: {
    position: 'absolute',
    top: HEIGHT(8),
    right: WIDTH(30),
    alignSelf: 'flex-end'
  },
  hitSlop: {
    top: 10,
    bottom: 10,
    left: 10,
    right: 10
  }
});
