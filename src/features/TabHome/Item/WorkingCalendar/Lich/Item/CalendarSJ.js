// @flow
import React from "react";
import { Text, View, StyleSheet, TouchableOpacity } from "react-native";
import LinearGradient from "react-native-linear-gradient";
// import
import {
  getFont,
  HEIGHT,
  getWidth,
  getHeight,
  WIDTH,
} from "../../../../../../config";
import ViewDay from "./ViewDay";
import R from "../../../../../../assets/R";

type Props = {
  dataDay: Array<string | number>,
  selected: number,
  month: string | number,
  navigation: Object,
  outColorForDay: Function,
  onButtonDay: Function,
  dataHighlight: Array<string | number>,
};

const CalendarSJ = (props: Props) => {
  const {
    dataDay,
    selected,
    month,
    navigation,
    outColorForDay,
    onButtonDay,
    dataHighlight,
  } = props;
  return (
    <View style={styles.container}>
      <ViewDay />
      <View style={styles.cntDay}>
        {dataDay.map((value, index) => {
          let res = {
            viewDay: {},
            btnDay: {},
            textDay: {},
          };
          // if (value !== '') {
          //   res = outColorForDay(value);
          // }
          // console.log(dataHighlight)
          let ba = [R.colors.colorMain, R.colors.colorMain];
          // console.log(value)
          // console.log(dataHighlight)
          if (dataHighlight.includes(value)) {
            ba = ["#78cc61", "#247b0c", "#78cc61"];
          }
          if (selected === index) {
            ba = [R.colors.yellow1c8, R.colors.yellow254, R.colors.yellow1c8];
          }
          let colorText =
            selected === index
              ? { color: R.colors.black050 }
              : { color: R.colors.white };
          let currentDay = new Date();
          if (
            currentDay.getMonth() + 1 === month &&
            currentDay.getDate() === value
          ) {
            ba = [R.colors.white, R.colors.white];
            colorText = { color: R.colors.black050 };
          }
          return (
            <View style={[styles.viewDay, res.viewDay]} key={index.toString()}>
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 1 }}
                style={[styles.btnDay, res.btnDay]}
                colors={ba}
              >
                <TouchableOpacity
                  hitSlop={{ top: 30, bottom: 30, left: 30, right: 30 }}
                  onPress={() => onButtonDay(index, value)}
                >
                  <Text
                    style={[
                      styles.text,
                      { fontWeight: "500" },
                      res.textDay,
                      colorText,
                    ]}
                  >
                    {value}
                  </Text>
                </TouchableOpacity>
              </LinearGradient>
            </View>
          );
        })}
      </View>
      {/* <ListEvent data={eventsByOnSL} navigation={navigation} /> */}
    </View>
  );
};

export default CalendarSJ;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    marginTop: HEIGHT(25),
  },
  text: {
    color: R.colors.white,
    fontSize: getFont(15),
    fontFamily: R.fonts.Roboto,
  },
  cntDay: {
    width: getWidth(),
    paddingHorizontal: WIDTH(20),
    flexWrap: "wrap",
    flexDirection: "row",
  },
  viewDay: {
    width: WIDTH(45),
    height: HEIGHT(45),
    justifyContent: "center",
    alignItems: "center",
    marginVertical: HEIGHT(3),
    marginLeft: WIDTH(1.5),
  },
  btnDay: {
    width: HEIGHT(45),
    height: HEIGHT(45),
    borderRadius: HEIGHT(45),
    justifyContent: "center",
    alignItems: "center",
  },
  checkEvent: {
    width: HEIGHT(5),
    height: HEIGHT(5),
    borderRadius: HEIGHT(5),
    backgroundColor: R.colors.grayNew,
  },
});
