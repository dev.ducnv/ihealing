// @flow
import React from 'react'
import {
  Text, View, StyleSheet, TouchableOpacity
} from 'react-native'
import moment from 'moment'
import DatePicker from 'react-native-datepicker';
import AntDesign from 'react-native-vector-icons/AntDesign';
// import
import { WIDTH, HEIGHT, getFont } from '../../../../../../config';
import R from '../../../../../../assets/R';

type Props = {
  title: string,
  onButtonRight: Function,
  onButtonLeft: Function,
  onChange: Function,
}
export default (props: Props) => {
  const { title, onButtonLeft, onButtonRight, onChange } = props;
  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => onButtonLeft()}
        hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
        style={styles.menuLeft}
      >
        <AntDesign name="caretleft" size={HEIGHT(15)} color={R.colors.yellow254} />
      </TouchableOpacity>
      <DatePicker
        locale="vi"
        showIcon={false}
        style={styles.titleTxt}
        date={title}
        mode="date"
        placeholder=""
        format="MM/YYYY"
        confirmBtnText="Chọn"
        cancelBtnText="Hủy"
        customStyles={{
          dateInput: { borderWidth: 0 },
          dateText: styles.text
        }}
        onDateChange={(date) => {
          onChange(date);
        }}
      />
      <TouchableOpacity
        onPress={() => onButtonRight()}
        style={styles.menuRight}
        hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
      >
        <AntDesign name="caretright" size={HEIGHT(15)} color={R.colors.yellow254} />
      </TouchableOpacity>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    flexDirection: 'row',
  },
  menuLeft: {
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: WIDTH(30),
  },
  menuRight: {
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: WIDTH(30),
  },
  text: {
    color: R.colors.white,
    fontSize: getFont(20),
    textAlign: 'center',
    fontWeight: '700',
    textAlignVertical: 'center',
    fontFamily: R.fonts.Roboto,
  },
  titleTxt: {
    height: HEIGHT(40),
    justifyContent: 'center',
    alignItems: 'center',
  }
})
