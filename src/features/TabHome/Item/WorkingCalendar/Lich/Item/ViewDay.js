import React from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';
import { WIDTH, getWidth, getFont, HEIGHT } from '../../../../../../config';
import R from '../../../../../../assets/R';

const weekday = ['T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'CN'];

const renderWeekDay = (item, index) => {
  let color = index === 5 || index === 6 ? R.colors.red646 : R.colors.grey400
  return (
    <View
      key={index.toString()}
      style={styles.viewDay}
    >
      <Text style={[styles.text, { color }]}>{item}</Text>
    </View>
  )
}

const ViewDay = () => (
  <View style={styles.container}>
    <FlatList
      data={weekday}
      horizontal
      renderItem={({ item, index }) => renderWeekDay(item, index)}
    />
  </View>
)

export default ViewDay;

const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.colorMain,
    width: getWidth(),
    paddingHorizontal: WIDTH(20),
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  text: {
    color: R.colors.colorBlack,
    fontSize: getFont(16),
    textAlign: 'center',
    textAlignVertical: 'center'
  },
  viewDay: {
    flex: 0,
    width: WIDTH(47),
  }
});
