import _ from 'lodash'
import moment from 'moment'
import { WIDTH, getHeight, HEIGHT } from '../../../../../../config';
import R from '../../../../../../assets/R';

let dataColor = [
  R.colors.black050,
  R.colors.black050,
  R.colors.black050,
  R.colors.black050
];

export const calListViewMonth = (day, month, year) => {
  // dua ra list cac ngay trong 1 thang theo dung view lich
  // va ngay hien tai dang la ngay nao
  let DataDay = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  let dateFirstMonth = new Date(year, month - 1, 1);
  let maxN = (year % 4 === 0 && month === 2) ? 29 : DataDay[month];
  let str = [];
  let thu = dateFirstMonth.getDay() === 0 ? 6 : dateFirstMonth.getDay() - 1;
  for (let u = 1; u <= thu; u++) str.push('');
  for (let i = 1; i <= maxN; i++) str.push(i);
  return {
    arr: str,
    selected: thu - 1 + day
  }
};

export const onToday = async (self: Object) => {
  let dateCurrent = new Date();
  await self.setState({
    day: dateCurrent.getDate(),
    month: dateCurrent.getMonth() + 1,
    year: dateCurrent.getFullYear(),
    thu: dateCurrent.getDay(),
    dataHighlight: []
  }, () => self.loadHighlight())
  const { day, month, year } = self.state;
  let res = calListViewMonth(day, month, year);
  self.setState({
    dataDay: res.arr,
  }, () => self.loadLich());
};

export const onChange = async (self: Object, date: string) => {
  let d = date.split('/')
  let afterMonth = parseInt(d[0], 10);
  let afterYear = parseInt(d[1], 10);
  await self.setState({ month: afterMonth, year: afterYear, dataHighlight: [] }, () => { self.loadLich(); self.loadHighlight() })
  let res = calListViewMonth(1, afterMonth, afterYear);
  self.setState({
    dataDay: res.arr,
  })
};

export const onButtonRight = async (self: Object) => {
  const { month, year } = self.state;
  let afterMonth = month === 12 ? 1 : month + 1;
  let afterYear = year;
  if (month === 12) afterYear += 1;
  await self.setState({ month: afterMonth, year: afterYear, dataHighlight: [] }, () => self.loadHighlight())
  let res = calListViewMonth(1, afterMonth, afterYear);
  self.setState({
    dataDay: res.arr,
  })
};

export const onButtonLeft = async (self: Object) => {
  const { month, year } = self.state;
  let afterMonth = month === 1 ? 12 : month - 1;
  let afterYear = year;
  if (month === 1) afterYear -= 1;
  await self.setState({ month: afterMonth, year: afterYear, dataHighlight: [] }, () => self.loadHighlight())
  let res = calListViewMonth(1, afterMonth, afterYear);
  self.setState({
    dataDay: res.arr,
  })
};

export const calMixColor = (colorA, colorB) => {
  if (
    (colorA === R.colors.blueCalendar && colorB === R.colors.greenCalendar)
    || (colorB === R.colors.blueCalendar && colorA === R.colors.greenCalendar)
  ) return R.colors.mixBlueGreen;
  if (
    (colorA === R.colors.blueCalendar && colorB === R.colors.yellowCalendar)
    || (colorB === R.colors.blueCalendar && colorA === R.colors.yellowCalendar)
  ) return R.colors.mixBlueYellow;
  if (
    (colorA === R.colors.yellowCalendar && colorB === R.colors.greenCalendar)
    || (colorB === R.colors.yellowCalendar && colorA === R.colors.greenCalendar)
  ) return R.colors.mixGreenYellow;
  return R.colors.mixAll;
};

export const outColorForDay = (self: Object, value: number) => {
  let arr = self.state.eventsByDay[value].dataColorEvent;
  let { minDay } = self.state.eventsByDay[value];
  let { maxDay } = self.state.eventsByDay[value];
  let res = {
    viewDay: {},
    btnDay: {},
    textDay: {}
  };
  for (let i = 0; i < arr.length; i++) {
    let colorMix = _.has(res, 'btnDay.backgroundColor')
      ? dataColor[arr[i].idEvent % 3]
      : calMixColor(dataColor[arr[i].idEvent % 3], res.btnDay.backgroundColor);
    // /////////////////////////Truong hop bang 0
    let id = arr[i].idFormat;
    if (id === 0) {
      if (value === minDay) {
        res.viewDay = {
          ...res.viewDay,
          borderBottomLeftRadius: HEIGHT(35),
          borderTopLeftRadius: HEIGHT(35),
          backgroundColor: colorMix
        };
      } else {
        res.viewDay = {
          ...res.viewDay,
          backgroundColor: colorMix
        };
      }
      res.btnDay = {
        ...res.btnDay,
        backgroundColor: colorMix
      };
      res.textDay = {
        ...res.textDay,
        color: R.colors.colorBlack
      };
    }
    if (id === 2) {
      if (value === maxDay) {
        res.viewDay = {
          ...res.viewDay,
          borderTopRightRadius: HEIGHT(35),
          borderBottomRightRadius: HEIGHT(35),
          backgroundColor: colorMix
        };
      } else {
        res.viewDay = {
          ...res.viewDay,
          backgroundColor: colorMix
        };
      }
      res.btnDay = {
        ...res.btnDay,
        backgroundColor: colorMix
      };
      res.textDay = {
        ...res.textDay,
        color: R.colors.colorBlack
      };
    }
    // /////////////////////////Truong hop bang 1
    if (id === 1) {
      res.viewDay = {
        ...res.viewDay,
        backgroundColor: colorMix
      };
      res.btnDay = {
        ...res.btnDay,
        backgroundColor: colorMix
      };
      res.textDay = {
        ...res.textDay,
        color: R.colors.colorBlack
      };
    }
    // /////////////////////////Truong hop bang 2

    if (id === 1) {
      res.viewDay.borderTopLeftRadius = 0;
      res.viewDay.borderBottomLeftRadius = 0;
      res.viewDay.borderTopRightRadius = 0;
      res.viewDay.borderBottomRightRadius = 0;
    }
  }
  return res;
};

export const onButtonDay = async (self: Object, id: number, value: number) => {
  self.setState({
    selected: id,
  }, () => {
    self.loadLich()
  });
};

const listMonth = ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12']

export {
  listMonth
}
