import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Text } from 'react-native';
import moment from 'moment';
import _ from 'lodash';
import { connect } from 'react-redux';
import BackgroundCLD from './Item/BackgroundCLD';
import CalendarSJ from './Item/CalendarSJ';
import { WIDTH, getFont, HEIGHT } from '../../../../../config';
import { numberCalendar } from '../../../../../actions'
import R from '../../../../../assets/R';
import ChiTietLich from '../../../../../common/Item/ChiTietLich';
import {
  outColorForDay,
  onButtonLeft,
  onButtonRight,
  onToday,
  onButtonDay,
  onChange,
} from './Item/FunctionCalendar';
import {
  getCalendarByDayEmployee,
  getHighlightNV,
} from '../../../../../apis/Functions/users';
import ViewChuThich from './Item/ViewChuThich';

let self;

class LichLamViec extends Component {
  constructor(props) {
    super(props);
    self = this;
    this.state = {
      month: 1,
      year: 2019,
      day: 1,
      thu: 2,
      selected: -1,
      dataDay: [],
      eventsByDay: [6, 8],
      eventsByOnSL: [5, 7],
      isReady: false,

      dataToDay: [],
      loadingDataDay: true,
      dataHighlight: [],
    };
  }

  async componentDidMount() {
    await this.onToday();
    this.setState({ isReady: true });
  }

  onButtonDay = async (index: number, value: Object) => {
    onButtonDay(self, index, value);
  };

  outColorForDay = (value) => outColorForDay(this.state.eventsByDay, value);

  onToday = async () => {
    onToday(self);
  };

  onChange = (date) => {
    onChange(self, date);
  };

  onButtonLeft = () => {
    onButtonLeft(self);
  };

  onButtonRight = () => {
    onButtonRight(self);
  };

  loadHighlight = async () => {
    // let fromDate = new (Date(this.state.year, this.state.month - 1, 0))()

    let fromdate = new Date(this.state.year, this.state.month - 1, 1, 0, 0, 0);
    let toDate = new Date(this.state.year, this.state.month, 0, 0, 0, 0);
    let param = `?fromdate=${moment(fromdate).format(
      'YYYY-MM-DD'
    )}&todate=${moment(toDate).format('YYYY-MM-DD')}`;
    let res = await getHighlightNV(param);
    // console.log(res)
    if (res.data) {
      let dataHighlight = [];
      res.data.map((item, index) => {
        let day = parseInt(moment(item.thoiGianCheckInDuKien).format('D'), 10);
        dataHighlight.push(day);
      });
      this.setState({ dataHighlight });
    }
  };

  loadLich = async () => {
    this.setState({ loadingDataDay: true });
    let start;
    let end;
    let querry;
    let dataDay;
    if (this.state.selected > 0) {
      start = moment(
        new Date(
          this.state.year,
          this.state.month - 1,
          this.state.dataDay[this.state.selected],
          0,
          0,
          0
        )
      ).format('YYYY-MM-DD HH:mm:ss');
      end = moment(
        new Date(
          this.state.year,
          this.state.month - 1,
          this.state.dataDay[this.state.selected],
          23,
          59,
          59
        )
      ).format('YYYY-MM-DD HH:mm:ss');
    } else {
      start = moment(
        new Date(this.state.year, this.state.month - 1, this.state.day, 0, 0, 0)
      ).format('YYYY-MM-DD HH:mm:ss');
      end = moment(
        new Date(
          this.state.year,
          this.state.month - 1,
          this.state.day,
          23,
          59,
          59
        )
      ).format('YYYY-MM-DD HH:mm:ss');
    }
    let ID_NhanVien = this.props.Account.iD_QuanLy;
    querry = `?${JSON.stringify({ ID_NhanVien, start, end })}`;
    // console.log("aaa", querry)
    dataDay = await getCalendarByDayEmployee(querry);
    // console.log(dataDay.data)

    if (_.has(dataDay, 'data')) {
      this.props.numberCalendar(dataDay.data.length, 'EMPLOYEE')
      this.setState({ dataToDay: dataDay.data, loadingDataDay: false });
    } else {
      this.props.numberCalendar(0, 'EMPLOYEE')
      this.setState({ loadingDataDay: false });
    }
  };

  render() {
    // let today = new Date(this.state.year, this.state.month, this.state.selected))
    // console.log('dataHighlight', this.state.dataHighlight)
    return (
      <View style={styles.container}>
        <ScrollView style={{ backgroundColor: R.colors.background, flex: 1 }}>
          <View
            style={{
              backgroundColor: R.colors.colorMain,
              paddingTop: HEIGHT(10),
              paddingBottom: HEIGHT(5),
            }}
          >
            <BackgroundCLD
              month={this.state.month}
              year={this.state.year}
              onButtonLeft={this.onButtonLeft}
              onButtonRight={this.onButtonRight}
              onChange={this.onChange}
              onToday={this.onToday}
            />
            <CalendarSJ
              dataDay={this.state.dataDay}
              selected={this.state.selected}
              month={this.state.month}
              // navigation={this.props.navigation}
              // outColorForDay={this.outColorForDay}
              dataHighlight={this.state.dataHighlight}
              onButtonDay={this.onButtonDay}
              eventsByOnSL={this.state.eventsByOnSL}
            />
          </View>
          <View style={{ backgroundColor: R.colors.background, flex: 1 }}>
            <ViewChuThich />
            <View style={styles.viewText}>
              <Text style={styles.text}>Chi tiết lịch</Text>
            </View>
            <ChiTietLich
              data={this.state.dataToDay}
              canChangeCelendar={false}
              type="chuyengia"
              loading={this.state.loadingDataDay}
              reloadFunc={() => this.loadLich()}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    // cart: state.cartReducers.cart,
    // amount: state.cartReducers.amount,
    Account: state.userReducers.Account,
  };
}

export default connect(mapStateToProps, {
  numberCalendar
})(LichLamViec);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerMonth: {
    flex: 1,
    backgroundColor: R.colors.white,
    borderTopLeftRadius: WIDTH(30),
    borderTopRightRadius: WIDTH(30),
  },
  viewText: {
    marginTop: HEIGHT(24),
    marginBottom: HEIGHT(18),
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: R.colors.yellow254,
    fontSize: getFont(20),
  },
});
