import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import R from 'assets/R';
import moment from 'moment'
import ModalPickLocation from 'common/Modal/ModalPickLocation';
import ModalSearch from 'common/Modal/ModalSearch';
import { updateCalendar } from 'apis';
import { LoadingComponent } from 'common/Loading/LoadingComponent';
import NavigationService from 'routers/NavigationService';
import BaseButton from '../../../../../common/Button/BaseButton';
import ItemMultiPicker from '../../../../../common/Item/ItemMultiPicker';
import { getFont, getLineHeight, HEIGHT, WIDTH, getWidth, popupOk } from '../../../../../config';
import HeaderBack from '../../../../../common/Header/HeaderBack';

const textInfo = 'Chuyên viên LÊ THỊ AN đã có lịch công tác vào thời gian bạn chọn!\n Bạn vui lòng đổi thời gian hoặc đổi chuyên viên chăm sóc để sử dụng dịch vụ!';

class DoiLichChamSoc extends Component {
  constructor(props) {
    super(props);

    const item = props.navigation.getParam('item')

    this.state = {
      date: item.thoiGianCheckInDuKien ? moment(item.thoiGianCheckInDuKien).format('DD/MM/YYYY') : null,
      time: item.thoiGianCheckInDuKien ? moment(item.thoiGianCheckInDuKien).format('HH/mm').replace('/', 'h') : null,
      address: item.diaChi || null,
      note: item.ghiChu || null,
      idEmployee: item.tenNhanVien || null,
      typeEmploy: item.tenNhanVien ? 1 : 0,
      loading: false,
      locationSelect: -1
    };
    this.nhanVienChamSoc = null;
    this.coSoLienKet = null;
    this.position = item.diaChi ? { position: { lat: item.viDo, lng: item.kinhDo }, formattedAddress: item.diaChi } : null
  }


  onCancel = async () => {
    const item = this.props.navigation.getParam('item')
    let body
    this.setState({ loading: true })
    if (this.state.locationSelect === 1) {
      body = {
        BatDau: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        ID: item.idKeHoach,
        ID_DichVu: item.iD_DichVu,
        ID_KhachHang: item.idKhachHang,
        ID_NhanVien: 0,
        KetThuc: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        Ngay: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        DiaChi: this.coSoLienKet?.diaChi || '',
        ViDo: this.coSoLienKet?.viDo || 0,
        KinhDo: this.coSoLienKet?.kinhDo || 0,
      }
    } else {
      body = {
        BatDau: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        ID: item.idKeHoach,
        ID_DichVu: item.iD_DichVu,
        ID_KhachHang: item.idKhachHang,
        ID_NhanVien: 0,
        KetThuc: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        Ngay: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        DiaChi: this.position.formattedAddress,
        ViDo: this.position.position.lat.toFixed(5),
        KinhDo: this.position.position.lng.toFixed(5)
      }
    }
    let resBooking = await updateCalendar(body)
    this.setState({ loading: false })
    if (resBooking.data && resBooking.data.txt) {
      popupOk('Thông báo', resBooking.data.txt)
      this.setState({ loading: false })
      this.props.navigation.getParam('reloadFunc')()
      NavigationService.pop()
      // this.props.getData()
      // reload
    } else {
      popupOk('Thông báo', 'Hủy lịch  không thành công, vui lòng kiểm tra kết nối')
      this.setState({ loading: false })
    }
  }


  onUpdate = async () => {
    const item = this.props.navigation.getParam('item')
    if (this.state.time && (this.position || this.coSoLienKet)) {
      this.setState({ loading: true })
      const parseDate = this.state.date ? this.state.date.split('/') : moment(new Date()).format('DD/MM/YYYY').split('/')
      const parseTime = this.state.time.split('h')
      let body
      if (this.state.locationSelect === 1) {
        body = {
          BatDau: moment(new Date(parseDate[2], parseDate[1] - 1, parseDate[0], parseTime[0], parseTime[1], 0, 0)).format('YYYY-MM-DD HH:mm:ss'),
          GhiChu: this.state.note || '',
          ID: item.idKeHoach,
          ID_DichVu: item.iD_DichVu,
          ID_KhachHang: item.idKhachHang,
          ID_NhanVien: this.coSoLienKet?.iD_QuanLy || -1,
          KetThuc: moment(new Date(parseDate[2], parseDate[1] - 1, parseDate[0], parseInt(parseTime[0], 10) + 1, parseTime[1], 0, 0)).format('YYYY-MM-DD HH:mm:ss'),
          Ngay: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
          ...this.position
          // DiaChi: this.coSoLienKet?.diaChi || '',
          // ViDo: this.coSoLienKet?.viDo || 0,
          // KinhDo: this.coSoLienKet?.kinhDo || 0,
        }
      } else {
        body = {
          BatDau: moment(new Date(parseDate[2], parseDate[1] - 1, parseDate[0], parseTime[0], parseTime[1], 0, 0)).format('YYYY-MM-DD HH:mm:ss'),
          GhiChu: this.state.note || '',
          ID: item.idKeHoach,
          ID_DichVu: item.iD_DichVu,
          ID_KhachHang: item.idKhachHang,
          ID_NhanVien: this.nhanVienChamSoc ? this.nhanVienChamSoc.iD_QuanLy : -1,
          KetThuc: moment(new Date(parseDate[2], parseDate[1] - 1, parseDate[0], parseInt(parseTime[0], 10) + 1, parseTime[1], 0, 0)).format('YYYY-MM-DD HH:mm:ss'),
          Ngay: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
          ViDo: 0,
          KinhDo: 0,
          ...this.position
          // ViDo: this.position.position.lat.toFixed(5),
          // KinhDo: this.position.position.lng.toFixed(5)
        }
      }
      let resBooking = await updateCalendar(body)
      if (resBooking.data && resBooking.data.txt) {
        popupOk('Thông báo', resBooking.data.txt)
        this.setState({ loading: false })
        this.props.navigation.getParam('reloadFunc')()
        NavigationService.pop()
        // this.props.getData()
        // reload
      } else {
        popupOk('Thông báo', 'Đổi lịch  không thành công, vui lòng kiểm tra kết nối')
        this.setState({ loading: false })
      }
    } else {
      popupOk('Thông báo', 'Vui lòng nhập đủ các thông tin về thời gian, địa chỉ')
    }
  }

  render() {
    const item = this.props.navigation.getParam('item')
    // console.log(this.position)
    // console.log(item)
    if (!this.state.loading) {
      return (
        <View style={styles.container}>
          <HeaderBack title="Đổi lịch chăm sóc" iconCart={false} iconSearch={false} />

          <ModalPickLocation
            ref={ref => { this.ModalPickLocation = ref }}
            onSearch={(dataDiachiDaydu) => {
              this.setState({ address: dataDiachiDaydu.DiaChi })
              this.position = dataDiachiDaydu
            }}
            title="Tìm địa chỉ"
          />

          <ModalSearch
            ref={ref => { this.ModalSearch = ref }}
            data={this.props.data}
            onSearch={(value, item) => {
              this.setState({ idEmployee: item.tenDayDu })
              this.nhanVienChamSoc = item
              this.coSoLienKet = item
            }}
            title={this.props.title}
          />

          <View>
            <Text style={styles.title}>{item.tenDichVu}</Text>
          </View>
          <View style={styles.line} />
          <ItemMultiPicker
            showModalLocation={() => this.ModalPickLocation.setModalVisible(true)}
            showModalSearch={() => this.ModalSearch.setModalVisible(true, this.state.locationSelect)}
            // item={item}
            data={{
              time: { value: this.state.time, func: (time, index) => this.setState({ time: time.value }) },
              date: { value: this.state.date, func: (date) => this.setState({ date }) },
              address: { value: this.state.address },
              locationSelect: { value: this.state.locationSelect, func: (locationSelect, indexLocation) => this.setState({ locationSelect: indexLocation }) },
              employee: { value: this.state.typeEmploy, func: (type) => this.setState({ typeEmploy: type }) },
              idEmployee: { value: this.state.idEmployee, func: (value, itemNv) => { this.nhanVienChamSoc = itemNv; this.setState({ idEmployee: value }) } },
              note: { value: this.state.note, func: (text) => this.setState({ note: text }) }
            }}
            stylesContainer={{ alignSelf: 'center' }}
          />
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: WIDTH(25) }}>
            <BaseButton
              title="Đổi lịch"
              width={WIDTH(150)}
              style={{ alignSelf: 'center', marginTop: HEIGHT(24), }}
              onButton={() => {
                // alert('Đổi Lịch');
                this.onUpdate()
              }}
            />
            <BaseButton
              title="Hủy lịch"
              width={WIDTH(150)}
              style={{ alignSelf: 'center', marginTop: HEIGHT(24), }}
              onButton={() => {
                // alert('Đổi Lịch');
                this.onCancel()
              }}
            />
          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <HeaderBack title="Đổi lịch chăm sóc" iconCart={false} iconSearch={false} />
          <LoadingComponent isLoading={this.state.loading} />
        </View>
      )
    }
  }
}

export default DoiLichChamSoc;

const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.backgroundBlack,
    flex: 1,
  },
  textView: {
    width: WIDTH(327),
    alignSelf: 'center',
    backgroundColor: R.colors.black050,
    marginTop: HEIGHT(20)
  },
  textInfo: {
    fontSize: getFont(14),
    lineHeight: getLineHeight(20),
    paddingHorizontal: WIDTH(12),
    paddingVertical: HEIGHT(12),
    color: R.colors.white
  },
  title: {
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    fontWeight: '700',
    color: R.colors.white,
    paddingHorizontal: WIDTH(16),
    marginTop: HEIGHT(20)
  },
  line: {
    height: HEIGHT(1),
    width: getWidth(),
    backgroundColor: R.colors.yellow254,
    marginTop: HEIGHT(16)
  },
});
