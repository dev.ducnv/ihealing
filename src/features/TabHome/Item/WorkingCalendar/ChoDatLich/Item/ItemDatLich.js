// @flow
import React from 'react'
import {
  Text, View, StyleSheet, TouchableOpacity
} from 'react-native'
import Entypo from 'react-native-vector-icons/Entypo';
import _ from 'lodash'
import moment from 'moment'
// import
import { connect } from 'react-redux';
import { postBookCalendar } from 'apis';
import global from 'features/global'
import ModalSearch from '../../../../../../common/Modal/ModalSearch';
import ModalPickLocation from '../../../../../../common/Modal/ModalPickLocation';
import { WIDTH, HEIGHT, getFont, getLineHeight, getWidth, popupOk } from '../../../../../../config';
import i18n from '../../../../../../assets/languages/i18n';
import R from '../../../../../../assets/R';
import BaseButton from '../../../../../../common/Button/BaseButton';
import ItemMultiPicker from '../../../../../../common/Item/ItemMultiPicker';
import PickerSearch from '../../../../../../common/Picker/PickerSearch';
import ModalBookingService from '../../../../../../common/Modal/ModalBookingService'
import { LoadingComponent } from '../../../../../../common/Loading/LoadingComponent';

type Props = {
  changeExpand: Function,
  item: Object,
  index: number,
  expand: boolean,
  onShowModal: Function
}
class ItemDatLich extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: null,
      time: null,
      address: null,
      note: '',
      idEmployee: null,
      typeEmploy: 0,
      locationSelect: -1,
      visible: false,
      loading: false
    };
    this.position = null
    this.nhanVienChamSoc = null
    this.coSoLienKet = null
    this.nguoiGioiThieu = '';
  }

  onBooking = async () => {
    if (this.state.time && (this.position || this.coSoLienKet)) {
      this.setState({ loading: true })
      const parseDate = this.state.date ? this.state.date.split('/') : moment(new Date()).format('DD/MM/YYYY').split('/')
      const parseTime = this.state.time.split('h')
      let param;
      if (this.state.locationSelect === 1) {
        param = {
          BatDau: moment(new Date(parseDate[2], parseDate[1] - 1, parseDate[0], parseTime[0], parseTime[1], 0, 0)).format('YYYY-MM-DD HH:mm:ss'),
          GhiChu: this.state.note,
          ID_DichVu: this.props.item.idMatHang,
          ID_DonHang: this.props.item.iD_DonHang,
          ID_KhachHang: this.props.Account.iD_QuanLy,

          ID_NhanVien: this.coSoLienKet?.iD_QuanLy || -1,

          KetThuc: moment(new Date(parseDate[2], parseDate[1] - 1, parseDate[0], parseInt(parseTime[0], 10) + 1, parseTime[1], 0, 0)).format('YYYY-MM-DD HH:mm:ss'),
          Ngay: moment(new Date(parseDate[2], parseDate[1] - 1, parseDate[0], parseTime[0], parseTime[1], 0, 0)).format('YYYY-MM-DD HH:mm:ss'),
          ViDo: 0,
          KinhDo: 0,
          ...this.position
          // DiaChi: this.coSoLienKet?.diaChi || '',
          // ViDo: this.coSoLienKet?.viDo || 0,
          // KinhDo: this.coSoLienKet?.kinhDo || 0,
        };
      } else {
        param = {
          BatDau: moment(new Date(parseDate[2], parseDate[1] - 1, parseDate[0], parseTime[0], parseTime[1], 0, 0)).format('YYYY-MM-DD HH:mm:ss'),
          GhiChu: this.state.note,
          ID_DichVu: this.props.item.idMatHang,
          ID_DonHang: this.props.item.iD_DonHang,
          ID_KhachHang: this.props.Account.iD_QuanLy,
          ID_NhanVien: this.nhanVienChamSoc ? this.nhanVienChamSoc.iD_QuanLy : -1,
          KetThuc: moment(new Date(parseDate[2], parseDate[1] - 1, parseDate[0], parseInt(parseTime[0], 10) + 1, parseTime[1], 0, 0)).format('YYYY-MM-DD HH:mm:ss'),
          Ngay: moment(new Date(parseDate[2], parseDate[1] - 1, parseDate[0], parseTime[0], parseTime[1], 0, 0)).format('YYYY-MM-DD HH:mm:ss'),
          ViDo: 0,
          KinhDo: 0,
          ...this.position
          // DiaChi: this.position.formattedAddress,
          // ViDo: this.position.position.lat.toFixed(5),
          // KinhDo: this.position.position.lng.toFixed(5),
        }
      }
      console.log('parrrammm', param)
      if (this.nguoiGioiThieu?.iD_QuanLy) {
        param = {
          ...param,
          ID_NguoiGioiThieu: this.nguoiGioiThieu?.iD_QuanLy
        }
      }
      let resBooking = await postBookCalendar(`?${JSON.stringify(param)}`)
      if (resBooking.data && resBooking.data.txt) {
        if (!resBooking.data?.success) {
          popupOk('Thông báo', resBooking.data.txt);
          // this.props.onShowModal(this.state.time, this.state.date, this.state.locationSelect, this.coSoLienKet, this.nhanVienChamSoc, this.position)
          this.setState({ loading: false })
        } else {
          this.setState({ loading: false })
          this.props.onShowModal(this.props.item, this.state.time, this.state.date, this.state.locationSelect, this.coSoLienKet, this.nhanVienChamSoc, this.position)
          this.props.getData()
          global.reloadHilight && global.reloadHilight()
        }
        // popupOk('Thông báo', resBooking.data.txt)
      } else {
        popupOk('Thông báo', 'Đặt lịch  không thành công, vui lòng kiểm tra kết nối')
        this.setState({ loading: false })
      }
    } else {
      popupOk('Thông báo', 'Vui lòng nhập đủ các thông tin về thời gian, địa chỉ')
    }
  }

  onShowModal = () => {
    this.setState({
      visible: !this.state.visible
    })
  }

  render() {
    const { changeExpand, item, index, expand } = this.props;
    // console.log('this.state.idEmployee', item)
    return (
      <View style={styles.itemView}>
        <LoadingComponent
          style={{
            left: WIDTH(360) / 2 - WIDTH(10),
            top: HEIGHT(640) / 2,
            position: 'absolute',
            backgroundColor: 'transparent'
          }}
          isLoading={this.state.loading}
        />
        <ModalPickLocation
          ref={ref => { this.ModalPickLocation = ref }}
          onSearch={(dataDiachiDaydu) => {
            // this.setState({ value: { ...code } })
            // console.log(item)
            this.setState({ address: dataDiachiDaydu.DiaChi })
            this.position = dataDiachiDaydu
          }}
          title="Tìm địa chỉ"
        />
        <ModalSearch
          ref={ref => { this.ModalSearch = ref }}
          data={this.state.idEmployee}
          onSearch={(value, item) => {
            this.setState({ idEmployee: item.tenDayDu })
            // this.props.onSearch(code)
            this.nhanVienChamSoc = item
            this.coSoLienKet = item
            // console.log(item)
            // tenPhuongThucThanhToan
          }}
          title={this.props.title}
        />
        <TouchableOpacity
          onPress={() => changeExpand(index)}
          style={styles.titleView}
        >
          <View style={{ width: WIDTH(290) }}>
            <Text style={styles.title}>{!_.has(item, 'tenHang') ? i18n.t('NULL_T') : item.tenHang + (item.soLuong && ` (${item.trangThaiThanhToan || 'Chưa thanh toán'})(${item.tenPhuongThucThanhToan || 'trống'})`)}</Text>
          </View>
          <View>
            <Entypo
              name={expand ? 'chevron-small-down' : 'chevron-right'}
              size={HEIGHT(32)}
              color={R.colors.yellow254}
            />
          </View>
        </TouchableOpacity>
        {
          (expand)
            ? (
              <View>
                <View style={styles.line} />
                <View style={styles.wrapper}>
                  <PickerSearch
                    width={WIDTH(300)}
                    title="ID hoặc Số điện thoại người giới thiệu"
                    onSearch={(body) => {
                      // console.log('result_search_ne', body);
                      this.nguoiGioiThieu = body
                    }}
                    type="nguoiGioiThieu"
                    checkRequire={false}
                    data={[]}
                    upDateData={(key) => { }}
                    onCheck={() => { }}
                    value=""
                  />
                </View>
                <ItemMultiPicker
                  stylesContainer={{ alignSelf: 'center' }}
                  showModalLocation={() => this.ModalPickLocation.setModalVisible(true)}
                  showModalSearch={() => this.ModalSearch.setModalVisible(true, this.state.locationSelect)}
                  // item={item}
                  data={{
                    time: { value: this.state.time, func: (time, index) => this.setState({ time: time.value }) },
                    date: { value: this.state.date, func: (date) => this.setState({ date }) },
                    address: { value: this.state.address },
                    locationSelect: { value: this.state.locationSelect, func: (locationSelect, indexLocation) => this.setState({ locationSelect: indexLocation }) },
                    employee: { value: this.state.typeEmploy, func: (type) => this.setState({ typeEmploy: type }) },
                    idEmployee: { value: this.state.idEmployee, func: (value, itemNv) => { this.nhanVienChamSoc = itemNv; this.setState({ idEmployee: value }) } },
                    note: { value: this.state.note, func: (text) => this.setState({ note: text }) }
                  }}
                />
                <BaseButton
                  title="Đặt lịch"
                  width={WIDTH(184)}
                  style={{ alignSelf: 'center', marginTop: HEIGHT(24), }}
                  onButton={() => {
                    // onShowModal(index);
                    this.onBooking()
                  }}
                />
              </View>
            )
            : <View />
        }
      </View>
    )
  }
}


function mapStateToProps(state) {
  return {
    Account: state.userReducers.Account
  };
}

export default connect(mapStateToProps, {
  // setAccount
})(ItemDatLich);

const styles = StyleSheet.create({

  itemView: {
    backgroundColor: R.colors.colorMain,
    width: getWidth(),
    marginTop: HEIGHT(16),
    paddingBottom: HEIGHT(16)
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: HEIGHT(12),
    width: WIDTH(319),
    paddingLeft: WIDTH(10),
    borderBottomColor: R.colors.yellow254,
    borderBottomWidth: 1,
    alignSelf: 'center'
  },
  line: {
    height: HEIGHT(1),
    width: getWidth(),
    backgroundColor: R.colors.yellow1c8,
    marginTop: HEIGHT(16)
  },
  titleView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: WIDTH(16),
    paddingBottom: 0
  },
  title: {
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    fontWeight: '700',
    color: R.colors.white
  },
})
