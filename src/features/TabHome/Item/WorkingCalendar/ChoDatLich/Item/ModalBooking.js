import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import BottomModal, { ModalContent, SlideAnimation } from 'react-native-modals';
import Icon from 'react-native-vector-icons/Ionicons';
import _ from 'lodash'
import NavigationService from '../../../../../../routers/NavigationService';
import { LichSuDonHang } from '../../../../../../routers/screenNames';
import i18n from '../../../../../../assets/languages/i18n';
import BaseButton from '../../../../../../common/Button/BaseButton';
import { WIDTH, getLineHeight, getFont, HEIGHT } from '../../../../../../config';
import R from '../../../../../../assets/R'

type Props = {
  onShowModal: Function,
  visible: boolean,
  item: Object
}

const ModalBooking = (props: Props) => {
  const { onShowModal, visible, item, time } = props;
  return (
    <BottomModal
      modalAnimation={new SlideAnimation({
        initialValue: 0,
        slideFrom: 'bottom',
        useNativeDriver: true,
      })}
      visible={visible}
      onTouchOutside={() => {
        onShowModal();
      }}
      onHardwareBackPress={() => {
        onShowModal();
      }}
    >
      <ModalContent>
        <View style={styles.container}>
          <Icon
            name="ios-checkmark-circle"
            size={WIDTH(92)}
            color={R.colors.blackChecked}
          />
          <Text style={styles.success}>Đặt lịch thành công</Text>
          <Text style={[styles.username, { color: R.colors.black0, marginTop: HEIGHT(16) }]}>{`Dịch vụ: ${!_.has(item, 'title') ? i18n.t('NULL_T') : item.title}`}</Text>
          <Text style={[styles.username, { marginTop: HEIGHT(16) }]}>{`Thời gian: ${time}`}</Text>
          <Text style={[styles.username, { marginTop: HEIGHT(16) }]}>Địa điểm: Chăm sóc tại nhà</Text>
          <View style={styles.flexRow}>
            <TouchableOpacity
              style={styles.btnExit}
              onPress={() => {
                onShowModal();
              }}
            >
              <Text style={[styles.username, { color: R.colors.yellow254 }]}>Thoát</Text>
            </TouchableOpacity>
            <BaseButton
              title="Xem chi tiết"
              width={WIDTH(186)}
              height={HEIGHT(48)}
              onButton={() => {
                onShowModal();
                NavigationService.navigate('DoiLichChamSoc', { item });
              }}
            />
          </View>
        </View>
      </ModalContent>
    </BottomModal>
  )
}

export default ModalBooking;

const styles = StyleSheet.create({
  container: {
    width: WIDTH(320),
    height: HEIGHT(402),
    paddingHorizontal: WIDTH(8),
    paddingVertical: HEIGHT(28),
    alignItems: 'center',
    justifyContent: 'center'
  },
  success: {
    fontWeight: '500',
    fontSize: getFont(22),
    lineHeight: getLineHeight(30),
    color: R.colors.black0,
    textAlign: 'justify'
  },
  username: {
    fontWeight: '500',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.blackChecked,
    textAlign: 'justify'
  },
  flexRow: {
    flexGrow: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: HEIGHT(28)
  },
  btnExit: {
    width: WIDTH(108),
    height: HEIGHT(48),
    borderRadius: WIDTH(100),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: R.colors.darkBlue,
    marginRight: WIDTH(17)
  }
})
