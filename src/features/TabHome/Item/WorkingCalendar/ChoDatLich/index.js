import React, { Component } from 'react';
import { View, FlatList, StyleSheet, } from 'react-native';
import moment from 'moment'
import { connect } from 'react-redux';
import { getCalendarWaiting } from 'apis';
import ItemNull from 'common/Item/ItemNull';
import { LoadingComponent } from 'common/Loading/LoadingComponent';
import { getLichDangCho } from 'actions';
import R from '../../../../../assets/R';

import { getWidth, getFont, getLineHeight, HEIGHT, WIDTH } from '../../../../../config';
import Modal from './Item/ModalBooking';
import ItemDatLich from './Item/ItemDatLich';
import ModalBookingService from '../../../../../common/Modal/ModalBookingService';

const data = require('./data.json');

class ChoDatLich extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expand: [],
      visible: false,
      item: {},
      time: '',
      loading: true,
      locationSelect: '',
      date: ''
    };
    this.coSoLienKet = ''
    this.nhanVienChamSoc = ''
    this.position = ''
  }

  componentDidMount() {
    this.getData()
  }

  getData = async () => {
    let querry = `?idKhachHang=${this.props.Account.iD_QuanLy}`
    this.props.getLichDangCho(querry)
    this.setState({ loading: false })
    // let mCalendarWaiting = await getCalendarWaiting(querry)
  }

  setLoading = (loading) => {
    this.setState({ loading })
  }


  onShowModal = (item, time, date, locationSelect, coSoLienKet, nhanVienChamSoc, position) => {
    this.coSoLienKet = coSoLienKet
    this.nhanVienChamSoc = nhanVienChamSoc
    this.position = position
    this.setState({
      locationSelect,
      date,
      time,
      item
    }, () => {
      this.setState({ visible: !this.state.visible })
    })
  }

  changeExpand = (i) => {
    let arr = this.state.expand
    arr[i] = !arr[i]
    this.setState({ expand: arr })
  }

  renderItem = (item, index) => (
    <ItemDatLich
      item={item}
      index={index}
      expand={this.state.expand[index] || false}
      onShowModal={this.onShowModal}
      changeExpand={this.changeExpand}
      setLoading={this.setLoading}
      getData={this.getData}
      onGet
    />
  )

  render() {
    if (!this.state.loading) {
      return (
        <View style={styles.container}>
          <FlatList
            data={this.props.data}
            extraData={this.state}
            ListEmptyComponent={<ItemNull text="Không có lịch đang chờ" />}
            renderItem={({ item, index }) => this.renderItem(item, index)}
          />
          {/* <Modal
            item={this.state.item}
            time={this.state.time}
            visible={this.state.visible}
            onShowModal={this.onShowModal}
          /> */}
          <ModalBookingService
            visible={this.state.visible}
            onShowModal={this.onShowModal}
            tenNhanVien={this.state.locationSelect === 1 ? (this.coSoLienKet?.tenDayDu) : this.nhanVienChamSoc?.tenDayDu}
            diaChiLam={this.state.locationSelect === 1 ? (this.coSoLienKet?.diaChi || '') : (this.position?.formattedAddress ?? '')}
            tenDichVu={this.state.item?.tenHang ?? ''}
            date={this.state.date}
            time={this.state.time}
          />
        </View>
      );
    } else {
      return (
        <>
          <ModalBookingService
            visible={this.state.visible}
            onShowModal={this.onShowModal}
            tenNhanVien={this.state.locationSelect === 1 ? (this.coSoLienKet?.tenDayDu) : this.nhanVienChamSoc?.tenDayDu}
            diaChiLam={this.state.locationSelect === 1 ? (this.coSoLienKet?.diaChi || '') : (this.position?.formattedAddress ?? '')}
            tenDichVu={this.state.item?.tenHang ?? ''}
            date={this.state.date}
            time={this.state.time}
          />
          <LoadingComponent isLoading={this.state.loading} />
        </>
      )
    }
  }
}

function mapStateToProps(state) {
  return {
    Account: state.userReducers.Account,
    data: state.userReducers.mCalendarWaiting
  };
}

export default connect(mapStateToProps, {
  getLichDangCho
})(ChoDatLich);

const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.backgroundBlack,
    flex: 1,
  },
});
