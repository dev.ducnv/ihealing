import React from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import NavigationService from 'routers/NavigationService';
import { Login } from 'routers/screenNames';
import R from 'assets/R';
import { WIDTH, getLineHeight, HEIGHT, getFont } from '../../../../config';
import BaseButton from '../../../../common/Button/BaseButton';
import TabLichChamSoc from './Item/TabLichChamSoc';
import HeaderTabHome from '../../../../common/Header/HeaderTabHome';


class WorkingCalendar extends React.PureComponent {
  render() {
    if (this.props.Account.iD_QuanLy) {
      return (
        <View style={{ flex: 1 }}>
          <HeaderTabHome
            title={this.props.Account.iD_QuanLy > 1 ? 'Lịch làm việc' : 'Lịch chăm sóc'}
            searchEnable={true}
            cartEnable={true}
            notiButtonVisible
          />
          <TabLichChamSoc />
        </View>
      );
    } else {
      return (
        <View style={{ flex: 1, backgroundColor: R.colors.black3, alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{
            marginHorizontal: WIDTH(24),
            fontFamily: R.fonts.Roboto,
            fontWeight: '500',
            fontSize: getFont(16),
            lineHeight: getLineHeight(24),
            color: R.colors.grey400,
            marginBottom: HEIGHT(20),
            textAlign: 'center'
          }}
          >
            Bạn chưa đăng nhập. Vui lòng đăng nhập để có thể trải nghiệm hết các tính năng

          </Text>
          <BaseButton title="Đăng nhập" onButton={() => { NavigationService.navigate(Login) }} width={WIDTH(160)} />
        </View>
      )
    }
  }
}
function mapStateToProps(state) {
  return {
    Account: state.userReducers.Account
  };
}

export default connect(mapStateToProps, {
})(WorkingCalendar);
