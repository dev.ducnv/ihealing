// @flow
import React, { PureComponent } from 'react';
import { StyleSheet, View, TouchableOpacity, Text, } from 'react-native';
// import { Text } from 'react-native-paper';
import { TabView } from 'react-native-tab-view';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import R from '../../../../../assets/R';
import { HEIGHT, getFont, getWidth } from '../../../../../config';
import ChoDatLich from '../ChoDatLich';
import LichChamSoc from '../Lich/LichChamSoc';
import LichLamViec from '../Lich/LichLamViec';

const LazyPlaceholder = ({ route }: Object) => (
  <View style={styles.scene}>
    <Text>
      Loading
      {' '}
      {route.title}
      …
    </Text>
  </View>
);

const routesKhach = [
  { key: '1', title: 'Dịch vụ đã đặt' },
  { key: '2', title: 'Dịch vụ chưa đặt' }
]
const routesNV = [
  { key: '0', title: 'Lịch làm việc' }
]

class TabLichChamSoc extends PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      index: 0,
      routes: this.props.Account.iD_NhomTaiKhoan > 1 ? routesNV : routesKhach,
    };
  }


  _renderTabBar = (props: any) => {
    if (this.state.routes.length === 1) return null
    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 1 }}
        style={styles.styleTabbar}
        colors={[R.colors.yellow1c8, R.colors.yellow254, R.colors.yellow1c8]}
      >
        {props.navigationState.routes.map((route, i) => {
          let opacity = this.state.index === i ? 1 : 0.5
          return (
            <TouchableOpacity style={this.state.index === i ? styles.stylePressed : styles.styleunPress} onPress={() => this.setState({ index: i })} key={i}>
              <Text style={[styles.styleText, { opacity }]}>{this.getTitle(route)}</Text>
            </TouchableOpacity>
          );
        })}
      </LinearGradient>
    );
  }

  getTitle = (route) => {
    switch (route.title) {
      case 'Lịch chăm sóc':
        if (this.props.numberCalendarCl > 0) {
          return (`${route.title} (${this.props.numberCalendarCl})`)
        } else return (route.title)
      case 'Lịch làm việc':
        if (this.props.numberCalendarEm > 0) {
          return (`${route.title} (${this.props.numberCalendarEm})`)
        } else return (route.title)

      default:
        return (`${route.title}`)
    }
  }

  _renderLazyPlaceholder = ({ route }: Object) => (<LazyPlaceholder route={route} />)
    ;


  renderScene = ({ route }: Object) => {
    switch (route.key) {
      case '0':
        return <LichLamViec />;
      case '1':
        return <LichChamSoc />;
      case '2':
        return <ChoDatLich />;
      default:
        return null;
    }
  };


  render() {
    return (
      <TabView
        navigationState={this.state}
        renderTabBar={this._renderTabBar}
        renderScene={this.renderScene}
        style={{ flex: 1 }}
        onIndexChange={index => {
          this.setState({ index })
        }}
        initialLayout={{ width: getWidth() }}
        // tabBarPosition="bottom"
        // lazy={true}
        lazyPreloadDistance={1}
        renderLazyPlaceholder={this._renderLazyPlaceholder}
      />
    );
  }
}
function mapStateToProps(state) {
  return {
    Account: state.userReducers.Account,
    numberCalendarEm: state.userReducers.numberCalendarEm,
    numberCalendarCl: state.userReducers.numberCalendarCl
  };
}

export default connect(mapStateToProps, {
})(TabLichChamSoc);
const styles = StyleSheet.create({
  scene: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  styleText: {
    fontSize: getFont(16),
    color: R.colors.black0,
    // marginTop: HEIGHT(2),
    fontFamily: R.fonts.Roboto,
    fontWeight: 'bold'
  },
  styleunPress: {
    flex: 1,
    width: getWidth() / 2,
    alignItems: 'center',
    justifyContent: 'center'
  },
  stylePressed: {
    flex: 1,
    width: getWidth() / 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: R.colors.black0
  },
  styleTabbar: {
    height: HEIGHT(44),
    width: getWidth(),
    backgroundColor: R.colors.colorMain,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  container: {
    flex: 1,
    backgroundColor: R.colors.white100,
    justifyContent: 'flex-end',
  }
});
