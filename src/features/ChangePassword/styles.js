import { StyleSheet } from 'react-native';
import { WIDTH, HEIGHT, getFont, getLineHeight, getWidth } from '../../config';
import R from '../../assets/R';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: R.colors.background,
    alignItems: 'center'
  },
  logoStyle: {
    width: WIDTH(240),
    height: HEIGHT(207),
    marginTop: HEIGHT(64),
    marginBottom: HEIGHT(28)
  },
  wrapForgot: {
    width: WIDTH(327),
    alignItems: 'center',
    marginTop: HEIGHT(20),
    marginBottom: HEIGHT(24)
  },
  btnForgot: {
    alignSelf: 'flex-end'
  },
  textForgot: {
    fontSize: getFont(15),
    lineHeight: getLineHeight(22),
    color: R.colors.orange726,
    textDecorationLine: 'underline',
    fontWeight: '500',
  },
  text: {
    fontWeight: '500',
    fontSize: getFont(15),
    lineHeight: getLineHeight(22),
  },
  navigate: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  contentBelowButton: {
    width: getWidth(),
    paddingHorizontal: WIDTH(24),
    marginTop: HEIGHT(25)
  },
  hotline: {
    fontWeight: '500',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.orange726,
    marginTop: HEIGHT(135),
    marginBottom: HEIGHT(20)
  }
})
export default styles;
