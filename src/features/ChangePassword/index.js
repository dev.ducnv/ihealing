import React, { Component } from 'react';
import { View, Text, StatusBar, TouchableOpacity, ScrollView, Keyboard, Alert } from 'react-native';
import FastImage from 'react-native-fast-image';
import Icon from 'react-native-vector-icons/AntDesign';
import NavigationService from 'routers/NavigationService';
import DeviceInfo from 'react-native-device-info';
import _ from 'lodash';
import { connect } from 'react-redux';
import { setAccount } from '../../actions/actionCreators';
import { WIDTH } from '../../config';
import R from '../../assets/R';
import { changePass } from '../../apis/Functions/users';
import FormInput from '../../common/Form/FormInput';
import BaseButton from '../../common/Button/BaseButton';
import styles from './styles';
import AsyncStorageUtils from '../../helpers/AsyncStorageUtils';
import Modal from './items/ModalChangePass';
import { LoadingComponent } from '../../common/Loading/LoadingComponent';
import { HEIGHT } from '../../config/Function';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      username: '',
      password: '',
      newPassword: '',
      confirmNewPassword: '',
      loading: false
    };
    this.textForgot = ''
  }

  deviceId = '';

  deviceName = '';

  keyboardWillShowListener = () => { };

  async componentDidMount() {
    this.deviceId = await DeviceInfo.getUniqueId();
    this.deviceName = await DeviceInfo.getDeviceName();
  }

  componentWillMount() {
    this.keyboardWillShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardWillShow);
    this.keyboardWillShowListener = Keyboard.addListener('keyboardDidHide', this._keyboardWillHide);
  }

  componentWillUnmount() {
    this.keyboardWillShowListener.remove();
    this.keyboardWillShowListener.remove();
  }

  onShowModal = () => {
    this.setState({ visible: false });
  }

  onChangeUsername = (value) => {
    this.setState({ username: value });
  }

  onChangePassword = (value) => {
    this.setState({ password: value });
  }

  onChangeNewPassword = (value) => {
    this.setState({ newPassword: value });
  }

  onChangeConfirmNewPassword = (value) => {
    this.setState({ confirmNewPassword: value });
  }


  onChangePass = async () => {
    const account = {
      Username: this.state.username.toLowerCase(),
      Password: this.state.password,
      NewPass: this.state.newPassword,
      ConfirmNewPass: this.state.confirmNewPassword,
    };
    if (account.Username.length > 20) {
      Alert.alert(
        'Thông báo',
        'Tên đăng nhập không được vượt quá 20 ký tự!',
        [
          {
            text: 'OK',
            onPress: () => {
              this.setState({ username: '', password: '' });
            }
          }
        ],
        { cancelable: false }
      )
    } else if (account.Username === '' || account.Password === '') {
      Alert.alert(
        'Thông báo',
        'Vui lòng điền đầy đủ thông tin đăng nhập!',
        [
          { text: 'OK' }
        ],
        { cancelable: false }
      )
    } else if (account.NewPass !== account.ConfirmNewPass) {
      Alert.alert(
        'Thông báo',
        'Mật khẩu mới không khớp!',
        [
          { text: 'OK' }
        ],
        { cancelable: false }
      )
    } else {
      this.setState({ loading: true }, async () => {
        let checkChangePass = await changePass(account);
        // console.log(checkChangePass.data.success)
        if (_.has(checkChangePass, 'data.success') && checkChangePass.data.success === true) {
          this.setState({ loading: false, visible: true });
        } else {
          this.setState({ loading: false })
          Alert.alert('Đổi mật khẩu thất bại');
        }
      })
    }
  }


  FormSignIn = [
    { image: R.images.ic_user_lock, hint: 'Tên đăng nhập', func: this.onChangeUsername, isCharacters: true },
    { image: R.images.ic_lock, hint: 'Mật khẩu hiện tại', isSecure: true, func: this.onChangePassword, isCharacters: true },
    { image: R.images.ic_lock, hint: 'Mật khẩu mới', isSecure: true, func: this.onChangeNewPassword, isCharacters: true },
    { image: R.images.ic_lock, hint: 'Xác nhận mật khẩu mới', isSecure: true, func: this.onChangeConfirmNewPassword, isCharacters: true }

  ]

  render() {
    if (!this.state.loading) {
      return (
        <View style={styles.container}>
          <StatusBar backgroundColor={R.colors.background} />
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.container}>
              <FastImage
                style={styles.logoStyle}
                source={R.images.logoHadoo}
                resizeMode={FastImage.resizeMode.contain}
              />
              <FormInput dataForm={this.FormSignIn} />

              <BaseButton
                style={{ marginTop: HEIGHT(30) }}
                title="Đổi mật khẩu"
                onButton={this.onChangePass}
              />
              <View style={[styles.navigate, styles.contentBelowButton]}>
                <TouchableOpacity style={styles.navigate} onPress={() => NavigationService.navigate('Home')}>
                  <Icon name="arrowleft" size={WIDTH(16)} color={R.colors.white} />
                  <Text style={[styles.text, { color: R.colors.white, marginLeft: WIDTH(10) }]}>Trang chủ</Text>
                </TouchableOpacity>
              </View>

              <Text style={styles.hotline}>Hotline: 093.652.5858 – 097.463.1299</Text>
            </View>
            <Modal
              visible={this.state.visible}
              onShowModal={this.onShowModal}
              text={this.textForgot}
            />
          </ScrollView>
        </View>
      )
    } return <LoadingComponent isLoading={this.state.loading} />
  }
}

function mapStateToProps(state) {
  return {
    Account: state.userReducers.Account
  };
}

export default connect(mapStateToProps, {
  setAccount
})(Login);
