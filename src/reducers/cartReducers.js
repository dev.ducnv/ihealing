

const defaultState = {
  cart: [
    // {
    //   item: {
    //     id: 1,
    //     icon: 'http://112.78.1.190:9090/FileUpload/Images/dv4.jpg',
    //     title: 'Thải độc dành cho đau vai gáy và đốt sống cổ',
    //     price: '1,380,000',
    //     typeProduct: 0,
    //     unit: 'buổi',
    //   },
    //   total: 0,
    // }
  ],
  amount: 0,
  isDichVu: 0
};
export default (state = defaultState, action) => {
  let array
  let tmpItem
  let existed = false
  let num
  let total = 0
  switch (action.key) {
    default:
      break;
  }
  switch (action.type) {
    case 'ADD_PRODUCT': {
      tmpItem = action.arr
      array = [...state.cart];
      for (let i = 0; i < array.length; i++) {
        if (tmpItem.idMatHang === array[i].item.idMatHang) {
          existed = true;
          num = i
          break
        }
      }
      if (existed) {
        array[num].total += 1
        for (let i = 0; i < array.length; i++) {
          total += array[i].total
        }
        return {
          ...state,
          cart: array,
          amount: total,
          isDichVu: tmpItem.isDichVu
        };
      } else {
        let item = {
          item: {
            id: 1,
            icon: 'http://112.78.1.190:9090/FileUpload/Images/dv4.jpg',
            title: 'Thải độc dành cho đau vai gáy và đốt sống cổ',
            price: '1,380,000',
            typeProduct: 0,
            unit: 'buổi',
          },
          total: 1,
        }
        item.item = tmpItem
        return {
          ...state,
          cart: state.cart.concat(item),
          amount: state.amount + 1,
          isDichVu: tmpItem.isDichVu
        };
      }
    }
    case 'MINUS_PRODUCT': {
      array = [...state.cart];
      let { amount } = state;
      amount--
      if (array[action.index].total > 1) {
        array[action.index].total--
        return {
          ...state,
          cart: array,
          amount
        };
      } else {
        array = [...state.cart];
        array.splice(action.index, 1);
        return {
          cart: array,
          amount
        };
      }
    }
    case 'DELETE_PRODUCT':
      array = [...state.cart];
      array.splice(action.index, 1);
      return {
        cart: array,
        amount: array.length
      };
    case 'SET_PRODUCT':
      return {
        cart: action.cart,
        amount: action.amount,
      };
    default:
      break;
  }
  return state;
};
