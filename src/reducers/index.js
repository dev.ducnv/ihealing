import { combineReducers } from 'redux';
import { locationReducers } from './locationReducers';
import userReducers from './userReducers';
import cartReducers from './cartReducers';

const rootReducers = combineReducers({
  userReducers,
  locationReducers,
  cartReducers
});

export default rootReducers;
