const defaultState = {
  Account: {},
  loadings: false,
  loading: false,
  info: {
    extra_info: null,
    info: {
    }
  },
  mCalendarWaiting: [],
  listArticle: [],
  numberCalendarEm: 0,
  numberCalendarCl: 0,
};
export default (state = defaultState, action) => {
  switch (action.key) {
    default:
      break;
  }
  switch (action.type) {
    case 'SET_ACCOUNT': {
      return {
        ...state,
        Account: action.arr,
      };
    }
    case 'PUT_NUMBER_CALENDAR_EM': {
      return {
        ...state,
        numberCalendarEm: action.payload,
      };
    }
    case 'PUT_NUMBER_CALENDAR_CL': {
      return {
        ...state,
        numberCalendarCl: action.payload,
      };
    }
    case 'SETLOADING': return {
      ...state,
      loadings: action.id
    };
    case 'GET_LICH_DANG_CHO_SUCCESS':
      return {
        ...state,
        mCalendarWaiting: action.payload
      }
    case 'GET_ALL_ARTICLE_SUCCESS':
      return {
        ...state,
        listArticle: action.payload
      }
    default:
      break;
  }
  return state;
};
