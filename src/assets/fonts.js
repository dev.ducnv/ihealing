import { Platform } from 'react-native';

const fonts = {
  Roboto: 'Roboto',
  RobotoMedium: 'Roboto-Medium',
  RobotoRegular: 'Roboto-Regular',
  RobotoLightItalic: 'Roboto-LightItalic',
  TimesNewRoman: Platform.OS === 'ios' ? 'Tinos-Regular' : 'SVN-Times-New-Roman',
  TimesNewRomanBold: Platform.OS === 'ios' ? 'Tinos-Bold' : 'SVN-Times-New-Roman-Bold',
  TimesNewRomanItalic: Platform.OS === 'ios' ? 'Tinos-Italic' : 'SVN-Times-New-Roman-Italic'
};
export default fonts;
