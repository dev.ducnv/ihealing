import { ImageSetting, NetworkSetting } from '../config';

/* eslint-disable global-require */

const images = {
  bg_cannot_connect: require('./images/general/bg_cannot_connect.png'),
  app_icon: require('./images/general/app_icon.png'),
  ic_upgrade: require('./images/general/ic_upgrade.png'),
  circleCheck: require('./images/general/circleCheck.png'),
  iconCar: require('./images/Icons/iconCar.png'),
  iconStore: require('./images/Icons/iconStore.png'),
  logoHadoo: require('./images/general/logo_hadoo.png'),
  //
  icAddress: require('./images/Icons/icAddress.png'),
  icPayment: require('./images/Icons/icPayment.png'),
  iconNote: require('./images/Icons/iconNote.png'),
  iconCard: require('./images/Icons/iconCard.png'),
  iconClock: require('./images/Icon/mdi_access_time.png'),

  // icon home
  iconHome: require('./images/Icon/icon_home.png'),
  iconAdmin: require('./images/Icon/icon_admin.png'),
  iconMucTieu: require('./images/Icon/icon_muc_tieu.png'),
  iconLichLam: require('./images/Icon/icon_lich_lam.png'),
  iconCaNhan: require('./images/Icon/icon_user.png'),

  ic_email: require('./images/formInput/ic_email.png'),
  ic_gender: require('./images/formInput/ic_gender.png'),
  ic_lock: require('./images/formInput/ic_lock.png'),
  ic_person_id: require('./images/formInput/ic_person_id.png'),
  ic_phone: require('./images/formInput/ic_phone.png'),
  ic_user_lock: require('./images/formInput/ic_user_lock.png'),
  ic_user: require('./images/formInput/ic_user.png'),
  ic_people: require('./images/formInput/ic_people.png'),
  ic_birthday: require('./images/formInput/ic_birthday.png'),
  ic_qr: require('./images/formInput/ic_qr.png'),
  ic_address: require('./images/formInput/ic_address.png'),
  iconLike: require('./images/general/iconLike.png'),
  iconAdd: require('./images/general/ic_add.png'),
  person1: require('./images/general/person1.png'),
  bannerHome: require('./images/home/bannerHome.jpg'),
  muaHang: require('./images/general/muaHang.jpg'),
  thanhToan: require('./images/general/thanhToan.jpg'),

  // tmp image
  iconspa: require('./images/general/tmp/iconspa.png'),
  avatar: require('./images/general/tmp/avatar.png'),
  // bank
  techcombank: require('./images/general/techcombank.png'),
  // header
  iconCart: require('./images/general/cart.png'),
  iconSearch: require('./images/general/search.png'),
  logoHome: require('./images/general/logo2.png'),
  // Cooperate image
  iconPerson: require('./images/general/iconPerson.png'),
  iconExpert: require('./images/general/iconExpert.png'),
  iconManager: require('./images/general/iconManager.png'),
  iconChief: require('./images/general/iconChief.png'),
  // Menu Item
  iconTime: require('./images/general/ic_time.png'),
  iconSetting: require('./images/general/ic_setting.png'),
  iconFlower: require('./images/general/ic_flower.png'),
  iconProduct: require('./images/general/iconProduct.jpg'),

  // icon home
  daoTao: require('./images/home/daoTao.png'),
  dichVu: require('./images/home/dichVu.png'),
  sanPham: require('./images/home/sanPham.png'),
  tuyenDung: require('./images/home/tuyenDung.png'),
  sound: require('./images/general/sound.png'),
  hotline: require('./images/Icons/hotline.gif'),
  IMAGE: NetworkSetting.IMAGE,
  ...ImageSetting,
};

export default images;
