export const local = {
  NoInternetComponent: {
    no_internet_connection: 'No Internet Connection',
    pls_check_your_internet_connection: 'Please check your internet connection',
  },
  welcome: {},

  login: {},

  name: {
    what_your_name: "What's your name?",
    first_name: 'First name',
    last_name: 'Last name',
  },

  email: {
    email_header_title: 'And, your email?',
    email_address: 'Email address',
    error_invalid_email_title: 'Invalid email',
    error_invalid_email_subtitle: 'Please check if your email has been entered correctly.',
    receive_email:
      "I'd like to receive promotional communications, including discounts, surveys, inspiration, and love from app via email, SMS and phone.",
  },

  password: {
    password_header_title: 'Create a password',
    password_subtitle:
      'Your password must include at least one symbol and be 6 or more characters long.',
    password: 'Password',
  },

  forgotPassword: {
    forgot_header_title: 'Forgot your password?',
    forgot_subtitle: 'Enter your email to find your account.',
    email_address: 'Email address',
  },

  phoneNumberScreen: {
    label_enter_phone_number: 'Enter your phone number',
    text_hint_policy:
      'By tapping "Send confirmation code" above, we will send you an SMS to confirm your phone number. Message & data rates may apply.',
    text_button_continue: 'Send confirmation code',
  },

  ConfirmationCodeScreen: {
    didnt_get_a_code: "I didn't get a code",
    send_sms_again: 'Send SMS again in',
    enter_the_code_was_sent_to: 'Enter the code that was sent to',
  },

  VersionChecker: {
    title: 'Update Available !',
    version: 'Version:',
    description: 'A newer version of RNBase is available. Update now to continue using RNBase and take advantage of the latest features and security upgrades!',
    updateAction: 'Update Now'
  },
};


export const TIME_ARRAY = [
  '7h00', '7h15', '7h30', '7h45', '8h00', '8h15', '8h30', '8h45',
  '9h00', '9h15', '9h30', '9h45', '10h00', '10h15', '10h30', '10h45',
  '11h00', '11h15', '11h30', '11h45', '12h00', '12h15', '12h30', '12h45',
  '13h00', '13h15', '13h30', '13h45', '14h00', '14h15', '14h30', '14h45',
  '15h00', '15h15', '15h30', '15h45', '16h00', '16h15', '16h30', '16h45',
  '17h00', '17h15', '17h30', '17h45', '18h00', '18h15', '18h30', '18h45',
  '19h00', '19h15', '19h30', '19h45', '20h00', '20h15', '20h30', '20h45',
  '21h00', '21h15', '21h30', '21h45', '22h00'
]

export const INIT_STORGE = '@ACCOUNT';
export const SAVE_TOKEN_NOTI = '@SAVE_TOKEN_NOTI';
export const ROLE_USER = {
  guest: 0,
  khachHang: 1,
  chuyenGia: 2,
  chuyenVienTuVan: 3,
  quanLy: 4,
  giamDoc: 5
}

export const GROUP_USER = [
  'Khách',
  'Khách hàng',
  'Chuyên gia',
  'Chuyên viên tư vấn',
  'Quản lý',
  'Giám đốc',
]

export const URL_REGISTER = 'https://ihealing.vn/ihealing/#!/register'
