// Thành công, may mắn, hạnh phúc.
import React, { Component } from 'react';
import { AppState, StyleSheet, SafeAreaView, Alert } from 'react-native';
import firebase from 'react-native-firebase'
import OneSignal from 'react-native-onesignal';
import AsyncStorageUtils from 'helpers/AsyncStorageUtils';
import { connect } from 'react-redux'
import geolocation from '@react-native-community/geolocation';
import NetInfo from '@react-native-community/netinfo'
import _ from 'lodash';
import NoInternetComponent from './common/NoInternet';
import VersionChecker from './common/VersionChecker';
import StoreRatingModal from './common/StoreRating/StoreRatingModal';
import { updateUserLocation } from './apis/Functions/users'
import R from './assets/R';
import { OneSignalKey } from './config/Setting';

const TIME_SEND_LOCATION = 5 * 60 * 1000
class RootView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      typeNotif: 3
    };

    OneSignal.init(OneSignalKey.development);

    OneSignal.inFocusDisplaying(2)
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  componentDidMount = () => {
    this.checkPermission();
    this.sendLocationToServer()
    this._checkConnection()
    AppState.addEventListener('change', this._handleAppStateChange);
    // this.timer = setInterval(() => this.sendLocationToServer(), TIME_SEND_LOCATION)

    // this.messageListener();
  }

  _checkConnection = () => {
    NetInfo.fetch().then(state => {
      if (state.type === 'cellular' && !__DEV__) Alert.alert('Cảnh báo', 'Kết nối mạng kém. Một vài tính năng có thể không hoạt động')
      // console.log('Connection type', state.type);
      // console.log('Is connected?', state);
    });
  }

  _handleAppStateChange = (nextAppState) => {
    // console.log('AppState', nextAppState);
    if (nextAppState === 'active') {
      this.sendLocationToServer()
    }
  };

  sendLocationToServer = async () => {
    const { Account } = this.props;
    if (Account && Account?.iD_QuanLy) {
      geolocation.getCurrentPosition(
        async position => {
          let ViDo = position.coords.latitude
          let KinhDo = position.coords.longitude
          let bodyUpdate = `?ID_Taikhoan=${Account?.iD_QuanLy ?? ''}&KinhDo=${KinhDo}&ViDo=${ViDo}`
          let resUpdate = await updateUserLocation(bodyUpdate)
        }
      )
    }
  }

  checkPermission = async () => {
    let enabled = await firebase.messaging().hasPermission();
    // console.log('gfgfg22')
    // console.log(222222222222, enabled)
    if (enabled) {
      this.getFcmToken();
    } else {
      this.requestPermission();
    }
  }

  getFcmToken = async () => {
    const fcmToken = await firebase.messaging().getToken();
    if (fcmToken) {
      AsyncStorageUtils.save(AsyncStorageUtils.KEY.DEVICE_TOKEN, fcmToken)
      // this.showAlert('Your Firebase Token is:', fcmToken);
    } else {
      this.showAlert('Failed', 'No token received');
    }
  }

  requestPermission = async () => {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
    } catch (error) {
      // User has rejected permissions
    }
  }

  onReceived = () => {
    this.props.fetchThongBao({
      token: _.has(this.props.Account, 'token') ? this.props.Account.token : null,
      content: this.props.Account.MaSV
    })
  }


  onOpened = async ({ notification }) => {
  }

  onIds = (device) => {
    AsyncStorageUtils.save(AsyncStorageUtils.KEY.SAVE_TOKEN_NOTI, device.userId)
  }

  render() {
    return (
      <>
        <SafeAreaView style={styles.containertTop} />
        <SafeAreaView style={styles.containerBottom}>
          {this.props.children}
          <NoInternetComponent />
          {/* <NotificationFirebase /> */}
          <StoreRatingModal />
          <VersionChecker />
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  containertTop: {
    flex: 0,
    backgroundColor: R.colors.colorMain
  },
  containerBottom: {
    flex: 1,
    backgroundColor: R.colors.white,
  },

});
function mapStateToProps(state) {
  return {
    Account: state.userReducers.Account,
  };
}

export default connect(mapStateToProps, {
})(RootView);
