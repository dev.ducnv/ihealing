/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format

 */

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import Reactotron from 'reactotron-react-native';
import DropdownAlert from 'react-native-dropdownalert';
import { Root } from 'native-base';

import { colors } from 'assets';
import MainNavigation from 'routers/MainNavigation';
import * as Sentry from '@sentry/react-native';
import codePush from 'react-native-code-push';
import DropdownManager from './src/common/DropdownAlert/DropdownManager';
import LoadingModal from './src/common/Loading/LoadingModal';
import LoadingManager from './src/common/Loading/LoadingManager';
import NavigationService from './src/routers/NavigationService';
import RootView from './src/RootView';
import configureStore from './src/stores/configureStore';
import rootSaga from './src/sagas';
import ReactotronConfig from './src/helpers/ReactotronConfig';

const reactotron = ReactotronConfig.configure();

const sagaMonitor = Reactotron.createSagaMonitor();
const sagaMiddleware = createSagaMiddleware({ sagaMonitor });

const store = configureStore(reactotron, sagaMiddleware);
Reactotron.clear();

sagaMiddleware.run(rootSaga);
!__DEV__ && Sentry.init({
  dsn: 'https://c46f0f8a7f7e4ff2b6409d23e529a4d6@o462925.ingest.sentry.io/5509503',
});
class App extends Component {
  componentDidMount() {
    LoadingManager.register(this.loadingRef);
    DropdownManager.register(this.dropDownAlertRef);
    !__DEV__ && codePush.sync({
      updateDialog: {
        title: 'Thông báo',
        optionalUpdateMessage:
          'Phiên bản bạn đang sử dụng đã cũ. Vui lòng cập nhật ứng dụng!',
        optionalIgnoreButtonLabel: 'Hủy',
        optionalInstallButtonLabel: 'Cập nhật',
      },
      installMode: codePush.InstallMode.IMMEDIATE,
    });
  }

  componentWillUnmount() {
    LoadingManager.unregister(this.loadingRef);
    DropdownManager.unregister(this.dropDownAlertRef);
  }

  render() {
    return (
      <Provider store={store}>
        <Root>
          <RootView>
            <MainNavigation
              ref={navigatorRef => NavigationService.setTopLevelNavigator(navigatorRef)}
            />
            <LoadingModal
              ref={ref => {
                this.loadingRef = ref;
              }}
            />

            <DropdownAlert
              inactiveStatusBarBackgroundColor={colors.primaryColor}
              // inactiveStatusBarBackgroundColor={colors.header}
              successImageSrc={null}
              infoImageSrc={null}
              warnImageSrc={null}
              errorImageSrc={null}
              ref={ref => {
                this.dropDownAlertRef = ref;
              }}
            />
          </RootView>
        </Root>
      </Provider>
    );
  }
}
export default codePush(App);
